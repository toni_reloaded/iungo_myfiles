CREATE DATABASE  IF NOT EXISTS `iungolive` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `iungolive`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: iungolive
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `async_operation`
--

DROP TABLE IF EXISTS `async_operation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `async_operation` (
  `ASYNC_OPERATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ASYNC_OPERATION_NUMBER` varchar(45) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `NAME` varchar(254) NOT NULL,
  `CLASS_NAME` varchar(254) NOT NULL,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `STATUS` varchar(30) NOT NULL,
  `EVENT` varchar(30) DEFAULT NULL,
  `EVENT_REASON` varchar(254) DEFAULT NULL,
  `THREAD_ID` bigint(20) DEFAULT NULL,
  `DATE_EVENT` datetime NOT NULL,
  `USERNAME` varchar(45) NOT NULL,
  `AUTHOR` varchar(45) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `EXCEPTION_LOG_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ASYNC_OPERATION_ID`),
  UNIQUE KEY `ASYNC_OPERATION_NUMBER` (`ASYNC_OPERATION_NUMBER`),
  KEY `EXCEPTION_LOG_ID` (`EXCEPTION_LOG_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `EVENT` (`EVENT`),
  KEY `STATUS` (`STATUS`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  KEY `DATE_CREATION` (`DATE_CREATION`),
  KEY `THREAD_ID` (`THREAD_ID`),
  KEY `DATE_EVENT` (`DATE_EVENT`),
  CONSTRAINT `async_operation_ibfk_1` FOREIGN KEY (`EXCEPTION_LOG_ID`) REFERENCES `exception_log` (`EXCEPTION_LOG_ID`),
  CONSTRAINT `async_operation_ibfk_2` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `async_operation_ibfk_3` FOREIGN KEY (`EVENT`) REFERENCES `ordine_wf_evento` (`NOME`),
  CONSTRAINT `async_operation_ibfk_4` FOREIGN KEY (`STATUS`) REFERENCES `ordine_wf_stato` (`NOME`),
  CONSTRAINT `async_operation_ibfk_5` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `async_operation`
--

LOCK TABLES `async_operation` WRITE;
/*!40000 ALTER TABLE `async_operation` DISABLE KEYS */;
/*!40000 ALTER TABLE `async_operation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `async_operation_log`
--

DROP TABLE IF EXISTS `async_operation_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `async_operation_log` (
  `ASYNC_OPERATION_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ASYNC_OPERATION_ID` int(11) NOT NULL,
  `ASYNC_OPERATION_NUMBER` varchar(45) NOT NULL,
  `DATE_CREATION` datetime NOT NULL,
  `NAME` varchar(254) NOT NULL,
  `CLASS_NAME` varchar(254) NOT NULL,
  `STATUS` varchar(30) NOT NULL,
  `EVENT` varchar(30) DEFAULT NULL,
  `EVENT_REASON` varchar(254) DEFAULT NULL,
  `THREAD_ID` bigint(20) DEFAULT NULL,
  `DATE_EVENT` datetime NOT NULL,
  `USERNAME` varchar(45) NOT NULL,
  `EXCEPTION_LOG_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ASYNC_OPERATION_LOG_ID`),
  KEY `ASYNC_OPERATION_ID` (`ASYNC_OPERATION_ID`),
  KEY `EXCEPTION_LOG_ID` (`EXCEPTION_LOG_ID`),
  KEY `EVENT` (`EVENT`),
  KEY `STATUS` (`STATUS`),
  KEY `ASYNC_OPERATION_NUMBER` (`ASYNC_OPERATION_NUMBER`),
  KEY `DATE_EVENT` (`DATE_EVENT`),
  KEY `ASYNCOP_DATE_EVENT` (`ASYNC_OPERATION_ID`,`DATE_EVENT`),
  CONSTRAINT `async_operation_log_ibfk_1` FOREIGN KEY (`ASYNC_OPERATION_ID`) REFERENCES `async_operation` (`ASYNC_OPERATION_ID`),
  CONSTRAINT `async_operation_log_ibfk_2` FOREIGN KEY (`EXCEPTION_LOG_ID`) REFERENCES `exception_log` (`EXCEPTION_LOG_ID`),
  CONSTRAINT `async_operation_log_ibfk_3` FOREIGN KEY (`EVENT`) REFERENCES `ordine_wf_evento` (`NOME`),
  CONSTRAINT `async_operation_log_ibfk_4` FOREIGN KEY (`STATUS`) REFERENCES `ordine_wf_stato` (`NOME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `async_operation_log`
--

LOCK TABLES `async_operation_log` WRITE;
/*!40000 ALTER TABLE `async_operation_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `async_operation_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction` (
  `AUCTION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MINIMUM_RAISE` decimal(12,5) DEFAULT '0.00000',
  `MINIMUM_PERIOD_AFTER_LAST_RAISE` decimal(12,5) DEFAULT '0.00000',
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `STATUS` varchar(20) DEFAULT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `RDA_ID` int(11) NOT NULL,
  `AUTHOR` varchar(254) DEFAULT NULL,
  `NOTA` mediumtext,
  PRIMARY KEY (`AUCTION_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `RDA_ID` (`RDA_ID`),
  CONSTRAINT `auction_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `auction_ibfk_2` FOREIGN KEY (`RDA_ID`) REFERENCES `rda` (`RDA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction`
--

LOCK TABLES `auction` WRITE;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `azione_qualita`
--

DROP TABLE IF EXISTS `azione_qualita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `azione_qualita` (
  `AZIONE_QUALITA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AZIONE_QUALITA_PARENT_ID` int(11) DEFAULT NULL,
  `AZIONE_QUALITA_LIVELLO_ID` int(11) DEFAULT NULL,
  `FG_CORRETTIVA_PREVENTIVA` int(11) DEFAULT '0',
  `NON_CONFORMITA_ID` int(11) DEFAULT NULL,
  `DESCRIZIONE` mediumtext,
  `DESCRIZIONE_CRITICITA` mediumtext,
  `STATO` varchar(32) DEFAULT '',
  `DATA_CREAZIONE` datetime DEFAULT NULL,
  `DATA_CHIUSURA` datetime DEFAULT NULL,
  `ESITO` varchar(32) DEFAULT '',
  `VERIFICA_ATTUAZIONE` varchar(254) DEFAULT '',
  `DATA_VERIFICA_ATTUAZIONE` datetime DEFAULT NULL,
  `VERIFICA_RAQ` varchar(254) DEFAULT '',
  `NOTA` mediumtext,
  PRIMARY KEY (`AZIONE_QUALITA_ID`),
  KEY `AZIONE_QUALITA_PARENT_ID` (`AZIONE_QUALITA_PARENT_ID`),
  KEY `AZIONE_QUALITA_LIVELLO_ID` (`AZIONE_QUALITA_LIVELLO_ID`),
  KEY `NON_CONFORMITA_ID` (`NON_CONFORMITA_ID`),
  CONSTRAINT `azione_qualita_ibfk_1` FOREIGN KEY (`AZIONE_QUALITA_PARENT_ID`) REFERENCES `azione_qualita` (`AZIONE_QUALITA_ID`),
  CONSTRAINT `azione_qualita_ibfk_2` FOREIGN KEY (`AZIONE_QUALITA_LIVELLO_ID`) REFERENCES `azione_qualita_livello` (`AZIONE_QUALITA_LIVELLO_ID`),
  CONSTRAINT `azione_qualita_ibfk_3` FOREIGN KEY (`NON_CONFORMITA_ID`) REFERENCES `non_conformita` (`NON_CONFORMITA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `azione_qualita`
--

LOCK TABLES `azione_qualita` WRITE;
/*!40000 ALTER TABLE `azione_qualita` DISABLE KEYS */;
/*!40000 ALTER TABLE `azione_qualita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `azione_qualita_livello`
--

DROP TABLE IF EXISTS `azione_qualita_livello`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `azione_qualita_livello` (
  `AZIONE_QUALITA_LIVELLO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `CODE` varchar(64) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  PRIMARY KEY (`AZIONE_QUALITA_LIVELLO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `azione_qualita_livello_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `azione_qualita_livello`
--

LOCK TABLES `azione_qualita_livello` WRITE;
/*!40000 ALTER TABLE `azione_qualita_livello` DISABLE KEYS */;
INSERT INTO `azione_qualita_livello` VALUES (1,5,'1',''),(2,5,'2',''),(3,5,'3',''),(4,5,'4',''),(5,5,'5',''),(6,5,'6',''),(7,5,'7',''),(8,5,'8',''),(9,5,'9',''),(10,5,'10','');
/*!40000 ALTER TABLE `azione_qualita_livello` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calendar_iungo`
--

DROP TABLE IF EXISTS `calendar_iungo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calendar_iungo` (
  `CALENDAR_IUNGO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(99) DEFAULT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `TYPE` varchar(15) DEFAULT '',
  `NAME` varchar(25) DEFAULT NULL,
  `DESCRIPTION` mediumtext,
  `GRUOP_IDENTIFICATION` decimal(19,0) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `DATE_HI` datetime DEFAULT NULL,
  `DATE_LOW` datetime DEFAULT NULL,
  `COLOR_HTML` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`CALENDAR_IUNGO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `OBJECT_TYPE` (`OBJECT_TYPE`),
  KEY `OBJECT_ID` (`OBJECT_ID`),
  KEY `DATE_HI` (`DATE_HI`),
  KEY `DATE_LOW` (`DATE_LOW`),
  CONSTRAINT `calendar_iungo_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calendar_iungo`
--

LOCK TABLES `calendar_iungo` WRITE;
/*!40000 ALTER TABLE `calendar_iungo` DISABLE KEYS */;
/*!40000 ALTER TABLE `calendar_iungo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `collaudo_tipo`
--

DROP TABLE IF EXISTS `collaudo_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `collaudo_tipo` (
  `COLLAUDO_TIPO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(64) NOT NULL DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  PRIMARY KEY (`COLLAUDO_TIPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `collaudo_tipo`
--

LOCK TABLES `collaudo_tipo` WRITE;
/*!40000 ALTER TABLE `collaudo_tipo` DISABLE KEYS */;
INSERT INTO `collaudo_tipo` VALUES (1,'Primo tipo collaudo',''),(2,'Secondo tipo collaudo',''),(3,'Terzo tipo collaudo','');
/*!40000 ALTER TABLE `collaudo_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connector_from_customer`
--

DROP TABLE IF EXISTS `connector_from_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connector_from_customer` (
  `CONNECTOR_FROM_CUSTOMER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CUSTOMER_CODE` varchar(99) DEFAULT NULL,
  `CONVERTER_DIR_NAME` varchar(99) DEFAULT NULL,
  `DESCRIPTION` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`CONNECTOR_FROM_CUSTOMER_ID`),
  UNIQUE KEY `CUSTOMER_CODE` (`CUSTOMER_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connector_from_customer`
--

LOCK TABLES `connector_from_customer` WRITE;
/*!40000 ALTER TABLE `connector_from_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `connector_from_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connector_from_supplier`
--

DROP TABLE IF EXISTS `connector_from_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connector_from_supplier` (
  `CONNECTOR_FROM_SUPPLIER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `SUPPLIER_EXTERNAL_CODE` varchar(99) DEFAULT NULL,
  `SUPPLIER_IUNGO_CODE` varchar(99) DEFAULT NULL,
  `DESCRIPTION` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`CONNECTOR_FROM_SUPPLIER_ID`),
  UNIQUE KEY `SUPPLIER_EXTERNAL_CODE` (`SUPPLIER_EXTERNAL_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connector_from_supplier`
--

LOCK TABLES `connector_from_supplier` WRITE;
/*!40000 ALTER TABLE `connector_from_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `connector_from_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connector_values_mapping`
--

DROP TABLE IF EXISTS `connector_values_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connector_values_mapping` (
  `CONNECTOR_VALUES_MAPPING_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `TYPE` varchar(30) DEFAULT NULL,
  `VALUE_SOURCE` varchar(99) DEFAULT NULL,
  `VALUE_TARGET` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`CONNECTOR_VALUES_MAPPING_ID`),
  UNIQUE KEY `FORNITORE_ID_2` (`FORNITORE_ID`,`TYPE`,`VALUE_SOURCE`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `connector_values_mapping_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `connector_values_mapping_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connector_values_mapping`
--

LOCK TABLES `connector_values_mapping` WRITE;
/*!40000 ALTER TABLE `connector_values_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `connector_values_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `controllo_qualita`
--

DROP TABLE IF EXISTS `controllo_qualita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `controllo_qualita` (
  `CONTROLLO_QUALITA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `QUALITA_ORIGINE_ID` int(11) NOT NULL,
  `PRODUCT_BATCH_FORMULA_ITEM_ID` int(11) DEFAULT NULL,
  `PRODUCT_BATCH_ID` int(11) DEFAULT NULL,
  `PIANO_CAMPIONAMENTO_ID` int(11) DEFAULT NULL,
  `LIVELLO_COLLAUDO_CELLA_ID` int(11) DEFAULT NULL,
  `USERNAME_APPROVAZIONE` varchar(99) DEFAULT '',
  `NOME_APPROVAZIONE` varchar(32) DEFAULT '',
  `COGNOME_APPROVAZIONE` varchar(32) DEFAULT '',
  `RUOLO_APPROVAZIONE` varchar(32) DEFAULT '',
  `DOCUMENTO_TRASPORTO_RIGA_ID` int(11) DEFAULT NULL,
  `FG_INBOUND_FREE_PASS` int(11) DEFAULT NULL,
  `DATA_CREAZIONE` datetime DEFAULT NULL,
  `DATA_CHIUSURA` datetime DEFAULT NULL,
  `COSTO` decimal(12,5) DEFAULT '0.00000',
  `USERNAME_OPERATORE` varchar(99) DEFAULT '',
  `NOME_OPERATORE` varchar(32) DEFAULT '',
  `COGNOME_OPERATORE` varchar(32) DEFAULT '',
  `RUOLO_OPERATORE` varchar(32) DEFAULT '',
  `STATO` varchar(32) DEFAULT '',
  `NUMERO_CAMPIONI` int(11) DEFAULT NULL,
  `NUMERO_DIFETTOSI` int(11) DEFAULT NULL,
  `QUANTITA_CONTROLLATA` decimal(12,5) DEFAULT '0.00000',
  `ESITO` varchar(32) DEFAULT '',
  `TEMPO_IMPIEGATO` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  PRIMARY KEY (`CONTROLLO_QUALITA_ID`),
  KEY `QUALITA_ORIGINE_ID` (`QUALITA_ORIGINE_ID`),
  KEY `DOCUMENTO_TRASPORTO_RIGA_ID` (`DOCUMENTO_TRASPORTO_RIGA_ID`),
  KEY `PRODUCT_BATCH_FORMULA_ITEM_ID` (`PRODUCT_BATCH_FORMULA_ITEM_ID`),
  KEY `PRODUCT_BATCH_ID` (`PRODUCT_BATCH_ID`),
  KEY `PIANO_CAMPIONAMENTO_ID` (`PIANO_CAMPIONAMENTO_ID`),
  KEY `LIVELLO_COLLAUDO_CELLA_ID` (`LIVELLO_COLLAUDO_CELLA_ID`),
  CONSTRAINT `controllo_qualita_ibfk_1` FOREIGN KEY (`QUALITA_ORIGINE_ID`) REFERENCES `qualita_origine` (`QUALITA_ORIGINE_ID`),
  CONSTRAINT `controllo_qualita_ibfk_2` FOREIGN KEY (`DOCUMENTO_TRASPORTO_RIGA_ID`) REFERENCES `documento_trasporto_riga` (`DOCUMENTO_TRASPORTO_RIGA_ID`),
  CONSTRAINT `controllo_qualita_ibfk_3` FOREIGN KEY (`PRODUCT_BATCH_FORMULA_ITEM_ID`) REFERENCES `product_batch_formula_item` (`PRODUCT_BATCH_FORMULA_ITEM_ID`),
  CONSTRAINT `controllo_qualita_ibfk_4` FOREIGN KEY (`PRODUCT_BATCH_ID`) REFERENCES `product_batch` (`PRODUCT_BATCH_ID`),
  CONSTRAINT `controllo_qualita_ibfk_5` FOREIGN KEY (`PIANO_CAMPIONAMENTO_ID`) REFERENCES `piano_campionamento` (`PIANO_CAMPIONAMENTO_ID`),
  CONSTRAINT `controllo_qualita_ibfk_6` FOREIGN KEY (`LIVELLO_COLLAUDO_CELLA_ID`) REFERENCES `livello_collaudo_cella` (`LIVELLO_COLLAUDO_CELLA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `controllo_qualita`
--

LOCK TABLES `controllo_qualita` WRITE;
/*!40000 ALTER TABLE `controllo_qualita` DISABLE KEYS */;
/*!40000 ALTER TABLE `controllo_qualita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaration_origin`
--

DROP TABLE IF EXISTS `declaration_origin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaration_origin` (
  `DECLARATION_ORIGIN_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `ORIGIN_START_DATE` datetime NOT NULL,
  `ORIGIN_END_DATE` datetime DEFAULT NULL,
  `ORIGIN_NUMBER` varchar(50) DEFAULT NULL,
  `CREATION_DATE` datetime NOT NULL,
  `FILLED_DATE` datetime DEFAULT NULL,
  `CONTACT_PERSON` varchar(254) DEFAULT NULL,
  `EMAIL` varchar(600) DEFAULT '',
  `NOTE` mediumtext,
  `AUX_HEAD1` varchar(99) DEFAULT NULL,
  `AUX_HEAD2` varchar(99) DEFAULT NULL,
  `AUX_HEAD3` varchar(99) DEFAULT NULL,
  `AUX_HEAD4` varchar(99) DEFAULT NULL,
  `AUX_HEAD5` varchar(99) DEFAULT NULL,
  `AUX_HEAD6` varchar(99) DEFAULT NULL,
  `AUX_HEAD7` varchar(99) DEFAULT NULL,
  `AUX_HEAD8` varchar(99) DEFAULT NULL,
  `AUX_HEAD9` varchar(99) DEFAULT NULL,
  `AUX_HEAD10` varchar(99) DEFAULT NULL,
  `AUX_HEAD_DATE1` datetime DEFAULT NULL,
  `AUX_HEAD_DATE2` datetime DEFAULT NULL,
  `AUX_HEAD_DATE3` datetime DEFAULT NULL,
  `AUX_HEAD_DATE4` datetime DEFAULT NULL,
  `AUX_HEAD_DATE5` datetime DEFAULT NULL,
  `AUX_HEAD_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM5` decimal(12,5) DEFAULT '0.00000',
  `AUTHOR` varchar(50) DEFAULT NULL,
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`DECLARATION_ORIGIN_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`FORNITORE_ID`,`ORIGIN_START_DATE`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  CONSTRAINT `declaration_origin_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `declaration_origin_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `declaration_origin_ibfk_3` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaration_origin`
--

LOCK TABLES `declaration_origin` WRITE;
/*!40000 ALTER TABLE `declaration_origin` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaration_origin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaration_origin_row`
--

DROP TABLE IF EXISTS `declaration_origin_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaration_origin_row` (
  `DECLARATION_ORIGIN_ROW_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DECLARATION_ORIGIN_ID` int(11) NOT NULL,
  `ITEM_CODE` varchar(254) NOT NULL DEFAULT '',
  `ITEM_DESCRIPTION` varchar(254) DEFAULT '',
  `PARTNER_ITEM_CODE` varchar(254) DEFAULT '',
  `PARTNER_ITEM_DESCRIPTION` varchar(254) DEFAULT '',
  `ORIGIN_COUNTRY` varchar(254) DEFAULT '',
  `ORIGIN_COUNTRY_CODE` varchar(25) DEFAULT '',
  `FG_VISTO` int(11) DEFAULT '0',
  `STATO` varchar(30) NOT NULL,
  `NOTE` mediumtext,
  `DATA_VISTO` datetime DEFAULT NULL,
  `AUX_ROW1` varchar(99) DEFAULT NULL,
  `AUX_ROW2` varchar(99) DEFAULT NULL,
  `AUX_ROW3` varchar(99) DEFAULT NULL,
  `AUX_ROW4` varchar(99) DEFAULT NULL,
  `AUX_ROW5` varchar(99) DEFAULT NULL,
  `AUX_ROW6` varchar(99) DEFAULT NULL,
  `AUX_ROW7` varchar(99) DEFAULT NULL,
  `AUX_ROW8` varchar(99) DEFAULT NULL,
  `AUX_ROW9` varchar(99) DEFAULT NULL,
  `AUX_ROW10` varchar(99) DEFAULT NULL,
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_DATE3` datetime DEFAULT NULL,
  `AUX_ROW_DATE4` datetime DEFAULT NULL,
  `AUX_ROW_DATE5` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `FG_CERTIFICATE` int(11) DEFAULT '0',
  `PRODUCT_TYPE_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DECLARATION_ORIGIN_ROW_ID`),
  UNIQUE KEY `DECLARATION_ORIGIN_ID_2` (`DECLARATION_ORIGIN_ID`,`ITEM_CODE`),
  KEY `DECLARATION_ORIGIN_ID` (`DECLARATION_ORIGIN_ID`),
  KEY `PRODUCT_TYPE_ID` (`PRODUCT_TYPE_ID`),
  CONSTRAINT `declaration_origin_row_ibfk_1` FOREIGN KEY (`DECLARATION_ORIGIN_ID`) REFERENCES `declaration_origin` (`DECLARATION_ORIGIN_ID`),
  CONSTRAINT `declaration_origin_row_ibfk_2` FOREIGN KEY (`PRODUCT_TYPE_ID`) REFERENCES `product_type` (`PRODUCT_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaration_origin_row`
--

LOCK TABLES `declaration_origin_row` WRITE;
/*!40000 ALTER TABLE `declaration_origin_row` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaration_origin_row` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `declaration_origin_row_log`
--

DROP TABLE IF EXISTS `declaration_origin_row_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `declaration_origin_row_log` (
  `DECLARATION_ORIGIN_ROW_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DECLARATION_ORIGIN_ROW_ID` int(11) NOT NULL,
  `ITEM_CODE` varchar(254) NOT NULL,
  `ITEM_DESCRIPTION` varchar(254) DEFAULT '',
  `PARTNER_ITEM_CODE` varchar(254) DEFAULT '',
  `PARTNER_ITEM_DESCRIPTION` varchar(254) DEFAULT '',
  `ORIGIN_COUNTRY` varchar(254) DEFAULT '',
  `ORIGIN_COUNTRY_CODE` varchar(25) DEFAULT '',
  `DATA_VISTO` datetime DEFAULT NULL,
  `STATO` varchar(30) NOT NULL,
  `EVENTO_NOME` varchar(30) NOT NULL,
  `USER_NAME` varchar(99) NOT NULL,
  `MOTIVO` varchar(254) DEFAULT '',
  `DATA` datetime DEFAULT NULL,
  `MITTENTE` varchar(80) DEFAULT '',
  `FG_ALLINEAMENTO` int(11) DEFAULT '0',
  `DOCUMENTAZIONE_CHANGES` mediumtext,
  `VISTO_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `NOTE` mediumtext,
  `AUX_ROW1` varchar(99) DEFAULT NULL,
  `AUX_ROW2` varchar(99) DEFAULT NULL,
  `AUX_ROW3` varchar(99) DEFAULT NULL,
  `AUX_ROW4` varchar(99) DEFAULT NULL,
  `AUX_ROW5` varchar(99) DEFAULT NULL,
  `AUX_ROW6` varchar(99) DEFAULT NULL,
  `AUX_ROW7` varchar(99) DEFAULT NULL,
  `AUX_ROW8` varchar(99) DEFAULT NULL,
  `AUX_ROW9` varchar(99) DEFAULT NULL,
  `AUX_ROW10` varchar(99) DEFAULT NULL,
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_DATE3` datetime DEFAULT NULL,
  `AUX_ROW_DATE4` datetime DEFAULT NULL,
  `AUX_ROW_DATE5` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `FG_CERTIFICATE` int(11) DEFAULT '0',
  PRIMARY KEY (`DECLARATION_ORIGIN_ROW_LOG_ID`),
  KEY `VISTO_NOTIFICA_LOG_RICEVUTA_ID` (`VISTO_NOTIFICA_LOG_RICEVUTA_ID`),
  KEY `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` (`MODFORN_NOTIFICA_LOG_RICEVUTA_ID`),
  KEY `DECLARATION_ORIGIN_ROW_ID` (`DECLARATION_ORIGIN_ROW_ID`),
  CONSTRAINT `declaration_origin_row_log_ibfk_1` FOREIGN KEY (`DECLARATION_ORIGIN_ROW_ID`) REFERENCES `declaration_origin_row` (`DECLARATION_ORIGIN_ROW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `declaration_origin_row_log`
--

LOCK TABLES `declaration_origin_row_log` WRITE;
/*!40000 ALTER TABLE `declaration_origin_row_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `declaration_origin_row_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `difettosi_qualita_istruzione`
--

DROP TABLE IF EXISTS `difettosi_qualita_istruzione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `difettosi_qualita_istruzione` (
  `DIFETTOSI_QUALITA_ISTRUZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTROLLO_QUALITA_ID` int(11) NOT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `QUALITA_ISTRUZIONE_ID` int(11) NOT NULL,
  `NUMERO` int(11) DEFAULT '-1',
  `FG_MISURA` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`DIFETTOSI_QUALITA_ISTRUZIONE_ID`),
  UNIQUE KEY `CONTROLLO_QUALITA_ID_2` (`CONTROLLO_QUALITA_ID`,`PRODUCT_ID`,`QUALITA_ISTRUZIONE_ID`),
  KEY `CONTROLLO_QUALITA_ID` (`CONTROLLO_QUALITA_ID`),
  KEY `QUALITA_ISTRUZIONE_ID` (`QUALITA_ISTRUZIONE_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`),
  CONSTRAINT `difettosi_qualita_istruzione_ibfk_1` FOREIGN KEY (`CONTROLLO_QUALITA_ID`) REFERENCES `controllo_qualita` (`CONTROLLO_QUALITA_ID`),
  CONSTRAINT `difettosi_qualita_istruzione_ibfk_2` FOREIGN KEY (`QUALITA_ISTRUZIONE_ID`) REFERENCES `qualita_istruzione` (`QUALITA_ISTRUZIONE_ID`),
  CONSTRAINT `difettosi_qualita_istruzione_ibfk_3` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `product` (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `difettosi_qualita_istruzione`
--

LOCK TABLES `difettosi_qualita_istruzione` WRITE;
/*!40000 ALTER TABLE `difettosi_qualita_istruzione` DISABLE KEYS */;
/*!40000 ALTER TABLE `difettosi_qualita_istruzione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ditta`
--

DROP TABLE IF EXISTS `ditta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ditta` (
  `DITTA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GROUP_NAME` varchar(99) DEFAULT NULL,
  `RAGIONE_SOCIALE` varchar(254) NOT NULL,
  `COMPANY_URI` varchar(254) DEFAULT NULL,
  `PARTITA_IVA` varchar(20) DEFAULT NULL,
  `FG_CLIENTE_QUALIFICANTE` int(11) DEFAULT NULL,
  `FG_APPROVED_DITTA` int(11) DEFAULT NULL,
  `VIA` varchar(254) DEFAULT NULL,
  `CAP` varchar(15) DEFAULT NULL,
  `CITTA` varchar(50) DEFAULT NULL,
  `PROVINCIA` varchar(30) DEFAULT NULL,
  `PAESE` varchar(30) DEFAULT NULL,
  `EMAIL` varchar(600) DEFAULT NULL,
  `PHONE_NUMBER` varchar(99) DEFAULT NULL,
  `FAX_NUMBER` varchar(99) DEFAULT NULL,
  `WEB_SITE` varchar(254) DEFAULT '',
  `CONTACT_PERSON` varchar(254) DEFAULT '',
  `CONTACT_PERSON_ADDRESS` varchar(254) DEFAULT '',
  `CONTACT_PERSON_PHONE` varchar(254) DEFAULT '',
  `CONTACT_PERSON_FAX` varchar(254) DEFAULT '',
  `CONTACT_PERSON_EMAIL` varchar(600) DEFAULT '',
  `CREATION_DATE` datetime DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `VALUTA` varchar(30) DEFAULT '',
  `NOTA` mediumtext,
  `MAIL_RIC_ABILITA` int(11) DEFAULT NULL,
  `MAIL_RIC_INTERV` int(11) DEFAULT NULL,
  `MAIL_SPED_ABILITA` int(11) DEFAULT NULL,
  `MAIL_ERRORI_INTERV` int(11) DEFAULT NULL,
  `COUNTRY_CODE` varchar(3) DEFAULT '',
  PRIMARY KEY (`DITTA_ID`),
  KEY `DITTA_GROUP_NAME` (`GROUP_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ditta`
--

LOCK TABLES `ditta` WRITE;
/*!40000 ALTER TABLE `ditta` DISABLE KEYS */;
INSERT INTO `ditta` VALUES (5,'base','base','','024687531',1,1,'Via Giardini 58','41100','Modena','Modena','Italy','info@baseMail.it','+39 059 622 93 22','+39 059 622 93 11','www.iungoapps.com','Mario Rossi','Via Giardini 45, 41100 MODENA','059/256598','059/123456','m.rossi@baseMail.it',NULL,NULL,'','VAT IT032596580123 }\r\nRegistro Imprese Modena 121212/1999 }\r\nCapitale Sociale EUR 100.000 i.v. }\r\nCodice Meccanografico MO0123456 }\r\nR.E.A. Modena 123456',1,5,1,10,''),(6,'base2','base2','','024687531',1,1,'Via Giardini 58','41100','Modena','Modena','Italy','info@baseMail.it','+39 059 622 93 22','+39 059 622 93 11','www.iungoapps.com','Mario Rossi','Via Giardini 45, 41100 MODENA','059/256598','059/123456','m.rossi@baseMail.it',NULL,NULL,'','VAT IT032596580123 }\r\nRegistro Imprese Modena 121212/1999 }\r\nCapitale Sociale EUR 100.000 i.v. }\r\nCodice Meccanografico MO0123456 }\r\nR.E.A. Modena 123456',1,5,1,10,'');
/*!40000 ALTER TABLE `ditta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ditta_certificate_role`
--

DROP TABLE IF EXISTS `ditta_certificate_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ditta_certificate_role` (
  `DITTA_CERTIFICATE_ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `ROLE_NAME` varchar(254) DEFAULT '',
  `CERTIFICATE` mediumtext,
  `NOTA` mediumtext,
  PRIMARY KEY (`DITTA_CERTIFICATE_ROLE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `ditta_certificate_role_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ditta_certificate_role`
--

LOCK TABLES `ditta_certificate_role` WRITE;
/*!40000 ALTER TABLE `ditta_certificate_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `ditta_certificate_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ditta_configurazione`
--

DROP TABLE IF EXISTS `ditta_configurazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ditta_configurazione` (
  `DITTA_CONFIGURAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `NOME` varchar(254) NOT NULL,
  `VALORE` mediumtext,
  PRIMARY KEY (`DITTA_CONFIGURAZIONE_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`NOME`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `ditta_configurazione_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ditta_configurazione`
--

LOCK TABLES `ditta_configurazione` WRITE;
/*!40000 ALTER TABLE `ditta_configurazione` DISABLE KEYS */;
INSERT INTO `ditta_configurazione` VALUES (71,5,'alt_emailOrdineField','FornitoreFax'),(72,5,'attributes_OrdineRiga_form','NomeAllegato'),(76,5,'customSysUtils','it.democenter.m2.customers.scambioDati._common.SysUtils'),(77,5,'doMonitor_dailyRoutines_dayList','never'),(78,5,'doMonitor_dailyRoutines_lastDate','2012-04-10 03:01:11'),(79,5,'giorniRitardoConferma1','1'),(80,5,'giorniRitardoConferma2','3'),(81,5,'it.democenter.m2.wf.acknowledgeFileExtension','.ack'),(82,5,'it.democenter.m2.wf.dataFileExtension','.xml'),(83,5,'it.democenter.m2.wf.outputDirectory','${it.democenter.m2.scambioDati.BaseDir}/fromIungo'),(84,5,'it.democenter.m2.wf.userName','user'),(85,5,'it.democenter.m2.wf.userPassword','pwd'),(86,5,'it.democenter.m2.wf.useShortFileName','false'),(87,5,'language.locale','it'),(88,5,'listaStatiPerEvidenziato','mForn'),(89,5,'mailProcessor_prNuovoOrd.actionOrdineAfterTransactions.class','it.democenter.m2.postaModello.processor.trigger.MailProcessorTrigger_Ordine_setAuxFields'),(90,5,'mailStatiFornitoreModifica','prop,conf,mForn'),(91,5,'mailStatiOrdinamento','crea,prop,annullato,conf,mForn,chiuso'),(92,5,'notifica.AnticipoBlocco','00:01:00'),(93,5,'ordini.reports.functions.class','it.democenter.m2.customers.scambioDati._common.report.ReportCustom'),(94,5,'pluginManager.alignDataSliceToM2.ordinePrimaryKey','ditta_tipo_numero_data'),(95,5,'sys_customizationCode','iungolive'),(97,5,'sys_documentazione.certificato','aaa.doc'),(98,5,'sys_documentazione.DeleteOriginal','true'),(99,5,'sys_documentazione.NoCopy','false'),(101,5,'sys_fornitorePrevisioneConsegna.InsertForDitta','false'),(102,5,'sys_logNotifica','${IUNGO_HOME}/scambioDati/StoricoNotifiche'),(103,5,'sys_productBatch_computeQuantities','false'),(104,5,'sys_productBatch_createOnDDTRowInsert','false'),(108,5,'sys_show_index_groupName','false'),(109,5,'urlForIungoSupport','${it.democenter.m2.IccsUrl}'),(110,5,'urlForLogo','http://www.iungoapps.com/iungo_logo/demo_logo.jpg'),(118,5,'pluginManager.alignDataSliceToM2.sdOrdineRigaDocumentazione.importMode','importIsMaster'),(119,5,'sys_logNotifica_Type','simple'),(120,5,'customMailProcessor_profferta','it.democenter.m2.customers.scambioDati.iungolive.MailProcessor_prOfferta'),(124,5,'ordineRiga_customUpdateFromMattoni','it.democenter.m2.customers.scambioDati.iungolive.UpdateOrdineRiga'),(125,5,'listaStatiPerRitardo','prop'),(126,5,'sys_productBatch_showOnlyQuantityRemainingGt0','true'),(128,5,'it.democenter.m2.wf.useDotFileName','true'),(129,5,'sys_mail_htmlFileName','true'),(132,5,'sys_logNotificaFromRigaLog_ListaEventi','prProposta,prModifica,prConferma,prAnnulla'),(133,5,'md5RigaFunction','or_OrdineNumero,or_DataConsegna,or_Prezzo,or_Quantita'),(134,5,'sys_logNotificaFromRigaLog_ShowSpecialAux','false'),(136,5,'it.democenter.m2.wf.charsetFromIungo','8859_1'),(138,5,'mailPianoConsegnaStati','prop,conf,mForn'),(139,5,'mailPianoConsegnaTipiOrdini','OA'),(140,5,'customMailSend_FornitoreOrdineFax','it.democenter.m2.customers.scambioDati._common.NotificaFornitoreOrdineFax_IungoFax'),(141,5,'md5RigaNegoziabili','or_OrdineNumero,or_DataConsegna,or_Prezzo,or_Quantita'),(142,5,'notificheBloccateMaxTentativi_emailFornitore','1'),(143,5,'notificheBloccateMaxTentativi_emailFornitorePrevisioneConsegna','1'),(144,5,'notificheBloccateMaxTentativi_emailCliente','1'),(145,5,'sys_logNotificaReceived','${IUNGO_HOME}/scambioDati/ricevute'),(146,5,'it.democenter.m2.report.directory','${IUNGO_HOME}/scambioDati/report'),(147,5,'it.democenter.m2.theme','verde_pag_left'),(148,5,'customMailSend_Fax','it.democenter.m2.customers.scambioDati._common.fax.NotificaFornitoreOrdineFax_IungoFax'),(149,5,'delayedUpdateThreadLevel','5'),(150,5,'dataPrimaSpedizioneAllineata','true'),(151,5,'dataPrimaSpedizioneAllineataLastFornitore','292'),(152,5,'listaStatiPerAnnullato','annullato'),(153,5,'listaStatiChiuso','chiuso'),(154,5,'vistoLogAllineato','true'),(155,5,'vistoLogAllineatoLastFornitore','292'),(156,5,'downloadServletClass','it.democenter.m2.servlets.DocumentationDownload_default'),(157,5,'notifiche.ordini.minWaitSeconds','180'),(158,5,'notifiche.forceSend','false'),(159,5,'email_backup',''),(160,5,'notificheBloccateMaxTentativi_emailAutorizzazione','1'),(161,5,'sys_documentazione.rootDirectory',''),(162,5,'urlForImage','0'),(163,5,'urlIungoCustomer','${it.democenter.m2.IungoUrl}'),(164,5,'urlForPdfLabels','${it.democenter.m2.IungoUrl}'),(165,5,'useLowerCaseEmailContent','false'),(166,5,'listaStatiConferma','conf'),(167,5,'email.preview.active','false'),(168,5,'md5RigaLog','or_OrdineNumero,or_DataConsegna,or_Prezzo,or_Quantita'),(169,5,'ordineRiga.getCoefQtyPrice.method','false'),(170,5,'ordini.reports.ultimaConsegna.method','or_DataUltimaConsegnaGestionale'),(171,5,'notifiche.outgoingPageNotices','500'),(172,5,'notificaLog_nGiorni_storico','365'),(173,5,'listaStatiPerVisto','visto'),(174,5,'it.democenter.m2.wf.customFilenameMethod',''),(175,5,'notifiche.alertSuspendTimeMinute','2'),(176,5,'sys_fornitorePrevisioneConsegna.RangeDay',''),(177,5,'sys_fornitorePrevisioneConsegna.RangeDayAnticipo',''),(178,5,'countriesIsoCode','AD;AE;AF;AG;AI;AL;AM;AN;AO;AQ;AR;AS;AT;AU;AW;AX;AZ;BA;BB;BD;BE;BF;BG;BH;BI;BJ;BL;BM;BN;BO;BQ;BR;BS;BT;BU;BV;BW;BY;BZ;CA;CC;CD;CF;CG;CH;CI;CK;CL;CM;CN;CO;CR;CS;CU;CV;CW;CX;CY;CZ;DE;DJ;DK;DM;DO;DZ;EC;EE;EG;EH;ER;ES;ET;FI;FJ;FK;FM;FO;FR;GA;GB;GD;GE;GF;GG;GH;GI;GL;GM;GN;GP;GQ;GR;GS;GT;GU;GW;GY;HK;HM;HN;HR;HT;HU;ID;IE;IL;IM;IN;IO;IQ;IR;IS;IT;JE;JM;JO;JP;KE;KG;KH;KI;KM;KN;KP;KR;KW;KY;KZ;LA;LB;LC;LI;LK;LR;LS;LT;LU;LV;LY;MA;MC;MD;ME;MF;MG;MH;MK;ML;MM;MN;MO;MP;MQ;MR;MS;MT;MU;MV;MW;MX;MY;MZ;NA;NC;NE;NF;NG;NI;NL;NO;NP;NR;NU;NZ;OM;PA;PE;PF;PG;PH;PK;PL;PM;PN;PR;PS;PT;PW;PY;QA;RE;RO;RS;RU;RW;SA;SB;SC;SD;SE;SG;SH;SI;SJ;SK;SL;SM;SN;SO;SR;SS;ST;SV;SX;SY;SZ;TC;TD;TF;TG;TH;TJ;TK;TL;TM;TN;TO;TP;TR;TT;TV;TW;TZ;UA;UG;UM;US;UY;UZ;VA;VC;VE;VG;VI;VN;VU;WF;WS;XK;YE;YT;ZA;ZM;ZW;'),(179,5,'countriesIsoCodeCE','AT;BE;BG;CY;CZ;DK;EE;FI;FR;DE;GR;HU;IE;IT;LV;LT;LU;MT;NL;PL;PT;RO;SK;SI;ES;SE;GB;'),(183,5,'partnerGiorniFestivi',''),(184,5,'primeGiorniFestivi',''),(185,5,'DeclarationOrigin.AskCertificationEvent','prDaCertificare'),(186,5,'DeclarationOrigin.AskCertificationCountriesIsoCode','AT;BE;BG;CY;CZ;DK;EE;FI;FR;DE;GR;HU;IE;IT;LV;LT;LU;MT;NL;PL;PT;RO;SK;SI;ES;SE;GB;'),(187,5,'DeclarationOrigin.ExportEvent','prConferma'),(188,5,'it.democenter.m2.wf.lockFileExtension','.lock'),(189,5,'it.democenter.m2.wf.exportType','ack'),(190,5,'it.democenter.m2.wf.dirLockFileName','_.lock'),(191,5,'dateConfermateAllineateLastFornitore','291'),(192,5,'counterMethod.ordineRiga','it.democenter.m2.om.Ditta_counters.incrementOrdineRiga'),(193,5,'customStyleSheetFile',''),(194,5,'doMonitor_dailyRoutines_lastDate_password','2016-05-12 09:35:52'),(195,5,'sys_password.InsertForDitta','true'),(196,5,'sys_password.validityDays','-1'),(197,5,'ditta.email.rcv.addr_auto','iungo@${mail.domain}'),(198,5,'ditta.email.rcv.host','${mail.server}'),(199,5,'ditta.email.rcv.protocol','${mail.rcv.protocol}'),(200,5,'ditta.email.rcv.starttls','true'),(201,5,'ditta.email.rcv.user','iungo@${mail.domain}'),(202,5,'ditta.email.rcv.pwd','iungo'),(203,5,'ditta.email.addr_error',''),(204,6,'alt_emailOrdineField','FornitoreFax'),(205,6,'attributes_OrdineRiga_form','NomeAllegato'),(206,6,'customSysUtils','it.democenter.m2.customers.scambioDati._common.SysUtils'),(207,6,'doMonitor_dailyRoutines_dayList','never'),(208,6,'doMonitor_dailyRoutines_lastDate','2012-04-10 03:01:11'),(209,6,'giorniRitardoConferma1','1'),(210,6,'giorniRitardoConferma2','3'),(211,6,'it.democenter.m2.wf.acknowledgeFileExtension','.ack'),(212,6,'it.democenter.m2.wf.dataFileExtension','.xml'),(213,6,'it.democenter.m2.wf.outputDirectory','C:/temp/fromIungo'),(214,6,'it.democenter.m2.wf.userName','user'),(215,6,'it.democenter.m2.wf.userPassword','pwd'),(216,6,'it.democenter.m2.wf.useShortFileName','false'),(217,6,'language.locale','it'),(218,6,'listaStatiPerEvidenziato','mForn'),(219,6,'mailProcessor_prNuovoOrd.actionOrdineAfterTransactions.class','it.democenter.m2.postaModello.processor.trigger.MailProcessorTrigger_Ordine_setAuxFields'),(220,6,'mailStatiFornitoreModifica','prop,conf,mForn'),(221,6,'mailStatiOrdinamento','crea,prop,annullato,conf,mForn,chiuso'),(222,6,'notifica.AnticipoBlocco','00:01:00'),(223,6,'ordini.reports.functions.class','it.democenter.m2.customers.scambioDati._common.report.ReportCustom'),(224,6,'pluginManager.alignDataSliceToM2.ordinePrimaryKey','ditta_tipo_numero_data'),(225,6,'sys_customizationCode','iungolive'),(226,6,'sys_documentazione.certificato','aaa.doc'),(227,6,'sys_documentazione.DeleteOriginal','false'),(228,6,'sys_documentazione.NoCopy','false'),(229,6,'sys_fornitorePrevisioneConsegna.InsertForDitta','false'),(230,6,'sys_logNotifica','C:/iungo/scambioDati/StoricoNotifiche'),(231,6,'sys_productBatch_computeQuantities','false'),(232,6,'sys_productBatch_createOnDDTRowInsert','false'),(233,6,'sys_show_index_groupName','false'),(234,6,'urlForIungoSupport','http://www.iungoapps.com/iungo_support'),(235,6,'urlForLogo','http://www.iungoapps.com/iungo_logo/demo_logo.jpg'),(236,6,'pluginManager.alignDataSliceToM2.sdOrdineRigaDocumentazione.importMode','importIsMaster'),(237,6,'sys_logNotifica_Type','simple'),(238,6,'customMailProcessor_profferta','it.democenter.m2.customers.scambioDati.iungolive.MailProcessor_prOfferta'),(239,6,'ordineRiga_customUpdateFromMattoni','it.democenter.m2.customers.scambioDati.iungolive.UpdateOrdineRiga'),(240,6,'listaStatiPerRitardo','prop'),(241,6,'sys_productBatch_showOnlyQuantityRemainingGt0','true'),(242,6,'it.democenter.m2.wf.useDotFileName','true'),(243,6,'sys_mail_htmlFileName','true'),(244,6,'sys_logNotificaFromRigaLog_ListaEventi','prProposta,prModifica,prConferma,prAnnulla'),(245,6,'md5RigaFunction','or_OrdineNumero,or_DataConsegna,or_Prezzo,or_Quantita'),(246,6,'sys_logNotificaFromRigaLog_ShowSpecialAux','false'),(247,6,'it.democenter.m2.wf.charsetFromIungo','8859_1'),(248,6,'mailPianoConsegnaStati','prop,conf,mForn'),(249,6,'mailPianoConsegnaTipiOrdini','OA'),(250,6,'customMailSend_FornitoreOrdineFax','it.democenter.m2.customers.scambioDati._common.NotificaFornitoreOrdineFax_IungoFax'),(251,6,'md5RigaNegoziabili','or_OrdineNumero,or_DataConsegna,or_Prezzo,or_Quantita'),(252,6,'notificheBloccateMaxTentativi_emailFornitore','1'),(253,6,'notificheBloccateMaxTentativi_emailFornitorePrevisioneConsegna','1'),(254,6,'notificheBloccateMaxTentativi_emailCliente','1'),(255,6,'sys_logNotificaReceived','false'),(256,6,'it.democenter.m2.report.directory','C:/iungo/scambioDati/report'),(257,6,'it.democenter.m2.theme','verde_pag_left'),(258,6,'customMailSend_Fax','it.democenter.m2.customers.scambioDati._common.fax.NotificaFornitoreOrdineFax_IungoFax'),(259,6,'delayedUpdateThreadLevel','5'),(260,6,'dataPrimaSpedizioneAllineata','true'),(261,6,'dataPrimaSpedizioneAllineataLastFornitore','292'),(262,6,'listaStatiPerAnnullato','annullato'),(263,6,'listaStatiChiuso','chiuso'),(264,6,'vistoLogAllineato','true'),(265,6,'vistoLogAllineatoLastFornitore','292'),(266,6,'downloadServletClass','it.democenter.m2.servlets.DocumentationDownload_default'),(267,6,'notifiche.ordini.minWaitSeconds','180'),(268,6,'notifiche.forceSend','false'),(269,6,'email_backup',''),(270,6,'notificheBloccateMaxTentativi_emailAutorizzazione','1'),(271,6,'sys_documentazione.rootDirectory',''),(272,6,'urlForImage','0'),(273,6,'urlIungoCustomer','${it.democenter.m2.IungoUrl}'),(274,6,'urlForPdfLabels','${it.democenter.m2.IungoUrl}'),(275,6,'useLowerCaseEmailContent','false'),(276,6,'listaStatiConferma','conf'),(277,6,'email.preview.active','false'),(278,6,'md5RigaLog','or_OrdineNumero,or_DataConsegna,or_Prezzo,or_Quantita'),(279,6,'ordineRiga.getCoefQtyPrice.method','false'),(280,6,'ordini.reports.ultimaConsegna.method','or_DataUltimaConsegnaGestionale'),(281,6,'notifiche.outgoingPageNotices','500'),(282,6,'notificaLog_nGiorni_storico','365'),(283,6,'listaStatiPerVisto','visto'),(284,6,'it.democenter.m2.wf.customFilenameMethod',''),(285,6,'notifiche.alertSuspendTimeMinute','2'),(286,6,'sys_fornitorePrevisioneConsegna.RangeDay',''),(287,6,'sys_fornitorePrevisioneConsegna.RangeDayAnticipo',''),(288,6,'countriesIsoCode','AD;AE;AF;AG;AI;AL;AM;AN;AO;AQ;AR;AS;AT;AU;AW;AX;AZ;BA;BB;BD;BE;BF;BG;BH;BI;BJ;BL;BM;BN;BO;BQ;BR;BS;BT;BU;BV;BW;BY;BZ;CA;CC;CD;CF;CG;CH;CI;CK;CL;CM;CN;CO;CR;CS;CU;CV;CW;CX;CY;CZ;DE;DJ;DK;DM;DO;DZ;EC;EE;EG;EH;ER;ES;ET;FI;FJ;FK;FM;FO;FR;GA;GB;GD;GE;GF;GG;GH;GI;GL;GM;GN;GP;GQ;GR;GS;GT;GU;GW;GY;HK;HM;HN;HR;HT;HU;ID;IE;IL;IM;IN;IO;IQ;IR;IS;IT;JE;JM;JO;JP;KE;KG;KH;KI;KM;KN;KP;KR;KW;KY;KZ;LA;LB;LC;LI;LK;LR;LS;LT;LU;LV;LY;MA;MC;MD;ME;MF;MG;MH;MK;ML;MM;MN;MO;MP;MQ;MR;MS;MT;MU;MV;MW;MX;MY;MZ;NA;NC;NE;NF;NG;NI;NL;NO;NP;NR;NU;NZ;OM;PA;PE;PF;PG;PH;PK;PL;PM;PN;PR;PS;PT;PW;PY;QA;RE;RO;RS;RU;RW;SA;SB;SC;SD;SE;SG;SH;SI;SJ;SK;SL;SM;SN;SO;SR;SS;ST;SV;SX;SY;SZ;TC;TD;TF;TG;TH;TJ;TK;TL;TM;TN;TO;TP;TR;TT;TV;TW;TZ;UA;UG;UM;US;UY;UZ;VA;VC;VE;VG;VI;VN;VU;WF;WS;XK;YE;YT;ZA;ZM;ZW;'),(289,6,'countriesIsoCodeCE','AT;BE;BG;CY;CZ;DK;EE;FI;FR;DE;GR;HU;IE;IT;LV;LT;LU;MT;NL;PL;PT;RO;SK;SI;ES;SE;GB;'),(290,6,'partnerGiorniFestivi',''),(291,6,'primeGiorniFestivi',''),(292,6,'DeclarationOrigin.AskCertificationEvent','prDaCertificare'),(293,6,'DeclarationOrigin.AskCertificationCountriesIsoCode','AT;BE;BG;CY;CZ;DK;EE;FI;FR;DE;GR;HU;IE;IT;LV;LT;LU;MT;NL;PL;PT;RO;SK;SI;ES;SE;GB;'),(294,6,'DeclarationOrigin.ExportEvent','prConferma'),(295,6,'it.democenter.m2.wf.lockFileExtension','.lock'),(296,6,'it.democenter.m2.wf.exportType','ack'),(297,6,'it.democenter.m2.wf.dirLockFileName','_.lock'),(298,6,'dateConfermateAllineateLastFornitore','291'),(299,6,'counterMethod.ordineRiga','it.democenter.m2.om.Ditta_counters.incrementOrdineRiga'),(300,6,'customStyleSheetFile',''),(301,6,'doMonitor_dailyRoutines_lastDate_password','2016-05-12 09:35:52'),(302,6,'sys_password.InsertForDitta','true'),(303,6,'sys_password.validityDays','-1'),(304,6,'ditta.email.rcv.addr_auto','iungo2@${mail.domain}'),(305,6,'ditta.email.rcv.host','${mail.server}'),(306,6,'ditta.email.rcv.protocol','pop3'),(307,6,'ditta.email.rcv.starttls','true'),(308,6,'ditta.email.rcv.user','iungo2@${mail.domain}'),(309,6,'ditta.email.rcv.pwd','iungo'),(310,6,'ditta.email.addr_error',''),(311,5,'ditta.email.rcv.port','${mail.rcv.port}'),(312,6,'ditta.email.rcv.port','110'),(313,5,'email_testAddress','fornitore@${mail.domain}'),(314,5,'ordini.elencoEventi.comboBox',''),(315,5,'sys.mail.eventOnResend',''),(316,5,'customSchedulingAgreementManager',''),(317,5,'tipiOrdineSchedulingAgreement','SA,PDC'),(318,5,'ordineRiga.totaleRiga.customMethod','false'),(319,5,'ordineRiga.prezzoUnitario.customMethod','false'),(320,5,'listaStatiValidazione',''),(321,5,'startDateNewProgressivoNotifica','01-01-2000'),(322,5,'pluginManager.alignDataSliceToM2.onlyIfMostRecent','false'),(323,5,'pluginManager.alignDataSliceToM2.sdOrdineRigaDocumentazione.performTransaction','false'),(324,5,'pluginManager.alignDataSliceToM2.sdOrdineRigaDocumentazione.deleteMissing','true'),(325,5,'ordineRiga.getTransportLeadTime.customMethod','false'),(326,5,'pluginManager.alignDataSliceToM2.sdOrdineRiga.updateAttributesBeforeTransaction','false'),(327,5,'pluginManager.alignDataSliceToM2.sdOrdineRiga.updateSplitBeforeTransaction','false'),(328,5,'pluginManager.alignDataSliceToM2.onlyIfChangedInAdministrativeDatabase','true'),(329,5,'pluginManager.alignDataSliceToM2.onlyIfChangedInAdministrativeDatabaseEmail','false'),(330,5,'partnerOrarioLavoro',''),(332,5,'sys_logNotificaReceived.avc','false'),(333,5,'md5RigaCampiCritici_DDT',''),(334,5,'doTimedPlugin_reportScheduler_base_lastDate','2016-05-10 12:00:53'),(335,5,'doTimedPlugin_AlignInfo_base_nextDate','2016-05-11 05:00:00'),(336,5,'doTimedPlugin_AlignInfo_base_lastDate','2016-05-10 11:50:54');
/*!40000 ALTER TABLE `ditta_configurazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document`
--

DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `DOCUMENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOCUMENT_TYPE_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `LAST_MODIFIED_BY_PRIME` datetime DEFAULT NULL,
  `LAST_MODIFIED_BY_SUPPLIER` datetime DEFAULT NULL,
  `VOTE` decimal(19,6) DEFAULT NULL,
  `MANDATORY_COMPILATION_RATE` decimal(19,6) DEFAULT NULL,
  `COMPILATION_RATE` decimal(19,6) DEFAULT NULL,
  `STATO` varchar(30) DEFAULT NULL,
  `LAST_INTERNAL_USER` varchar(32) DEFAULT NULL,
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`DOCUMENT_ID`),
  KEY `DOCUMENT_TYPE_ID` (`DOCUMENT_TYPE_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `document_ibfk_1` FOREIGN KEY (`DOCUMENT_TYPE_ID`) REFERENCES `document_type` (`DOCUMENT_TYPE_ID`),
  CONSTRAINT `document_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document`
--

LOCK TABLES `document` WRITE;
/*!40000 ALTER TABLE `document` DISABLE KEYS */;
/*!40000 ALTER TABLE `document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_type`
--

DROP TABLE IF EXISTS `document_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_type` (
  `DOCUMENT_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(254) NOT NULL,
  `DESCRIPTION` varchar(1500) DEFAULT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `ORDINE_WF_ID` int(11) NOT NULL,
  `CUSTOM_FUNCTION_CLASS` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENT_TYPE_ID`),
  UNIQUE KEY `NAME` (`NAME`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `ORDINE_WF_ID` (`ORDINE_WF_ID`),
  CONSTRAINT `document_type_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `document_type_ibfk_2` FOREIGN KEY (`ORDINE_WF_ID`) REFERENCES `ordine_wf` (`ORDINE_WF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_type`
--

LOCK TABLES `document_type` WRITE;
/*!40000 ALTER TABLE `document_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_type_translation`
--

DROP TABLE IF EXISTS `document_type_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_type_translation` (
  `DOCUMENT_TYPE_TRANSLATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOCUMENT_TYPE_ID` int(11) NOT NULL,
  `LANGUAGE` varchar(254) NOT NULL,
  `NAME` varchar(254) DEFAULT NULL,
  `DESCRIPTION` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENT_TYPE_TRANSLATION_ID`),
  KEY `DOCUMENT_TYPE_ID` (`DOCUMENT_TYPE_ID`),
  CONSTRAINT `document_type_translation_ibfk_1` FOREIGN KEY (`DOCUMENT_TYPE_ID`) REFERENCES `document_type` (`DOCUMENT_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_type_translation`
--

LOCK TABLES `document_type_translation` WRITE;
/*!40000 ALTER TABLE `document_type_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_type_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentazione`
--

DROP TABLE IF EXISTS `documentazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentazione` (
  `DOCUMENTAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `FILE_NAME` varchar(254) NOT NULL,
  `FILE_LAST_MODIFIED` datetime DEFAULT NULL,
  `FILE_SIZE` int(11) DEFAULT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `DOCUMENTAZIONE_TIPO_ID` int(11) NOT NULL,
  `USER_NAME` varchar(99) DEFAULT NULL,
  `MANUALE` varchar(1) DEFAULT NULL,
  `DA_PARTNER` varchar(1) DEFAULT '0',
  `CRC` varchar(50) DEFAULT NULL,
  `FILE_EXIST` int(11) DEFAULT '0',
  `LAST_TESTED_DATE` datetime DEFAULT NULL,
  `MANAGER_TYPE` varchar(254) DEFAULT 'FS',
  `KEY_FOR_CALL` varchar(500) DEFAULT '',
  `IMPORT_DEAL` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENTAZIONE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `DOCUMENTAZIONE_TIPO_ID` (`DOCUMENTAZIONE_TIPO_ID`),
  KEY `DOCUMENTAZIONE_TIPO_CODICE` (`DITTA_ID`,`FILE_NAME`),
  CONSTRAINT `documentazione_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `documentazione_ibfk_2` FOREIGN KEY (`DOCUMENTAZIONE_TIPO_ID`) REFERENCES `documentazione_tipo` (`DOCUMENTAZIONE_TIPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentazione`
--

LOCK TABLES `documentazione` WRITE;
/*!40000 ALTER TABLE `documentazione` DISABLE KEYS */;
INSERT INTO `documentazione` VALUES (5,5,'sysIOuser1202113601634',NULL,-1,NULL,1,'sysScheduler','0','0',NULL,0,NULL,'FS','',NULL),(6,5,'sysIOuser1202127522130',NULL,-1,NULL,1,'sysScheduler','0','0',NULL,0,NULL,'FS','',NULL);
/*!40000 ALTER TABLE `documentazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentazione_box`
--

DROP TABLE IF EXISTS `documentazione_box`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentazione_box` (
  `DOCUMENTAZIONE_BOX_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `NUMBER` varchar(50) DEFAULT NULL,
  `ISSUE_DATE` datetime NOT NULL,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `CONTACT_PERSON` varchar(254) DEFAULT NULL,
  `TRANSMISSION_MODE` varchar(20) DEFAULT NULL,
  `NOTA` mediumtext,
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `AUX_DATE1` datetime DEFAULT NULL,
  `AUX_DATE2` datetime DEFAULT NULL,
  `AUX_DATE3` datetime DEFAULT NULL,
  `AUX_DATE4` datetime DEFAULT NULL,
  `AUX_DATE5` datetime DEFAULT NULL,
  `AUX_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM5` decimal(12,5) DEFAULT '0.00000',
  `OGGETTO` varchar(80) DEFAULT NULL,
  `NOTA_CORPO` varchar(8000) DEFAULT NULL,
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`DOCUMENTAZIONE_BOX_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`NUMBER`,`ISSUE_DATE`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  CONSTRAINT `documentazione_box_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `documentazione_box_ibfk_2` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentazione_box`
--

LOCK TABLES `documentazione_box` WRITE;
/*!40000 ALTER TABLE `documentazione_box` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentazione_box` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentazione_box_destinatario`
--

DROP TABLE IF EXISTS `documentazione_box_destinatario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentazione_box_destinatario` (
  `DOCUMENTAZIONE_BOX_DESTINATARIO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOCUMENTAZIONE_BOX_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) DEFAULT NULL,
  `FG_VISTO` int(11) DEFAULT '0',
  `STATO` varchar(30) NOT NULL,
  `EMAIL` varchar(600) DEFAULT NULL,
  `FAX_NUMBER` varchar(99) DEFAULT NULL,
  `PARTNER_GROUP` varchar(50) DEFAULT NULL,
  `RECIPIENT_NAME` varchar(254) DEFAULT NULL,
  `NOTA` mediumtext,
  `DATA_VISTO` datetime DEFAULT NULL,
  `NOTE_SPEDIZIONE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENTAZIONE_BOX_DESTINATARIO_ID`),
  UNIQUE KEY `DOCUMENTAZIONE_BOX_ID_2` (`DOCUMENTAZIONE_BOX_ID`,`FORNITORE_ID`),
  KEY `DOCUMENTAZIONE_BOX_ID` (`DOCUMENTAZIONE_BOX_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `documentazione_box_destinatario_ibfk_1` FOREIGN KEY (`DOCUMENTAZIONE_BOX_ID`) REFERENCES `documentazione_box` (`DOCUMENTAZIONE_BOX_ID`),
  CONSTRAINT `documentazione_box_destinatario_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentazione_box_destinatario`
--

LOCK TABLES `documentazione_box_destinatario` WRITE;
/*!40000 ALTER TABLE `documentazione_box_destinatario` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentazione_box_destinatario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentazione_box_log`
--

DROP TABLE IF EXISTS `documentazione_box_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentazione_box_log` (
  `DOCUMENTAZIONE_BOX_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOCUMENTAZIONE_BOX_DESTINATARIO_ID` int(11) NOT NULL,
  `STATO_NOME` varchar(30) NOT NULL,
  `EVENTO_NOME` varchar(30) NOT NULL,
  `USER_NAME` varchar(99) NOT NULL,
  `MOTIVO` varchar(254) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `MITTENTE` varchar(80) DEFAULT '',
  `DATA_VISTO` datetime DEFAULT NULL,
  `VISTO_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `NOTE_SPEDIZIONE` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENTAZIONE_BOX_LOG_ID`),
  KEY `DOCUMENTAZIONE_BOX_DESTINATARIO_ID` (`DOCUMENTAZIONE_BOX_DESTINATARIO_ID`),
  CONSTRAINT `documentazione_box_log_ibfk_1` FOREIGN KEY (`DOCUMENTAZIONE_BOX_DESTINATARIO_ID`) REFERENCES `documentazione_box_destinatario` (`DOCUMENTAZIONE_BOX_DESTINATARIO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentazione_box_log`
--

LOCK TABLES `documentazione_box_log` WRITE;
/*!40000 ALTER TABLE `documentazione_box_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentazione_box_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentazione_tipo`
--

DROP TABLE IF EXISTS `documentazione_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documentazione_tipo` (
  `DOCUMENTAZIONE_TIPO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODICE` varchar(50) NOT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `PERMISSION_INSERT` varchar(99) NOT NULL,
  `PERMISSION_READ` varchar(99) NOT NULL,
  `PERMISSION_DELETE` varchar(99) NOT NULL,
  `PERMISSION_UPDATE` varchar(99) NOT NULL,
  `FG_USE_MIME_TYPE` int(11) DEFAULT NULL,
  `NOTA` mediumtext,
  PRIMARY KEY (`DOCUMENTAZIONE_TIPO_ID`),
  UNIQUE KEY `CODICE` (`CODICE`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentazione_tipo`
--

LOCK TABLES `documentazione_tipo` WRITE;
/*!40000 ALTER TABLE `documentazione_tipo` DISABLE KEYS */;
INSERT INTO `documentazione_tipo` VALUES (1,'sys.OrdineRiga','Documentazioni correlate alle Righe Ordine','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',1,''),(4,'sys.Product','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(5,'sys.ProductBatch','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(6,'sys.ProductFormula','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(7,'sys.Resource','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(8,'sys.ProductFormulaProcessing','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(9,'sys.ControlloQualita','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(10,'sys.NonConformita','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(11,'sys.OrdineRigaManuale','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(12,'sys.Ordine','','docs_sys.Ordine_insert','docs_sys.Ordine_read','docs_sys.Ordine_delete','docs_sys.Ordine_update',0,''),(13,'sys.Fornitore','','docs_sys.Fornitore_insert','docs_sys.Fornitore_read','docs_sys.Fornitore_delete','docs_sys.Fornitore_update',0,''),(14,'sys.RdaRdo','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(15,'sys.UndefinedDoc','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(16,'sys.ProductList','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(17,'sys.IungoMailMessage','Messaggi allegati alla mail di iungo','docs_sys.IungoMailMessage_insert','docs_sys.IungoMailMessage_read','docs_sys.IungoMailMessage_delete','docs_sys.IungoMailMessage_update',1,''),(18,'sys.DeclarationOrigin','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(19,'sys.DeclarationOriginRow','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,''),(20,'sys.Invoice','','docs_sys.OrdineRiga_insert','docs_sys.OrdineRiga_read','docs_sys.OrdineRiga_delete','docs_sys.OrdineRiga_update',0,'');
/*!40000 ALTER TABLE `documentazione_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento_trasporto`
--

DROP TABLE IF EXISTS `documento_trasporto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento_trasporto` (
  `DOCUMENTO_TRASPORTO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FG_OUTBOUND` int(11) DEFAULT '0',
  `FORNITORE_ID` int(11) NOT NULL,
  `NOTA` varchar(254) DEFAULT NULL,
  `DATA_EMISSIONE` datetime DEFAULT NULL,
  `NUMERO` varchar(50) DEFAULT NULL,
  `DESTINAZIONE_INDIRIZZO` varchar(254) DEFAULT NULL,
  `VETTORE` varchar(254) DEFAULT NULL,
  `COLLI_NUMERO` int(11) DEFAULT NULL,
  `DATA_RICEZIONE` datetime DEFAULT NULL,
  `DATA_CONSEGNA_PREVISTA` datetime DEFAULT NULL,
  `COLLI_RICEVUTI` int(11) DEFAULT NULL,
  `TRASMISSION_NUMBER` int(11) DEFAULT '0',
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `AUX_DATE1` datetime DEFAULT NULL,
  `AUX_DATE2` datetime DEFAULT NULL,
  `AUX_DATE3` datetime DEFAULT NULL,
  `AUX_DATE4` datetime DEFAULT NULL,
  `AUX_DATE5` datetime DEFAULT NULL,
  `AUX_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM5` decimal(12,5) DEFAULT '0.00000',
  `SUB_FORNITORE_ID` int(11) DEFAULT NULL,
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  `ORDINE_TIPO_ID` int(11) DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  `NUMERO_GESTIONALE` varchar(50) DEFAULT '',
  PRIMARY KEY (`DOCUMENTO_TRASPORTO_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `DOCUMENTO_TRASPORTO_NUMERO` (`NUMERO`),
  KEY `documento_trasporto_ibfk_2` (`ORDINE_TIPO_ID`),
  CONSTRAINT `documento_trasporto_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `documento_trasporto_ibfk_2` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento_trasporto`
--

LOCK TABLES `documento_trasporto` WRITE;
/*!40000 ALTER TABLE `documento_trasporto` DISABLE KEYS */;
INSERT INTO `documento_trasporto` VALUES (57,1,291,'','2006-02-22 11:24:00','S00001','','',0,NULL,NULL,0,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,25,NULL,''),(78,0,288,'','2006-03-17 00:00:00','1002','-','-',1,'2008-01-23 09:23:00',NULL,1,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,25,'2008-01-23 09:23:00',''),(81,0,287,'','2008-01-29 17:49:00','1003','Emilia, 29 41100 Modena MO I','',0,'2008-01-29 17:49:00',NULL,0,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,25,'2008-01-29 17:49:00','');
/*!40000 ALTER TABLE `documento_trasporto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento_trasporto_riga`
--

DROP TABLE IF EXISTS `documento_trasporto_riga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento_trasporto_riga` (
  `DOCUMENTO_TRASPORTO_RIGA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NUMERO_RIGA` varchar(30) DEFAULT NULL,
  `DOCUMENTO_TRASPORTO_ID` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `STATO` varchar(30) DEFAULT NULL,
  `NOTA` varchar(254) DEFAULT '',
  `ORDINE_RIGA_ID` int(11) DEFAULT NULL,
  `PRODUCT_CODE` varchar(254) DEFAULT '',
  `PRODUCT_CODE_PARTNER` varchar(254) DEFAULT '',
  `PRODUCT_DESCRIPTION` varchar(254) DEFAULT '',
  `PRODUCT_UNIT_OF_MEASURE` varchar(100) DEFAULT '',
  `PRODUCT_BATCH_CODE` varchar(254) DEFAULT '',
  `PRODUCT_BATCH_CODE_PARTNER` varchar(254) DEFAULT '',
  `QUANTITA` decimal(15,5) DEFAULT NULL,
  `FG_A_SALDO` int(11) DEFAULT NULL,
  `DATA_ACCETTAZIONE` datetime DEFAULT NULL,
  `QUANTITA_ACCETTATA` decimal(15,5) DEFAULT '0.00000',
  `FG_A_SALDO_ACCETTAZIONE` int(11) DEFAULT NULL,
  `PRODUCT_DESCRIPTION_PARTNER` varchar(254) DEFAULT '',
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT '',
  `INVOICE_NUMBER` varchar(50) DEFAULT '',
  `INVOICE_EMISSION_DATE` datetime DEFAULT NULL,
  `INVOICE_LINE_NUMBER` varchar(30) DEFAULT '',
  `PREVISIONALE_TIPO` varchar(99) DEFAULT NULL,
  `PREVISIONALE_NUMERO` varchar(50) DEFAULT '',
  `PREVISIONALE_DATA` datetime DEFAULT NULL,
  `PREVISIONALE_NUMERO_RIGA` varchar(30) DEFAULT '',
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `AUX_DATE1` datetime DEFAULT NULL,
  `AUX_DATE2` datetime DEFAULT NULL,
  `AUX_DATE3` datetime DEFAULT NULL,
  `AUX_DATE4` datetime DEFAULT NULL,
  `AUX_DATE5` datetime DEFAULT NULL,
  `AUX_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM5` decimal(12,5) DEFAULT '0.00000',
  `ORDINE_RIGA_NUMERO` varchar(30) DEFAULT '',
  `ORDINE_NUMERO` varchar(50) DEFAULT '',
  `ORDINE_DATA_EMISSIONE` datetime DEFAULT NULL,
  `ORDINE_TIPO_NOME` varchar(99) DEFAULT '',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`DOCUMENTO_TRASPORTO_RIGA_ID`),
  KEY `DOCUMENTO_TRASPORTO_ID` (`DOCUMENTO_TRASPORTO_ID`),
  KEY `ORDINE_RIGA_ID` (`ORDINE_RIGA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `DOCUMENTO_T_R_PRODUCT_BATCH_CODE` (`PRODUCT_BATCH_CODE`),
  CONSTRAINT `documento_trasporto_riga_ibfk_1` FOREIGN KEY (`DOCUMENTO_TRASPORTO_ID`) REFERENCES `documento_trasporto` (`DOCUMENTO_TRASPORTO_ID`),
  CONSTRAINT `documento_trasporto_riga_ibfk_2` FOREIGN KEY (`ORDINE_RIGA_ID`) REFERENCES `ordine_riga` (`ORDINE_RIGA_ID`),
  CONSTRAINT `documento_trasporto_riga_ibfk_3` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento_trasporto_riga`
--

LOCK TABLES `documento_trasporto_riga` WRITE;
/*!40000 ALTER TABLE `documento_trasporto_riga` DISABLE KEYS */;
INSERT INTO `documento_trasporto_riga` VALUES (123,'10',57,5,'out','',16960,'Item1','','','','Batch1a',NULL,1000.00000,0,NULL,0.00000,0,'',0.00000,'','',NULL,'',NULL,'',NULL,'','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,'','',NULL,'',NULL),(148,'3',78,5,'acc','',16992,'ItemX','','Item X','Nr.','X0001','',100.00000,1,'2008-01-23 00:00:00',100.00000,1,'',50.00000,'','',NULL,'','','',NULL,'','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,'','',NULL,'',NULL),(150,'20',57,5,'out','',NULL,'Item1','','Item 1','Nr','Item1Lotto0001','',1.00000,1,'2006-02-22 11:24:00',1.00000,1,'',0.00000,'','',NULL,'','','',NULL,'','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,'','',NULL,'',NULL),(151,'1',81,5,'acc','',17017,'Item1','','Item 1','Nr','10001','',1.00000,0,'2008-01-29 00:00:00',0.00000,0,'',0.00000,'','',NULL,'','','',NULL,'','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,'','',NULL,'',NULL);
/*!40000 ALTER TABLE `documento_trasporto_riga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento_trasporto_riga_log`
--

DROP TABLE IF EXISTS `documento_trasporto_riga_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento_trasporto_riga_log` (
  `DOCUMENTO_TRASPORTO_RIGA_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOCUMENTO_TRASPORTO_RIGA_ID` int(11) NOT NULL,
  `NUMERO_RIGA` varchar(30) DEFAULT NULL,
  `DOCUMENTO_TRASPORTO_ID` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `STATO` varchar(30) DEFAULT NULL,
  `NOTA` varchar(254) DEFAULT '',
  `ORDINE_RIGA_ID` int(11) DEFAULT NULL,
  `PRODUCT_CODE` varchar(254) DEFAULT '',
  `PRODUCT_CODE_PARTNER` varchar(254) DEFAULT '',
  `PRODUCT_DESCRIPTION` varchar(254) DEFAULT '',
  `PRODUCT_UNIT_OF_MEASURE` varchar(100) DEFAULT '',
  `PRODUCT_BATCH_CODE` varchar(254) DEFAULT '',
  `PRODUCT_BATCH_CODE_PARTNER` varchar(254) DEFAULT '',
  `QUANTITA` decimal(15,5) DEFAULT NULL,
  `FG_A_SALDO` int(11) DEFAULT NULL,
  `DATA_ACCETTAZIONE` datetime DEFAULT NULL,
  `QUANTITA_ACCETTATA` decimal(15,5) DEFAULT '0.00000',
  `FG_A_SALDO_ACCETTAZIONE` int(11) DEFAULT NULL,
  `PRODUCT_DESCRIPTION_PARTNER` varchar(254) DEFAULT '',
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT '',
  `INVOICE_NUMBER` varchar(50) DEFAULT '',
  `INVOICE_EMISSION_DATE` datetime DEFAULT NULL,
  `INVOICE_LINE_NUMBER` varchar(30) DEFAULT '',
  `PREVISIONALE_TIPO` varchar(99) DEFAULT NULL,
  `PREVISIONALE_NUMERO` varchar(50) DEFAULT '',
  `PREVISIONALE_DATA` datetime DEFAULT NULL,
  `PREVISIONALE_NUMERO_RIGA` varchar(30) DEFAULT '',
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `AUX_DATE1` datetime DEFAULT NULL,
  `AUX_DATE2` datetime DEFAULT NULL,
  `AUX_DATE3` datetime DEFAULT NULL,
  `AUX_DATE4` datetime DEFAULT NULL,
  `AUX_DATE5` datetime DEFAULT NULL,
  `AUX_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM5` decimal(12,5) DEFAULT '0.00000',
  `ORDINE_RIGA_NUMERO` varchar(30) DEFAULT '',
  `ORDINE_NUMERO` varchar(50) DEFAULT NULL,
  `ORDINE_DATA_EMISSIONE` datetime DEFAULT NULL,
  `ORDINE_TIPO_NOME` varchar(99) DEFAULT '',
  `EVENTO_NOME` varchar(30) NOT NULL,
  `USER_NAME` varchar(99) NOT NULL,
  `MOTIVO` varchar(254) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `FG_ALLINEAMENTO` int(11) DEFAULT '0',
  `DOCUMENTAZIONE_CHANGES` mediumtext,
  `VISTO_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`DOCUMENTO_TRASPORTO_RIGA_LOG_ID`),
  KEY `DOCUMENTO_TRASPORTO_RIGA_ID` (`DOCUMENTO_TRASPORTO_RIGA_ID`),
  KEY `DOCUMENTO_TRASPORTO_ID` (`DOCUMENTO_TRASPORTO_ID`),
  KEY `ORDINE_RIGA_ID` (`ORDINE_RIGA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `DOCUMENTO_T_R_PRODUCT_BATCH_CODE` (`PRODUCT_BATCH_CODE`),
  KEY `VISTO_NOTIFICA_LOG_RICEVUTA_ID` (`VISTO_NOTIFICA_LOG_RICEVUTA_ID`),
  KEY `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` (`MODFORN_NOTIFICA_LOG_RICEVUTA_ID`),
  CONSTRAINT `documento_trasporto_riga_log_ibfk_1` FOREIGN KEY (`DOCUMENTO_TRASPORTO_RIGA_ID`) REFERENCES `documento_trasporto_riga` (`DOCUMENTO_TRASPORTO_RIGA_ID`),
  CONSTRAINT `documento_trasporto_riga_log_ibfk_2` FOREIGN KEY (`DOCUMENTO_TRASPORTO_ID`) REFERENCES `documento_trasporto` (`DOCUMENTO_TRASPORTO_ID`),
  CONSTRAINT `documento_trasporto_riga_log_ibfk_3` FOREIGN KEY (`ORDINE_RIGA_ID`) REFERENCES `ordine_riga` (`ORDINE_RIGA_ID`),
  CONSTRAINT `documento_trasporto_riga_log_ibfk_4` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento_trasporto_riga_log`
--

LOCK TABLES `documento_trasporto_riga_log` WRITE;
/*!40000 ALTER TABLE `documento_trasporto_riga_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `documento_trasporto_riga_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addr`
--

DROP TABLE IF EXISTS `email_addr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addr` (
  `EMAIL_ADDR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(255) NOT NULL,
  `BLACK` int(11) DEFAULT '0',
  `SKIP` int(11) NOT NULL DEFAULT '0',
  `BLACK_TIMES` int(11) NOT NULL DEFAULT '0',
  `BLACK_LAST_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`EMAIL_ADDR_ID`),
  UNIQUE KEY `EMAIL` (`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addr`
--

LOCK TABLES `email_addr` WRITE;
/*!40000 ALTER TABLE `email_addr` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_addr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enum_value`
--

DROP TABLE IF EXISTS `enum_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enum_value` (
  `ENUM_VALUE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TEXT` varchar(500) DEFAULT NULL,
  `DESCRIPTION` varchar(1500) DEFAULT NULL,
  `WEIGHT` decimal(19,6) DEFAULT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `USER_IDENTIFIER` varchar(254) DEFAULT NULL,
  `POSITION` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ENUM_VALUE_ID`),
  UNIQUE KEY `ENUM_VALUE_USER_IDENTIFIER_UNIQUE_8764512` (`FIELD_ID`,`USER_IDENTIFIER`),
  KEY `FIELD_ID` (`FIELD_ID`),
  CONSTRAINT `enum_value_ibfk_1` FOREIGN KEY (`FIELD_ID`) REFERENCES `field` (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enum_value`
--

LOCK TABLES `enum_value` WRITE;
/*!40000 ALTER TABLE `enum_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `enum_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `enum_value_translation`
--

DROP TABLE IF EXISTS `enum_value_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `enum_value_translation` (
  `ENUM_VALUE_TRANSLATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ENUM_VALUE_ID` int(11) NOT NULL,
  `LANGUAGE` varchar(254) NOT NULL,
  `TEXT` varchar(500) DEFAULT NULL,
  `DESCRIPTION` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`ENUM_VALUE_TRANSLATION_ID`),
  KEY `ENUM_VALUE_ID` (`ENUM_VALUE_ID`),
  CONSTRAINT `enum_value_translation_ibfk_1` FOREIGN KEY (`ENUM_VALUE_ID`) REFERENCES `enum_value` (`ENUM_VALUE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `enum_value_translation`
--

LOCK TABLES `enum_value_translation` WRITE;
/*!40000 ALTER TABLE `enum_value_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `enum_value_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etichetta`
--

DROP TABLE IF EXISTS `etichetta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etichetta` (
  `ETICHETTA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSONA_ID` int(11) DEFAULT NULL,
  `CATEGORIA` varchar(50) DEFAULT NULL,
  `VALORE` varchar(30) NOT NULL,
  `DESCRIZIONE` varchar(99) DEFAULT NULL,
  `COLORE_HTML` varchar(10) NOT NULL,
  `COLORE_TESTO_HTML` varchar(10) NOT NULL,
  `IS_PUBLIC` int(1) DEFAULT '0',
  PRIMARY KEY (`ETICHETTA_ID`),
  UNIQUE KEY `PERSONA_ID_2` (`PERSONA_ID`,`CATEGORIA`,`VALORE`),
  KEY `PERSONA_ID` (`PERSONA_ID`),
  CONSTRAINT `etichetta_ibfk_1` FOREIGN KEY (`PERSONA_ID`) REFERENCES `persona` (`PERSONA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etichetta`
--

LOCK TABLES `etichetta` WRITE;
/*!40000 ALTER TABLE `etichetta` DISABLE KEYS */;
INSERT INTO `etichetta` VALUES (1,NULL,'SYSTEM','elem_private','','#000000','#FFFFFF',0),(2,NULL,'SYSTEM','Sollecito','Preferiti','#000000','#FFFFFF',0);
/*!40000 ALTER TABLE `etichetta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `etichetta_associazione`
--

DROP TABLE IF EXISTS `etichetta_associazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `etichetta_associazione` (
  `ETICHETTA_ASSOCIAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `PERSONA_ID` int(11) NOT NULL,
  `ETICHETTA_ID` int(11) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ETICHETTA_ASSOCIAZIONE_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`PERSONA_ID`,`ETICHETTA_ID`,`OBJECT_TYPE`,`OBJECT_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `ETICHETTA_ID` (`ETICHETTA_ID`),
  KEY `PERSONA_ID` (`PERSONA_ID`),
  KEY `OGGETTO_RIFERITO` (`OBJECT_TYPE`,`OBJECT_ID`),
  CONSTRAINT `etichetta_associazione_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `etichetta_associazione_ibfk_2` FOREIGN KEY (`ETICHETTA_ID`) REFERENCES `etichetta` (`ETICHETTA_ID`),
  CONSTRAINT `etichetta_associazione_ibfk_3` FOREIGN KEY (`PERSONA_ID`) REFERENCES `persona` (`PERSONA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `etichetta_associazione`
--

LOCK TABLES `etichetta_associazione` WRITE;
/*!40000 ALTER TABLE `etichetta_associazione` DISABLE KEYS */;
/*!40000 ALTER TABLE `etichetta_associazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exception_log`
--

DROP TABLE IF EXISTS `exception_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exception_log` (
  `EXCEPTION_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` datetime NOT NULL,
  `MESSAGE` varchar(1000) DEFAULT NULL,
  `JAVA_EXCEPTION` longblob NOT NULL,
  `TYPE` varchar(45) NOT NULL,
  PRIMARY KEY (`EXCEPTION_LOG_ID`),
  KEY `DATE` (`DATE`),
  KEY `TYPE` (`TYPE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exception_log`
--

LOCK TABLES `exception_log` WRITE;
/*!40000 ALTER TABLE `exception_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `exception_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_sd_file`
--

DROP TABLE IF EXISTS `export_sd_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_sd_file` (
  `EXPORT_SD_FILE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `DATA` datetime DEFAULT NULL,
  `FG_EXPORTED` int(11) NOT NULL DEFAULT '0',
  `CONTENT` longblob,
  `FILENAME` varchar(254) NOT NULL,
  `FILEPATH` varchar(254) NOT NULL,
  `ACKEXTENSION` varchar(10) NOT NULL,
  `FG_ERROR` int(11) NOT NULL DEFAULT '0',
  `USER_NAME` varchar(99) DEFAULT NULL,
  `LOCKEXTENSION` varchar(10) DEFAULT NULL,
  `DIRLOCKFILENAME` varchar(30) DEFAULT NULL,
  `EXPORTTYPE` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`EXPORT_SD_FILE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `export_sd_file_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_sd_file`
--

LOCK TABLES `export_sd_file` WRITE;
/*!40000 ALTER TABLE `export_sd_file` DISABLE KEYS */;
INSERT INTO `export_sd_file` VALUES (1,5,'2012-01-26 15:04:10',1,'‚Äö√†√∂‚àö√Ü‚Äö√†√∂‚Äö√†√®‚âà√≠¬¨¬©‚Äö√†√∂‚àö√Ü‚Äö√†√∂‚Äö√†√®‚âà√≠¬¨¬©\0tD<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<!DOCTYPE SdDataSlice SYSTEM \"m2Data.dtd\">\n\n<SdDataSlice>\n\n<SdCompanyHeader>\n	<LegalName>base</LegalName>\n	<Group>base</Group>\n	<VAT>024687531</VAT>\n	<Address>Via Giardini 58</Address>\n	<ZIPCode>41100</ZIPCode>\n	<City>Modena</City>\n	<Town>Modena</Town>\n	<Country>Italy</Country>\n	<IungoEmailAddress></IungoEmailAddress>\n	<PhoneNr>+39 059 622 93 22</PhoneNr>\n	<FaxNr>+39 059 622 93 11</FaxNr>\n	<Note>VAT IT032596580123 }\r\nRegistro Imprese Modena 121212/1999 }\r\nCapitale Sociale EUR 100.000 i.v. }\r\nCodice Meccanografico MO0123456 }\r\nR.E.A. Modena 123456</Note>\n	<TransmissionDate>26-01-2012 15:04</TransmissionDate>\n	<FromTo>fromM2</FromTo>\n	<UserName>user</UserName>\n	<UserPassword>pwd</UserPassword>\n</SdCompanyHeader>\n\n\n\n\n\n<SdSingleOrderLine>\n		<OrderNumber>008888</OrderNumber>\n		<OrderTypeName>OA</OrderTypeName>\n		<OrderDate>28-09-2007 08:30</OrderDate>\n		<PartnerId>000036</PartnerId>\n		<OrderLineNumber>010</OrderLineNumber>\n		<ItemCode>C138</ItemCode>\n		<ItemDescription>COPPA MARLENE TG.34D/36C</ItemDescription>\n		<PartnerItemCode></PartnerItemCode>\n		<PartnerItemDescription></PartnerItemDescription>\n		<PhaseNumber></PhaseNumber>\n		<PhaseDescription></PhaseDescription>\n		<PriceUnit></PriceUnit>\n		<PriceUnitCode></PriceUnitCode>\n		<QtyUnit>CP</QtyUnit>\n		<QtyUnitCode></QtyUnitCode>\n		<Qty>5.0</Qty>\n		<Price>1.5</Price>\n		<CoefQtyPrice>1.0</CoefQtyPrice>\n		<AvailabilityDate></AvailabilityDate>\n		<DeliveryDate></DeliveryDate>\n		<Currency>EUR</Currency>\n		<CurrencyAlphabeticCode>EUR</CurrencyAlphabeticCode>\n		<OrderWfStatusName>conf</OrderWfStatusName>\n		<ProjectId></ProjectId>\n		<DeliveryAddress></DeliveryAddress>\n		<PropQty>0.0</PropQty>\n		<PropCoefQtyPrice>1.0</PropCoefQtyPrice>\n		<PropPrice>0.0</PropPrice>\n		<PropDeliveryDate></PropDeliveryDate>\n		<QtyDelivered>0.0</QtyDelivered>\n		<LastDeliveredDate></LastDeliveredDate>\n		<RelatedCompany></RelatedCompany>\n		<RelatedCompanyGroupName></RelatedCompanyGroupName>\n		<RelatedOrderTypeName></RelatedOrderTypeName>\n		<RelatedOrderNumber></RelatedOrderNumber>\n		<RelatedOrderDate></RelatedOrderDate>\n		<RelatedOrderLineNumber></RelatedOrderLineNumber>\n		<Discount1>0.0</Discount1>\n		<Discount2>0.0</Discount2>\n		<Discount3>0.0</Discount3>\n		<Discount4>0.0</Discount4>\n		<Increase1>0.0</Increase1>\n		<Increase2>0.0</Increase2>\n		<AuxRow1>00260 PINK LILLY</AuxRow1>\n		<AuxRow2></AuxRow2>\n		<AuxRow3></AuxRow3>\n		<AuxRow4></AuxRow4>\n		<AuxRow5></AuxRow5>\n		<AuxRow6></AuxRow6>\n		<AuxRow7></AuxRow7>\n		<AuxRow8></AuxRow8>\n		<AuxRow9></AuxRow9>\n		<AuxRow10></AuxRow10>\n		<AuxRowDate1></AuxRowDate1>\n		<AuxRowDate2></AuxRowDate2>\n		<AuxRowDate3></AuxRowDate3>\n		<AuxRowDate4></AuxRowDate4>\n		<AuxRowDate5></AuxRowDate5>\n		<AuxRowNum1>0.0</AuxRowNum1>\n		<AuxRowNum2>0.0</AuxRowNum2>\n		<AuxRowNum3>0.0</AuxRowNum3>\n		<AuxRowNum4>0.0</AuxRowNum4>\n		<AuxRowNum5>0.0</AuxRowNum5>\n		<PropDiscount1>0.0</PropDiscount1>\n		<PropDiscount2>0.0</PropDiscount2>\n		<PropDiscount3>0.0</PropDiscount3>\n		<PropDiscount4>0.0</PropDiscount4>\n		<PropIncrease1>0.0</PropIncrease1>\n		<PropIncrease2>0.0</PropIncrease2>\n		<PropAuxRowDate1></PropAuxRowDate1>\n		<PropAuxRowDate2></PropAuxRowDate2>\n		<PropAuxRowDate3></PropAuxRowDate3>\n		<PropAuxRowDate4></PropAuxRowDate4>\n		<PropAuxRowDate5></PropAuxRowDate5>\n		<PropAuxRowNum1>0.0</PropAuxRowNum1>\n		<PropAuxRowNum2>0.0</PropAuxRowNum2>\n		<PropAuxRowNum3>0.0</PropAuxRowNum3>\n		<PropAuxRowNum4>0.0</PropAuxRowNum4>\n		<PropAuxRowNum5>0.0</PropAuxRowNum5>\n		<Note></Note>\n		<Event>frConferma</Event>\n		<EventNote></EventNote>\n		<SdAttribute name=\"sys_firstSend_date\" type=\"s\">2007-09-28</SdAttribute>\n		<SdAttribute name=\"sys_firstSend_time\" type=\"s\">08:30:00</SdAttribute>\n		<SdOrderLineDocument>\n			<FileName>Fondi.txt</FileName>\n			<Description> </Description>\n			<FileFullPathName>C:\\iungo\\allegati\\basedDocs\\p07</FileFullPathName>\n			<DocumentTypeCode>sys.OrdineRiga</DocumentTypeCode>\n			<ExternalUrl>http://localhost:8080/iungo/servlet/documentationDownload/documentazioneid/7/md/wf4mejfp5hdysvwsyxvw7a%3D%3D</ExternalUrl>\n			<FileChecksum></FileChecksum>\n		</SdOrderLineDocument>\n</SdSingleOrderLine>\n\n\n\n\n\n\n\n\n</SdDataSlice>','r17171.1327586650741.xml','C:/temp/fromIungo','.ack',1,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `export_sd_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `export_sd_object`
--

DROP TABLE IF EXISTS `export_sd_object`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `export_sd_object` (
  `EXPORT_SD_FILE_ID` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  PRIMARY KEY (`EXPORT_SD_FILE_ID`,`OBJECT_ID`,`OBJECT_TYPE`),
  KEY `OBJECT_ID` (`OBJECT_ID`,`OBJECT_TYPE`),
  KEY `OBJECT_TYPE` (`OBJECT_TYPE`),
  KEY `EXPORT_SD_FILE_ID` (`EXPORT_SD_FILE_ID`),
  CONSTRAINT `export_sd_object_ibfk_1` FOREIGN KEY (`EXPORT_SD_FILE_ID`) REFERENCES `export_sd_file` (`EXPORT_SD_FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `export_sd_object`
--

LOCK TABLES `export_sd_object` WRITE;
/*!40000 ALTER TABLE `export_sd_object` DISABLE KEYS */;
INSERT INTO `export_sd_object` VALUES (1,17171,20);
/*!40000 ALTER TABLE `export_sd_object` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fax_log`
--

DROP TABLE IF EXISTS `fax_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fax_log` (
  `FAX_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATA` datetime DEFAULT NULL,
  `PAGINE_INVIATE` int(11) NOT NULL,
  `PAGINE_RESIDUE` int(11) NOT NULL,
  `PAGINE_RICARICATE` int(11) NOT NULL,
  `DESTINATARIO` varchar(50) NOT NULL,
  `EMAIL_FILE` varchar(140) DEFAULT NULL,
  `ORDINE_ID` int(11) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `VISTO_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FAX_LOG_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `VISTO_NOTIFICA_LOG_RICEVUTA_ID` (`VISTO_NOTIFICA_LOG_RICEVUTA_ID`),
  CONSTRAINT `fax_log_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fax_log`
--

LOCK TABLES `fax_log` WRITE;
/*!40000 ALTER TABLE `fax_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `fax_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field`
--

DROP TABLE IF EXISTS `field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field` (
  `FIELD_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CATEGORY` varchar(254) NOT NULL,
  `POSITION` int(11) NOT NULL,
  `TEXT` varchar(500) DEFAULT NULL,
  `DESCRIPTION` varchar(1500) DEFAULT NULL,
  `DOMAIN` varchar(254) NOT NULL,
  `OPTIONAL` int(11) NOT NULL,
  `SCOPE` varchar(254) NOT NULL,
  `REGEX` varchar(254) DEFAULT NULL,
  `TEXT_MODE` varchar(254) DEFAULT NULL,
  `ALTERNATIVE_CHOISE_MODE` varchar(254) DEFAULT NULL,
  `NUMERIC_RANGE_FROM` decimal(19,6) DEFAULT NULL,
  `NUMERIC_RANGE_TO` decimal(19,6) DEFAULT NULL,
  `TRUE_WEIGHT` decimal(19,6) DEFAULT NULL,
  `FALSE_WEIGHT` decimal(19,6) DEFAULT NULL,
  `DATE_RANGE_FROM` datetime DEFAULT NULL,
  `DATE_RANGE_TO` datetime DEFAULT NULL,
  `ADD_TO_VOTE` int(11) DEFAULT NULL,
  `CHECK_FOR_DUE_DATE` int(11) DEFAULT NULL,
  `DUE_NOTIFICATION_THRESHOLD` int(11) DEFAULT NULL,
  `DUE_NOTIFICATION_THRESHOLD_UNIT` varchar(254) DEFAULT NULL,
  `DOCUMENT_TYPE_ID` int(11) NOT NULL,
  `DELETED` int(11) DEFAULT NULL,
  `TABLE_ID` int(11) DEFAULT NULL,
  `TABLE_ROW` int(11) DEFAULT NULL,
  `TABLE_COLUMN` int(11) DEFAULT NULL,
  `NUMBER_OF_ROWS` int(11) DEFAULT NULL,
  `NUMBER_OF_COLUMNS` int(11) DEFAULT NULL,
  `TABLE_CELL_MODIFIED` int(11) DEFAULT NULL,
  `USER_IDENTIFIER` varchar(254) DEFAULT NULL,
  `DERIVED` int(11) DEFAULT '0',
  `TABLE_COLUMN_WIDTH` decimal(19,6) DEFAULT NULL,
  `PERMISSION_READ` varchar(254) DEFAULT '',
  `PERMISSION_WRITE` varchar(254) DEFAULT '',
  PRIMARY KEY (`FIELD_ID`),
  UNIQUE KEY `FIELD_USER_IDENTIFIER_UNIQUE_75432712` (`DOCUMENT_TYPE_ID`,`USER_IDENTIFIER`),
  KEY `DOCUMENT_TYPE_ID` (`DOCUMENT_TYPE_ID`),
  KEY `FIELD_TABLE_FK_1` (`TABLE_ID`),
  CONSTRAINT `FIELD_TABLE_FK_1` FOREIGN KEY (`TABLE_ID`) REFERENCES `field` (`FIELD_ID`),
  CONSTRAINT `field_ibfk_1` FOREIGN KEY (`DOCUMENT_TYPE_ID`) REFERENCES `document_type` (`DOCUMENT_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field`
--

LOCK TABLES `field` WRITE;
/*!40000 ALTER TABLE `field` DISABLE KEYS */;
/*!40000 ALTER TABLE `field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_classemerceologica`
--

DROP TABLE IF EXISTS `field_classemerceologica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_classemerceologica` (
  `FIELD_CLASSEMERCEOLOGICA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIELD_ID` int(11) NOT NULL,
  `FORNITORE_CLASSEMERCEOLOGICA_ID` int(11) NOT NULL,
  PRIMARY KEY (`FIELD_CLASSEMERCEOLOGICA_ID`),
  KEY `FIELD_ID` (`FIELD_ID`),
  KEY `FORNITORE_CLASSEMERCEOLOGICA_ID` (`FORNITORE_CLASSEMERCEOLOGICA_ID`),
  CONSTRAINT `field_classemerceologica_ibfk_1` FOREIGN KEY (`FIELD_ID`) REFERENCES `field` (`FIELD_ID`),
  CONSTRAINT `field_classemerceologica_ibfk_2` FOREIGN KEY (`FORNITORE_CLASSEMERCEOLOGICA_ID`) REFERENCES `fornitore_classemerceologica` (`FORNITORE_CLASSEMERCEOLOGICA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_classemerceologica`
--

LOCK TABLES `field_classemerceologica` WRITE;
/*!40000 ALTER TABLE `field_classemerceologica` DISABLE KEYS */;
/*!40000 ALTER TABLE `field_classemerceologica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_element`
--

DROP TABLE IF EXISTS `field_element`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_element` (
  `FIELD_ELEMENT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REVISION_ID` int(11) DEFAULT NULL,
  `DOCUMENT_ID` int(11) NOT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `CREATION_DATE` datetime DEFAULT NULL,
  `BELONG` int(11) DEFAULT '1',
  PRIMARY KEY (`FIELD_ELEMENT_ID`),
  KEY `REVISION_ID` (`REVISION_ID`),
  KEY `FIELD_ID` (`FIELD_ID`),
  KEY `DOCUMENT_ID` (`DOCUMENT_ID`),
  CONSTRAINT `field_element_ibfk_1` FOREIGN KEY (`REVISION_ID`) REFERENCES `revision` (`REVISION_ID`),
  CONSTRAINT `field_element_ibfk_2` FOREIGN KEY (`FIELD_ID`) REFERENCES `field` (`FIELD_ID`),
  CONSTRAINT `field_element_ibfk_3` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`DOCUMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_element`
--

LOCK TABLES `field_element` WRITE;
/*!40000 ALTER TABLE `field_element` DISABLE KEYS */;
/*!40000 ALTER TABLE `field_element` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_translation`
--

DROP TABLE IF EXISTS `field_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_translation` (
  `FIELD_TRANSLATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIELD_ID` int(11) NOT NULL,
  `LANGUAGE` varchar(254) NOT NULL,
  `TEXT` varchar(500) DEFAULT NULL,
  `DESCRIPTION` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`FIELD_TRANSLATION_ID`),
  KEY `FIELD_ID` (`FIELD_ID`),
  CONSTRAINT `field_translation_ibfk_1` FOREIGN KEY (`FIELD_ID`) REFERENCES `field` (`FIELD_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_translation`
--

LOCK TABLES `field_translation` WRITE;
/*!40000 ALTER TABLE `field_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `field_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `field_value`
--

DROP TABLE IF EXISTS `field_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `field_value` (
  `FIELD_VALUE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CURRENT_VALUE` varchar(1500) DEFAULT NULL,
  `DOCUMENT_ID` int(11) DEFAULT NULL,
  `REVISION_ID` int(11) DEFAULT NULL,
  `FIELD_ID` int(11) NOT NULL,
  `DOCUMENTAZIONE_ID` int(11) DEFAULT NULL,
  `DELETED` int(11) DEFAULT NULL,
  `BELONG` int(11) DEFAULT NULL,
  PRIMARY KEY (`FIELD_VALUE_ID`),
  KEY `FIELD_ID` (`FIELD_ID`),
  KEY `DOCUMENT_ID` (`DOCUMENT_ID`),
  KEY `REVISION_ID` (`REVISION_ID`),
  KEY `DOCUMENTAZIONE_ID` (`DOCUMENTAZIONE_ID`),
  CONSTRAINT `field_value_ibfk_1` FOREIGN KEY (`FIELD_ID`) REFERENCES `field` (`FIELD_ID`),
  CONSTRAINT `field_value_ibfk_2` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`DOCUMENT_ID`),
  CONSTRAINT `field_value_ibfk_3` FOREIGN KEY (`REVISION_ID`) REFERENCES `revision` (`REVISION_ID`),
  CONSTRAINT `field_value_ibfk_4` FOREIGN KEY (`DOCUMENTAZIONE_ID`) REFERENCES `documentazione` (`DOCUMENTAZIONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `field_value`
--

LOCK TABLES `field_value` WRITE;
/*!40000 ALTER TABLE `field_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `field_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `firma`
--

DROP TABLE IF EXISTS `firma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firma` (
  `FIRMA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `EMAIL` varchar(254) DEFAULT NULL,
  `LIVELLO` int(11) NOT NULL,
  `VALORE_MASSIMO` decimal(12,5) NOT NULL,
  `USERNAME` varchar(99) DEFAULT NULL,
  `NOME` varchar(254) DEFAULT NULL,
  `NOTA` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`FIRMA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `firma_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firma`
--

LOCK TABLES `firma` WRITE;
/*!40000 ALTER TABLE `firma` DISABLE KEYS */;
/*!40000 ALTER TABLE `firma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore`
--

DROP TABLE IF EXISTS `fornitore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore` (
  `FORNITORE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `FORNITORE_ID_DITTA` varchar(99) NOT NULL,
  `RAGIONE_SOCIALE` varchar(254) NOT NULL,
  `PARTITA_IVA` varchar(20) DEFAULT '',
  `VIA` varchar(254) DEFAULT '',
  `CAP` varchar(15) DEFAULT NULL,
  `CITTA` varchar(50) DEFAULT '',
  `PROVINCIA` varchar(30) DEFAULT '',
  `PAESE` varchar(30) DEFAULT '',
  `EMAIL` varchar(600) DEFAULT '',
  `PHONE_NUMBER` varchar(99) DEFAULT '',
  `FAX_NUMBER` varchar(99) DEFAULT '',
  `NOTA` mediumtext,
  `PREVISIONE_CONSEGNA_GIORNI` int(11) DEFAULT '-1',
  `MAIL_SEND_HTML_INLINE` int(11) DEFAULT NULL,
  `COMPANY_URI` varchar(254) DEFAULT '',
  `FG_CUSTOMER` int(11) DEFAULT NULL,
  `FG_SUPPLIER` int(11) DEFAULT NULL,
  `XML_SEND_ENABLE` int(11) DEFAULT '0',
  `XML_GET_ENABLE` int(11) DEFAULT '0',
  `FORNITORE_CLASSEMERCEOLOGICA_ID` int(11) DEFAULT NULL,
  `EMAIL_REPORT` varchar(600) DEFAULT '',
  `VALUTATORE` varchar(32) DEFAULT '',
  `EMAIL_RDO` varchar(600) DEFAULT '',
  `EMAIL_VENDITE` varchar(600) DEFAULT '',
  `EMAIL_PDC` varchar(600) DEFAULT '',
  `DA_RITIRARE` int(11) DEFAULT NULL,
  `CORRIERE` varchar(99) DEFAULT NULL,
  `IMBALLO` varchar(99) DEFAULT NULL,
  `IMBALLO_CODICE` varchar(2) DEFAULT NULL,
  `NOTA_RESO` varchar(99) DEFAULT NULL,
  `MODALITA_PAGAMENTO` varchar(50) DEFAULT NULL,
  `MODALITA_PAGAMENTO_CODICE` varchar(2) DEFAULT NULL,
  `COORDINATE_BANCARIE` varchar(99) DEFAULT NULL,
  `PREVISIONE_CONSEGNA_INTERVALLO_SPEDIZIONE` int(11) DEFAULT '-1',
  `PREVISIONE_CONSEGNA_GIORNI_ANTICIPO` int(11) DEFAULT '-1',
  `FORNITORE_CATEGORIA_ID` int(11) DEFAULT NULL,
  `FORNITORE_CATEGORIA_DESCRIZIONE` varchar(99) DEFAULT NULL,
  `PARTNER_ALTERNATE_KEY` varchar(255) DEFAULT '',
  `PORTALE_ESTERNO_NOME` varchar(30) DEFAULT '',
  `PORTALE_ESTERNO_INDIRIZZO` varchar(200) DEFAULT '',
  `PORTALE_ESTERNO_USERNAME` varchar(30) DEFAULT '',
  `PORTALE_ESTERNO_PASSWORD` varchar(30) DEFAULT '',
  `PORTALE_ESTERNO_URL_IMMAGINE` varchar(200) DEFAULT '',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  `RANKING_RFI` decimal(12,5) DEFAULT '0.00000',
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `AUX11` varchar(99) DEFAULT '',
  `AUX12` varchar(99) DEFAULT '',
  `AUX13` varchar(99) DEFAULT '',
  `AUX14` varchar(99) DEFAULT '',
  `AUX15` varchar(99) DEFAULT '',
  `AUX16` varchar(99) DEFAULT '',
  `AUX17` varchar(99) DEFAULT '',
  `AUX18` varchar(99) DEFAULT '',
  `AUX19` varchar(99) DEFAULT '',
  `AUX20` varchar(99) DEFAULT '',
  `AUX_DATE1` datetime DEFAULT NULL,
  `AUX_DATE2` datetime DEFAULT NULL,
  `AUX_DATE3` datetime DEFAULT NULL,
  `AUX_DATE4` datetime DEFAULT NULL,
  `AUX_DATE5` datetime DEFAULT NULL,
  `AUX_DATE6` datetime DEFAULT NULL,
  `AUX_DATE7` datetime DEFAULT NULL,
  `AUX_DATE8` datetime DEFAULT NULL,
  `AUX_DATE9` datetime DEFAULT NULL,
  `AUX_DATE10` datetime DEFAULT NULL,
  `AUX_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM5` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM6` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM7` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM8` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM9` decimal(12,5) DEFAULT '0.00000',
  `AUX_NUM10` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`FORNITORE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_CLASSEMERCEOLOGICA_ID` (`FORNITORE_CLASSEMERCEOLOGICA_ID`),
  KEY `FORNITORE_FORNITORE_ID_DITTA` (`FORNITORE_ID_DITTA`),
  KEY `FORNITORE_IBFK_3` (`FORNITORE_CATEGORIA_ID`),
  KEY `FORNITORE_PARTNER_ALTERNATE_KEY` (`PARTNER_ALTERNATE_KEY`),
  CONSTRAINT `FORNITORE_IBFK_3` FOREIGN KEY (`FORNITORE_CATEGORIA_ID`) REFERENCES `fornitore_categoria` (`FORNITORE_CATEGORIA_ID`),
  CONSTRAINT `fornitore_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=297 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore`
--

LOCK TABLES `fornitore` WRITE;
/*!40000 ALTER TABLE `fornitore` DISABLE KEYS */;
INSERT INTO `fornitore` VALUES (287,5,'SupplierA','SupplierA S.r.l','0123456789','Emilia, 29','41100','Modena','MO','Italy','fornitore@localmail','+39 059 111111','+39 059 999999','',360,0,'URISupplierA',0,1,0,0,NULL,'','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,-1,1,NULL,'','','','','','',NULL,0.00000,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000),(288,5,'SupplierC','SupplierC S.r.l','0123456789','Emilia, 18','41100','Modena','MO','I','fornitore@localmail','+39 059 111111','+39 059 111111','',360,0,'URISupplierC',0,1,0,0,NULL,'','',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,-1,1,NULL,'','','','','','',NULL,0.00000,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000),(291,5,'SupplierB','SupplierB S.n.c','00123589741','Via Giardini 150','41100','Modena','MO','Italy','fornitore@localmail','059-123456','059-987654','',360,0,'Uriforn',0,1,0,0,NULL,'Email per Report','','','','',0,'','',NULL,'','',NULL,'',-1,-1,1,NULL,'','','','turbine',NULL,'',NULL,0.00000,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000),(293,5,'00000786421786','SupplierE S.p.a.','098765432111','VIA MODENA 4','10064','MODENA','MO','IT','fornitore@localmail.it','045 1234567','045 1234567',NULL,-1,0,'',0,0,0,0,NULL,'','','','','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,-1,1,NULL,NULL,'','','','','','2015-12-14 10:05:26',0.00000,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000),(294,5,'35235325111','Ditta Cliente2 s.p.a.','','Via Muovetevi,3','12345','Villanova','MO','IT','','','','$ot.Nota',-1,0,'',0,0,0,0,NULL,'','','','','',0,'','',NULL,'','',NULL,'',-1,-1,4,NULL,NULL,'','','turbine',NULL,'','2015-12-14 10:05:26',0.00000,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000),(295,5,'00000123456','SupplierD S.p.a.','098765432111','VIA MODENA 4','10064','MODENA','MO','IT','fornitore@localmail.it','045 1234567','045 1234567',NULL,-1,0,'',0,0,0,0,NULL,'','','','','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,-1,1,NULL,NULL,'','','','','','2016-05-10 12:05:13',0.00000,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000),(296,5,'0987633333','Ditta Cliente s.p.a.','','Via Muovetevi,3','12345','Villanova','MO','IT','','','',NULL,-1,0,'',0,0,0,0,NULL,'','','','','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,-1,1,NULL,NULL,'','','','','','2016-05-10 12:05:13',0.00000,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000);
/*!40000 ALTER TABLE `fornitore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_2_gruppo`
--

DROP TABLE IF EXISTS `fornitore_2_gruppo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_2_gruppo` (
  `FORNITORE_ID` int(11) NOT NULL,
  `GRUPPO_ID` int(11) NOT NULL,
  PRIMARY KEY (`FORNITORE_ID`,`GRUPPO_ID`),
  KEY `GRUPPO_ID` (`GRUPPO_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `fornitore_2_gruppo_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `fornitore_2_gruppo_ibfk_2` FOREIGN KEY (`GRUPPO_ID`) REFERENCES `gruppo` (`GRUPPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_2_gruppo`
--

LOCK TABLES `fornitore_2_gruppo` WRITE;
/*!40000 ALTER TABLE `fornitore_2_gruppo` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornitore_2_gruppo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_categoria`
--

DROP TABLE IF EXISTS `fornitore_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_categoria` (
  `FORNITORE_CATEGORIA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `NOME` varchar(30) NOT NULL,
  `DESCRIZIONE` varchar(99) DEFAULT NULL,
  PRIMARY KEY (`FORNITORE_CATEGORIA_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`NOME`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `fornitore_categoria_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_categoria`
--

LOCK TABLES `fornitore_categoria` WRITE;
/*!40000 ALTER TABLE `fornitore_categoria` DISABLE KEYS */;
INSERT INTO `fornitore_categoria` VALUES (1,5,'Supplier',NULL),(2,5,'Customer',NULL),(3,5,'Carrier',NULL),(4,5,'Address',NULL),(5,5,'Sub-Partner',NULL),(6,6,'Supplier',NULL),(7,6,'Customer',NULL),(8,6,'Carrier',NULL),(9,6,'Address',NULL),(10,6,'Sub-Partner',NULL);
/*!40000 ALTER TABLE `fornitore_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_categoria_modulo`
--

DROP TABLE IF EXISTS `fornitore_categoria_modulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_categoria_modulo` (
  `FORNITORE_CATEGORIA_MODULO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_CATEGORIA_ID` int(11) NOT NULL,
  `ORDINE_TIPO_CATEGORIA_ID` int(11) NOT NULL,
  `FG_ENABLED` int(11) DEFAULT '0',
  PRIMARY KEY (`FORNITORE_CATEGORIA_MODULO_ID`),
  KEY `FORNITORE_CATEGORIA_ID` (`FORNITORE_CATEGORIA_ID`),
  KEY `ORDINE_TIPO_CATEGORIA_ID` (`ORDINE_TIPO_CATEGORIA_ID`),
  CONSTRAINT `fornitore_categoria_modulo_ibfk_1` FOREIGN KEY (`FORNITORE_CATEGORIA_ID`) REFERENCES `fornitore_categoria` (`FORNITORE_CATEGORIA_ID`),
  CONSTRAINT `fornitore_categoria_modulo_ibfk_2` FOREIGN KEY (`ORDINE_TIPO_CATEGORIA_ID`) REFERENCES `ordine_tipo_categoria` (`ORDINE_TIPO_CATEGORIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_categoria_modulo`
--

LOCK TABLES `fornitore_categoria_modulo` WRITE;
/*!40000 ALTER TABLE `fornitore_categoria_modulo` DISABLE KEYS */;
INSERT INTO `fornitore_categoria_modulo` VALUES (1,1,2,0),(2,1,1,1),(3,1,3,1),(4,1,4,1),(5,1,5,1),(6,1,6,1),(7,1,7,1),(8,1,8,1),(9,1,9,1),(10,1,10,1),(11,1,11,1),(12,1,12,0),(13,1,13,1),(14,1,14,1),(15,1,15,1),(16,1,16,1),(17,1,17,1),(18,1,18,1),(19,1,19,1),(20,1,20,0),(21,1,21,0),(22,1,22,0),(23,2,2,1),(24,2,1,0),(25,2,3,0),(26,2,4,0),(27,2,5,0),(28,2,6,1),(29,2,7,0),(30,2,8,0),(31,2,9,0),(32,2,10,0),(33,2,11,0),(34,2,12,1),(35,2,13,0),(36,2,14,0),(37,2,15,0),(38,2,16,0),(39,2,17,0),(40,2,18,0),(41,2,19,0),(42,2,20,0),(43,2,21,0),(44,2,22,0),(45,3,2,1),(46,3,1,1),(47,3,3,1),(48,3,4,1),(49,3,5,1),(50,3,6,1),(51,3,7,1),(52,3,8,1),(53,3,9,1),(54,3,10,1),(55,3,11,1),(56,3,12,1),(57,3,13,1),(58,3,14,1),(59,3,15,1),(60,3,16,1),(61,3,17,1),(62,3,18,1),(63,3,19,1),(64,3,20,1),(65,3,21,1),(66,3,22,1),(67,4,2,0),(68,4,1,0),(69,4,3,0),(70,4,4,0),(71,4,5,0),(72,4,6,0),(73,4,7,0),(74,4,8,0),(75,4,9,0),(76,4,10,0),(77,4,11,0),(78,4,12,0),(79,4,13,0),(80,4,14,0),(81,4,15,0),(82,4,16,0),(83,4,17,0),(84,4,18,0),(85,4,19,0),(86,4,20,0),(87,4,21,0),(88,4,22,0),(89,5,2,0),(90,5,1,0),(91,5,3,0),(92,5,4,0),(93,5,5,0),(94,5,6,0),(95,5,7,0),(96,5,8,0),(97,5,9,0),(98,5,10,0),(99,5,11,0),(100,5,12,0),(101,5,13,0),(102,5,14,0),(103,5,15,0),(104,5,16,0),(105,5,17,0),(106,5,18,0),(107,5,19,0),(108,5,20,1),(109,5,21,0),(110,5,22,0),(111,9,2,0),(112,9,1,0),(113,9,3,0),(114,9,4,0),(115,9,5,0),(116,9,6,0),(117,9,7,0),(118,9,8,0),(119,9,9,0),(120,9,10,0),(121,9,11,0),(122,9,12,0),(123,9,13,0),(124,9,14,0),(125,9,15,0),(126,9,16,0),(127,9,17,0),(128,9,18,0),(129,9,19,0),(130,9,20,0),(131,9,21,0),(132,9,22,0),(133,8,2,1),(134,8,1,1),(135,8,3,1),(136,8,4,1),(137,8,5,1),(138,8,6,1),(139,8,7,1),(140,8,8,1),(141,8,9,1),(142,8,10,1),(143,8,11,1),(144,8,12,1),(145,8,13,1),(146,8,14,1),(147,8,15,1),(148,8,16,1),(149,8,17,1),(150,8,18,1),(151,8,19,1),(152,8,20,1),(153,8,21,1),(154,8,22,1),(155,7,2,1),(156,7,1,0),(157,7,3,0),(158,7,4,0),(159,7,5,0),(160,7,6,1),(161,7,7,0),(162,7,8,0),(163,7,9,0),(164,7,10,0),(165,7,11,0),(166,7,12,1),(167,7,13,0),(168,7,14,0),(169,7,15,0),(170,7,16,0),(171,7,17,0),(172,7,18,0),(173,7,19,0),(174,7,20,0),(175,7,21,0),(176,7,22,0),(177,10,2,0),(178,10,1,0),(179,10,3,0),(180,10,4,0),(181,10,5,0),(182,10,6,0),(183,10,7,0),(184,10,8,0),(185,10,9,0),(186,10,10,0),(187,10,11,0),(188,10,12,0),(189,10,13,0),(190,10,14,0),(191,10,15,0),(192,10,16,0),(193,10,17,0),(194,10,18,0),(195,10,19,0),(196,10,20,1),(197,10,21,0),(198,10,22,0),(199,6,2,0),(200,6,1,1),(201,6,3,1),(202,6,4,1),(203,6,5,1),(204,6,6,1),(205,6,7,1),(206,6,8,1),(207,6,9,1),(208,6,10,1),(209,6,11,1),(210,6,12,0),(211,6,13,1),(212,6,14,1),(213,6,15,1),(214,6,16,1),(215,6,17,1),(216,6,18,1),(217,6,19,1),(218,6,20,0),(219,6,21,0),(220,6,22,0);
/*!40000 ALTER TABLE `fornitore_categoria_modulo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_classemerceologica`
--

DROP TABLE IF EXISTS `fornitore_classemerceologica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_classemerceologica` (
  `FORNITORE_CLASSEMERCEOLOGICA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIZIONE` varchar(254) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `EMAIL_REFERENTE` varchar(600) DEFAULT '',
  `CODICE` varchar(254) NOT NULL,
  PRIMARY KEY (`FORNITORE_CLASSEMERCEOLOGICA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `fornitore_classemerceologica_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_classemerceologica`
--

LOCK TABLES `fornitore_classemerceologica` WRITE;
/*!40000 ALTER TABLE `fornitore_classemerceologica` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornitore_classemerceologica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_classemerito`
--

DROP TABLE IF EXISTS `fornitore_classemerito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_classemerito` (
  `FORNITORE_CLASSEMERITO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(64) NOT NULL,
  `DESCRIZIONE` varchar(254) NOT NULL,
  `MIN` decimal(12,5) NOT NULL DEFAULT '0.00000',
  `DITTA_ID` int(11) NOT NULL,
  PRIMARY KEY (`FORNITORE_CLASSEMERITO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `fornitore_classemerito_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_classemerito`
--

LOCK TABLES `fornitore_classemerito` WRITE;
/*!40000 ALTER TABLE `fornitore_classemerito` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornitore_classemerito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_configurazione`
--

DROP TABLE IF EXISTS `fornitore_configurazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_configurazione` (
  `FORNITORE_CONFIGURAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_ID` int(11) NOT NULL,
  `NOME` varchar(254) NOT NULL,
  `VALORE` mediumtext,
  PRIMARY KEY (`FORNITORE_CONFIGURAZIONE_ID`),
  UNIQUE KEY `FORNITORE_ID_2` (`FORNITORE_ID`,`NOME`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `fornitore_configurazione_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_configurazione`
--

LOCK TABLES `fornitore_configurazione` WRITE;
/*!40000 ALTER TABLE `fornitore_configurazione` DISABLE KEYS */;
INSERT INTO `fornitore_configurazione` VALUES (1,287,'lingua','IT'),(2,291,'lingua','EN'),(3,288,'lingua','EN'),(4,287,'sys_mail_htmlFileName','true'),(5,288,'sys_mail_htmlFileName','true'),(6,291,'sys_mail_htmlFileName','true'),(9,291,'sys_tipo_invio','MAIL POST'),(10,287,'sys_tipo_invio','MAIL POST'),(11,288,'sys_tipo_invio','MAIL'),(12,291,'sys_mail_post','true'),(13,287,'sys_mail_post','true'),(14,288,'sys_mail_post','false'),(16,291,'sys_abilSpedizione.','true'),(17,287,'sys_abilSpedizione.','true'),(18,288,'sys_abilSpedizione.','true'),(22,291,'sys_transportLeadTime','-1'),(23,287,'sys_transportLeadTime','-1'),(24,288,'sys_transportLeadTime','-1'),(25,291,'sys_avvisoRicezioneEnabled ','false'),(26,291,'sys_fatturaPrevistaEnabled ','false'),(27,291,'sys_abilSpedizione.rdo.','true'),(28,291,'sys_abilSpedizione.ContoLavoro.','true'),(29,291,'sys_abilSpedizione.vendite.','true'),(30,291,'sys_lingua','EN'),(31,291,'sys_etichettePdf','false'),(32,291,'sys_prebolle','false'),(33,291,'fileFromPartner_charsetName','8859_1'),(34,291,'fileToPartner_charsetName','UTF-8'),(35,291,'sys_thirdPartyDeliveryManagement','false'),(36,291,'sys_mail_sendOnlyNew','false'),(37,291,'sys_firstSend','2015-11-10 17:11:43'),(38,293,'sys_lingua','IT'),(39,293,'sys_mail_post','true'),(40,293,'sys_tipo_invio','MAIL POST'),(41,294,'sys_lingua','IT'),(42,294,'sys_mail_post','true'),(43,294,'sys_tipo_invio','MAIL POST'),(44,293,'sys_transportLeadTime','-1'),(45,293,'sys_abilSpedizione.','true'),(46,293,'sys_mail_htmlFileName','true'),(47,293,'sys_firstSend','2015-12-14 10:05:42'),(48,294,'sys_transportLeadTime','-1'),(49,294,'sys_abilSpedizione.','true'),(50,294,'sys_avvisoRicezioneEnabled ','false'),(51,294,'sys_fatturaPrevistaEnabled ','false'),(52,294,'sys_abilSpedizione.rdo.','true'),(53,294,'sys_abilSpedizione.ContoLavoro.','true'),(54,294,'sys_abilSpedizione.vendite.','true'),(55,294,'sys_etichettePdf','false'),(56,294,'sys_prebolle','false'),(57,294,'fileFromPartner_charsetName','8859_1'),(58,294,'fileToPartner_charsetName','UTF-8'),(59,294,'sys_thirdPartyDeliveryManagement','false'),(60,294,'sys_mail_htmlFileName','false'),(61,294,'sys_mail_sendOnlyNew','false'),(62,295,'sys_lingua','IT'),(63,295,'sys_mail_post','true'),(64,295,'sys_tipo_invio','MAIL POST'),(65,296,'sys_lingua','IT'),(66,296,'sys_mail_post','true'),(67,296,'sys_tipo_invio','MAIL POST'),(68,295,'sys_transportLeadTime','-1'),(69,295,'sys_abilSpedizione.','true'),(70,295,'sys_mail_htmlFileName','true'),(71,295,'sys_firstSend','2016-05-10 12:05:37');
/*!40000 ALTER TABLE `fornitore_configurazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_pagamento`
--

DROP TABLE IF EXISTS `fornitore_pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_pagamento` (
  `FORNITORE_PAGAMENTO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DESCRIZIONE` varchar(254) NOT NULL,
  `GIORNI` int(11) NOT NULL,
  `VALORE` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  PRIMARY KEY (`FORNITORE_PAGAMENTO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `fornitore_pagamento_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_pagamento`
--

LOCK TABLES `fornitore_pagamento` WRITE;
/*!40000 ALTER TABLE `fornitore_pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornitore_pagamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_peso`
--

DROP TABLE IF EXISTS `fornitore_peso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_peso` (
  `FORNITORE_PESO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(300) NOT NULL,
  `PESO` int(11) NOT NULL,
  `FORNITORE_CLASSEMERCEOLOGICA_ID` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `CON_DATI` int(11) NOT NULL,
  `MODALITA_PAGAMENTO` int(11) NOT NULL,
  `NON_CONFORMITA` int(11) NOT NULL,
  `RITARDO_CONSEGNA` int(11) NOT NULL,
  `VALORI_CONSENTITI` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`FORNITORE_PESO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_CLASSEMERCEOLOGICA_ID` (`FORNITORE_CLASSEMERCEOLOGICA_ID`),
  CONSTRAINT `fornitore_peso_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `fornitore_peso_ibfk_2` FOREIGN KEY (`FORNITORE_CLASSEMERCEOLOGICA_ID`) REFERENCES `fornitore_classemerceologica` (`FORNITORE_CLASSEMERCEOLOGICA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_peso`
--

LOCK TABLES `fornitore_peso` WRITE;
/*!40000 ALTER TABLE `fornitore_peso` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornitore_peso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_relation_classemerceologica`
--

DROP TABLE IF EXISTS `fornitore_relation_classemerceologica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_relation_classemerceologica` (
  `FORNITORE_RELATION_CLASSEMERCEOLOGICA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_CLASSEMERCEOLOGICA_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  PRIMARY KEY (`FORNITORE_RELATION_CLASSEMERCEOLOGICA_ID`),
  UNIQUE KEY `FORNITORE_CLASSEMERCEOLOGICA_I_2` (`FORNITORE_CLASSEMERCEOLOGICA_ID`,`FORNITORE_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `FORNITORE_CLASSEMERCEOLOGICA_ID` (`FORNITORE_CLASSEMERCEOLOGICA_ID`),
  CONSTRAINT `fornitore_relation_classemerceologica_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `fornitore_relation_classemerceologica_ibfk_2` FOREIGN KEY (`FORNITORE_CLASSEMERCEOLOGICA_ID`) REFERENCES `fornitore_classemerceologica` (`FORNITORE_CLASSEMERCEOLOGICA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_relation_classemerceologica`
--

LOCK TABLES `fornitore_relation_classemerceologica` WRITE;
/*!40000 ALTER TABLE `fornitore_relation_classemerceologica` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornitore_relation_classemerceologica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fornitore_ritardoconsegna`
--

DROP TABLE IF EXISTS `fornitore_ritardoconsegna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fornitore_ritardoconsegna` (
  `FORNITORE_RITARDOCONSEGNA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `GIORNO_MIN` int(11) NOT NULL,
  `GIORNO_MAX` int(11) NOT NULL,
  `VALORE` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  PRIMARY KEY (`FORNITORE_RITARDOCONSEGNA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `fornitore_ritardoconsegna_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fornitore_ritardoconsegna`
--

LOCK TABLES `fornitore_ritardoconsegna` WRITE;
/*!40000 ALTER TABLE `fornitore_ritardoconsegna` DISABLE KEYS */;
/*!40000 ALTER TABLE `fornitore_ritardoconsegna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gruppo`
--

DROP TABLE IF EXISTS `gruppo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gruppo` (
  `GRUPPO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(254) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `PERSONA_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`GRUPPO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `NOME` (`NOME`),
  KEY `gruppo_ibfk_2` (`PERSONA_ID`),
  CONSTRAINT `gruppo_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `gruppo_ibfk_2` FOREIGN KEY (`PERSONA_ID`) REFERENCES `persona` (`PERSONA_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gruppo`
--

LOCK TABLES `gruppo` WRITE;
/*!40000 ALTER TABLE `gruppo` DISABLE KEYS */;
/*!40000 ALTER TABLE `gruppo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `id_table`
--

DROP TABLE IF EXISTS `id_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `id_table` (
  `ID_TABLE_ID` int(11) NOT NULL,
  `TABLE_NAME` varchar(255) NOT NULL,
  `NEXT_ID` int(11) DEFAULT NULL,
  `QUANTITY` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_TABLE_ID`),
  UNIQUE KEY `TABLE_NAME` (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `id_table`
--

LOCK TABLES `id_table` WRITE;
/*!40000 ALTER TABLE `id_table` DISABLE KEYS */;
INSERT INTO `id_table` VALUES (10,'TURBINE_PERMISSION',252230,1354),(20,'TURBINE_ROLE',560,10),(30,'TURBINE_GROUP',540,10),(40,'TURBINE_ROLE_PERMISSION',100,10),(50,'TURBINE_USER',741,10),(60,'TURBINE_USER_GROUP_ROLE',100,10),(70,'TURBINE_SCHEDULED_JOB',100,10),(110,'PERSONA',100,10),(120,'DITTA',100,10),(130,'PERSONA_DITTA',100,10),(140,'ORDINE_WF',100,10),(150,'ORDINE_WF_STATO',100,10),(160,'ORDINE_TIPO',100,10),(170,'ORDINE',100,10),(180,'ORDINE_RIGA',100,10),(190,'ORDINE_RIGA_LOG',100,10),(200,'ORDINE_WF_EVENTO',100,10),(300,'PRODUCT',100,10),(310,'PRODUCT_FORMULA',100,10),(320,'PRODUCT_BATCH',100,10),(330,'PRODUCT_BATCH_FORMULA',100,10),(340,'PRODUCT_BATCH_MOVEMENT',100,10),(350,'PRODUCT_TYPE',100,10),(360,'PRODUCT_FORMULA_ITEM',100,10);
/*!40000 ALTER TABLE `id_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice` (
  `INVOICE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_ID` int(11) NOT NULL,
  `FG_OUTBOUND` int(11) DEFAULT '0',
  `EMISSION_DATE` datetime DEFAULT NULL,
  `INVOICE_NUMBER` varchar(50) DEFAULT '',
  `DESTINATION_ADDRESS` varchar(254) DEFAULT NULL,
  `FORNITORE_EMAIL` varchar(600) DEFAULT NULL,
  `REGISTRATION_DATE` datetime DEFAULT NULL,
  `NOTA` mediumtext,
  PRIMARY KEY (`INVOICE_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `INVOICE_NUMBER` (`INVOICE_NUMBER`),
  CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
INSERT INTO `invoice` VALUES (1,287,0,'2006-03-16 00:00:00','0001','Emilia, 29 41100 Modena MO I','fornitore@localmail','2006-03-16 00:00:00',NULL),(2,288,0,'2007-04-02 00:00:00','Autofattura Marzo 2007','','m.speranza@impresaestesa.it','2007-04-02 00:00:00',NULL);
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `invoice_row`
--

DROP TABLE IF EXISTS `invoice_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `invoice_row` (
  `INVOICE_ROW_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INVOICE_ID` int(11) NOT NULL,
  `NUMBER` varchar(30) DEFAULT '',
  `PRODUCT_CODE` varchar(100) DEFAULT '',
  `PRODUCT_DESCRIPTION` varchar(254) DEFAULT '',
  `PRODUCT_CODE_PARTNER` varchar(100) DEFAULT '',
  `PRICE_UNIT_OF_MEASURE` varchar(100) DEFAULT '',
  `QUANTITY_UNIT_OF_MEASURE` varchar(100) DEFAULT '',
  `QUANTITY` decimal(12,5) DEFAULT '0.00000',
  `PRICE` decimal(12,5) DEFAULT '0.00000',
  `AMOUNT` decimal(12,5) DEFAULT '0.00000',
  `STATO` varchar(30) NOT NULL,
  `ORDINE_DATA_EMISSIONE` datetime DEFAULT NULL,
  `ORDINE_NUMERO` varchar(50) DEFAULT '',
  `ORDINE_RIGA_NUMERO` varchar(50) DEFAULT '',
  `NOTA` mediumtext,
  `ORIGIN_COUNTRY_CODE` varchar(25) DEFAULT '',
  PRIMARY KEY (`INVOICE_ROW_ID`),
  KEY `INVOICE_ID` (`INVOICE_ID`),
  KEY `INVOICE_RIGA_PRODOTTO_CODICE` (`PRODUCT_CODE`),
  CONSTRAINT `invoice_row_ibfk_1` FOREIGN KEY (`INVOICE_ID`) REFERENCES `invoice` (`INVOICE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `invoice_row`
--

LOCK TABLES `invoice_row` WRITE;
/*!40000 ALTER TABLE `invoice_row` DISABLE KEYS */;
INSERT INTO `invoice_row` VALUES (1,1,'','ItemA','','','','',2000.00000,1.01000,0.00000,'',NULL,'','',NULL,''),(2,2,'','1AT0021A00','','','','PA',1000.00000,0.70500,0.00000,'',NULL,'','',NULL,''),(3,2,'','1AT0023A00','','','','PA',250.00000,1.90000,0.00000,'',NULL,'','',NULL,'');
/*!40000 ALTER TABLE `invoice_row` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_partner`
--

DROP TABLE IF EXISTS `item_partner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_partner` (
  `ITEM_PARTNER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `ITEM_ID` int(11) NOT NULL,
  `PARTNER_ITEM_CODE` varchar(254) DEFAULT '',
  `PARTNER_ITEM_DESCRIPTION` varchar(254) DEFAULT '',
  `CREATION_DATE` datetime DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `NOTE` mediumtext,
  PRIMARY KEY (`ITEM_PARTNER_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`FORNITORE_ID`,`ITEM_ID`,`PARTNER_ITEM_CODE`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `ITEM_ID` (`ITEM_ID`),
  CONSTRAINT `item_partner_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `item_partner_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `item_partner_ibfk_3` FOREIGN KEY (`ITEM_ID`) REFERENCES `product` (`PRODUCT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_partner`
--

LOCK TABLES `item_partner` WRITE;
/*!40000 ALTER TABLE `item_partner` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_partner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `labels_pdf`
--

DROP TABLE IF EXISTS `labels_pdf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `labels_pdf` (
  `LABELS_PDF_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_SUPPORT_ID` varchar(99) NOT NULL,
  `FORNITORE_RAGIONE_SOCIALE` varchar(255) NOT NULL,
  `PROGRESSIVO_ETICHETTA` varchar(99) NOT NULL,
  `ORDINE_NUMERO` varchar(50) DEFAULT NULL,
  `ORDINE_TIPO` varchar(99) DEFAULT NULL,
  `ORDINE_DATA_EMISSIONE` datetime DEFAULT NULL,
  `ORDINE_RIGA_NUMERO` varchar(30) DEFAULT NULL,
  `FORNITORE_ID_DITTA` varchar(99) DEFAULT NULL,
  `DESTINATARIO_RAGIONE_SOCIALE` varchar(255) DEFAULT NULL,
  `DELIVERY_ADDRESS` varchar(255) DEFAULT NULL,
  `QUANTITY` decimal(12,5) DEFAULT '0.00000',
  `DATE` datetime DEFAULT NULL,
  `PRODUCT_CODE` varchar(100) DEFAULT NULL,
  `PRODUCT_DESCRIPTION` varchar(255) DEFAULT NULL,
  `AUX_LABEL_1` varchar(99) DEFAULT NULL,
  `AUX_LABEL_2` varchar(99) DEFAULT NULL,
  `AUX_LABEL_3` varchar(99) DEFAULT NULL,
  `AUX_LABEL_4` varchar(99) DEFAULT NULL,
  `AUX_LABEL_5` varchar(99) DEFAULT NULL,
  `AUX_LABEL_6` varchar(99) DEFAULT NULL,
  `AUX_LABEL_7` varchar(99) DEFAULT NULL,
  `AUX_LABEL_8` varchar(99) DEFAULT NULL,
  `AUX_LABEL_9` varchar(99) DEFAULT NULL,
  `AUX_LABEL_10` varchar(99) DEFAULT NULL,
  `STAMP_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`LABELS_PDF_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `labels_pdf`
--

LOCK TABLES `labels_pdf` WRITE;
/*!40000 ALTER TABLE `labels_pdf` DISABLE KEYS */;
/*!40000 ALTER TABLE `labels_pdf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `layout_destinatari`
--

DROP TABLE IF EXISTS `layout_destinatari`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `layout_destinatari` (
  `LAYOUT_DESTINATARI_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `NOME` varchar(50) NOT NULL,
  `DESTINATARI` mediumtext,
  `TO_REFERENTE` int(11) DEFAULT NULL,
  `TO_FORNITORE` int(11) DEFAULT NULL,
  `TO_OTHERS` int(11) DEFAULT NULL,
  `LAYOUT` varchar(50) NOT NULL,
  `OGGETTO_EMAIL` varchar(50) DEFAULT '',
  PRIMARY KEY (`LAYOUT_DESTINATARI_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  CONSTRAINT `layout_destinatari_ibfk_1` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `layout_destinatari`
--

LOCK TABLES `layout_destinatari` WRITE;
/*!40000 ALTER TABLE `layout_destinatari` DISABLE KEYS */;
/*!40000 ALTER TABLE `layout_destinatari` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `livello_collaudo`
--

DROP TABLE IF EXISTS `livello_collaudo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livello_collaudo` (
  `LIVELLO_COLLAUDO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(64) NOT NULL DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `PREDEFINITO` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`LIVELLO_COLLAUDO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `livello_collaudo`
--

LOCK TABLES `livello_collaudo` WRITE;
/*!40000 ALTER TABLE `livello_collaudo` DISABLE KEYS */;
INSERT INTO `livello_collaudo` VALUES (1,'S-1','',0),(2,'S-2','',0),(3,'S-3','',1),(4,'S-4','',0),(5,'I','',0),(6,'II','',0),(7,'III','',0);
/*!40000 ALTER TABLE `livello_collaudo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `livello_collaudo_cella`
--

DROP TABLE IF EXISTS `livello_collaudo_cella`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livello_collaudo_cella` (
  `LIVELLO_COLLAUDO_CELLA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LIVELLO_COLLAUDO_ID` int(11) NOT NULL,
  `NUMEROSITA_MIN` int(11) NOT NULL,
  `NUMEROSITA_MAX` int(11) NOT NULL,
  `NUMEROSITA_CAMPIONE_ID` int(11) NOT NULL,
  PRIMARY KEY (`LIVELLO_COLLAUDO_CELLA_ID`),
  UNIQUE KEY `LIVELLO_COLLAUDO_ID_2` (`LIVELLO_COLLAUDO_ID`,`NUMEROSITA_MIN`,`NUMEROSITA_MAX`),
  KEY `LIVELLO_COLLAUDO_ID` (`LIVELLO_COLLAUDO_ID`),
  KEY `NUMEROSITA_CAMPIONE_ID` (`NUMEROSITA_CAMPIONE_ID`),
  CONSTRAINT `livello_collaudo_cella_ibfk_1` FOREIGN KEY (`LIVELLO_COLLAUDO_ID`) REFERENCES `livello_collaudo` (`LIVELLO_COLLAUDO_ID`),
  CONSTRAINT `livello_collaudo_cella_ibfk_2` FOREIGN KEY (`NUMEROSITA_CAMPIONE_ID`) REFERENCES `numerosita_campione` (`NUMEROSITA_CAMPIONE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=113 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `livello_collaudo_cella`
--

LOCK TABLES `livello_collaudo_cella` WRITE;
/*!40000 ALTER TABLE `livello_collaudo_cella` DISABLE KEYS */;
INSERT INTO `livello_collaudo_cella` VALUES (1,1,2,8,1),(2,2,2,8,1),(3,3,2,8,1),(4,4,2,8,1),(5,5,2,8,1),(6,6,2,8,1),(7,7,2,8,2),(8,1,9,15,1),(9,2,9,15,1),(10,3,9,15,1),(11,4,9,15,1),(12,5,9,15,1),(13,6,9,15,2),(14,7,9,15,3),(15,1,16,25,1),(16,2,16,25,1),(17,3,16,25,2),(18,4,16,25,2),(19,5,16,25,2),(20,6,16,25,3),(21,7,16,25,4),(22,1,26,50,1),(23,2,26,50,2),(24,3,26,50,2),(25,4,26,50,3),(26,5,26,50,3),(27,6,26,50,4),(28,7,26,50,5),(29,1,51,90,2),(30,2,51,90,2),(31,3,51,90,3),(32,4,51,90,3),(33,5,51,90,3),(34,6,51,90,5),(35,7,51,90,6),(36,1,91,150,2),(37,2,91,150,2),(38,3,91,150,3),(39,4,91,150,4),(40,5,91,150,4),(41,6,91,150,6),(42,7,91,150,7),(43,1,151,280,2),(44,2,151,280,3),(45,3,151,280,4),(46,4,151,280,5),(47,5,151,280,5),(48,6,151,280,7),(49,7,151,280,8),(50,1,281,500,2),(51,2,281,500,3),(52,3,281,500,4),(53,4,281,500,5),(54,5,281,500,6),(55,6,281,500,8),(56,7,281,500,9),(57,1,501,1200,3),(58,2,501,1200,3),(59,3,501,1200,5),(60,4,501,1200,6),(61,5,501,1200,7),(62,6,501,1200,9),(63,7,501,1200,10),(64,1,1201,3200,3),(65,2,1201,3200,4),(66,3,1201,3200,5),(67,4,1201,3200,7),(68,5,1201,3200,8),(69,6,1201,3200,10),(70,7,1201,3200,11),(71,1,3201,10000,3),(72,2,3201,10000,4),(73,3,3201,10000,6),(74,4,3201,10000,7),(75,5,3201,10000,9),(76,6,3201,10000,11),(77,7,3201,10000,12),(78,1,10001,35000,3),(79,2,10001,35000,4),(80,3,10001,35000,6),(81,4,10001,35000,8),(82,5,10001,35000,10),(83,6,10001,35000,12),(84,7,10001,35000,13),(85,1,35001,150000,4),(86,2,35001,150000,5),(87,3,35001,150000,7),(88,4,35001,150000,9),(89,5,35001,150000,11),(90,6,35001,150000,13),(91,7,35001,150000,14),(92,1,150001,500000,4),(93,2,150001,500000,5),(94,3,150001,500000,7),(95,4,150001,500000,9),(96,5,150001,500000,12),(97,6,150001,500000,14),(98,7,150001,500000,15),(99,1,500001,1000000,4),(100,2,500001,1000000,5),(101,3,500001,1000000,8),(102,4,500001,1000000,10),(103,5,500001,1000000,13),(104,6,500001,1000000,15),(105,7,500001,1000000,16),(106,1,1,1,17),(107,2,1,1,17),(108,3,1,1,17),(109,4,1,1,17),(110,5,1,1,17),(111,6,1,1,17),(112,7,1,1,17);
/*!40000 ALTER TABLE `livello_collaudo_cella` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `misurazione`
--

DROP TABLE IF EXISTS `misurazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `misurazione` (
  `MISURAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTROLLO_QUALITA_ID` int(11) NOT NULL,
  `PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID` int(11) NOT NULL,
  `VALORE_EFFETTIVO` decimal(12,5) DEFAULT '-1.00000',
  `NUMERO_ORDINE_CAMPIONI` int(11) NOT NULL,
  PRIMARY KEY (`MISURAZIONE_ID`),
  UNIQUE KEY `CONTROLLO_QUALITA_ID_2` (`CONTROLLO_QUALITA_ID`,`PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID`,`NUMERO_ORDINE_CAMPIONI`),
  KEY `CONTROLLO_QUALITA_ID` (`CONTROLLO_QUALITA_ID`),
  KEY `PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID` (`PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID`),
  CONSTRAINT `misurazione_ibfk_1` FOREIGN KEY (`CONTROLLO_QUALITA_ID`) REFERENCES `controllo_qualita` (`CONTROLLO_QUALITA_ID`),
  CONSTRAINT `misurazione_ibfk_2` FOREIGN KEY (`PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID`) REFERENCES `product_misura_qualita_istruzione` (`PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `misurazione`
--

LOCK TABLES `misurazione` WRITE;
/*!40000 ALTER TABLE `misurazione` DISABLE KEYS */;
/*!40000 ALTER TABLE `misurazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `non_conformita`
--

DROP TABLE IF EXISTS `non_conformita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `non_conformita` (
  `NON_CONFORMITA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NON_CONFORMITA_PARENT_ID` int(11) DEFAULT NULL,
  `QUALITA_ORIGINE_ID` int(11) NOT NULL,
  `NON_CONFORMITA_LIVELLO_ID` int(11) DEFAULT NULL,
  `DOCUMENTO_TRASPORTO_RIGA_ID` int(11) DEFAULT NULL,
  `PRODUCT_BATCH_FORMULA_ITEM_ID` int(11) DEFAULT NULL,
  `PRODUCT_BATCH_ID` int(11) DEFAULT NULL,
  `CONTROLLO_QUALITA_ID` int(11) DEFAULT NULL,
  `ANALISI_CAUSA` mediumtext,
  `DESCRIZIONE_AZIONE_IMMEDIATA` mediumtext,
  `STATO` varchar(32) DEFAULT '',
  `DATA_CREAZIONE` datetime DEFAULT NULL,
  `DATA_SEGNALAZIONE` datetime DEFAULT NULL,
  `DATA_CHIUSURA` datetime DEFAULT NULL,
  `DATA_COMUNICAZIONE_AL_RAQ` datetime DEFAULT NULL,
  `NOME_SEGNALANTE` varchar(32) DEFAULT '',
  `COGNOME_SEGNALANTE` varchar(32) DEFAULT '',
  `USERNAME_SEGNALANTE` varchar(99) DEFAULT '',
  `RUOLO_SEGNALANTE` varchar(32) DEFAULT '',
  `NOME_RICEVENTE_NOTIFICA` varchar(32) DEFAULT '',
  `COGNOME_RICEVENTE_NOTIFICA` varchar(32) DEFAULT '',
  `USERNAME_RICEVENTE_NOTIFICA` varchar(99) DEFAULT '',
  `VERIFICA_CHIUSURA` varchar(254) DEFAULT '',
  `DATA_VERIFICA_CHIUSURA` datetime DEFAULT NULL,
  `TEMPO_IMPIEGATO_CHIUSURA` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  PRIMARY KEY (`NON_CONFORMITA_ID`),
  KEY `NON_CONFORMITA_PARENT_ID` (`NON_CONFORMITA_PARENT_ID`),
  KEY `QUALITA_ORIGINE_ID` (`QUALITA_ORIGINE_ID`),
  KEY `NON_CONFORMITA_LIVELLO_ID` (`NON_CONFORMITA_LIVELLO_ID`),
  KEY `DOCUMENTO_TRASPORTO_RIGA_ID` (`DOCUMENTO_TRASPORTO_RIGA_ID`),
  KEY `PRODUCT_BATCH_FORMULA_ITEM_ID` (`PRODUCT_BATCH_FORMULA_ITEM_ID`),
  KEY `PRODUCT_BATCH_ID` (`PRODUCT_BATCH_ID`),
  KEY `CONTROLLO_QUALITA_ID` (`CONTROLLO_QUALITA_ID`),
  CONSTRAINT `non_conformita_ibfk_1` FOREIGN KEY (`NON_CONFORMITA_PARENT_ID`) REFERENCES `non_conformita` (`NON_CONFORMITA_ID`),
  CONSTRAINT `non_conformita_ibfk_2` FOREIGN KEY (`QUALITA_ORIGINE_ID`) REFERENCES `qualita_origine` (`QUALITA_ORIGINE_ID`),
  CONSTRAINT `non_conformita_ibfk_3` FOREIGN KEY (`NON_CONFORMITA_LIVELLO_ID`) REFERENCES `non_conformita_livello` (`NON_CONFORMITA_LIVELLO_ID`),
  CONSTRAINT `non_conformita_ibfk_4` FOREIGN KEY (`DOCUMENTO_TRASPORTO_RIGA_ID`) REFERENCES `documento_trasporto_riga` (`DOCUMENTO_TRASPORTO_RIGA_ID`),
  CONSTRAINT `non_conformita_ibfk_5` FOREIGN KEY (`PRODUCT_BATCH_FORMULA_ITEM_ID`) REFERENCES `product_batch_formula_item` (`PRODUCT_BATCH_FORMULA_ITEM_ID`),
  CONSTRAINT `non_conformita_ibfk_6` FOREIGN KEY (`PRODUCT_BATCH_ID`) REFERENCES `product_batch` (`PRODUCT_BATCH_ID`),
  CONSTRAINT `non_conformita_ibfk_7` FOREIGN KEY (`CONTROLLO_QUALITA_ID`) REFERENCES `controllo_qualita` (`CONTROLLO_QUALITA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `non_conformita`
--

LOCK TABLES `non_conformita` WRITE;
/*!40000 ALTER TABLE `non_conformita` DISABLE KEYS */;
/*!40000 ALTER TABLE `non_conformita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `non_conformita_livello`
--

DROP TABLE IF EXISTS `non_conformita_livello`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `non_conformita_livello` (
  `NON_CONFORMITA_LIVELLO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `CODE` varchar(64) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  PRIMARY KEY (`NON_CONFORMITA_LIVELLO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `non_conformita_livello_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `non_conformita_livello`
--

LOCK TABLES `non_conformita_livello` WRITE;
/*!40000 ALTER TABLE `non_conformita_livello` DISABLE KEYS */;
INSERT INTO `non_conformita_livello` VALUES (1,5,'1','Bassissimo'),(2,5,'2','Basso'),(3,5,'3','Medio'),(4,5,'4','Alto'),(5,5,'5','Altissimo');
/*!40000 ALTER TABLE `non_conformita_livello` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifica`
--

DROP TABLE IF EXISTS `notifica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifica` (
  `NOTIFICA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATA` datetime DEFAULT NULL,
  `NOME` varchar(50) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `MESSAGGIO` varchar(1500) DEFAULT NULL,
  `INDIRIZZO` varchar(600) DEFAULT NULL,
  `SOGGETTO` varchar(254) DEFAULT NULL,
  `HTML_INLINE` int(11) DEFAULT '0',
  `SENT_TO` varchar(500) DEFAULT NULL,
  `DITTA_ID` int(11) DEFAULT NULL,
  `EXCEPTION_LOG_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`NOTIFICA_ID`),
  KEY `NOTIFICA_NOME` (`NOME`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `EXCEPTION_LOG_IDX` (`EXCEPTION_LOG_ID`),
  CONSTRAINT `EXCEPTION_LOG` FOREIGN KEY (`EXCEPTION_LOG_ID`) REFERENCES `exception_log` (`EXCEPTION_LOG_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `notifica_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifica`
--

LOCK TABLES `notifica` WRITE;
/*!40000 ALTER TABLE `notifica` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifica_log`
--

DROP TABLE IF EXISTS `notifica_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifica_log` (
  `NOTIFICA_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATA` datetime DEFAULT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `TEMPLATE` varchar(50) DEFAULT NULL,
  `MESSAGGIO` varchar(255) DEFAULT NULL,
  `INDIRIZZO` varchar(600) DEFAULT NULL,
  `SOGGETTO` varchar(255) DEFAULT NULL,
  `DATA_VISTO` datetime DEFAULT NULL,
  `VISTO_EMAIL_FILE` varchar(140) DEFAULT NULL,
  `NOTIFICA_LOG_ID_EMAIL` decimal(19,0) DEFAULT NULL,
  `NOTA_SPEDIZIONE` varchar(200) DEFAULT NULL,
  `VISTO_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `NOTIFICA_ID` int(11) DEFAULT NULL,
  `TIPO_INVIO` varchar(80) DEFAULT '',
  PRIMARY KEY (`NOTIFICA_LOG_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `NOTIFICA_LOG_ID_EMAIL_IDX` (`NOTIFICA_LOG_ID_EMAIL`),
  KEY `VISTO_NOTIFICA_LOG_RICEVUTA_ID` (`VISTO_NOTIFICA_LOG_RICEVUTA_ID`),
  KEY `DATA` (`DATA`),
  KEY `OBJECT` (`OBJECT_TYPE`,`OBJECT_ID`),
  KEY `TEMPLATE` (`TEMPLATE`),
  CONSTRAINT `notifica_log_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifica_log`
--

LOCK TABLES `notifica_log` WRITE;
/*!40000 ALTER TABLE `notifica_log` DISABLE KEYS */;
INSERT INTO `notifica_log` VALUES (37,'2008-04-07 17:38:51',5,60,1081,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-38-51-546_D5_T60_O1081_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10523 - Riepilogo stato ordine - Progressivo Notifiche: 17',NULL,NULL,0,NULL,NULL,NULL,''),(38,'2008-04-07 17:38:51',5,60,1073,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-38-51-781_D5_T60_O1073_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10168 - Riepilogo stato ordine - Progressivo Notifiche: 8',NULL,NULL,0,NULL,NULL,NULL,''),(39,'2008-04-07 17:38:52',5,60,1092,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-38-52-031_D5_T60_O1092_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 11253 - Riepilogo stato ordine - Progressivo Notifiche: 28',NULL,NULL,0,NULL,NULL,NULL,''),(40,'2008-04-07 17:39:16',5,40,287,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-39-16-234_D5_T40_O287_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 02-04-2009',NULL,NULL,0,NULL,NULL,NULL,''),(41,'2008-04-07 17:40:01',5,40,287,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-40-01-718_D5_T40_O287_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 02-04-2009',NULL,NULL,0,NULL,NULL,NULL,''),(42,'2008-04-07 17:54:50',5,60,1092,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-54-50-734_D5_T60_O1092_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 11253 - Riepilogo stato ordine - Progressivo Notifiche: 29',NULL,NULL,0,NULL,NULL,NULL,''),(43,'2008-04-07 17:55:06',5,60,1092,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-55-06-609_D5_T60_O1092_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 11253 - Riepilogo stato ordine - Progressivo Notifiche: 30',NULL,NULL,0,NULL,NULL,NULL,''),(44,'2008-04-07 17:55:56',5,60,1092,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-04-07_17-55-56-671_D5_T60_O1092_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 11253 - Riepilogo stato ordine - Progressivo Notifiche: 30',NULL,NULL,0,NULL,NULL,NULL,''),(45,'2008-09-11 18:00:58',5,40,287,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-00-58-796_D5_T40_O287_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 06-09-2009',NULL,NULL,0,NULL,NULL,NULL,''),(46,'2008-09-11 18:00:59',5,40,288,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-00-59-125_D5_T40_O288_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 06-09-2009',NULL,NULL,0,NULL,NULL,NULL,''),(47,'2008-09-11 18:00:59',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-00-59-687_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 06-09-2009',NULL,NULL,0,NULL,NULL,NULL,''),(48,'2008-09-11 18:04:21',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-04-21-859_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 8',NULL,NULL,0,NULL,NULL,NULL,''),(49,'2008-09-11 18:06:40',5,60,1081,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-06-40-781_D5_T60_O1081_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10523 - Riepilogo stato ordine - Progressivo Notifiche: 18',NULL,NULL,0,NULL,NULL,NULL,''),(50,'2008-09-11 18:14:23',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-14-23-625_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 9',NULL,NULL,0,NULL,NULL,NULL,''),(51,'2008-09-11 18:31:01',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-31-01-218_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 9',NULL,NULL,0,NULL,NULL,NULL,''),(52,'2008-09-11 18:35:21',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-35-21-406_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 9',NULL,NULL,0,NULL,NULL,NULL,''),(53,'2008-09-11 18:36:01',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-36-01-890_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 9',NULL,NULL,0,NULL,NULL,NULL,''),(54,'2008-09-11 18:36:44',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_18-36-44-562_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 9',NULL,NULL,0,NULL,NULL,NULL,''),(55,'2008-09-11 22:55:47',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_22-55-47-343_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 10',NULL,NULL,0,NULL,NULL,NULL,''),(56,'2008-09-11 22:56:54',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_22-56-54-328_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 11',NULL,NULL,0,NULL,NULL,NULL,''),(57,'2008-09-11 22:58:50',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_22-58-50-640_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 12',NULL,NULL,0,NULL,NULL,NULL,''),(58,'2008-09-11 22:59:41',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_22-59-41-734_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 13',NULL,NULL,0,NULL,NULL,NULL,''),(59,'2008-09-11 23:00:56',5,60,1142,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2008-09-11_23-00-56-109_D5_T60_O1142_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0006 - Riepilogo stato ordine - Progressivo Notifiche: 13',NULL,NULL,0,NULL,NULL,NULL,''),(60,'2009-01-26 17:48:22',5,60,1073,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_17-48-22-593_D5_T60_O1073_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10168 - Riepilogo stato ordine - Progressivo Notifiche: 9',NULL,NULL,0,NULL,NULL,NULL,''),(61,'2009-01-26 17:48:50',5,60,1073,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_17-48-50-125_D5_T60_O1073_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10168 - Riepilogo stato ordine - Progressivo Notifiche: 10',NULL,NULL,0,NULL,NULL,NULL,''),(62,'2009-01-26 17:52:27',5,60,1073,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_17-52-27-140_D5_T60_O1073_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10168 - Riepilogo stato ordine - Progressivo Notifiche: 11',NULL,NULL,0,NULL,NULL,NULL,''),(63,'2009-01-26 17:58:24',5,60,1073,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_17-58-24-062_D5_T60_O1073_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10168 - Riepilogo stato ordine - Progressivo Notifiche: 11',NULL,NULL,0,NULL,NULL,NULL,''),(64,'2009-01-26 17:59:02',5,60,1073,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_17-59-02-906_D5_T60_O1073_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10168 - Riepilogo stato ordine - Progressivo Notifiche: 11',NULL,NULL,0,NULL,NULL,NULL,''),(65,'2009-01-26 17:59:44',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_17-59-44-515_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 21-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(66,'2009-01-26 18:02:01',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_18-02-01-312_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 21-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(67,'2009-01-26 18:05:12',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_18-05-12-265_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 21-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(68,'2009-01-26 18:08:06',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_18-08-06-906_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 21-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(69,'2009-01-26 18:13:20',5,60,1073,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_18-13-20-718_D5_T60_O1073_ordineFornitoreRiepilogo.log','fornitore@localmail','OA - 10168 - Riepilogo stato ordine - Progressivo Notifiche: 12',NULL,NULL,0,NULL,NULL,NULL,''),(70,'2009-01-26 23:41:13',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_23-41-13-406_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 21-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(71,'2009-01-26 23:43:43',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_23-43-43-218_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 21-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(72,'2009-01-26 23:45:05',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-26_23-45-05-796_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 21-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(73,'2009-01-27 00:07:24',5,40,287,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-07-24-718_D5_T40_O287_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 22-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(74,'2009-01-27 00:07:25',5,40,291,'fornitorePrevisioneConsegna','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-07-25-437_D5_T40_O291_fornitorePrevisioneConsegna.log','fornitore@localmail','Documento di Previsione di Consegna per il giorno 22-01-2010',NULL,NULL,0,NULL,NULL,NULL,''),(75,'2009-01-27 00:08:15',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-08-15-437_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 7',NULL,NULL,0,NULL,NULL,NULL,''),(76,'2009-01-27 00:17:04',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-17-04-796_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 8',NULL,NULL,0,NULL,NULL,NULL,''),(77,'2009-01-27 00:21:32',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-21-32-343_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 10',NULL,NULL,0,NULL,NULL,NULL,''),(78,'2009-01-27 00:22:45',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-22-45-656_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 11',NULL,NULL,0,NULL,NULL,NULL,''),(79,'2009-01-27 00:30:03',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-30-03-328_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 12',NULL,NULL,0,NULL,NULL,NULL,''),(80,'2009-01-27 00:36:44',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-36-44-906_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 13',NULL,NULL,0,NULL,NULL,NULL,''),(81,'2009-01-27 00:38:25',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-38-25-890_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 14',NULL,NULL,0,NULL,NULL,NULL,''),(82,'2009-01-27 00:39:53',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-39-53-203_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 15',NULL,NULL,0,NULL,NULL,NULL,''),(83,'2009-01-27 00:42:32',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-42-32-703_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 16',NULL,NULL,0,NULL,NULL,NULL,''),(84,'2009-01-27 00:43:39',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-43-39-156_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 16',NULL,NULL,0,NULL,NULL,NULL,''),(85,'2009-01-27 00:49:52',5,60,1143,'ordineFornitoreRiepilogo','C:/iungo/Tomcat 5.5/webapps/iungo/StoricoNotifiche\\2009-01-27_00-49-52-578_D5_T60_O1143_ordineFornitoreRiepilogo.log','fornitore@localmail','RDO - 08-0007 - Riepilogo stato ordine - Progressivo Notifiche: 16',NULL,NULL,0,NULL,NULL,NULL,''),(86,'2015-11-10 17:11:43',5,60,1092,'ordineFornitoreRiepilogo','/Applications/apache-tomcat-7.0.63/bin/C:/iungo/scambioDati/StoricoNotifiche/2015/11/2015-11-10_17-11-43-355_D5_T60_O1092_ordineFornitoreRiepilogo.html.zip','fornitore@thor.local','OA - 11253 - Riepilogo stato ordine - Progressivo Notifiche: 30',NULL,NULL,1447171902790,NULL,0,42,'MAIL POST'),(87,'2015-12-14 10:05:43',5,60,1145,'ordineFornitoreNuovo','/Applications/apache-tomcat-7.0.63/bin/C:/iungo/scambioDati/StoricoNotifiche/2015/12/2015-12-14_10-05-42-639_D5_T60_O1145_ordineFornitoreNuovo.html.zip','fornitore@thor.local','OA - 151555555 - Nuovo ordine',NULL,NULL,1450083941991,NULL,0,42,'MAIL POST'),(88,'2016-02-17 15:29:24',5,60,1145,'ordineFornitoreRiepilogo','/Applications/apache-tomcat-7.0.63/bin/C:/iungo/scambioDati/StoricoNotifiche/2016/02/2016-02-17_15-29-23-927_D5_T60_O1145_ordineFornitoreRiepilogo.html.zip','fornitore@thor.local','OA - 151555555 - Ordine aggiornato - Progressivo: 1','2016-02-17 15:29:57',NULL,1455719363181,NULL,0,42,'MAIL POST'),(89,'2016-03-15 13:33:39',5,60,1145,'ordineFornitoreRiepilogo','/private/var/iungo/iungo/scambioDati/StoricoNotifiche/2016/03/2016-03-15_13-33-38-852_D5_T60_O1145_ordineFornitoreRiepilogo.html.zip','fornitore@thor.local','OA - 151555555 - Ordine aggiornato - Progressivo: 2',NULL,NULL,1458045217517,NULL,0,43,'MAIL POST'),(90,'2016-04-14 15:48:00',5,40,291,'fornitorePrevisioneConsegna','/private/var/iungo/iungo/scambioDati/StoricoNotifiche/2016/04/2016-04-14_15-48-00-349_D5_T40_O291_fornitorePrevisioneConsegna.html.zip','fornitore@thor.local','Document with order to delivery created on 14-04-2016 15:48','2016-04-14 15:48:25',NULL,1460641679700,NULL,5,42,'MAIL POST'),(91,'2016-05-10 12:05:37',5,60,1146,'ordineFornitoreNuovo','/private/var/iungo/iungo/scambioDati/StoricoNotifiche/2016/05/2016-05-10_12-05-37-343_D5_T60_O1146_ordineFornitoreNuovo.html.zip','fornitore@thor.local','OA-SPLIT - 5988000012 - Nuovo ordine','2016-05-10 12:05:59',NULL,1462874736458,NULL,6,42,'MAIL POST');
/*!40000 ALTER TABLE `notifica_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifica_log_ricevute`
--

DROP TABLE IF EXISTS `notifica_log_ricevute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifica_log_ricevute` (
  `NOTIFICA_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATA` datetime DEFAULT NULL,
  `SOGGETTO` varchar(255) DEFAULT NULL,
  `EMAIL_FILE` varchar(140) DEFAULT NULL,
  `EMAIL_ID` decimal(19,0) DEFAULT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `MITTENTE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`NOTIFICA_LOG_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `EMAIL_ID_IDX` (`EMAIL_ID`),
  KEY `EMAIL_FILE` (`EMAIL_FILE`),
  KEY `DATA` (`DATA`),
  CONSTRAINT `notifica_log_ricevute_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifica_log_ricevute`
--

LOCK TABLES `notifica_log_ricevute` WRITE;
/*!40000 ALTER TABLE `notifica_log_ricevute` DISABLE KEYS */;
INSERT INTO `notifica_log_ricevute` VALUES (1,'2016-02-17 15:29:43','Visto_OA_151555555',NULL,1455719363181,5,'fornitore@localmail.it'),(2,'2016-02-17 15:29:46','Visto_OA_151555555',NULL,1455719363181,5,'fornitore@localmail.it'),(3,'2016-02-17 15:29:57','Visto_OA_151555555',NULL,1455719363181,5,'fornitore@localmail.it'),(4,'2016-02-17 15:29:58','Visto_OA_151555555',NULL,NULL,5,'fornitore@localmail.it'),(5,'2016-04-14 15:48:25','Visto_PianoConsegna','2016-04-14_15-48-25-311_[fornitore@localmail]_D5_Visto_PianoConsegna.eml.zip',NULL,5,'fornitore@localmail'),(6,'2016-05-10 12:05:59','Visto_OA-SPLIT_5988000012','2016-05-10_12-05-59-273_[fornitore@localmailit]_D5_Visto_OA-SPLIT_5988000012.eml.zip',NULL,5,'fornitore@localmail.it');
/*!40000 ALTER TABLE `notifica_log_ricevute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `numerosita_campione`
--

DROP TABLE IF EXISTS `numerosita_campione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `numerosita_campione` (
  `NUMEROSITA_CAMPIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `LETTERA_CODICE` varchar(2) DEFAULT '',
  PRIMARY KEY (`NUMEROSITA_CAMPIONE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `numerosita_campione`
--

LOCK TABLES `numerosita_campione` WRITE;
/*!40000 ALTER TABLE `numerosita_campione` DISABLE KEYS */;
INSERT INTO `numerosita_campione` VALUES (1,'A'),(2,'B'),(3,'C'),(4,'D'),(5,'E'),(6,'F'),(7,'G'),(8,'H'),(9,'J'),(10,'K'),(11,'L'),(12,'M'),(13,'N'),(14,'P'),(15,'Q'),(16,'R'),(17,'S');
/*!40000 ALTER TABLE `numerosita_campione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine`
--

DROP TABLE IF EXISTS `ordine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine` (
  `ORDINE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `USER_NAME` varchar(99) NOT NULL,
  `REFERENTE` varchar(254) DEFAULT NULL,
  `REFERENTE_TELEFONO` varchar(30) DEFAULT NULL,
  `PROGRESSIVO_NOTIFICA` int(11) NOT NULL DEFAULT '0',
  `NOTA` mediumtext,
  `DATA_EMISSIONE` datetime DEFAULT NULL,
  `DATA_CONSEGNA_PROPOSTA` datetime DEFAULT NULL,
  `ORDINE_NUMERO` varchar(50) DEFAULT NULL,
  `DA_RITIRARE` int(11) DEFAULT NULL,
  `DESTINAZIONE_INDIRIZZO` varchar(254) DEFAULT NULL,
  `FORNITORE_FAX` varchar(30) DEFAULT NULL,
  `FORNITORE_EMAIL` varchar(600) DEFAULT NULL,
  `CORRIERE` varchar(99) DEFAULT NULL,
  `IMBALLO` varchar(99) DEFAULT NULL,
  `IMBALLO_CODICE` varchar(2) DEFAULT NULL,
  `NOTA_RESO` varchar(99) DEFAULT NULL,
  `ESENTE_IVA` int(11) DEFAULT NULL,
  `VALUTA` varchar(20) DEFAULT NULL,
  `VALUTA_CODICE` varchar(3) DEFAULT NULL,
  `MODALITA_PAGAMENTO` varchar(100) DEFAULT NULL,
  `MODALITA_PAGAMENTO_CODICE` varchar(2) DEFAULT NULL,
  `COORDINATE_BANCARIE` varchar(99) DEFAULT NULL,
  `CAMBIO` decimal(12,5) DEFAULT '1.00000',
  `FIRMA_LIVELLO` int(11) DEFAULT '0',
  `DATA_DISP_MERCE` datetime DEFAULT NULL,
  `INCOTERM_CODICE` varchar(3) DEFAULT NULL,
  `INCOTERM_LUOGO` varchar(255) DEFAULT NULL,
  `TRASPORTO_MODALITA_CODICE` varchar(3) DEFAULT NULL,
  `PAGAMENTO_TERMINI_CODICE` varchar(50) DEFAULT NULL,
  `SCONTO1` decimal(12,5) DEFAULT '0.00000',
  `SCONTO2` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD1` varchar(99) DEFAULT NULL,
  `AUX_HEAD2` varchar(99) DEFAULT NULL,
  `AUX_HEAD3` varchar(99) DEFAULT NULL,
  `AUX_HEAD4` varchar(99) DEFAULT NULL,
  `AUX_HEAD5` varchar(255) DEFAULT NULL,
  `AUX_HEAD6` varchar(99) DEFAULT NULL,
  `AUX_HEAD7` varchar(99) DEFAULT NULL,
  `AUX_HEAD8` mediumtext,
  `AUX_HEAD9` mediumtext,
  `AUX_HEAD10` mediumtext,
  `AUX_HEAD_DATE1` datetime DEFAULT NULL,
  `AUX_HEAD_DATE2` datetime DEFAULT NULL,
  `AUX_HEAD_DATE3` datetime DEFAULT NULL,
  `AUX_HEAD_DATE4` datetime DEFAULT NULL,
  `AUX_HEAD_DATE5` datetime DEFAULT NULL,
  `AUX_HEAD_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM5` decimal(12,5) DEFAULT '0.00000',
  `IBAN` varchar(40) DEFAULT NULL,
  `REFERENTE_FAX` varchar(30) DEFAULT NULL,
  `REFERENTE_EMAIL` varchar(600) DEFAULT NULL,
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  `ORDINE_TRANSPORT_LEAD_TIME` int(11) DEFAULT '-1',
  PRIMARY KEY (`ORDINE_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `ORDINE_ORDINE_NUMERO` (`ORDINE_NUMERO`),
  CONSTRAINT `ordine_ibfk_1` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`),
  CONSTRAINT `ordine_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1147 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine`
--

LOCK TABLES `ordine` WRITE;
/*!40000 ALTER TABLE `ordine` DISABLE KEYS */;
INSERT INTO `ordine` VALUES (1039,17,291,'turbine','',NULL,0,'','2006-02-20 00:00:00','2006-02-22 00:00:00','00001',0,'','','','','',NULL,'',0,'',NULL,'',NULL,'ABI CAB',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1065,11,288,'turbine','',NULL,3,'','2007-03-21 00:00:00','2007-03-21 00:00:00','10095',0,'','','','','',NULL,'',0,'',NULL,'',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1068,11,291,'turbine','',NULL,7,'','2007-03-23 00:00:00','2007-03-30 00:00:00','10133',0,'','','','','',NULL,'',0,'',NULL,'',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1069,11,291,'Mattia','',NULL,5,'','2007-03-23 00:00:00','2007-03-30 00:00:00','10120',0,'','','','','',NULL,'',0,'',NULL,'',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1073,11,291,'turbine','','',13,'','2007-03-26 00:00:00','2007-04-11 00:00:00','10168',0,'','','','','',NULL,'',0,'',NULL,'',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1081,11,287,'turbine','',NULL,19,'','2007-03-30 00:00:00','2007-03-11 00:00:00','10523',0,'','','','','',NULL,'',0,'',NULL,'',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1092,11,291,'turbine','',NULL,30,'','2007-05-28 00:00:00','2007-05-28 00:00:00','11253',0,'','','','','',NULL,'',0,'',NULL,'',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1142,13,287,'turbine','',NULL,13,'','2007-11-15 11:55:00','2008-05-24 10:11:00','08-0006',0,'','','','30','',NULL,'29',0,'',NULL,'31',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1143,13,288,'turbine','',NULL,16,'','2008-05-24 11:40:00','2008-05-24 11:40:00','08-0007',0,'','','','proprio','',NULL,'12',0,'',NULL,'60 gg',NULL,'',1.00000,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,-1),(1145,11,293,'sysScheduler','030 - Giovanni Bellini',NULL,5,'','2015-06-29 00:00:00',NULL,'151555555',0,NULL,NULL,'','','GRATIS',NULL,NULL,0,'EUR','EUR','B121 - Ricevuta bancaria 120 gg df fm',NULL,'CREDITO ROMAGNOLO -AG.DI MANTOVA- (ABI 1234 CAB 009988)',1.00000,0,NULL,'DAP','.',NULL,NULL,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,'2015-12-14 10:05:26',-1),(1146,26,295,'sysScheduler','030 - Giovanni Bellini',NULL,4,'','2015-10-29 00:00:00',NULL,'5988000012',0,NULL,NULL,'','','GRATIS',NULL,NULL,0,'EUR','EUR','B121 - Ricevuta bancaria 120 gg df fm',NULL,'CREDITO ROMAGNOLO -AG.DI MANTOVA- (ABI 1234 CAB 009988)',1.00000,0,NULL,'DAP','.',NULL,NULL,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,'2016-05-10 12:05:13',-1);
/*!40000 ALTER TABLE `ordine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_attribute`
--

DROP TABLE IF EXISTS `ordine_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_attribute` (
  `ORDINE_ATTRIBUTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_ID` int(11) NOT NULL,
  `NAME` varchar(254) NOT NULL,
  `VALUE` mediumtext,
  `TYPE` varchar(15) DEFAULT 's',
  `DICTIONARY` int(11) DEFAULT '0',
  PRIMARY KEY (`ORDINE_ATTRIBUTE_ID`),
  UNIQUE KEY `ORDINE_ID_2` (`ORDINE_ID`,`NAME`),
  KEY `ORDINE_ID` (`ORDINE_ID`),
  CONSTRAINT `ordine_attribute_ibfk_1` FOREIGN KEY (`ORDINE_ID`) REFERENCES `ordine` (`ORDINE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_attribute`
--

LOCK TABLES `ordine_attribute` WRITE;
/*!40000 ALTER TABLE `ordine_attribute` DISABLE KEYS */;
INSERT INTO `ordine_attribute` VALUES (1,1081,'sys_firstSend_date','2008-04-07','s',0),(2,1081,'sys_firstSend_time','17:38:51','s',0),(3,1142,'sys_firstSend_date','2008-09-11','s',0),(4,1142,'sys_firstSend_time','18:04:21','s',0),(5,1143,'sys_firstSend_date','2009-01-27','s',0),(6,1143,'sys_firstSend_time','00:08:15','s',0),(7,1073,'sys_firstSend_date','2008-04-07','s',0),(8,1073,'sys_firstSend_time','17:38:51','s',0),(9,1092,'sys_firstSend_date','2008-04-07','s',0),(10,1092,'sys_firstSend_time','17:38:52','s',0),(11,1145,'sys_firstSend_date','2015-12-14','s',0),(12,1145,'sys_firstSend_time','10:05:42','s',0),(13,1145,'sys_firstProgressivoNotifica','4','s',0),(14,1146,'sys_firstSend_date','2016-05-10','s',0),(15,1146,'sys_firstSend_time','12:05:37','s',0),(16,1146,'sys_firstProgressivoNotifica','4','s',0);
/*!40000 ALTER TABLE `ordine_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_riga`
--

DROP TABLE IF EXISTS `ordine_riga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_riga` (
  `ORDINE_RIGA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_ID` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `ORDINE_WF_STATO_NOME` varchar(30) NOT NULL,
  `NOTA` mediumtext,
  `PRODOTTO_CODICE` varchar(200) DEFAULT '',
  `PRODOTTO_DESCRIZIONE` varchar(254) DEFAULT '',
  `FORNITORE_PRODOTTO_CODICE` varchar(200) DEFAULT '',
  `FORNITORE_PRODOTTO_DESCRIZIONE` varchar(254) DEFAULT '',
  `FASE_NUMERO` varchar(15) DEFAULT '',
  `FASE_DESCRIZIONE` varchar(254) DEFAULT '',
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT '',
  `PREZZO_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `QUANTITA_UNITA_MISURA` varchar(100) DEFAULT '',
  `QUANTITA_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `NUMERO_COMMESSA` varchar(100) DEFAULT '',
  `NUMERO_RIGA` varchar(30) DEFAULT '',
  `DESTINAZIONE_INDIRIZZO` varchar(254) DEFAULT '',
  `QUANTITA` decimal(15,5) DEFAULT '0.00000',
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `DATA_DISPONIBILITA` datetime DEFAULT NULL,
  `DATA_CONSEGNA` datetime DEFAULT NULL,
  `DATA_ULTIMA_SPEDIZIONE` datetime DEFAULT NULL,
  `VALUTA` varchar(30) DEFAULT '',
  `VALUTA_CODICE` varchar(3) DEFAULT NULL,
  `PROP_QUANTITA` decimal(15,5) DEFAULT '0.00000',
  `PROP_PREZZO` decimal(12,5) DEFAULT '0.00000',
  `PROP_DATA_CONSEGNA` datetime DEFAULT NULL,
  `LEAD_TIME` decimal(12,5) DEFAULT '-1.00000',
  `DATA_CONSEGNA_LEAD_TIME` datetime DEFAULT NULL,
  `SCONTO1` decimal(12,5) DEFAULT '0.00000',
  `SCONTO2` decimal(12,5) DEFAULT '0.00000',
  `SCONTO3` decimal(12,5) DEFAULT '0.00000',
  `SCONTO4` decimal(12,5) DEFAULT '0.00000',
  `PROP_SCONTO1` decimal(12,5) DEFAULT '0.00000',
  `PROP_SCONTO2` decimal(12,5) DEFAULT '0.00000',
  `PROP_SCONTO3` decimal(12,5) DEFAULT '0.00000',
  `PROP_SCONTO4` decimal(12,5) DEFAULT '0.00000',
  `AUMENTO1` decimal(12,5) DEFAULT '0.00000',
  `AUMENTO2` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUMENTO1` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUMENTO2` decimal(12,5) DEFAULT '0.00000',
  `QUANTITA_CONSEGNATA_GESTIONALE` decimal(15,5) DEFAULT '0.00000',
  `DATA_ULTIMA_CONSEGNA_GESTIONALE` datetime DEFAULT NULL,
  `QUANTITA_PRE_ACCETTAZIONE` decimal(15,5) DEFAULT '0.00000',
  `QUANTITA_ACCETTAZIONE` decimal(15,5) DEFAULT '0.00000',
  `FG_A_SALDO` int(11) DEFAULT NULL,
  `REF_ORDINE_RIGA_ID` int(11) DEFAULT '-1',
  `FG_VISTO` int(11) DEFAULT '0',
  `DATA_VISTO` datetime DEFAULT NULL,
  `AUX_ROW1` varchar(99) DEFAULT '',
  `AUX_ROW2` varchar(99) DEFAULT '',
  `AUX_ROW3` varchar(99) DEFAULT '',
  `AUX_ROW4` varchar(99) DEFAULT '',
  `AUX_ROW5` varchar(255) DEFAULT '',
  `AUX_ROW6` varchar(99) DEFAULT '',
  `AUX_ROW7` varchar(99) DEFAULT '',
  `AUX_ROW8` varchar(99) DEFAULT '',
  `AUX_ROW9` varchar(99) DEFAULT '',
  `AUX_ROW10` varchar(99) DEFAULT '',
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_DATE3` datetime DEFAULT NULL,
  `AUX_ROW_DATE4` datetime DEFAULT NULL,
  `AUX_ROW_DATE5` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE1` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE2` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE3` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE4` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE5` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `PROP_COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `LAST_TRANSACTION_DATE` datetime DEFAULT NULL,
  `LAST_DELIVERY_NOTE_NUMBER` varchar(50) DEFAULT NULL,
  `LAST_DELIVERY_NOTE_DATE` datetime DEFAULT NULL,
  `IVA_PERC` decimal(12,5) DEFAULT '0.00000',
  `PARTNER_CONFIRMATION_NUMBER` varchar(50) DEFAULT NULL,
  `PARTNER_CONFIRMATION_DATE` datetime DEFAULT NULL,
  `SUB_FORNITORE_ID` int(11) DEFAULT '-1',
  `SUB_FORNITORE_ENABLED` int(11) DEFAULT '0',
  `NEXT_PARTNER_ID` int(11) DEFAULT '-1',
  `PROP_LEAD_TIME` decimal(12,5) DEFAULT '-1.00000',
  `DATA_PRIMA_CONFERMA` datetime DEFAULT NULL,
  `DATA_ULTIMA_CONFERMA` datetime DEFAULT NULL,
  `RITARDO_CONSEGNA_ULTIMA_CONFERMA` varchar(10) DEFAULT NULL,
  `RITARDO_CONSEGNA_PRIMA_CONFERMA` varchar(10) DEFAULT NULL,
  `RITARDO_RISPOSTA` varchar(10) DEFAULT NULL,
  `CAMBIO` decimal(12,5) DEFAULT '1.00000',
  `PROP_CAMBIO` decimal(12,5) DEFAULT '1.00000',
  `AUX_ROW11` varchar(99) DEFAULT '',
  `AUX_ROW12` varchar(99) DEFAULT '',
  `AUX_ROW13` varchar(99) DEFAULT '',
  `AUX_ROW14` varchar(99) DEFAULT '',
  `AUX_ROW15` varchar(99) DEFAULT '',
  `AUX_ROW16` mediumtext,
  `AUX_ROW17` mediumtext,
  `AUX_ROW18` mediumtext,
  `AUX_ROW19` mediumtext,
  `AUX_ROW20` mediumtext,
  `AUX_ROW_DATE6` datetime DEFAULT NULL,
  `AUX_ROW_DATE7` datetime DEFAULT NULL,
  `AUX_ROW_DATE8` datetime DEFAULT NULL,
  `AUX_ROW_DATE9` datetime DEFAULT NULL,
  `AUX_ROW_DATE10` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE6` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE7` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE8` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE9` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE10` datetime DEFAULT NULL,
  `AUX_ROW_NUM6` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM7` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM8` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM9` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM10` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM6` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM7` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM8` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM9` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM10` decimal(12,5) DEFAULT '0.00000',
  `ORDINE_WF_STATO_TIPO` varchar(25) DEFAULT '',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  `FIRMA_LIVELLO` int(11) DEFAULT '0',
  `LV_VALUTA` varchar(3) DEFAULT NULL,
  `PROP_LV_VALUTA` varchar(3) DEFAULT NULL,
  `LV_TASSO` decimal(12,5) DEFAULT '0.00000',
  `PROP_LV_TASSO` decimal(12,5) DEFAULT '0.00000',
  `LV_VARIAZIONE` decimal(12,5) DEFAULT '0.00000',
  `PROP_LV_VARIAZIONE` decimal(12,5) DEFAULT '0.00000',
  `ORDINE_RIGA_TRANSPORT_LEAD_TIME` int(11) DEFAULT '-1',
  `DATA_SPEDIZIONE_ULTIMA_MODIFICA` datetime DEFAULT NULL,
  PRIMARY KEY (`ORDINE_RIGA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `ORDINE_ID` (`ORDINE_ID`),
  KEY `ORDINE_WF_STATO_NOME` (`ORDINE_WF_STATO_NOME`),
  KEY `ORDINE_RIGA_PRODOTTO_CODICE` (`PRODOTTO_CODICE`),
  KEY `ORDINE_RIGA_DITTA_ID` (`DITTA_ID`),
  KEY `ORDINE_RIGA_ORDINE_WF_STATO_NOME` (`ORDINE_WF_STATO_NOME`),
  CONSTRAINT `ordine_riga_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `ordine_riga_ibfk_2` FOREIGN KEY (`ORDINE_ID`) REFERENCES `ordine` (`ORDINE_ID`),
  CONSTRAINT `ordine_riga_ibfk_3` FOREIGN KEY (`ORDINE_WF_STATO_NOME`) REFERENCES `ordine_wf_stato` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=17180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_riga`
--

LOCK TABLES `ordine_riga` WRITE;
/*!40000 ALTER TABLE `ordine_riga` DISABLE KEYS */;
INSERT INTO `ordine_riga` VALUES (16960,1039,5,'conf','','Item1','Item 1','','',NULL,NULL,'',NULL,'Nr',NULL,'','10','',1000.00000,2.00000,NULL,'2006-12-31 00:00:00',NULL,'',NULL,0.00000,0.00000,NULL,-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,1000.00000,0.00000,0,-1,0,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2006-12-31 00:00:00','2006-12-31 00:00:00','3418:00','3418:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(16961,1039,5,'conf','','Item1','Item 1','','',NULL,NULL,'',NULL,'Nr',NULL,'','20','',500.00000,2.00000,NULL,'2007-02-28 00:00:00',NULL,'',NULL,0.00000,0.00000,NULL,-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,0,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2007-02-28 00:00:00','2007-02-28 00:00:00','3359:00','3359:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(16992,1065,5,'prop','','ItemX','Item X','','','$or.NumeroFase',NULL,'',NULL,'Nr.',NULL,'','10','via Di qua 12',25.00000,2.00000,NULL,'2007-03-28 17:27:00',NULL,'',NULL,26.00000,2.00000,'2007-03-28 00:00:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,100.00000,1,-1,0,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2007-03-28 17:27:00','2007-03-28 17:27:00','3331:00','3331:00','',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(16995,1068,5,'conf','','ItemB','Item B','','',NULL,NULL,'',NULL,'Nr.',NULL,'','10','',91.00000,10.00000,NULL,'2007-03-30 10:07:00','2016-04-14 15:48:00','',NULL,89.00000,10.00000,'2009-06-03 10:07:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,0,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2007-03-30 10:07:00','2007-03-30 10:07:00','3329:00','3329:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,'2016-04-14 15:48:00'),(16996,1069,5,'chiuso','','ItemB','Item B','','',NULL,NULL,'',NULL,'Nr.',NULL,'','20','',100.00000,0.00000,NULL,'2007-03-30 12:21:00',NULL,'',NULL,5.00000,5.00000,'2009-06-03 12:21:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'2007-03-23 16:48:59',0.00000,0.00000,0,-1,0,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,NULL,NULL,'-1','-1','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(17001,1073,5,'conf','','Item1','Item 1','','',NULL,NULL,'',NULL,'Nr',NULL,'','20','',56.32000,2.23000,NULL,'2009-04-14 10:21:00','2016-04-14 15:48:00','',NULL,56.32000,2.23000,'2009-04-14 10:21:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,0,'1970-01-01 01:00:00','','','RIFIUTATA','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2009-04-14 10:21:00','2009-04-14 10:21:00','2583:00','2583:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,'2016-04-14 15:48:00'),(17002,1073,5,'conf','','ItemC','Item C','','',NULL,NULL,'',NULL,'Nr.',NULL,'','10','',26.00000,5.53000,NULL,'2009-06-13 00:00:00','2016-04-14 15:48:00','',NULL,20.00000,5.00000,'2007-04-11 10:21:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2007-04-11 10:21:00','2009-06-13 00:00:00','2523:00','3317:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,'2016-04-14 15:48:00'),(17016,1081,5,'chiuso','','ItemA','Item A','','',NULL,NULL,'',NULL,'Nr.',NULL,'','10','',100.00000,50.34000,NULL,'2008-03-11 16:40:00',NULL,'',NULL,100.00000,50.34000,'2008-07-11 00:00:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'2008-01-29 11:09:29',0.00000,0.00000,0,0,0,NULL,'-1','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2008-03-11 16:40:00','2008-03-11 16:40:00','-1','-1','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(17017,1081,5,'conf','','Item1','Item 1','','',NULL,NULL,'',NULL,'Nr',NULL,'','20','',52.00000,78.50000,NULL,'2008-03-19 00:00:00',NULL,'',NULL,52.00000,78.50000,'2008-03-19 00:00:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,2,NULL,'','','',NULL,'','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2008-03-11 16:40:00','2008-03-19 00:00:00','2974:00','2982:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(17037,1092,5,'mForn','','Item1','Item 1','Item 1 Partner','',NULL,NULL,'',NULL,'Nr',NULL,'','001','',1.00000,1.00000,NULL,'2007-05-28 12:24:00','2016-04-14 15:48:00','',NULL,1.00000,1.50000,'2007-05-28 12:24:00',-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2015-11-10 17:13:15','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2007-05-28 12:24:00','2007-05-28 12:24:00','3270:00','3270:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,'2015-11-10 17:11:43'),(17160,1142,5,'conf','','11-33-44','','','',NULL,NULL,'',NULL,'',NULL,'','001','',10.00000,24.00000,NULL,'2008-05-24 00:00:00',NULL,'',NULL,0.00000,0.00000,NULL,-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,NULL,'22-05-2008','23','20.0','25.0','30.0','26.0','27','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2008-05-24 00:00:00','2008-05-24 00:00:00','2908:00','2908:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(17161,1143,5,'conf','','11-33-44','','','',NULL,NULL,'',NULL,'',NULL,'','001','',10.00000,201.00000,NULL,'2008-05-24 00:00:00',NULL,'',NULL,0.00000,0.00000,NULL,-1.00000,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,0,NULL,'25-05-2008','12-34-45','20.0','202.0','30.0','203.0','45','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,NULL,NULL,NULL,0.00000,NULL,NULL,-1,0,-1,-1.00000,'2008-05-24 00:00:00','2008-05-24 00:00:00','2908:00','2908:00','-1',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',NULL,0,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1,NULL),(17172,1145,5,'prop','','8.7.1.12345','AN.T.LAB.P.BAUDSL MPO90x8x123','','','','','',NULL,'NR',NULL,'','00010/0001','',23.00000,0.99990,NULL,'2030-10-05 00:00:00','2016-03-15 13:33:39','',NULL,0.00000,0.00000,NULL,70.00000,'2016-02-22 10:05:42',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-02-17 15:29:58','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,100.00000,1.00000,'2016-02-17 15:29:53','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0148:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2015-12-14 10:05:26',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2015-12-14 10:05:43'),(17173,1145,5,'prop','','8.7.1.152125','AN.T.LAB.P.BAUDSL 12412','','','','','',NULL,'NR',NULL,'','00020/0001','',10.00000,100.00000,NULL,'2025-10-05 00:00:00','2016-03-15 13:33:39','',NULL,0.00000,0.00000,NULL,70.00000,'2016-02-22 10:05:42',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-02-17 15:29:43','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,'2015-12-14 10:05:28','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0148:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2015-12-14 10:05:26',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2015-12-14 10:05:43'),(17174,1145,5,'prop','','8.124.1.12345','AN.T.asf 123','','','','','',NULL,'NR',NULL,'','00030/0001','',250.00000,0.00200,NULL,'2025-10-05 00:00:00','2016-03-15 13:33:39','',NULL,0.00000,0.00000,NULL,70.00000,'2016-02-22 10:05:42',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-02-17 15:29:43','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1000.00000,1.00000,'2015-12-14 10:05:28','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0148:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2015-12-14 10:05:26',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2015-12-14 10:05:43'),(17175,1145,5,'prop','','8.7.1245.12345','AN.T.LAB.P.BAUDSL 12521','','','','','',NULL,'NR',NULL,'','00040/0001','',40.00000,54.34000,NULL,'2025-10-05 00:00:00','2016-03-15 13:33:39','',NULL,0.00000,0.00000,NULL,70.00000,'2016-02-22 10:05:42',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-02-17 15:29:43','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,'2015-12-14 10:05:29','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0148:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2015-12-14 10:05:26',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2015-12-14 10:05:43'),(17176,1146,5,'prop','','8.7.1.12345','AN.T.LAB.P.BAUDSL MPO90x8x123','','','','','',NULL,'NR',NULL,'','00010/0001','',40.00000,0.99990,NULL,'2025-10-05 00:00:00','2016-05-10 12:05:37','',NULL,0.00000,0.00000,NULL,70.00000,'2016-07-19 12:05:37',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-05-10 12:06:00','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,100.00000,1.00000,'2016-05-10 12:05:14','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0000:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2016-05-10 12:05:13',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2016-05-10 12:05:37'),(17177,1146,5,'prop','','8.7.1.152125','AN.T.LAB.P.BAUDSL 12412','','','','','',NULL,'NR',NULL,'','00020/0001','',10.00000,100.00000,NULL,'2025-10-05 00:00:00','2016-05-10 12:05:37','',NULL,0.00000,0.00000,NULL,70.00000,'2016-07-19 12:05:37',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-05-10 12:06:00','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,'2016-05-10 12:05:15','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0000:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2016-05-10 12:05:13',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2016-05-10 12:05:37'),(17178,1146,5,'prop','','8.124.1.12345','AN.T.asf 123','','','','','',NULL,'NR',NULL,'','00030/0001','',250.00000,0.00200,NULL,'2025-10-05 00:00:00','2016-05-10 12:05:37','',NULL,0.00000,0.00000,NULL,70.00000,'2016-07-19 12:05:37',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-05-10 12:06:00','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1000.00000,1.00000,'2016-05-10 12:05:15','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0000:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2016-05-10 12:05:13',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2016-05-10 12:05:37'),(17179,1146,5,'prop','','8.7.1245.12345','AN.T.LAB.P.BAUDSL 12521','','','','','',NULL,'NR',NULL,'','00040/0001','',40.00000,54.34000,NULL,'2025-10-05 00:00:00','2016-05-10 12:05:37','',NULL,0.00000,0.00000,NULL,70.00000,'2016-07-19 12:05:37',0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0.00000,0.00000,0,-1,1,'2016-05-10 12:06:00','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,1.00000,1.00000,'2016-05-10 12:05:15','',NULL,0.00000,'',NULL,-1,0,0,-1.00000,NULL,NULL,'-1','-1','0000:00',1.00000,1.00000,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,'2016-05-10 12:05:13',0,'','',0.00000,0.00000,0.00000,0.00000,-1,'2016-05-10 12:05:37');
/*!40000 ALTER TABLE `ordine_riga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_riga_attribute`
--

DROP TABLE IF EXISTS `ordine_riga_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_riga_attribute` (
  `ORDINE_RIGA_ATTRIBUTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_RIGA_ID` int(11) NOT NULL,
  `NAME` varchar(254) NOT NULL,
  `VALUE` mediumtext,
  `TYPE` varchar(15) DEFAULT 's',
  `DICTIONARY` int(11) DEFAULT '0',
  PRIMARY KEY (`ORDINE_RIGA_ATTRIBUTE_ID`),
  UNIQUE KEY `ORDINE_RIGA_ID_2` (`ORDINE_RIGA_ID`,`NAME`),
  KEY `ORDINE_RIGA_ID` (`ORDINE_RIGA_ID`),
  CONSTRAINT `ordine_riga_attribute_ibfk_1` FOREIGN KEY (`ORDINE_RIGA_ID`) REFERENCES `ordine_riga` (`ORDINE_RIGA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_riga_attribute`
--

LOCK TABLES `ordine_riga_attribute` WRITE;
/*!40000 ALTER TABLE `ordine_riga_attribute` DISABLE KEYS */;
INSERT INTO `ordine_riga_attribute` VALUES (1,17017,'sys_firstSend_date','2008-04-07','s',0),(2,17017,'sys_firstSend_time','17:38:51','s',0),(3,17160,'sys_firstSend_date','2008-09-11','s',0),(4,17160,'sys_firstSend_time','18:04:21','s',0),(5,16992,'sys_firstSend_date','2007-03-21','s',0),(6,16992,'sys_firstSend_time','00:00:00','s',0),(7,17161,'sys_firstSend_date','2009-01-27','s',0),(8,17161,'sys_firstSend_time','00:08:15','s',0),(9,16960,'sys_firstSend_date','2006-02-20','s',0),(10,16960,'sys_firstSend_time','00:00:00','s',0),(11,16961,'sys_firstSend_date','2006-02-20','s',0),(12,16961,'sys_firstSend_time','00:00:00','s',0),(13,16995,'sys_firstSend_date','2007-03-23','s',0),(14,16995,'sys_firstSend_time','00:00:00','s',0),(15,17001,'sys_firstSend_date','2008-04-07','s',0),(16,17001,'sys_firstSend_time','17:38:51','s',0),(17,17002,'sys_firstSend_date','2008-04-07','s',0),(18,17002,'sys_firstSend_time','17:38:51','s',0),(19,17037,'sys_firstSend_date','2008-04-07','s',0),(20,17037,'sys_firstSend_time','17:38:52','s',0),(41,17172,'sys_firstImport_time','14-12-2015 10:05:26','s',0),(42,17173,'sys_firstImport_time','14-12-2015 10:05:27','s',0),(43,17174,'sys_firstImport_time','14-12-2015 10:05:28','s',0),(44,17175,'sys_firstImport_time','14-12-2015 10:05:28','s',0),(45,17172,'sys_firstSend_date','2015-12-14','s',0),(46,17172,'sys_firstSend_time','10:05:42','s',0),(47,17173,'sys_firstSend_date','2015-12-14','s',0),(48,17173,'sys_firstSend_time','10:05:42','s',0),(49,17174,'sys_firstSend_date','2015-12-14','s',0),(50,17174,'sys_firstSend_time','10:05:42','s',0),(51,17175,'sys_firstSend_date','2015-12-14','s',0),(52,17175,'sys_firstSend_time','10:05:42','s',0),(53,17176,'sys_firstImport_time','10-05-2016 12:05:13','s',0),(54,17177,'sys_firstImport_time','10-05-2016 12:05:14','s',0),(55,17178,'sys_firstImport_time','10-05-2016 12:05:14','s',0),(56,17179,'sys_firstImport_time','10-05-2016 12:05:15','s',0),(57,17176,'sys_firstSend_date','2016-05-10','s',0),(58,17176,'sys_firstSend_time','12:05:37','s',0),(59,17177,'sys_firstSend_date','2016-05-10','s',0),(60,17177,'sys_firstSend_time','12:05:37','s',0),(61,17178,'sys_firstSend_date','2016-05-10','s',0),(62,17178,'sys_firstSend_time','12:05:37','s',0),(63,17179,'sys_firstSend_date','2016-05-10','s',0),(64,17179,'sys_firstSend_time','12:05:37','s',0);
/*!40000 ALTER TABLE `ordine_riga_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_riga_log`
--

DROP TABLE IF EXISTS `ordine_riga_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_riga_log` (
  `ORDINE_RIGA_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_RIGA_ID` int(11) NOT NULL,
  `ORDINE_WF_STATO_NOME` varchar(30) NOT NULL,
  `ORDINE_WF_EVENTO_NOME` varchar(30) NOT NULL,
  `USER_NAME` varchar(99) NOT NULL,
  `MOTIVO` varchar(254) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `MITTENTE` varchar(80) DEFAULT '',
  `QUANTITA` decimal(15,5) DEFAULT NULL,
  `PREZZO` decimal(12,5) DEFAULT NULL,
  `DATA_CONSEGNA` datetime DEFAULT NULL,
  `PROP_QUANTITA` decimal(15,5) DEFAULT NULL,
  `PROP_PREZZO` decimal(12,5) DEFAULT NULL,
  `PROP_DATA_CONSEGNA` datetime DEFAULT NULL,
  `PROP_SCONTI` varchar(60) DEFAULT NULL,
  `AUMENTO1` decimal(12,5) DEFAULT '0.00000',
  `AUMENTO2` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUMENTO1` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUMENTO2` decimal(12,5) DEFAULT '0.00000',
  `AUX1` varchar(99) DEFAULT NULL,
  `AUX2` varchar(99) DEFAULT NULL,
  `AUX3` varchar(99) DEFAULT NULL,
  `AUX4` varchar(99) DEFAULT NULL,
  `AUX5` varchar(99) DEFAULT NULL,
  `DATA_VISTO` datetime DEFAULT NULL,
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_DATE3` datetime DEFAULT NULL,
  `AUX_ROW_DATE4` datetime DEFAULT NULL,
  `AUX_ROW_DATE5` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE1` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE2` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE3` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE4` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE5` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `EMAIL_FILE` varchar(140) DEFAULT '',
  `FG_ALLINEAMENTO` int(11) DEFAULT '0',
  `DATA_SPEDIZIONE` datetime DEFAULT NULL,
  `AUX_ROW1` varchar(99) DEFAULT '',
  `AUX_ROW2` varchar(99) DEFAULT '',
  `AUX_ROW3` varchar(99) DEFAULT '',
  `AUX_ROW4` varchar(99) DEFAULT '',
  `AUX_ROW5` varchar(255) DEFAULT '',
  `AUX_ROW6` varchar(99) DEFAULT '',
  `AUX_ROW7` varchar(99) DEFAULT '',
  `AUX_ROW8` varchar(99) DEFAULT '',
  `AUX_ROW9` varchar(99) DEFAULT '',
  `AUX_ROW10` varchar(99) DEFAULT '',
  `NOTA` mediumtext,
  `PRODOTTO_DESCRIZIONE` varchar(1000) DEFAULT '',
  `DESTINAZIONE_INDIRIZZO` varchar(254) DEFAULT '',
  `FASE_DESCRIZIONE` varchar(254) DEFAULT '',
  `FORNITORE_PRODOTTO_CODICE` varchar(200) DEFAULT '',
  `FORNITORE_PRODOTTO_DESCRIZIONE` varchar(254) DEFAULT '',
  `VISTO_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `PROP_COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `PRODOTTO_CODICE` varchar(200) DEFAULT '',
  `IVA_PERC` decimal(12,5) DEFAULT '0.00000',
  `DOCUMENTAZIONE_CHANGES` mediumtext,
  `SUB_FORNITORE_ID` int(11) DEFAULT NULL,
  `SUB_FORNITORE_ENABLED` int(11) DEFAULT '0',
  `LEAD_TIME` decimal(12,5) DEFAULT '-1.00000',
  `PROP_LEAD_TIME` decimal(12,5) DEFAULT '-1.00000',
  `CAMBIO` decimal(12,5) DEFAULT '1.00000',
  `PROP_CAMBIO` decimal(12,5) DEFAULT '1.00000',
  `FASE_NUMERO` varchar(15) DEFAULT NULL,
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT NULL,
  `PREZZO_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `QUANTITA_UNITA_MISURA` varchar(100) DEFAULT NULL,
  `QUANTITA_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `VALUTA` varchar(30) DEFAULT NULL,
  `VALUTA_CODICE` varchar(3) DEFAULT NULL,
  `NUMERO_COMMESSA` varchar(100) DEFAULT NULL,
  `QUANTITA_CONSEGNATA_GESTIONALE` decimal(15,5) DEFAULT '0.00000',
  `DATA_ULTIMA_CONSEGNA_GESTIONALE` datetime DEFAULT NULL,
  `DATA_DISPONIBILITA` datetime DEFAULT NULL,
  `PARTNER_CONFIRMATION_NUMBER` varchar(50) DEFAULT NULL,
  `PARTNER_CONFIRMATION_DATE` datetime DEFAULT NULL,
  `NEXT_PARTNER_ID` int(11) DEFAULT NULL,
  `AUX_ROW11` varchar(99) DEFAULT '',
  `AUX_ROW12` varchar(99) DEFAULT '',
  `AUX_ROW13` varchar(99) DEFAULT '',
  `AUX_ROW14` varchar(99) DEFAULT '',
  `AUX_ROW15` varchar(99) DEFAULT '',
  `AUX_ROW16` mediumtext,
  `AUX_ROW17` mediumtext,
  `AUX_ROW18` mediumtext,
  `AUX_ROW19` mediumtext,
  `AUX_ROW20` mediumtext,
  `AUX_ROW_DATE6` datetime DEFAULT NULL,
  `AUX_ROW_DATE7` datetime DEFAULT NULL,
  `AUX_ROW_DATE8` datetime DEFAULT NULL,
  `AUX_ROW_DATE9` datetime DEFAULT NULL,
  `AUX_ROW_DATE10` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE6` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE7` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE8` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE9` datetime DEFAULT NULL,
  `PROP_AUX_ROW_DATE10` datetime DEFAULT NULL,
  `AUX_ROW_NUM6` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM7` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM8` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM9` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM10` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM6` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM7` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM8` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM9` decimal(12,5) DEFAULT '0.00000',
  `PROP_AUX_ROW_NUM10` decimal(12,5) DEFAULT '0.00000',
  `LV_VALUTA` varchar(3) DEFAULT NULL,
  `PROP_LV_VALUTA` varchar(3) DEFAULT NULL,
  `LV_TASSO` decimal(12,5) DEFAULT '0.00000',
  `PROP_LV_TASSO` decimal(12,5) DEFAULT '0.00000',
  `LV_VARIAZIONE` decimal(12,5) DEFAULT '0.00000',
  `PROP_LV_VARIAZIONE` decimal(12,5) DEFAULT '0.00000',
  `ORDINE_RIGA_TRANSPORT_LEAD_TIME` int(11) DEFAULT '-1',
  PRIMARY KEY (`ORDINE_RIGA_LOG_ID`),
  KEY `ORDINE_RIGA_ID` (`ORDINE_RIGA_ID`),
  KEY `ORDINE_WF_STATO_NOME` (`ORDINE_WF_STATO_NOME`),
  KEY `VISTO_NOTIFICA_LOG_RICEVUTA_ID` (`VISTO_NOTIFICA_LOG_RICEVUTA_ID`),
  KEY `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` (`MODFORN_NOTIFICA_LOG_RICEVUTA_ID`),
  CONSTRAINT `ordine_riga_log_ibfk_1` FOREIGN KEY (`ORDINE_RIGA_ID`) REFERENCES `ordine_riga` (`ORDINE_RIGA_ID`),
  CONSTRAINT `ordine_riga_log_ibfk_2` FOREIGN KEY (`ORDINE_WF_STATO_NOME`) REFERENCES `ordine_wf_stato` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=1224 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_riga_log`
--

LOCK TABLES `ordine_riga_log` WRITE;
/*!40000 ALTER TABLE `ordine_riga_log` DISABLE KEYS */;
INSERT INTO `ordine_riga_log` VALUES (458,16961,'crea','prConferma','turbine','','2006-02-20 12:55:13',NULL,1000.00000,2.00000,'2006-02-28 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(459,16960,'crea','prConferma','turbine','','2006-02-20 12:55:13',NULL,1000.00000,2.00000,'2006-02-22 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(516,16992,'crea','prProposta','turbine','','2007-03-21 17:31:10',NULL,25.00000,2.00000,'2007-03-21 17:27:00',25.00000,2.00000,'2007-03-28 17:27:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(517,16992,'prop','frModifica','turbine','','2007-03-21 17:31:32',NULL,25.00000,2.00000,'2007-03-21 17:27:00',25.00000,2.00000,'2007-03-28 17:27:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(518,16992,'mForn','frModifica','turbine','','2007-03-21 17:31:52',NULL,27.00000,2.00000,'2007-03-21 17:27:00',25.00000,2.00000,'2007-03-28 17:27:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(519,16992,'mForn','prConferma','turbine','','2007-03-21 17:32:11',NULL,27.00000,2.00000,'2007-03-21 17:27:00',25.00000,2.00000,'2007-03-28 17:27:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(525,16995,'crea','prProposta','turbine','','2007-03-23 10:09:16',NULL,10.00000,10.00000,'2007-03-30 10:07:00',10.00000,10.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(526,16995,'prop','prModifica','turbine','','2007-03-23 10:48:29',NULL,10.00000,10.00000,'2007-03-30 10:07:00',10.00000,10.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(527,16995,'prop','frModifica','sysemail','test','2007-03-23 10:59:21',NULL,10.00000,10.00000,'2007-03-30 10:07:00',10.00000,10.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(528,16995,'mForn','prModifica','turbine','','2007-03-23 11:09:06',NULL,10.00000,10.00000,'2007-03-30 10:07:00',110.00000,10.00000,'2009-06-03 10:07:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(529,16995,'prop','frConferma','sysemail','','2007-03-23 11:14:22',NULL,90.00000,10.00000,'2007-03-30 10:07:00',110.00000,10.00000,'2009-06-03 10:07:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(530,16995,'conf','prModifica','turbine','','2007-03-23 11:38:13',NULL,90.00000,10.00000,'2007-03-30 10:07:00',110.00000,10.00000,'2009-06-03 10:07:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(538,16995,'mForn','prModifica','Mattia','','2007-03-23 12:10:22',NULL,90.00000,10.00000,'2007-03-30 10:07:00',89.00000,10.00000,'2009-06-03 10:07:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(540,16995,'prop','prModifica','turbine','','2007-03-23 12:13:44',NULL,90.00000,10.00000,'2007-03-30 10:07:00',89.00000,10.00000,'2009-06-03 10:07:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(543,16996,'crea','prProposta','Mattia','','2007-03-23 14:53:50',NULL,0.00000,0.00000,'2007-03-30 12:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(546,16996,'mForn','prModifica','turbine','','2007-03-23 15:52:06',NULL,0.00000,0.00000,'2007-03-30 12:21:00',5.00000,10.00000,'2009-06-03 12:21:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(547,16996,'prop','prModifica','Mattia','','2007-03-23 16:37:57',NULL,0.00000,0.00000,'2007-03-30 12:21:00',5.00000,10.00000,'2009-06-03 12:21:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(550,16996,'mForn','prModifica','turbine','','2007-03-23 16:46:12',NULL,0.00000,0.00000,'2007-03-30 12:21:00',5.00000,5.00000,'2009-06-03 12:21:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(551,16996,'prop','prModifica','Mattia','','2007-03-23 16:48:59',NULL,0.00000,0.00000,'2007-03-30 12:21:00',5.00000,5.00000,'2009-06-03 12:21:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(555,17002,'crea','prProposta','turbine','','2007-03-27 10:41:14',NULL,23.00000,5.00000,'2007-04-11 10:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(556,17001,'crea','prProposta','turbine','','2007-03-27 10:41:14',NULL,55.00000,2.00000,'2007-04-11 10:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(557,17002,'crea','prProposta','turbine','','2007-03-27 13:13:07',NULL,23.00000,5.00000,'2007-04-11 10:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(558,17001,'crea','prProposta','turbine','','2007-03-27 13:13:08',NULL,55.00000,2.00000,'2007-04-11 10:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(559,17001,'prop','prModifica','turbine','test','2007-03-27 13:14:32',NULL,55.00000,2.00000,'2007-04-11 10:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(560,17002,'prop','frModifica','sysemail',' RIFIUTATA','2007-03-27 13:29:44',NULL,23.00000,5.00000,'2007-04-11 10:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(580,17001,'crea','prProposta','Mattia','','2007-03-28 12:42:40',NULL,55.00000,2.00000,'2007-04-11 10:21:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(581,17002,'crea','prProposta','Mattia','','2007-03-28 12:42:40',NULL,23.00000,5.00000,'2007-04-11 10:21:00',20.00000,5.00000,'2007-04-11 10:21:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(582,17002,'prop','frConferma','sysemail','','2007-03-28 12:45:57',NULL,23.00000,5.00000,'2007-04-11 10:21:00',20.00000,5.00000,'2007-04-11 10:21:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(613,17028,'crea','prProposta','turbine','','2007-04-02 12:38:27',NULL,50000.00000,0.70500,'2007-03-30 12:28:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(614,17029,'crea','prProposta','turbine','','2007-04-02 12:38:27',NULL,250.00000,1.90000,'2007-03-30 12:28:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(615,17030,'crea','prProposta','turbine','','2007-04-02 12:38:27',NULL,60000.00000,0.80500,'2007-03-30 12:28:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(625,17028,'conf','prChiusura','turbine','Auto Close','2007-04-02 13:11:06',NULL,50000.00000,0.70500,'2007-03-30 12:28:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(654,17037,'crea','prProposta','turbine','','2007-05-28 12:25:18',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(655,17037,'prop','prModifica','turbine','','2007-05-28 12:45:51',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(656,17037,'prop','prModifica','turbine','','2007-05-28 12:46:44',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(657,17037,'prop','prModifica','turbine','','2007-05-28 12:47:13',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(695,17016,'crea','prProposta','Utente','','2007-07-17 16:01:09',NULL,86.00000,50.34000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(696,17017,'crea','prProposta','Utente','','2007-07-17 16:01:09',NULL,46.00000,80.00000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(698,17016,'prop','prModifica','Utente','','2007-07-19 12:55:09',NULL,86.00000,50.34000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(703,17017,'conf','prModifica','Utente','','2007-07-23 10:29:50',NULL,46.00000,80.00000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(704,17017,'prop','prModifica','Utente','','2007-07-23 10:29:50',NULL,50.00000,79.00000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(705,17017,'prop','prModifica','Utente','','2007-07-23 10:29:53',NULL,50.00000,79.00000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(706,17016,'conf','prModifica','Utente','','2007-07-23 10:31:39',NULL,100.00000,50.34000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(709,17016,'prop','prModifica','Utente','reinvio','2007-08-04 12:15:34',NULL,100.00000,50.34000,'2008-03-11 16:40:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(713,17016,'mForn','frModifica','sysemail','errore','2007-08-04 12:19:57',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2008-08-15 16:40:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(714,17016,'mForn','frModifica','sysemail','','2007-08-04 12:22:25',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2007-10-18 16:40:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(990,17016,'mForn','prModifica','Utente','RIDATAZIONE PER RITARDO','2007-10-15 19:31:20',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2008-07-11 00:00:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(991,17016,'prop','prAnnulla','Utente','RIDATAZIONE PER RITARDO','2007-10-15 19:32:20',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2008-07-11 00:00:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(995,17017,'prop','prModifica','Utente','','2007-10-15 19:38:30',NULL,50.00000,78.50000,'2008-03-14 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1062,17160,'crea','prProposta','turbine','','2008-01-24 10:14:16',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1066,17037,'prop','prModifica','turbine','','2008-01-24 10:57:37',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1070,16992,'conf','prModifica','turbine','','2008-01-24 11:05:46',NULL,25.00000,2.00000,'2007-03-28 17:27:00',25.00000,2.00000,'2007-03-28 17:27:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1077,17160,'prop','prModifica','turbine','','2008-01-24 11:28:25',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1081,17160,'prop','prModifica','turbine','','2008-01-24 11:35:41',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1083,17160,'prop','prModifica','turbine','','2008-01-24 11:38:18',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1084,17161,'crea','prProposta','turbine','','2008-01-24 11:41:47',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1087,17161,'conf','prModifica','turbine','','2008-01-24 23:55:36',NULL,10.00000,20.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1088,17160,'prop','prModifica','turbine','','2008-01-24 23:55:38',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1093,17160,'conf','prChiude','turbine','','2008-01-25 00:02:07',NULL,10.00000,20.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1109,17161,'prop','prModifica','turbine','','2008-01-25 15:04:40',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1110,17160,'prop','prModifica','turbine','','2008-01-25 15:04:40',NULL,10.00000,0.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1116,17016,'annullato','prModifica','turbine','','2008-01-28 09:35:53',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2008-07-11 00:00:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1117,17016,'prop','prModifica','turbine','','2008-01-28 09:37:29',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2008-07-11 00:00:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1118,17017,'prop','prModifica','turbine','','2008-01-28 09:38:00',NULL,50.00000,78.50000,'2008-03-19 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1119,17016,'prop','prModifica','turbine','','2008-01-28 09:38:00',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2008-07-11 00:00:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1121,17016,'prop','prChiude','turbine','','2008-01-29 11:09:29',NULL,100.00000,50.34000,'2008-03-11 16:40:00',100.00000,50.34000,'2008-07-11 00:00:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1132,17161,'conf','prModifica','turbine','','2008-02-03 10:52:35',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1136,17037,'prop','prModifica','turbine','','2008-02-04 11:00:46',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1137,17037,'prop','prModifica','turbine','','2008-02-04 11:04:04',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1138,17037,'prop','prModifica','turbine','','2008-02-04 11:13:46',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1139,17037,'prop','prModifica','turbine','','2008-02-04 11:18:39',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1140,17037,'prop','prModifica','turbine','','2008-02-04 11:20:00',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1141,17037,'prop','prModifica','turbine','','2008-02-04 11:22:59',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1142,17037,'prop','prModifica','turbine','','2008-02-04 11:23:58',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1143,17037,'prop','prModifica','turbine','','2008-02-04 11:25:08',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1144,17037,'prop','prModifica','turbine','','2008-02-04 11:25:56',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1145,17037,'prop','prModifica','turbine','','2008-02-04 11:39:15',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1146,17037,'prop','prModifica','turbine','','2008-02-04 11:49:10',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1148,17037,'prop','prModifica','turbine','','2008-02-04 13:01:34',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1149,17161,'prop','prModifica','turbine','','2008-02-04 13:05:26',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1150,17161,'prop','prModifica','turbine','','2008-02-04 13:06:27',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1151,17160,'conf','prModifica','turbine','','2008-02-04 13:06:31',NULL,10.00000,50.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1160,17037,'prop','prConferma','turbine','','2008-02-05 15:17:40',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1161,17037,'conf','prModifica','turbine','','2008-02-05 15:18:00',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1162,17037,'prop','prModifica','turbine','','2008-04-03 12:07:07',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1163,17037,'prop','prModifica','turbine','','2008-04-03 12:11:48',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1164,17037,'prop','prModifica','turbine','','2008-04-03 12:14:42',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1165,17037,'prop','prModifica','turbine','','2008-04-03 12:16:54',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1166,17037,'prop','prModifica','turbine','','2008-04-07 17:26:57',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1167,17037,'prop','prModifica','turbine','','2008-04-07 17:30:47',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1168,17037,'prop','prModifica','turbine','','2008-04-07 17:36:39',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1169,17017,'prop','prModifica','turbine','','2008-04-07 17:36:50',NULL,50.00000,78.50000,'2008-03-19 00:00:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1170,17002,'conf','prModifica','turbine','','2008-04-07 17:37:00',NULL,23.00000,5.00000,'2007-04-11 10:21:00',20.00000,5.00000,'2007-04-11 10:21:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1171,17037,'prop','frModifica','sysemail','','2008-04-07 17:41:12',NULL,1.00000,1.00000,'2007-05-28 12:24:00',0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1172,17037,'mForn','prModifica','turbine','','2008-04-07 17:54:33',NULL,1.00000,1.00000,'2007-05-28 12:24:00',1.00000,2.00000,'2007-05-28 12:24:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1173,17037,'prop','prModifica','turbine','','2008-04-07 17:55:02',NULL,1.00000,1.00000,'2007-05-28 12:24:00',1.00000,2.00000,'2007-05-28 12:24:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1174,17037,'prop','frModifica','sysemail','','2008-04-07 17:55:52',NULL,1.00000,1.00000,'2007-05-28 12:24:00',1.00000,2.00000,'2007-05-28 12:24:00',NULL,0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1175,17160,'prop','prModifica','turbine','','2008-09-11 18:04:13',NULL,10.00000,50.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1176,17017,'prop','prModifica','turbine','','2008-09-11 18:06:15',NULL,50.00000,78.50000,'2008-03-19 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1177,17017,'prop','frModifica','sysemail','','2008-09-11 18:07:13',NULL,50.00000,78.50000,'2008-03-19 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1178,17017,'mForn','frModifica','sysemail','','2008-09-11 18:10:30',NULL,50.00000,78.50000,'2008-03-19 00:00:00',51.00000,78.50000,'2008-03-19 00:00:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1179,17160,'prop','prModifica','turbine','','2008-09-11 18:14:18',NULL,10.00000,50.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1182,17160,'conf','frConferma','sysemail','','2008-09-11 18:35:21',NULL,10.00000,50.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1183,17160,'conf','frConferma','sysemail','','2008-09-11 18:36:01',NULL,10.00000,50.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1184,17160,'conf','frConferma','sysemail','','2008-09-11 18:36:44',NULL,10.00000,51.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1185,17160,'conf','prModifica','turbine','','2008-09-11 22:55:39',NULL,10.00000,51.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1186,17160,'prop','prModifica','turbine','','2008-09-11 22:56:47',NULL,10.00000,51.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1187,17160,'prop','prModifica','turbine','','2008-09-11 22:58:43',NULL,10.00000,51.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1188,17160,'prop','prModifica','turbine','','2008-09-11 22:59:34',NULL,10.00000,51.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1191,17002,'prop','prModifica','turbine','','2009-01-26 17:47:29',NULL,23.00000,5.00000,'2007-04-11 10:21:00',20.00000,5.00000,'2007-04-11 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1192,17002,'prop','prModifica','turbine','','2009-01-26 17:48:42',NULL,25.00000,5.52000,'2009-06-11 00:00:00',20.00000,5.00000,'2007-04-11 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1193,17001,'mForn','frModifica','sysemail - fornitore@localmail','','2009-01-26 17:51:03',NULL,55.00000,2.00000,'2007-04-11 10:21:00',55.00000,2.00000,'2007-04-11 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1194,17002,'prop','frConferma','sysemail - fornitore@localmail','','2009-01-26 17:51:58',NULL,26.00000,5.53000,'2009-06-13 00:00:00',20.00000,5.00000,'2007-04-11 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1195,17001,'mForn','prConferma','turbine','','2009-01-26 17:52:18',NULL,55.00000,2.00000,'2007-04-11 10:21:00',55.32000,2.23000,'2009-04-14 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1196,17001,'conf','prModifica','turbine','','2009-01-26 18:13:12',NULL,55.32000,2.23000,'2009-04-14 10:21:00',55.32000,2.23000,'2009-04-14 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1197,17001,'prop','frModifica','sysemail - fornitore@localmail','','2009-01-26 18:14:31',NULL,55.32000,2.23000,'2009-04-14 10:21:00',55.32000,2.23000,'2009-04-14 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1198,17001,'mForn','frModifica','sysemail - fornitore@localmail','','2009-01-26 18:15:20',NULL,55.32000,2.23000,'2009-04-14 10:21:00',55.32000,2.52000,'2009-04-14 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1199,17161,'prop','prModifica','turbine','','2009-01-27 00:08:09',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1200,17161,'prop','prModifica','turbine','','2009-01-27 00:16:59',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1201,17161,'prop','prModifica','turbine','','2009-01-27 00:20:39',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1202,17161,'prop','prModifica','turbine','','2009-01-27 00:21:27',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1203,17161,'prop','prModifica','turbine','','2009-01-27 00:22:40',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1204,17161,'prop','prModifica','turbine','','2009-01-27 00:29:57',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1205,17161,'prop','prModifica','turbine','','2009-01-27 00:36:40',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1206,17161,'prop','prModifica','turbine','','2009-01-27 00:38:21',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1207,17161,'prop','prModifica','turbine','','2009-01-27 00:39:48',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1208,17161,'prop','prModifica','turbine','','2009-01-27 00:42:27',NULL,10.00000,200.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1209,17161,'prop','frConferma','sysemail','','2009-01-27 00:43:38',NULL,10.00000,201.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1210,17161,'conf','frConferma','sysemail','mia nota','2009-01-27 00:49:52',NULL,10.00000,201.00000,'2008-05-24 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1211,17017,'mForn','prConferma','turbine','','2009-01-28 16:31:21',NULL,50.00000,78.50000,'2008-03-19 00:00:00',52.00000,78.50000,'2008-03-19 00:00:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1212,17001,'mForn','prConferma','turbine','','2009-01-28 17:03:20',NULL,55.32000,2.23000,'2009-04-14 10:21:00',56.32000,2.23000,'2009-04-14 10:21:00','0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1.00000,1.00000,NULL,0.00000,NULL,NULL,0,-1.00000,-1.00000,1.00000,1.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,NULL,NULL,0.00000,0.00000,0.00000,0.00000,-1),(1215,17172,'crea','prProposta','sysScheduler','','2015-12-14 10:05:27','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',1,NULL,'','','','','','','','','','',NULL,'','','','','',0,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1216,17173,'crea','prProposta','sysScheduler','','2015-12-14 10:05:28','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',1,NULL,'','','','','','','','','','',NULL,'','','','','',0,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1217,17174,'crea','prProposta','sysScheduler','','2015-12-14 10:05:28','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',1,NULL,'','','','','','','','','','',NULL,'','','','','',0,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1218,17175,'crea','prProposta','sysScheduler','','2015-12-14 10:05:29','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',1,NULL,'','','','','','','','','','',NULL,'','','','','',0,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1219,17172,'prop','prModifica','turbine','','2016-02-17 15:29:53','',40.00000,0.99990,'2030-10-05 00:00:00',0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,'2016-02-17 15:29:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',0,'2016-02-17 15:29:24','','','','','','','','','','','','AN.T.LAB.P.BAUDSL MPO90x8x123','','','','',0,0,100.00000,1.00000,'8.7.1.12345',0.00000,'',0,0,70.00000,-1.00000,1.00000,1.00000,'','','','NR','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1220,17176,'crea','prProposta','sysScheduler','','2016-05-10 12:05:14','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',0,NULL,'','','','','','','','','','',NULL,'','','','','',6,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1221,17177,'crea','prProposta','sysScheduler','','2016-05-10 12:05:15','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',0,NULL,'','','','','','','','','','',NULL,'','','','','',6,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1222,17178,'crea','prProposta','sysScheduler','','2016-05-10 12:05:15','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',0,NULL,'','','','','','','','','','',NULL,'','','','','',6,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1),(1223,17179,'crea','prProposta','sysScheduler','','2016-05-10 12:05:15','',0.00000,0.00000,NULL,0.00000,0.00000,NULL,'0.0;0.0;0.0;0.0;0.0;0.0;0.0;0.0;',0.00000,0.00000,0.00000,0.00000,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'',0,NULL,'','','','','','','','','','',NULL,'','','','','',6,0,1.00000,1.00000,'',0.00000,'',0,0,-1.00000,-1.00000,1.00000,1.00000,'','','','','','','','',0.00000,NULL,NULL,'',NULL,0,'','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,0.00000,'','',0.00000,0.00000,0.00000,0.00000,-1);
/*!40000 ALTER TABLE `ordine_riga_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_riga_split`
--

DROP TABLE IF EXISTS `ordine_riga_split`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_riga_split` (
  `ORDINE_RIGA_SPLIT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_RIGA_ID` int(11) NOT NULL,
  `POSIZIONE` varchar(30) DEFAULT '',
  `QUANTITA` decimal(15,5) DEFAULT '0.00000',
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `DATA_CONSEGNA` datetime DEFAULT NULL,
  `NOTA` varchar(254) DEFAULT '',
  `FG_ESPORTATO` int(11) DEFAULT '0',
  `FG_SUPPLIER` int(11) NOT NULL DEFAULT '1',
  `USER_NAME` varchar(99) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `AUX_ROW1` varchar(99) DEFAULT '',
  `AUX_ROW2` varchar(99) DEFAULT '',
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `ORDINE_RIGA_LOG_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ORDINE_RIGA_SPLIT_ID`),
  KEY `ORDINE_RIGA_ID` (`ORDINE_RIGA_ID`),
  KEY `ORDINE_RIGA_LOG_ID` (`ORDINE_RIGA_LOG_ID`),
  CONSTRAINT `ordine_riga_split_ibfk_1` FOREIGN KEY (`ORDINE_RIGA_ID`) REFERENCES `ordine_riga` (`ORDINE_RIGA_ID`),
  CONSTRAINT `ordine_riga_split_ibfk_2` FOREIGN KEY (`ORDINE_RIGA_LOG_ID`) REFERENCES `ordine_riga_log` (`ORDINE_RIGA_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_riga_split`
--

LOCK TABLES `ordine_riga_split` WRITE;
/*!40000 ALTER TABLE `ordine_riga_split` DISABLE KEYS */;
/*!40000 ALTER TABLE `ordine_riga_split` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_tipo`
--

DROP TABLE IF EXISTS `ordine_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_tipo` (
  `ORDINE_TIPO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `FG_OUTBOUND` int(11) DEFAULT '1',
  `NOME` varchar(99) NOT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `ORDINE_WF_ID` int(11) NOT NULL,
  `AUX_HEAD1_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD1_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD2_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD2_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD3_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD3_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD4_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD4_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD5_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD5_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD6_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD6_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD7_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD7_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD8_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD8_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD9_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD9_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD10_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD10_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_DATE1_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_DATE1_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_DATE2_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_DATE2_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_DATE3_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_DATE3_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_DATE4_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_DATE4_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_DATE5_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_DATE5_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_NUM1_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_NUM1_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_NUM2_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_NUM2_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_NUM3_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_NUM3_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_NUM4_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_NUM4_TIPO` varchar(10) DEFAULT NULL,
  `AUX_HEAD_NUM5_DESCR` varchar(254) DEFAULT NULL,
  `AUX_HEAD_NUM5_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW1_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW1_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW2_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW2_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW3_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW3_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW4_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW4_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW5_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW5_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW6_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW6_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW7_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW7_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW8_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW8_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW9_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW9_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW10_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW10_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_DATE1_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_DATE1_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_DATE2_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_DATE2_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_DATE3_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_DATE3_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_DATE4_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_DATE4_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_DATE5_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_DATE5_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_NUM1_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_NUM1_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_NUM2_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_NUM2_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_NUM3_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_NUM3_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_NUM4_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_NUM4_TIPO` varchar(10) DEFAULT NULL,
  `AUX_ROW_NUM5_DESCR` varchar(254) DEFAULT NULL,
  `AUX_ROW_NUM5_TIPO` varchar(10) DEFAULT NULL,
  `EMAIL_ATTR` varchar(254) DEFAULT NULL,
  `AUX_ROW11_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW11_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW12_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW12_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW13_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW13_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW14_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW14_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW15_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW15_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW16_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW16_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW17_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW17_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW18_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW18_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW19_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW19_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW20_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW20_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_DATE6_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_DATE6_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_DATE7_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_DATE7_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_DATE8_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_DATE8_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_DATE9_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_DATE9_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_DATE10_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_DATE10_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_NUM6_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_NUM6_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_NUM7_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_NUM7_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_NUM8_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_NUM8_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_NUM9_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_NUM9_TIPO` varchar(10) DEFAULT '',
  `AUX_ROW_NUM10_DESCR` varchar(254) DEFAULT '',
  `AUX_ROW_NUM10_TIPO` varchar(10) DEFAULT '',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ORDINE_TIPO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `ORDINE_WF_ID` (`ORDINE_WF_ID`),
  CONSTRAINT `ordine_tipo_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `ordine_tipo_ibfk_2` FOREIGN KEY (`ORDINE_WF_ID`) REFERENCES `ordine_wf` (`ORDINE_WF_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_tipo`
--

LOCK TABLES `ordine_tipo` WRITE;
/*!40000 ALTER TABLE `ordine_tipo` DISABLE KEYS */;
INSERT INTO `ordine_tipo` VALUES (11,5,1,'OA','ORDINE DI ACQUISTO',1,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(13,5,2,'RDO','RICHIESTA DI OFFERTA',4,'Termine Offerta','DEFINITO','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','N. Offerta Fornitore','DEFINITO','Codice Fornitore','DEFINITO','Quantita\' Richiesta 1','DEFINITO','Prezzo Quantita\' Richiesta 1','DEFINITO','Quantita\' Richiesta 2','DEFINITO','Prezzo Quantita\' Richiesta 2','DEFINITO','Giorni Consegna Lavorativi','DEFINITO','','DEFINITO','','DEFINITO','','DEFINITO','','','','','','','','','','','','','','','','','','','','',NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(17,5,0,'OV','ORDINE DI VENDITA',5,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(20,5,1,'CALL-OFF','ORDINE DI CONSEGNA',1,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(21,5,5,'Product_COM','Comunicazione per Produzione',6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'sys_email_produzione','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(22,5,5,'Generic_COM','COMUNICAZIONE GENERICA',6,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(23,5,6,'RDA','RICHIESTA DI ACQUISTO',10,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(24,5,9,'ORIGIN','Dichiarazione di origine',8,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL),(25,5,20,'DDT','Documento di Trasporto',9,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(26,5,1,'OA-SPLIT','Ordine di Acquisto Splittabile',15,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ordine_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_tipo_categoria`
--

DROP TABLE IF EXISTS `ordine_tipo_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_tipo_categoria` (
  `ORDINE_TIPO_CATEGORIA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPO` int(11) NOT NULL DEFAULT '1',
  `NOME` varchar(99) DEFAULT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `POSIZIONE` int(11) DEFAULT '10',
  `LANG_CODE` varchar(50) DEFAULT NULL,
  `SUFFIX_LANG` varchar(25) DEFAULT NULL,
  `PERMISSION` varchar(30) DEFAULT NULL,
  `SHOW_DDT` int(11) DEFAULT '0',
  `SHOW_OTHER` int(11) DEFAULT '0',
  PRIMARY KEY (`ORDINE_TIPO_CATEGORIA_ID`),
  UNIQUE KEY `TIPO` (`TIPO`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_tipo_categoria`
--

LOCK TABLES `ordine_tipo_categoria` WRITE;
/*!40000 ALTER TABLE `ordine_tipo_categoria` DISABLE KEYS */;
INSERT INTO `ordine_tipo_categoria` VALUES (1,1,NULL,NULL,10,'app.ordineTipo.FgOutbound_true','','menu_ordini_out',1,0),(2,0,NULL,NULL,20,'app.ordineTipo.FgOutbound_false','vendite.','menu_ordini_in',1,0),(3,2,NULL,NULL,30,'app.ordineTipo.RdO','rdo.','menu_ordini_rdo',0,0),(4,3,NULL,NULL,40,'app.ordineTipo.Previsionale','Previsionale.','menu_previsionale',0,0),(5,4,NULL,NULL,50,'app.ordineTipo.ContoLavoro','ContoLavoro.','menu_ordini_contoLavoro',1,0),(6,5,NULL,NULL,60,'app.ordineTipo.Documentazione','DocumentazioneBox.','menu_documentazione',0,0),(7,6,NULL,NULL,70,'app.ordineTipo.Rda','rda.','menu_rda',0,0),(8,7,NULL,NULL,80,'app.ordineTipo.DocNonConformita','DocNonConformita.','menu_ordini_NonConformita',1,0),(9,8,NULL,NULL,90,'app.ordineTipo.Refilling','Refilling.','menu_refilling',0,0),(10,9,NULL,NULL,100,'app.ordineTipo.DeclarationOrigin','DeclarationOrigin.','menu_DeclarationOrigin',0,0),(11,10,NULL,NULL,110,'app.ordineTipo.Homepage','Homepage.','menu_homepage',0,0),(12,11,NULL,NULL,120,'app.ordineTipo.Fatture_Att','Fatture_Att.','invoice_read',0,0),(13,12,NULL,NULL,130,'app.ordineTipo.Fatture_Pass','Fatture_Pass.','invoice_read',0,0),(14,13,NULL,NULL,140,'app.ordineTipo.Giacenze','Giacenze.','stock_read',0,0),(15,14,NULL,NULL,150,'app.ordineTipo.Prodotti','Prodotti.','product_read',0,0),(16,15,NULL,NULL,160,'app.ordineTipo.Transcodifica','Transcodifica.','item_partner_read',0,0),(17,16,NULL,NULL,170,'app.ordineTipo.Listini','Listini.','product_listino_read',0,0),(18,17,NULL,NULL,180,'app.ordineTipo.Processi','Processi.','product_batch_formula_read',0,0),(19,18,NULL,NULL,190,'app.ordineTipo.Qualificazione_Fornitori','Qualificazione_Fornitori.','fornitore_valutazione_read',0,0),(20,19,NULL,NULL,200,'app.ordineTipo.Triangolazione','Triangolazione.','menu_triangolazione',0,0),(21,20,NULL,NULL,210,'app.ordineTipo.DDT','DDT.','menu_ddt',0,0),(22,22,NULL,NULL,220,'app.ordineTipo.Albo','Albo.','fornitore_read',0,0),(23,23,NULL,NULL,230,'app.ordineTipo.AsyncOperation','AsyncOperation.','menu_async_operation',0,0);
/*!40000 ALTER TABLE `ordine_tipo_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_wf`
--

DROP TABLE IF EXISTS `ordine_wf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_wf` (
  `ORDINE_WF_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(30) NOT NULL,
  `ORDINE_WF_STATO_INIZIALE` varchar(30) DEFAULT NULL,
  `ORDINE_WF_EVENTO_ON_NUOVO` varchar(30) DEFAULT NULL,
  `ORDINE_WF_EVENTO_ON_MODIFICA` varchar(30) DEFAULT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `ORDINE_WF_STATI_BLOCCO_EMAIL` varchar(150) DEFAULT NULL,
  `ORDINE_WF_EVENTO_RIAPERTURA_ANNULLATO` varchar(30) DEFAULT NULL,
  `ORDINE_WF_EVENTO_RIAPERTURA_CHIUSO` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ORDINE_WF_ID`),
  UNIQUE KEY `NOME` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_wf`
--

LOCK TABLES `ordine_wf` WRITE;
/*!40000 ALTER TABLE `ordine_wf` DISABLE KEYS */;
INSERT INTO `ordine_wf` VALUES (1,'simple','crea','prProposta','prModifica','WF OA',NULL,NULL,NULL),(4,'offers','crea','prProposta','prModifica','Richiesta di Offerta',NULL,NULL,NULL),(5,'sales','crea','prProposta','prModifica','Sales order',NULL,NULL,NULL),(6,'WF_NO_WORKFLOW','crea','','','Nessun Workflow',NULL,NULL,NULL),(7,'WF_RDO_RDA','crea','prProposta','prModifica','WF RDO da RDA',NULL,NULL,NULL),(8,'WF_ORIGIN','crea','prProposta','prModifica','WF_ORIGIN',NULL,NULL,NULL),(9,'ddtWf','crea','prProposta','prModifica','ddtWf','','',''),(10,'rdaWf','crea','prAttiva','','rdaWf','','',''),(11,'alboFornitoriWF','crea','prProposta','','alboFornitoriWF','','',''),(12,'WF_SA','crea','prProposta','prModifica','WF_SA','','',''),(13,'WF_ASYNC_OP','crea','prAsyncOpRun','','Operazioni Asincrone','','',''),(15,'WF_SIMPLE_SPLIT','crea','prProposta','prModifica','WF OA SPLIT','','','');
/*!40000 ALTER TABLE `ordine_wf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_wf_evento`
--

DROP TABLE IF EXISTS `ordine_wf_evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_wf_evento` (
  `ORDINE_WF_EVENTO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(30) NOT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`ORDINE_WF_EVENTO_ID`),
  UNIQUE KEY `NOME` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_wf_evento`
--

LOCK TABLES `ordine_wf_evento` WRITE;
/*!40000 ALTER TABLE `ordine_wf_evento` DISABLE KEYS */;
INSERT INTO `ordine_wf_evento` VALUES (1,'frConferma','Fornitore: conferma proposta prime'),(2,'frModifica','Fornitore: modifica ordine'),(4,'prAnnulla','Prime: annullamento ordine'),(5,'prChiude','Prime: forza chiusura ordine'),(6,'prChiusura','Prime: chiusura ordine'),(7,'prConferma','Prime: conferma proposta fornitore'),(8,'prModifica','Prime: modifica ordine'),(9,'prProposta','Prime: invio proposta ordine'),(10,'prErroreBolla','Prime: anullamento chiusura'),(11,'prDaCertificare','Prime: richiesta di certificazione'),(12,'prAccettato','Prime: Accettato DDT'),(13,'prAttiva','Prime: Attiva RdaRiga'),(14,'prCompleta','Prime: completato RDA'),(15,'prInRda','Prime: RdaRiga collegata a RDA'),(16,'prInRdo','Prime: generato RDO'),(17,'prInScadenza','Iungo: campo data in scadenza'),(18,'prAltraModifica','Prime: modifica di altro tipo'),(19,'prForecast','Prime: riga previsionale dello Scheduling Agreement'),(20,'prConfForecast','Prime: riga previsionale dello Scheduling Agreement confermata'),(21,'prAsyncOpDone','AsyncOp risposta di OK dal Thread'),(22,'prAsyncOpEnd','AsyncOp Finita Normalmente'),(23,'prAsyncOpError','AsyncOp Errore'),(24,'prAsyncOpInterrupt','AsyncOp Interrotta'),(25,'prAsyncOpKill','AsyncOp Killata'),(26,'prAsyncOpResume','AsyncOp Resume Running'),(27,'prAsyncOpRun','AsyncOp Mandata in esecuzione'),(28,'prAsyncOpSleep','AsyncOp Sleep'),(29,'prAsyncOpStop','AsyncOp richiesta di Kill manuale'),(30,'frSplit','Fornitore: proposta di Split'),(31,'prSplit','Prime: proposta di Split');
/*!40000 ALTER TABLE `ordine_wf_evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_wf_stato`
--

DROP TABLE IF EXISTS `ordine_wf_stato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_wf_stato` (
  `ORDINE_WF_STATO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(30) NOT NULL,
  `ORDINE` int(11) DEFAULT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `COLORE_HTML` varchar(10) DEFAULT NULL,
  `MOSTRA_VALORI_PROPOSTI` int(11) DEFAULT NULL,
  `TIPO` varchar(25) DEFAULT '',
  PRIMARY KEY (`ORDINE_WF_STATO_ID`),
  UNIQUE KEY `NOME` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_wf_stato`
--

LOCK TABLES `ordine_wf_stato` WRITE;
/*!40000 ALTER TABLE `ordine_wf_stato` DISABLE KEYS */;
INSERT INTO `ordine_wf_stato` VALUES (1,'crea',10,'Creazione Ordine','#E0E0E0',0,'unknown'),(2,'prop',80,'Ordine proposto','#FFFF00',0,'prime_proposal'),(4,'mForn',180,'Ordine modificato dal Fornitore','#FF0000',1,'partner_modify'),(5,'conf',200,'Ordine confermato','#A0FFA0',0,'confirmed'),(6,'chiuso',220,'Ordine Ricevuto e chiuso','#0000FF',0,'closed'),(7,'annullato',230,'Ordine annullato','#505050',0,'cancelled'),(8,'daCertificare',40,'In attesa di certificato firmato','#FFB3E8',0,'unknown'),(9,'sped',30,'Spedito','#FFFFCC',0,'unknown'),(10,'ric',100,'Ricevuto','#FFA000',0,'unknown'),(11,'acc',120,'Accettato','#A0FFA0',0,'unknown'),(12,'out',170,'Consegnato','#A0FFC0',0,'unknown'),(13,'inRda',20,'Entrato RDA','#FFFF00',0,'unknown'),(14,'attivo',70,'Attivo','#FFFFCC',0,'unknown'),(15,'inRdo',140,'Generato Rdo','#A0FFA0',0,'unknown'),(16,'completato',160,'Completato','#00FF33',0,'unknown'),(17,'forecast',60,'Riga previsionale dello Scheduling Agreement','#B8F5FF',0,'unknown'),(18,'annullatoForecast',90,'Riga previsionale dello Scheduling Agreement annullata','#2E7365',0,'unknown'),(19,'AsyncOpRun',50,'AsyncOp in esecuzione','#F0FF17',0,'unknown'),(20,'AsyncOpTerminated',110,'AsyncOp terminata con successo','#4FFF0F',0,'unknown'),(21,'AsyncOpError',130,'AsyncOp terminata con errore','#FF2D0D',0,'unknown'),(22,'AsyncOpPleaseInterrupt',150,'AsyncOp richiesta di interruzione','#A6A6A6',0,'unknown'),(23,'AsyncOpInterrupted',190,'AsyncOp Interrotta','#3B3B3B',0,'unknown'),(24,'AsyncOpPleaseSleep',210,'AsyncOp richiesta di attesa','#08BDFF',0,'unknown'),(25,'AsyncOpSleep',240,'AsyncOp in attesa','#0808FF',0,'unknown'),(26,'AsyncOpKill',250,'AsyncOp kill','#FFBFFF',0,'unknown'),(27,'AsyncOpKilled',260,'AsyncOp termitata con kill','#FF2EF1',0,'unknown'),(28,'AsyncOpResume',270,'AsyncOp richiesta risveglio','#FFFF8F',0,'unknown'),(29,'split',280,'Split del Fornitore','#FF0099',1,'partner_modify'),(30,'controSplit',290,'Split del cliente','#00E5FF',0,'prime_proposal');
/*!40000 ALTER TABLE `ordine_wf_stato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_wf_transazione`
--

DROP TABLE IF EXISTS `ordine_wf_transazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_wf_transazione` (
  `ORDINE_WF_TRANSAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_WF_NOME` varchar(30) NOT NULL,
  `ORDINE_WF_EVENTO_NOME` varchar(30) NOT NULL,
  `PERMISSION` varchar(30) NOT NULL,
  `STATO_INIZIALE_NOME` varchar(30) NOT NULL,
  `STATO_FINALE_NOME` varchar(30) NOT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `ORDINE` int(11) NOT NULL,
  `MODO` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ORDINE_WF_TRANSAZIONE_ID`),
  KEY `ORDINE_WF_NOME` (`ORDINE_WF_NOME`),
  KEY `ORDINE_WF_EVENTO_NOME` (`ORDINE_WF_EVENTO_NOME`),
  KEY `STATO_INIZIALE_NOME` (`STATO_INIZIALE_NOME`),
  KEY `STATO_FINALE_NOME` (`STATO_FINALE_NOME`),
  CONSTRAINT `ordine_wf_transazione_ibfk_1` FOREIGN KEY (`ORDINE_WF_NOME`) REFERENCES `ordine_wf` (`NOME`),
  CONSTRAINT `ordine_wf_transazione_ibfk_2` FOREIGN KEY (`ORDINE_WF_EVENTO_NOME`) REFERENCES `ordine_wf_evento` (`NOME`),
  CONSTRAINT `ordine_wf_transazione_ibfk_3` FOREIGN KEY (`STATO_INIZIALE_NOME`) REFERENCES `ordine_wf_stato` (`NOME`),
  CONSTRAINT `ordine_wf_transazione_ibfk_4` FOREIGN KEY (`STATO_FINALE_NOME`) REFERENCES `ordine_wf_stato` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=751 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_wf_transazione`
--

LOCK TABLES `ordine_wf_transazione` WRITE;
/*!40000 ALTER TABLE `ordine_wf_transazione` DISABLE KEYS */;
INSERT INTO `ordine_wf_transazione` VALUES (595,'alboFornitoriWF','frModifica','wf_chiuso_mForn_albo','chiuso','chiuso','Il fornitore tenta una modifica ma il documento e\' chiuso',10,1),(596,'alboFornitoriWF','prChiude','wf_conf_chiuso_albo','conf','chiuso','app.alboFornitori.transazioni.prChiude',20,0),(597,'alboFornitoriWF','frModifica','wf_conf_mForn_albo','conf','conf','Il fornitore tenta una modifica ma il documento e\' confermato',30,1),(598,'alboFornitoriWF','prInScadenza','wf_conf_prop_albo','conf','prop','Documento con un campo data in scadenza re-inviato automaticamente al fornitore',40,1),(599,'alboFornitoriWF','frModifica','wf_prop_mForn_albo','crea','mForn','Documento inviato a fornitore e da questi modificato, ma che non ha completato con successo la transizione a prop',50,1),(600,'alboFornitoriWF','prProposta','wf_crea_prop_albo','crea','prop','app.alboFornitori.transazioni.prProposta',60,0),(601,'alboFornitoriWF','prConferma','wf_mForn_conf_albo','mForn','conf','app.alboFornitori.transazioni.prConferma',70,0),(602,'alboFornitoriWF','frModifica','wf_mForn_mForn_albo','mForn','mForn','app.alboFornitori.transazioni.frModifica',80,0),(603,'alboFornitoriWF','prAltraModifica','wf_mForn_mForn_pr_albo','mForn','mForn','app.alboFornitori.transazioni.prAltraModifica',90,0),(604,'alboFornitoriWF','prModifica','wf_mForn_prop_albo','mForn','prop','app.alboFornitori.transazioni.prModifica',100,0),(605,'alboFornitoriWF','frModifica','wf_prop_mForn_albo','prop','mForn','app.alboFornitori.transazioni.frModifica',110,0),(606,'alboFornitoriWF','prAltraModifica','wf_prop_prop_albo','prop','prop','app.alboFornitori.transazioni.prAltraModifica',120,0),(607,'alboFornitoriWF','prModifica','wf_prop_prop_albo','prop','prop','app.alboFornitori.transazioni.prModifica',130,0),(608,'ddtWf','prAnnulla','wf_ddt_annullato','acc','annullato','',10,0),(609,'ddtWf','prConferma','wf_ddt_consegnato','acc','out','',20,0),(610,'ddtWf','prModifica','wf_ddt_ricevuto','crea','ric','',30,0),(611,'ddtWf','prProposta','wf_ddt_spedito','crea','sped','',40,0),(612,'ddtWf','prAnnulla','wf_conf_prop','out','annullato','',50,0),(613,'ddtWf','prAccettato','wf_ddt_accettato','ric','acc','',60,0),(614,'ddtWf','prAnnulla','wf_ddt_annullato','ric','annullato','',70,0),(615,'ddtWf','prAnnulla','wf_ddt_annullato','sped','annullato','',80,0),(616,'ddtWf','prModifica','wf_ddt_ricevuto','sped','ric','',90,0),(617,'offers','prAnnulla','wf_prop_annullato','conf','annullato','Riga Offerta Annullata',10,0),(618,'offers','prChiude','wf_conf_chiuso','conf','chiuso','Accettazione Offerta',20,0),(619,'offers','frConferma','wf_mForn_mForn','conf','conf','Risposta Fornitore',30,0),(620,'offers','prModifica','wf_mForn_prop','conf','prop','Contro Proposta',40,0),(621,'offers','prProposta','wf_crea_prop','crea','prop','Nuova Richiesta al Fornitore',50,0),(622,'offers','prAnnulla','wf_prop_annullato','prop','annullato','Riga Offerta Annullata',60,0),(623,'offers','frConferma','wf_prop_mForn','prop','conf','Risposta Fornitore',70,0),(624,'offers','prModifica','wf_prop_prop','prop','prop','Modifica Offerta',80,0),(625,'rdaWf','prAnnulla','wf_rda_annullato','attivo','annullato','',10,0),(626,'rdaWf','prInRda','wf_rda_inRDA','attivo','inRda','',20,0),(627,'rdaWf','prAttiva','wf_rda_attivo','crea','attivo','',30,0),(628,'rdaWf','prAnnulla','wf_rda_annullato','inRda','annullato','',40,0),(629,'rdaWf','prInRdo','wf_rda_inRDO','inRda','inRdo','',50,0),(630,'rdaWf','prAnnulla','wf_rda_annullato','inRdo','annullato','',60,0),(631,'rdaWf','prCompleta','wf_rda_completato','inRdo','completato','',70,0),(632,'sales','prAnnulla','wf_customer_chiuso_annullato','chiuso','annullato','Riga Ordine annullata',10,0),(633,'sales','prAnnulla','wf_customer_conf_annullato','conf','annullato','Riga Ordine Annullata',20,0),(634,'sales','prChiude','wf_customer_conf_chiuso','conf','chiuso','Riga Ordine Chiusa',30,0),(635,'sales','prConferma','wf_customer_crea_conf','conf','conf','Aggiorna Riga Ordine',40,0),(636,'sales','prAnnulla','wf_customer_crea_annullato','crea','annullato','Riga Ordine Annullata',50,0),(637,'sales','prConferma','wf_customer_crea_conf','crea','conf','Conferma Ordine',60,0),(638,'simple','prModifica','wf_annullato_prop','annullato','prop','Riga Riproposta',10,0),(639,'simple','prErroreBolla','wf_chiuso_conf','chiuso','conf','Annullamento Chiusura',20,0),(640,'simple','prModifica','wf_chiuso_conf','chiuso','conf','Annullamento Chiusura',30,0),(641,'simple','prAnnulla','wf_prop_annullato','conf','annullato','Riga Ordine Annullata',40,0),(642,'simple','prChiude','wf_conf_chiuso','conf','chiuso','Forza la chiusura riga ordine',50,0),(643,'simple','frModifica','wf_conf_mForn','conf','mForn','Modifica Fornitore',60,0),(644,'simple','prModifica','wf_conf_prop','conf','prop','Proposta Modifica',70,0),(645,'simple','prAnnulla','wf_prop_annullato','crea','annullato','Forza Annullamento Riga',80,0),(646,'simple','prChiude','wf_conf_chiuso','crea','chiuso','Forza la chiusura riga ordine',90,0),(647,'simple','prProposta','wf_crea_prop','crea','prop','Nuovo Ordine al Fornitore',100,0),(648,'simple','prAnnulla','wf_prop_annullato','mForn','annullato','Riga Ordine Annullata',110,0),(649,'simple','prChiude','wf_mForn_chiuso','mForn','chiuso','Forza la chiusura riga ordine',120,0),(650,'simple','prConferma','wf_mForn_conf','mForn','conf','Modifica Accettata',130,0),(651,'simple','frModifica','wf_mForn_mForn','mForn','mForn','Modifica Fornitore',140,0),(652,'simple','prModifica','wf_mForn_prop','mForn','prop','Contro Proposta',150,0),(653,'simple','prAnnulla','wf_prop_annullato','prop','annullato','Riga Ordine Annullata',160,0),(654,'simple','prChiude','wf_prop_chiuso','prop','chiuso','Forza la chiusura riga ordine',170,0),(655,'simple','frConferma','wf_prop_conf','prop','conf','Conferma del Fornitore',180,0),(656,'simple','frModifica','wf_prop_mForn','prop','mForn','Modifica Fornitore',190,0),(657,'simple','prModifica','wf_prop_prop','prop','prop','Modifica Proposta',200,0),(658,'WF_SIMPLE_SPLIT','prSplit','wf_conf_prop','conf','controSplit','Proposta Split di Riga',10,1),(659,'WF_SIMPLE_SPLIT','frSplit','wf_conf_mForn','conf','split','Proposta Split di Riga',20,1),(660,'WF_SIMPLE_SPLIT','prChiude','wf_prop_chiuso','controSplit','chiuso','Forza la chiusura riga ordine',30,0),(661,'WF_SIMPLE_SPLIT','frConferma','wf_prop_conf','controSplit','conf','Conferma del Fornitore',40,0),(662,'WF_SIMPLE_SPLIT','prSplit','wf_prop_prop','controSplit','controSplit','Proposta Split di Riga',50,1),(663,'WF_SIMPLE_SPLIT','prModifica','wf_prop_prop','controSplit','prop','Proposta Modifica',60,0),(666,'WF_SIMPLE_SPLIT','frSplit','wf_prop_mForn','controSplit','split','Proposta Split di Riga',90,1),(667,'WF_SIMPLE_SPLIT','prSplit','wf_mForn_prop','mForn','controSplit','Proposta Split di Riga',100,1),(668,'WF_SIMPLE_SPLIT','prSplit','wf_prop_prop','prop','controSplit','Proposta di Split',110,1),(669,'WF_SIMPLE_SPLIT','frSplit','wf_prop_mForn','prop','split','Proposta Split di Riga',120,1),(670,'WF_SIMPLE_SPLIT','prChiude','wf_mForn_chiuso','split','chiuso','Forza la chiusura riga ordine',130,0),(671,'WF_SIMPLE_SPLIT','prConferma','wf_mForn_conf','split','conf','Modifica Accettata',140,0),(673,'WF_SIMPLE_SPLIT','prSplit','wf_mForn_prop','split','controSplit','Proposta Split di Riga',160,1),(674,'WF_SIMPLE_SPLIT','prModifica','wf_mForn_prop','split','prop','Contro Proposta',170,0),(677,'WF_SIMPLE_SPLIT','frSplit','wf_mForn_mForn','split','split','Proposta Split di Riga',200,1),(678,'WF_SIMPLE_SPLIT','prModifica','wf_annullato_prop','annullato','prop','Riga Riproposta',210,0),(679,'WF_SIMPLE_SPLIT','prErroreBolla','wf_chiuso_conf','chiuso','conf','Annullamento Chiusura',220,0),(680,'WF_SIMPLE_SPLIT','prModifica','wf_chiuso_conf','chiuso','conf','Annullamento Chiusura',230,0),(681,'WF_SIMPLE_SPLIT','prAnnulla','wf_prop_annullato','conf','annullato','Riga Ordine Annullata',240,0),(682,'WF_SIMPLE_SPLIT','prChiude','wf_conf_chiuso','conf','chiuso','Forza la chiusura riga ordine',250,0),(683,'WF_SIMPLE_SPLIT','frModifica','wf_conf_mForn','conf','mForn','Modifica Fornitore',260,0),(684,'WF_SIMPLE_SPLIT','prModifica','wf_conf_prop','conf','prop','Proposta Modifica',270,0),(685,'WF_SIMPLE_SPLIT','prAnnulla','wf_prop_annullato','crea','annullato','Forza Annullamento Riga',280,0),(686,'WF_SIMPLE_SPLIT','prChiude','wf_conf_chiuso','crea','chiuso','Forza la chiusura riga ordine',290,0),(687,'WF_SIMPLE_SPLIT','prProposta','wf_crea_prop','crea','prop','Nuovo Ordine al Fornitore',300,0),(688,'WF_SIMPLE_SPLIT','prAnnulla','wf_prop_annullato','mForn','annullato','Riga Ordine Annullata',310,0),(689,'WF_SIMPLE_SPLIT','prChiude','wf_mForn_chiuso','mForn','chiuso','Forza la chiusura riga ordine',320,0),(690,'WF_SIMPLE_SPLIT','prConferma','wf_mForn_conf','mForn','conf','Modifica Accettata',330,0),(691,'WF_SIMPLE_SPLIT','frModifica','wf_mForn_mForn','mForn','mForn','Modifica Fornitore',340,0),(692,'WF_SIMPLE_SPLIT','prModifica','wf_mForn_prop','mForn','prop','Contro Proposta',350,0),(693,'WF_SIMPLE_SPLIT','prAnnulla','wf_prop_annullato','prop','annullato','Riga Ordine Annullata',360,0),(694,'WF_SIMPLE_SPLIT','prChiude','wf_prop_chiuso','prop','chiuso','Forza la chiusura riga ordine',370,0),(695,'WF_SIMPLE_SPLIT','frConferma','wf_prop_conf','prop','conf','Conferma del Fornitore',380,0),(696,'WF_SIMPLE_SPLIT','frModifica','wf_prop_mForn','prop','mForn','Modifica Fornitore',390,0),(697,'WF_SIMPLE_SPLIT','prModifica','wf_prop_prop','prop','prop','Modifica Proposta',400,0),(698,'WF_ASYNC_OP','prAsyncOpKill','wf_asyncop_killed','AsyncOpKill','AsyncOpKilled','Kill Eseguito',10,0),(699,'WF_ASYNC_OP','prAsyncOpError','wf_asyncop_error','AsyncOpPleaseInterrupt','AsyncOpError','Operazione in Errore',20,0),(700,'WF_ASYNC_OP','prAsyncOpDone','wf_asyncop_interrupted','AsyncOpPleaseInterrupt','AsyncOpInterrupted','Interruzione Eseguita',30,0),(701,'WF_ASYNC_OP','prAsyncOpStop','wf_asyncop_kill','AsyncOpPleaseInterrupt','AsyncOpKill','Richiesta di Kill',40,0),(702,'WF_ASYNC_OP','prAsyncOpKill','wf_asyncop_killed','AsyncOpPleaseInterrupt','AsyncOpKilled','Kill Eseguito',50,0),(703,'WF_ASYNC_OP','prAsyncOpError','wf_asyncop_error','AsyncOpPleaseSleep','AsyncOpError','Operazione in Errore',60,0),(704,'WF_ASYNC_OP','prAsyncOpStop','wf_asyncop_kill','AsyncOpPleaseSleep','AsyncOpKill','Richiesta di Kill',70,0),(705,'WF_ASYNC_OP','prAsyncOpKill','wf_asyncop_killed','AsyncOpPleaseSleep','AsyncOpKilled','Kill Eseguito',80,0),(706,'WF_ASYNC_OP','prAsyncOpDone','wf_asyncop_sleep','AsyncOpPleaseSleep','AsyncOpSleep','Sleep Eseguito',90,0),(707,'WF_ASYNC_OP','prAsyncOpError','wf_asyncop_error','AsyncOpResume','AsyncOpError','Operazione in Errore',100,0),(708,'WF_ASYNC_OP','prAsyncOpStop','wf_asyncop_kill','AsyncOpResume','AsyncOpKill','Richiesta di Kill',110,0),(709,'WF_ASYNC_OP','prAsyncOpKill','wf_asyncop_killed','AsyncOpResume','AsyncOpKilled','Kill Eseguito',120,0),(710,'WF_ASYNC_OP','prAsyncOpDone','wf_asyncop_run','AsyncOpResume','AsyncOpRun','Resume Eseguito',130,0),(711,'WF_ASYNC_OP','prAsyncOpError','wf_asyncop_error','AsyncOpRun','AsyncOpError','Operazione in Errore',140,0),(712,'WF_ASYNC_OP','prAsyncOpStop','wf_asyncop_kill','AsyncOpRun','AsyncOpKill','Richiesta di Kill',150,0),(713,'WF_ASYNC_OP','prAsyncOpKill','wf_asyncop_killed','AsyncOpRun','AsyncOpKilled','Kill Eseguito',160,0),(714,'WF_ASYNC_OP','prAsyncOpInterrupt','wf_asyncop_pleaseinterrupt','AsyncOpRun','AsyncOpPleaseInterrupt','Richiesta di Interruzione',170,0),(715,'WF_ASYNC_OP','prAsyncOpSleep','wf_asyncop_pleasesleep','AsyncOpRun','AsyncOpPleaseSleep','Richiesta di Sleep',180,0),(716,'WF_ASYNC_OP','prAsyncOpDone','wf_asyncop_terminated','AsyncOpRun','AsyncOpTerminated','Operazione Terminata',190,0),(717,'WF_ASYNC_OP','prAsyncOpStop','wf_asyncop_kill','AsyncOpSleep','AsyncOpKill','Richiesta di Kill',200,0),(718,'WF_ASYNC_OP','prAsyncOpKill','wf_asyncop_killed','AsyncOpSleep','AsyncOpKilled','Kill Eseguito',210,0),(719,'WF_ASYNC_OP','prAsyncOpInterrupt','wf_asyncop_pleaseinterrupt','AsyncOpSleep','AsyncOpPleaseInterrupt','Richiesta di Interruzione',220,0),(720,'WF_ASYNC_OP','prAsyncOpResume','wf_asyncop_resume','AsyncOpSleep','AsyncOpResume','Richiesta di Resume',230,0),(721,'WF_ASYNC_OP','prAsyncOpRun','wf_asyncop_create','crea','AsyncOpRun','In Esecuzione',240,0),(722,'WF_ORIGIN','prAnnulla','wf_conf_annullato_orig','conf','annullato','annulla',10,0),(723,'WF_ORIGIN','prChiude','wf_conf_chiuso','conf','chiuso','Archiviazione',20,0),(724,'WF_ORIGIN','frModifica','wf_conf_mForn','conf','mForn','Origine ricevuta',30,0),(725,'WF_ORIGIN','prAnnulla','wf_crea_annullato','crea','annullato','Annullata',40,0),(726,'WF_ORIGIN','prProposta','wf_crea_prop','crea','prop','Invio richiesta',50,0),(727,'WF_ORIGIN','prAnnulla','wf_mForn_annullato_origin','daCertificare','annullato','Annullata',60,0),(728,'WF_ORIGIN','prConferma','wf_mForn_conf','daCertificare','conf','Accettazione origine ricevuta',70,0),(729,'WF_ORIGIN','prDaCertificare','wf_mForn_daCertificare_origin','daCertificare','daCertificare','Richiedere la certificazione',80,0),(730,'WF_ORIGIN','frModifica','wf_mForn_mForn','daCertificare','mForn','Accettazione origine ricevuta',90,0),(731,'WF_ORIGIN','prAnnulla','wf_mForn_annullato_origin','mForn','annullato','Annullata',100,0),(732,'WF_ORIGIN','prConferma','wf_mForn_conf','mForn','conf','Accettazione origine ricevuta',110,0),(733,'WF_ORIGIN','prDaCertificare','wf_mForn_daCertificare_origin','mForn','daCertificare','Richiedere la certificazione',120,0),(734,'WF_ORIGIN','frModifica','wf_mForn_mForn','mForn','mForn','Origine ricevuta',130,0),(735,'WF_ORIGIN','prModifica','wf_mForn_prop','mForn','prop','Invio richiesta',140,0),(736,'WF_ORIGIN','prAnnulla','wf_prop_annullato_origin','prop','annullato','Annullata',150,0),(737,'WF_ORIGIN','frModifica','wf_prop_mForn','prop','mForn','Origine ricevuta',160,0),(738,'WF_ORIGIN','prModifica','wf_prop_prop','prop','prop','Invio richiesta',170,0),(739,'WF_RDO_RDA','prAnnulla','wf_prop_annullato_rdo','conf','annullato','Offerta annullata',10,0),(740,'WF_RDO_RDA','prConferma','wf_conf_conf','conf','conf','Offerta Accettata',20,0),(741,'WF_RDO_RDA','prProposta','wf_crea_prop','crea','prop','Nuova Richiesta al Fornitore',30,0),(742,'WF_RDO_RDA','prAnnulla','wf_mForn_annullato_rdo','mForn','annullato','Offerta annullata',40,0),(743,'WF_RDO_RDA','prConferma','wf_mForn_conf','mForn','conf','Offerta Accettata',50,0),(744,'WF_RDO_RDA','frModifica','wf_mForn_mForn','mForn','mForn','Offerta dal Fornitore',60,0),(745,'WF_RDO_RDA','prAnnulla','wf_prop_annullato_rdo','prop','annullato','Offerta annullata',70,0),(746,'WF_RDO_RDA','frModifica','wf_prop_mForn','prop','mForn','Offerta dal Fornitore',80,0),(747,'WF_RDO_RDA','prModifica','wf_prop_prop','prop','prop','Richiesta al Fornitore',90,0),(748,'WF_SA','prForecast','wf_crea_forecast','forecast','forecast','Riga previsionale modificata',10,0),(749,'WF_SIMPLE_SPLIT','prAnnulla','wf_mForn_annullato','split','annullato','Riga Ordine Annullata',170,0),(750,'WF_SIMPLE_SPLIT','prAnnulla','wf_prop_annullato','controSplit','annullato','Riga Ordine Annullata',170,0);
/*!40000 ALTER TABLE `ordine_wf_transazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordine_wf_transazione_az`
--

DROP TABLE IF EXISTS `ordine_wf_transazione_az`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordine_wf_transazione_az` (
  `ORDINE_WF_TRANSAZIONE_AZ_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_WF_TRANSAZIONE_ID` int(11) NOT NULL,
  `NOME` varchar(254) NOT NULL,
  `ORDINE` int(11) NOT NULL,
  PRIMARY KEY (`ORDINE_WF_TRANSAZIONE_AZ_ID`),
  KEY `ORDINE_WF_TRANSAZIONE_ID` (`ORDINE_WF_TRANSAZIONE_ID`),
  CONSTRAINT `ordine_wf_transazione_az_ibfk_1` FOREIGN KEY (`ORDINE_WF_TRANSAZIONE_ID`) REFERENCES `ordine_wf_transazione` (`ORDINE_WF_TRANSAZIONE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1154 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordine_wf_transazione_az`
--

LOCK TABLES `ordine_wf_transazione_az` WRITE;
/*!40000 ALTER TABLE `ordine_wf_transazione_az` DISABLE KEYS */;
INSERT INTO `ordine_wf_transazione_az` VALUES (895,595,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.launchWrongStateException',10),(896,596,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkDocumentMandatoryFields',10),(897,596,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',20),(898,597,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.launchWrongStateException',10),(899,598,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',10),(900,598,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.sendToSupplier',20),(901,599,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkDocumentVersion',10),(902,599,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkSupplierInputMandatoryFields',20),(903,599,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',30),(904,599,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',40),(905,600,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',10),(906,600,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.sendToSupplier',20),(907,601,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',10),(908,601,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkDocumentMandatoryFields',20),(909,601,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',30),(910,602,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkDocumentVersion',10),(911,602,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkSupplierInputMandatoryFields',20),(912,602,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',30),(913,602,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',40),(914,603,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',10),(915,603,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',20),(916,604,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',10),(917,604,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',20),(918,604,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.sendToSupplier',30),(919,605,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkDocumentVersion',10),(920,605,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.checkSupplierInputMandatoryFields',20),(921,605,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',30),(922,605,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',40),(923,606,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',10),(924,606,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',20),(925,607,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.updateDocumentValues',10),(926,607,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.saveRevision',20),(927,607,'it.democenter.m2.workflow.WorkFlow_systemActions_AlboFornitori.sendToSupplier',30),(928,609,'it.democenter.m2.workflow.WorkFlow_systemActions_DocumentoTrasportoRiga.updateFiledFromMattoneParameters',10),(929,610,'it.democenter.m2.workflow.WorkFlow_systemActions_DocumentoTrasportoRiga.updateFiledFromMattoneParameters',10),(930,611,'it.democenter.m2.workflow.WorkFlow_systemActions_DocumentoTrasportoRiga.updateFiledFromMattoneParameters',10),(931,613,'it.democenter.m2.workflow.WorkFlow_systemActions_DocumentoTrasportoRiga.updateFiledFromMattoneParameters',10),(932,616,'it.democenter.m2.workflow.WorkFlow_systemActions_DocumentoTrasportoRiga.updateFiledFromMattoneParameters',10),(933,617,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(934,617,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(935,619,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(936,619,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',20),(937,620,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(938,620,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',20),(939,620,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',30),(940,620,'it.democenter.m2.customers.scambioDati.iungolive.TransazioneAzioni.updateFromDati',40),(941,621,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreNuovo',10),(942,621,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',20),(943,621,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',30),(944,621,'it.democenter.m2.customers.scambioDati.iungolive.TransazioneAzioni.updateFromDati',40),(945,622,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(946,622,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(947,623,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(948,623,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',20),(949,624,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(950,624,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',20),(951,624,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',30),(952,624,'it.democenter.m2.customers.scambioDati.iungolive.TransazioneAzioni.updateFromDati',40),(953,626,'it.democenter.m2.workflow.WorkFlow_systemActions_RdaRiga.updateFiledFromMattoneParameters',10),(954,627,'it.democenter.m2.workflow.WorkFlow_systemActions_RdaRiga.updateFiledFromMattoneParameters',10),(955,628,'it.democenter.m2.workflow.WorkFlow_systemActions_RdaRiga.updateFiledFromMattoneParameters',10),(956,631,'it.democenter.m2.workflow.WorkFlow_systemActions_RdaRiga.updateFiledFromMattoneParameters',10),(957,638,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(958,638,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(959,638,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(960,638,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(961,639,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(962,640,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(963,641,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(964,641,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(965,641,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',30),(966,643,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(967,644,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(968,644,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(969,644,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(970,644,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(971,647,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreNuovo',10),(972,647,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreNuovoFax',20),(973,647,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(974,647,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(975,648,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(976,648,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(977,650,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesFromPropValues',10),(978,650,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',20),(979,650,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',30),(980,650,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(981,650,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',50),(982,651,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(983,652,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(984,652,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',20),(985,652,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',30),(986,652,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(987,653,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(988,653,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(989,653,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',30),(990,655,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',10),(991,656,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(992,657,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(993,657,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(994,657,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(995,657,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(996,658,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(997,658,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(998,658,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',30),(999,658,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',40),(1000,659,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValuesToNull',10),(1001,659,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',20),(1002,660,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',10),(1004,661,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',20),(1005,661,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',30),(1007,662,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1008,662,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(1009,662,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',30),(1010,662,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',40),(1011,663,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(1012,663,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(1013,663,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(1014,663,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1015,663,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',50),(1016,666,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValuesToNull',10),(1017,666,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',20),(1020,667,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',20),(1021,667,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',30),(1022,667,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1023,668,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1024,668,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(1025,668,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',30),(1026,668,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',40),(1027,669,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValuesToNull',10),(1028,669,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',20),(1030,670,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',10),(1031,671,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesFromPropValues_And_checkNull',10),(1034,671,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',40),(1035,671,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',50),(1036,671,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',60),(1038,673,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1039,673,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(1040,673,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',30),(1041,674,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues_And_PropValuesToNull',10),(1042,674,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(1043,674,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',30),(1044,674,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',40),(1045,674,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',50),(1046,677,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValuesToNull',10),(1047,677,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',20),(1048,678,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(1049,678,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(1050,678,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(1051,678,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1052,679,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1053,680,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1054,681,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(1055,681,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(1056,681,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',30),(1057,683,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(1058,684,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(1059,684,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(1060,684,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(1061,684,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1062,687,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreNuovo',10),(1063,687,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreNuovoFax',20),(1064,687,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(1065,687,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1066,688,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(1067,688,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(1068,690,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesFromPropValues',10),(1069,690,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',20),(1070,690,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',30),(1071,690,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1072,690,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',50),(1073,691,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(1074,692,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1075,692,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',20),(1076,692,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',30),(1077,692,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1078,693,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',10),(1079,693,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(1080,693,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',30),(1081,695,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',10),(1082,696,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(1083,697,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(1084,697,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',20),(1085,697,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',30),(1086,697,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1087,699,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.sendFailureNotify',10),(1088,700,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.rollbackOperation',10),(1089,701,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.stopThread',10),(1090,703,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.sendFailureNotify',10),(1091,704,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.stopThread',10),(1092,707,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.sendFailureNotify',10),(1093,708,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.stopThread',10),(1094,711,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.sendFailureNotify',10),(1095,712,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.stopThread',10),(1096,714,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.pleaseStopThread',10),(1097,715,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.suspendThread',10),(1098,716,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.sendSuccessfullNotify',10),(1099,717,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.stopThread',10),(1100,719,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.pleaseStopThread',10),(1101,720,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.resumeThread',10),(1102,721,'it.democenter.m2.workflow.WorkFlow_systemActions_AsyncOperation.createAsyncThread',10),(1103,722,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1104,724,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.updateFiledFromParameters',10),(1105,725,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1106,726,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1107,726,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitore',20),(1108,727,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1109,727,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitore',20),(1110,729,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1111,729,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitoreRicevuta',20),(1112,730,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.updateFiledFromParameters',10),(1113,731,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1114,731,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitore',20),(1115,733,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1116,733,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitoreRicevuta',20),(1117,734,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.updateFiledFromParameters',10),(1118,735,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1119,735,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitore',20),(1120,736,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1121,736,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitore',20),(1122,737,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.updateFiledFromParameters',10),(1123,738,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.incrementaProgressivoNotifica',10),(1124,738,'it.democenter.m2.workflow.WorkFlow_systemActions_DeclarationOriginRow.sendNotificaFornitore',20),(1125,740,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1126,740,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesOthers',20),(1127,740,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',30),(1128,741,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreNuovo',10),(1129,741,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',20),(1130,741,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesOthers',30),(1131,741,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1132,743,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesFromPropValues',10),(1133,743,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesOthers',20),(1134,743,'it.democenter.m2.workflow.WorkFlow_systemActions.writeFileXmlEng_OrdineRiga',30),(1135,744,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(1136,744,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesOthers',20),(1137,746,'it.democenter.m2.workflow.WorkFlow_systemActions.changePropValues',10),(1138,746,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesOthers',20),(1139,747,'it.democenter.m2.workflow.WorkFlow_systemActions.sendRicConfermaFornitore',10),(1140,747,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',20),(1141,747,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValuesOthers',30),(1142,747,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',40),(1143,748,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10),(1145,749,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(1146,749,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',30),(1147,749,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',40),(1148,749,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',50),(1149,750,'it.democenter.m2.workflow.WorkFlow_systemActions.incrementaProgressivoNotifica',20),(1150,750,'it.democenter.m2.workflow.WorkFlow_systemActions.setOrdineRigaSplitFgArchived',30),(1151,750,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitore',40),(1152,750,'it.democenter.m2.workflow.WorkFlow_systemActions.sendNotificaFornitoreModificaFax',50),(1153,667,'it.democenter.m2.workflow.WorkFlow_systemActions.changeValues',10);
/*!40000 ALTER TABLE `ordine_wf_transazione_az` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partner_relation`
--

DROP TABLE IF EXISTS `partner_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partner_relation` (
  `PARTNER_RELATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `FORNITORE_CATEGORIA_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PARTNER_RELATION_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`OBJECT_TYPE`,`OBJECT_ID`,`FORNITORE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `FORNITORE_CATEGORIA_ID` (`FORNITORE_CATEGORIA_ID`),
  CONSTRAINT `partner_relation_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `partner_relation_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `partner_relation_ibfk_3` FOREIGN KEY (`FORNITORE_CATEGORIA_ID`) REFERENCES `fornitore_categoria` (`FORNITORE_CATEGORIA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partner_relation`
--

LOCK TABLES `partner_relation` WRITE;
/*!40000 ALTER TABLE `partner_relation` DISABLE KEYS */;
INSERT INTO `partner_relation` VALUES (1,5,20,17172,294,4),(2,5,20,17173,294,4),(3,5,20,17174,294,4),(4,5,20,17175,294,4),(5,5,20,17176,296,4),(6,5,20,17177,296,4),(7,5,20,17178,296,4),(8,5,20,17179,296,4);
/*!40000 ALTER TABLE `partner_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona` (
  `PERSONA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_NAME` varchar(99) NOT NULL,
  `VIA` varchar(254) DEFAULT NULL,
  `CAP` varchar(15) DEFAULT NULL,
  `CITTA` varchar(50) DEFAULT NULL,
  `PROVINCIA` varchar(30) DEFAULT NULL,
  `PAESE` varchar(30) DEFAULT NULL,
  `EMAIL` varchar(600) DEFAULT NULL,
  `PHONE_NUMBER` varchar(99) DEFAULT NULL,
  `FAX_NUMBER` varchar(99) DEFAULT NULL,
  `LAST_DITTA_ID` int(11) DEFAULT NULL,
  `NOTA` mediumtext,
  `LAST_PWD_CHANGE_DATE` datetime DEFAULT NULL,
  `LAST_PWD_EXPIRING_MAIL_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`PERSONA_ID`),
  UNIQUE KEY `USER_NAME` (`USER_NAME`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (3,'turbine','','','','','','g.borelli@iungo.it','','',5,'',NULL,NULL),(5,'anonymous','','','','','','','','',-1,'',NULL,NULL),(21,'sysScheduler',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,4,NULL,NULL,NULL),(53,'acquisti','','','','','','','','',5,'',NULL,NULL),(55,'sysIOuser_fornitore',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,NULL,NULL,NULL),(57,'sysIungoService',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,NULL,NULL,NULL),(58,'fornitore@localmail',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,NULL,NULL,NULL),(59,'fornitore@localmail.it',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,5,NULL,NULL,NULL),(60,'turbine_root',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,-1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_configurazione`
--

DROP TABLE IF EXISTS `persona_configurazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_configurazione` (
  `PERSONA_CONFIGURAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PERSONA_ID` int(11) NOT NULL,
  `NOME` varchar(254) NOT NULL,
  `VALORE` mediumtext,
  PRIMARY KEY (`PERSONA_CONFIGURAZIONE_ID`),
  UNIQUE KEY `PERSONA_ID_2` (`PERSONA_ID`,`NOME`),
  KEY `PERSONA_ID` (`PERSONA_ID`),
  CONSTRAINT `persona_configurazione_ibfk_1` FOREIGN KEY (`PERSONA_ID`) REFERENCES `persona` (`PERSONA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1362 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_configurazione`
--

LOCK TABLES `persona_configurazione` WRITE;
/*!40000 ALTER TABLE `persona_configurazione` DISABLE KEYS */;
INSERT INTO `persona_configurazione` VALUES (5,3,'numeroRecordPerPagina','15'),(7,3,'mattoniSys_Ordine_listByStato','oNumO,rNumO,rEsAz,frnO,pcO,qtaI,przI,dtcI,qtcI,pprzI,pqtaI,pdtcI,pumO,vltO'),(10,3,'mattoniSys_DocumentoTrasporto_list','ddDrSt,dNumO,dDatO,frnO,dcolni,ddtRic,ddari,dcolri'),(16,3,'mattoniSys_OrdineRiga_formDDTRiga','dNRgO,oNumO,oNRgO,ProductCode,qtaI,fSaldI,frnO'),(19,5,'mattoniSys_Ordine_formRiga','rNumO,rEsAz,pcO,pdO,qtaI,przI,dtcI,qtcI,qdcO'),(20,5,'mattoniSys_Ordine_list','oTipoO,oNumO,frnO,oDEmO,oRefO'),(22,8,'mattoniSys_Ordine_formRiga','rNumO,rEsAz,pcO,pdO,qtaI,przI,dtcI,qtcI,qdcO'),(23,8,'mattoniSys_Ordine_list','oTipoO,oNumO,frnO,oDEmO,oRefO'),(173,3,'mattoniSys_Fornitore_list','RagioneSociale,Email,FornitoreIdDitta,MailSendHtmlInline,PrevisioneConsegnaGiorni'),(658,3,'language.locale','it'),(660,3,'navigation_user_home_menu','menu_ordini_out.vm'),(661,3,'navigation_user_home_template','Blank.vm'),(662,3,'mattoniSys_Ditta_list','RagioneSociale,GroupName,Via,Cap,Citta,Email,PhoneNumber'),(670,53,'mattoniSys_Fornitore_list','RagioneSociale,Citta,Provincia,Paese,Email'),(671,53,'numeroRecordPerPagina','10'),(673,53,'mattoniSys_Ordine_list','oTipoO,oNumO,frnO,oDEmO,oRefO,oA2O,AuxHead3I'),(674,53,'mattoniSys_OrdineRiga_list','frnO,oTipoO,oNumO,rNumO,pcO,rEsAz,hasDoc,qta2RigheI,prz2RigheI,dtc2RigheI,rRisp,llmO,llmoSFull'),(675,53,'mattoniSys_Ordine_formRiga','rNumO,pcO,rEsAz,hasDoc,qta2RigheI,prz2RigheI,dtc2RigheI,rRisp,llmO,llmoSFull'),(678,3,'mattoniSys_Ordine_list','oTipoO,oNumO,frnO,oDEmO,oRefO,ofemO,hasDoc'),(679,3,'mattoniSys_OrdineRiga_list','frnO,oTipoO,oNumO,rNumO,pcO,rEsAz,hasDoc,qta2RigheI,prz2RigheI,dtc2RigheI,rRisp,llmO,llmoSFull'),(681,3,'mattoniSys_Ordine_formRiga','rNumO,pcO,rEsAz,hasDoc,qta2RigheI,prz2RigheI,dtc2RigheI,rRisp,llmoSFull,ntO'),(682,3,'mattoniSys_Product_list','ProductTypeName,Code,Description,CodeInternal,UnitOfMeasure,FgSold,FgPurchased'),(686,3,'mattoniSys_Fornitore_search','RagioneSociale,Citta'),(687,3,'ordini.reports.paramaters.timeInterval','month'),(688,3,'ordini.reports.paramaters.datainizio','01-10-2007'),(689,3,'ordini.reports.paramaters.numerodicolonne','8'),(690,3,'mattoniSys_Product_searchInline','ProductTypeName,Code,Description,UnitOfMeasure'),(691,3,'mattoniSys_ReportOrdineRiga_listOrdineRiga','frnO,oTipoO,oNumO,rNumO,pcO,rEsAz,qtaI,pqtaO,przI,pprzO,dtcI,pdtcO,llmO,ddDrSt'),(692,3,'mattoniSys_Ordine_listSpec','ordineTipoFgOutbound,oTipoO,oNumO,oDEmO,frnO,ddDrSt'),(693,3,'mattoniSys_OrdineTipo_search','Nome,Descrizione'),(694,3,'mattoniSys_Fornitore_searchInline','RagioneSociale,Citta,Provincia'),(695,3,'mattoniSys_DocumentoTrasportoRiga_list','frnO,dNumO,dDatO,dDaRO,dNRgO,oNumO,oNRgO,dPrCO,orQO,orQAO,qtaO,fSaldo,dracc,ddaai,qtaai,fSaldAI'),(696,53,'mattoniSys_Fornitore_search','RagioneSociale,Citta'),(697,3,'mattoniSys_DocumentoTrasporto_list_searchDDT','dNumO,dDatO,frnO,dColNO,FgOutbound'),(698,3,'mattoniSys_DocumentoTrasporto_formRiga','dNRgO,ProductCode,ProductDescription,oNumO,oNRgO,orQO,orQAO,qtaO,fSaldo,dracc,ddaai,qtaai,fSaldAI'),(699,3,'mattoniSys_OrdineRiga_search','ordineTipoFgOutbound,oTipoO,oNumO,rNumO,pcO,pdO,ddDrSt,frnO,llmoSFull'),(700,3,'mattoniSys_ProductBatch_search','Code,Quantity,QuantityRemaining,QuantityScrap,CreationDate,ExpirationDate'),(701,3,'mattoniSys_OrdineRiga_listSpec','oNumO,rNumO,oTipoO,rEsAz'),(702,3,'mattoniSys_DocumentoTrasporto_listSpec','FgOutbound,ddDrSt,dNumO,dDatO,frnO,dcolni,ddtRic,ddari,dcolri'),(703,3,'mattoniSys_DocumentoTrasportoRiga_listSpec','frnO,FgOutbound,dNumO,dNRgO,ProductCode,ProductBatchCode,fSaldo,ProductCodePartner,ProductBatchCodePartner,dracc,qtaai,ddaai,fSaldAI'),(704,3,'mattoniSys_ProductType_list','Name,Description'),(705,3,'mattoniSys_Documentazione_list','DocumentazioneTipoDescrizione,FileName,FileSize,FileLastModified,Descrizione'),(706,3,'mattoniSys_Processing_list','Code,Description,Type'),(707,3,'mattoniSys_Resource_list','ResourceTypeCode,ResourceTypeDescription,Code'),(708,3,'mattoniSys_ResourceType_list','Code,Description,Notait.democenter.m2.Persona.conf.mattoniSys_Resource_list=Code,Description,ResourceTypeCode,ResourceTypeDescription,AvailabilityMinutes'),(709,3,'mattoniSys_DittaCertificateRole_list','RoleName,Nota'),(710,3,'mattoniSys_Processing_formResourceCapabilities','ResourceCode,ResourceDescription,ProcessingType,TimeSetupSeconds,TimePerUnitSeconds,Description'),(711,3,'mattoniSys_Processing_formProductFormula','ProductFormulaCode,ProductFormulaDescription,ProcessingCode,ProcessingDescription,Nota'),(712,3,'mattoniSys_ResourceType_search','Code,Description'),(725,3,'mattoniSys_Previsionale_list','FornitoreRS,OrdineTipoNome,DataEmissione,Stato,Nota,AuxHead10'),(726,3,'mattoniSys_PrevisionaleRiga_list','DataEmissione,FornitoreRS,OrdineTipoNome,OrdineNumero,OrdineData,OrdineNumeroRiga,ProdottoCodice,Quantita,Prezzo,DataConsegna'),(727,3,'mattoniSys_Previsionale_formRiga','OrdineTipoNome,OrdineNumero,Stato,OrdineData,OrdineNumeroRiga,ProdottoCodice,Quantita,Prezzo,DataConsegna'),(732,3,'mattoniSys_ProductFormula_list','Code,Description,Nota,CreationDate,ExpirationDate'),(734,3,'mattoniSys_ProductListino_list','ProductCode,ProductDescription,FornitoreRagioneSociale,CodePartner,PrezzoUnitaMisura,Prezzo,ValidityStartDate,ValidityEndDate,Nota'),(735,3,'mattoniSys_ProductBatch_list','Code,ProductCode,ProductTypeDescription,ProductDescription,Quantity,CreationDate,QuantityRemaining'),(736,3,'mattoniSys_ProductBatchFormula_list','Code,Description,DateStart,ProductFormulaCode'),(739,53,'mattoniSys_DocumentoTrasporto_list','ddDrSt,frnO,dNumO,dDatO,dColNO,dNotaO,ddtRic,ddari,dcolri'),(740,53,'mattoniSys_DocumentoTrasportoRiga_list','frnO,dNumO,dDatO,dDaRO,dNRgO,oNumO,oNRgO,dPrCO,orQO,orQAO,qtaO,fSaldo,dracc,ddaai,qtaai,fSaldAI'),(741,53,'mattoniSys_Previsionale_list','FornitoreRS,OrdineTipoNome,DataEmissione,Stato,Nota'),(745,3,'mattoniSys_InvoiceRow_list','FgOutbound,FornitoreRagioneSociale,EmissionDate,InvoiceNumber,NotaInvoice,Number,ProductCode,ProductDescription,ProductCodePartner'),(746,3,'mattoniSys_Invoice_list','FornitoreRagioneSociale,FgOutbound,InvoiceNumber,DestinationAddress,Note,EmissionDate'),(754,3,'mattoniSys_Product_formFormula','Type,ProductFormulaCode,ProductFormulaDescription,Quantity,ProductTypeName'),(756,3,'mattoniSys_Product_formProductListinoList','ProductCode,ProductDescription,FornitoreRagioneSociale,CodePartner,PrezzoUnitaMisura,Prezzo,ValidityStartDate,ValidityEndDate,Nota'),(757,3,'mattoniSys_Product_formBatchList_list','Code,Quantity,QuantityScrap,CreationDate,ExpirationDate,QuantityRemaining'),(758,3,'mattoniSys_ProductBatch_formFormula','Type,ProductCode,ProductDescription,ProductBatchCode'),(759,3,'mattoniSys_ProductBatch_formDocumentoTrasportoRiga','frnO,FgOutbound,dDatO,dNumO,dNRgO,dDaRO,oNumO,oNRgO,ProductUnitOfMeasure,qtaO'),(761,3,'mattoniSys_Invoice_formInvoiceRow','Number,ProductCode,ProductDescription,ProductCodePartner,Quantity,Price,Amount'),(764,3,'mattoniSys_ProductFormula_formItem','Type,QuantityI,ProductUnitOfMeasure,ProductCode,ProductDescription,ProductTypeName'),(765,3,'mattoniSys_ProductFormula_formProcessing','ProductFormulaCode,ProcessingCode,Nota'),(766,3,'mattoniSys_ProductBatchFormula_formItem','Type,ProductCode,ProductBatchCode,ProductBatchFormulaCode,ProductUnitOfMeasure,Quantity,QuantityScrap'),(767,3,'mattoniSys_Processing_search','Code,Description,Type'),(768,3,'mattoniSys_ProductFormula_formProductBatchFormulaInsert','Type,ProductTypeName,ProductCode,Quantity'),(771,3,'mattoniSys_Refilling_list','FornitoreRagioneSociale,DataEmissione,Stato,Numero'),(772,3,'mattoniSys_rdo.Ordine_list','oNumO,oDEmO,frnO,oValO'),(773,3,'mattoniSys_rdo.OrdineRiga_list','frnO,pcO,customMattone_OrdineRiga_6,customMattone_OrdineRiga_5,customMattone_OrdineRiga_7,rEsAz,customMattone_OrdineRiga_8,oA1O,oA2O,qtaI,przO,oA3I,oA4O,oA5I,oA6O,oA7O,llmO'),(774,3,'mattoniSys_vendite.Ordine_list','frnO,oTipoO,oNumO,oRefO,onotaO,ddDrSt'),(775,3,'mattoniSys_rdo.Ordine_formRiga','rNumO,frnO,customMattone_OrdineRiga_6,customMattone_OrdineRiga_5,pdI,rEsAz,hasDoc,qumO,oA1O,oA2O,qtaI,przO,oA3I,oA4O,oA5I,oA6O,oA7O,llmO'),(785,3,'mattoniSys_RefillingRow_list',''),(788,3,'mattoniSys_Stock_list','FornitoreRagioneSociale,DataEmissione,TrasmissionNumber'),(791,3,'mattoniSys_vendite.Ordine_formRiga','oNumO,rNumO,rEsAz,pcO,qtaO,przO,dtcO,ntO'),(794,3,'mattoniSys_StockRow_list',''),(795,3,'mattoniSys_ProductBatchFormula_formResourceOccupation','Status,Quantity,TimeSetupSeconds,TimePerUnitSeconds'),(796,3,'mattoniSys_vendite.OrdineRiga_list',''),(802,53,'mattoniSys_OrdineRiga_formDDTRiga','dNumO,dDatO,dDaRO,dNRgO,qtaO,fSaldo,dracc,ddaai,qtaai,fSaldAI'),(823,53,'mattoniSys_Previsionale_formRiga','OrdineTipoNome,OrdineNumero,OrdineData,OrdineNumeroRiga,ProdottoCodice,Quantita,Prezzo,DataConsegna'),(841,53,'mattoniSys_DocumentoTrasporto_formRiga','dNRgO,oNumO,oNRgO,dPrCO,orQO,orQAO,qtaO,fSaldo,dracc,ddaai,qtaai,fSaldAI'),(846,3,'mattoniSys_NotificaLog_list','DataSpedizione,Template,Indirizzo,Soggetto'),(867,53,'mattoniSys_rdo.Ordine_list','oNumO,oTipoO,oDEmO,oRefO,frnO,onotaO'),(868,53,'mattoniSys_rdo.OrdineRiga_list','oNumO,rNumO,pdI,rEsAz,oA1O,qtaO,przI,oA2O,oA3I,oA4O,oA5I,oA6O,oA7O'),(869,53,'mattoniSys_rdo.Ordine_formRiga','oNumO,rNumO,pdI,rEsAz,oA1O,qtaO,przI,oA2O,oA3I,oA4O,oA5I,oA6O,oA7O,llmoS'),(876,3,'filtriSys__Menu','Fvisto,FstatoRigheOrdine,Fspacer,Fpartner,Freferente,Fspacer,FprodottoCodice,FordineNumero,FtipoOrdine,Fspacer,Fdata'),(885,3,'navigation_user_last_report_menu','menu_report_acquisti.vm'),(886,3,'navigation_user_last_report_menu_famiglia','1'),(887,3,'filtriSys_Report__Menu','Fvisto,FstatoRigheOrdine,Fspacer,Fpartner,Freferente,Fspacer,FprodottoCodice,FordineNumero,FtipoOrdine,Fspacer,Fdata'),(888,3,'filtriSys_Sinottico__Menu','Fvisto,FstatoRigheOrdine,Fspacer,Fpartner,Freferente,Fspacer,FprodottoCodice,FordineNumero,FtipoOrdine,Fspacer,Fdata'),(891,8,'numeroRecordPerPagina','300'),(892,54,'numeroRecordPerPagina','300'),(893,3,'showFloatingMenu','false'),(903,3,'text_cell_maxsize','40'),(905,3,'mattoniSys_OrdineRiga_headerShort','frnO,oTipoO,oNumO,rNumO,pcO,dtcO,qtaO,przO'),(906,3,'mattoniSys_ScheduleMixedInput','SchedQtyI,SchedDeliveryDateI'),(907,3,'5.ordiniParametriRicerca000','fS:0;fgVistoOrdineRiga:0;fSDDT:0;fSPrevisionale:0;fF:0;fR:0;fD:0;fC:0;fgDdtOutTrue:0;fgDdtOutFalse:1;fgFPrd:0;fgFLot:0;fgFPrdSld:0;fgFPrdPur:0;fgInvoiceOutTrue:1;fgInvoiceOutFalse:0;fgCQ:0;fgQI:0;st:mforn;ot:11;sd:;scq:;snc:;ecq:;sp:;fid:287;dt:07070100'),(908,3,'5.ordiniParametriRicerca001','00,,,,,,,,,;on:;pd:;pc:;nc:;pcF:;bc:;frmlCode:;frmlDesc:;prcssCode:;prcssDesc:;ref:a;otFamiglia:1;oCheckVisto:1;dtPBClow:;dtPBChi:;dtPBXlow:;dtPBXhi:;dtNCClow:;dtNCChi:;NCorigine:0;NCstato:null;dtCQClow:;dtCQChi:;dtCQChlow:;dtCQChhi:;CQorigine:0;CQstato'),(909,3,'5.ordiniParametriRicerca002',':null;CQesito:null;QIcode:;QIdescription:;QItipo:0;pah1:;pah2:;pah3:;pah4:;pah5:;pah6:;pah7:;pah8:;pah9:;pah10:;par1:;par2:;par3:;par4:;par5:;par6:;par7:;par8:;par9:;par10:;dtVFClow:;dtVFChi:;fgVFftp:0;fgVFftpv:0;fgVFftprv:0;fgVFftpnc:0;fgVFftprc:0;VFfc'),(910,3,'5.ordiniParametriRicerca003','mid:0;VFfcmtid:0'),(924,53,'5.ordiniParametriRicerca000','fS:0;fgVistoOrdineRiga:0;fSDDT:0;fSPrevisionale:0;fF:0;fR:0;fD:0;fC:0;fgDdtOutTrue:0;fgDdtOutFalse:0;fgFPrd:0;fgFLot:0;fgFPrdSld:0;fgFPrdPur:0;fgInvoiceOutTrue:0;fgInvoiceOutFalse:0;fgCQ:0;fgQI:0;st:conf;ot:;sd:;scq:;snc:;ecq:;sp:;fid:41;dt:,,,080331000'),(925,53,'5.ordiniParametriRicerca001','0,,,,,,;on:;pd:;pc:;nc:;pcF:;bc:;frmlCode:;frmlDesc:;prcssCode:;prcssDesc:;ref:turbine;otFamiglia:2;oCheckVisto:1;dtPBClow:;dtPBChi:;dtPBXlow:;dtPBXhi:;dtNCClow:;dtNCChi:;NCorigine:0;NCstato:null;dtCQClow:;dtCQChi:;dtCQChlow:;dtCQChhi:;CQorigine:0;CQsta'),(926,53,'5.ordiniParametriRicerca002','to:null;CQesito:null;QIcode:;QIdescription:;QItipo:0;pah1:;pah2:;pah3:;pah4:;pah5:;pah6:;pah7:;pah8:;pah9:;pah10:;par1:;par2:;par3:;par4:;par5:;par6:;par7:;par8:;par9:;par10:;dtVFClow:;dtVFChi:;fgVFftp:0;fgVFftpv:0;fgVFftprv:0;fgVFftpnc:0;fgVFftprc:0;VF'),(927,53,'5.ordiniParametriRicerca003','fcmid:0;VFfcmtid:0'),(960,21,'ggAnticipoAvvisoValutazione','-1'),(961,21,'text_cell_maxsize','40'),(990,53,'5.mattoniSys_ProductList_searchInline_attach','FornitoreRagioneSociale,ValidityStartDate,ValidityEndDate'),(991,53,'5.mattoniSys_Product_searchInline_attach','Code,Description,hasDoc'),(992,3,'mattoniSys_ProductList_searchInline','FornitoreRagioneSociale,ProductCode,PrezzoUnitaMisura,Prezzo,ValidityStartDate'),(993,5,'mattoniSys_ProductList_searchInline','FornitoreRagioneSociale,ProductCode,PrezzoUnitaMisura,Prezzo,ValidityStartDate'),(994,8,'mattoniSys_ProductList_searchInline','FornitoreRagioneSociale,ProductCode,PrezzoUnitaMisura,Prezzo,ValidityStartDate'),(995,21,'mattoniSys_ProductList_searchInline','FornitoreRagioneSociale,ProductCode,PrezzoUnitaMisura,Prezzo,ValidityStartDate'),(996,53,'mattoniSys_ProductList_searchInline','FornitoreRagioneSociale,ProductCode,PrezzoUnitaMisura,Prezzo,ValidityStartDate'),(997,54,'mattoniSys_ProductList_searchInline','FornitoreRagioneSociale,ProductCode,PrezzoUnitaMisura,Prezzo,ValidityStartDate'),(998,55,'mattoniSys_ProductList_searchInline','FornitoreRagioneSociale,ProductCode,PrezzoUnitaMisura,Prezzo,ValidityStartDate'),(999,5,'mattoniSys_Product_searchInline','ProductTypeName,Code,Description,UnitOfMeasure,Prezzo,FgIsMaster'),(1000,8,'mattoniSys_Product_searchInline','ProductTypeName,Code,Description,UnitOfMeasure,Prezzo,FgIsMaster'),(1001,21,'mattoniSys_Product_searchInline','ProductTypeName,Code,Description,UnitOfMeasure,Prezzo,FgIsMaster'),(1002,53,'mattoniSys_Product_searchInline','ProductTypeName,Code,Description,UnitOfMeasure,Prezzo,FgIsMaster'),(1003,54,'mattoniSys_Product_searchInline','ProductTypeName,Code,Description,UnitOfMeasure,Prezzo,FgIsMaster'),(1004,55,'mattoniSys_Product_searchInline','ProductTypeName,Code,Description,UnitOfMeasure,Prezzo,FgIsMaster'),(1005,3,'filtriSys_declarationOrigin_Menu','FstatoWorkFlow,Fpartner,ForiginNumber,FitemCode,ForiginCreationDate,ForiginStartDate,ForiginCountryCode,ForiginCountry,ForiginContactPerson'),(1006,5,'filtriSys_declarationOrigin_Menu','FstatoWorkFlow,Fpartner,ForiginNumber,FitemCode,ForiginCreationDate,ForiginStartDate,ForiginCountryCode,ForiginCountry,ForiginContactPerson'),(1007,8,'filtriSys_declarationOrigin_Menu','FstatoWorkFlow,Fpartner,ForiginNumber,FitemCode,ForiginCreationDate,ForiginStartDate,ForiginCountryCode,ForiginCountry,ForiginContactPerson'),(1008,21,'filtriSys_declarationOrigin_Menu','FstatoWorkFlow,Fpartner,ForiginNumber,FitemCode,ForiginCreationDate,ForiginStartDate,ForiginCountryCode,ForiginCountry,ForiginContactPerson'),(1009,53,'filtriSys_declarationOrigin_Menu','FstatoWorkFlow,Fpartner,ForiginNumber,FitemCode,ForiginCreationDate,ForiginStartDate,ForiginCountryCode,ForiginCountry,ForiginContactPerson'),(1010,54,'filtriSys_declarationOrigin_Menu','FstatoWorkFlow,Fpartner,ForiginNumber,FitemCode,ForiginCreationDate,ForiginStartDate,ForiginCountryCode,ForiginCountry,ForiginContactPerson'),(1011,55,'filtriSys_declarationOrigin_Menu','FstatoWorkFlow,Fpartner,ForiginNumber,FitemCode,ForiginCreationDate,ForiginStartDate,ForiginCountryCode,ForiginCountry,ForiginContactPerson'),(1012,3,'mattoniSys_DeclarationOrigin_list','Fornitore,hasDoc,OriginNumber,OrdineTipoNome,CreationDate,OriginStartDate,FilledDate,Note,ContactPerson'),(1013,5,'mattoniSys_DeclarationOrigin_list','Fornitore,hasDoc,OriginNumber,OrdineTipoNome,CreationDate,OriginStartDate,FilledDate,Note,ContactPerson'),(1014,8,'mattoniSys_DeclarationOrigin_list','Fornitore,hasDoc,OriginNumber,OrdineTipoNome,CreationDate,OriginStartDate,FilledDate,Note,ContactPerson'),(1015,21,'mattoniSys_DeclarationOrigin_list','Fornitore,hasDoc,OriginNumber,OrdineTipoNome,CreationDate,OriginStartDate,FilledDate,Note,ContactPerson'),(1016,53,'mattoniSys_DeclarationOrigin_list','Fornitore,hasDoc,OriginNumber,OrdineTipoNome,CreationDate,OriginStartDate,FilledDate,Note,ContactPerson'),(1017,54,'mattoniSys_DeclarationOrigin_list','Fornitore,hasDoc,OriginNumber,OrdineTipoNome,CreationDate,OriginStartDate,FilledDate,Note,ContactPerson'),(1018,55,'mattoniSys_DeclarationOrigin_list','Fornitore,hasDoc,OriginNumber,OrdineTipoNome,CreationDate,OriginStartDate,FilledDate,Note,ContactPerson'),(1019,3,'mattoniSys_DeclarationOriginRow_list','OrdineTipoNome,Fornitore,OriginNumber,CreationDate,OriginStartDate,rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1020,5,'mattoniSys_DeclarationOriginRow_list','OrdineTipoNome,Fornitore,OriginNumber,CreationDate,OriginStartDate,rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1021,8,'mattoniSys_DeclarationOriginRow_list','OrdineTipoNome,Fornitore,OriginNumber,CreationDate,OriginStartDate,rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1022,21,'mattoniSys_DeclarationOriginRow_list','OrdineTipoNome,Fornitore,OriginNumber,CreationDate,OriginStartDate,rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1023,53,'mattoniSys_DeclarationOriginRow_list','OrdineTipoNome,Fornitore,OriginNumber,CreationDate,OriginStartDate,rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1024,54,'mattoniSys_DeclarationOriginRow_list','OrdineTipoNome,Fornitore,OriginNumber,CreationDate,OriginStartDate,rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1025,55,'mattoniSys_DeclarationOriginRow_list','OrdineTipoNome,Fornitore,OriginNumber,CreationDate,OriginStartDate,rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1026,3,'mattoniSys_DeclarationOrigin_formRiga','rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1027,5,'mattoniSys_DeclarationOrigin_formRiga','rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1028,8,'mattoniSys_DeclarationOrigin_formRiga','rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1029,21,'mattoniSys_DeclarationOrigin_formRiga','rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1030,53,'mattoniSys_DeclarationOrigin_formRiga','rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1031,54,'mattoniSys_DeclarationOrigin_formRiga','rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1032,55,'mattoniSys_DeclarationOrigin_formRiga','rEsAz,ItemCode,ItemDescription,OriginCountryCode,OriginCountry,Note,ContactPerson'),(1033,53,'5.0','rigaTooltip,hasDoc'),(1034,3,'mattoniSys_rdo.Ordine_inline','rNumO,frnO,customMattone_OrdineRiga_6,customMattone_OrdineRiga_5,pdI,rEsAz,hasDoc,qumO,oA1O,oA2O,qtaI,przO,oA3I,oA4O,oA5I,oA6O,oA7O,llmO'),(1035,21,'mattoniSys_rdo.Ordine_inline','hasDoc,llmoSFull,pcO,rEsAz,prz2RigheI,dtc2RigheI,qta2RigheI,orSconto1I_2Righe,qumO,oRefO'),(1036,53,'mattoniSys_rdo.Ordine_inline','oNumO,rNumO,pdI,rEsAz,oA1O,qtaO,przI,oA2O,oA3I,oA4O,oA5I,oA6O,oA7O,llmoS'),(1037,53,'5.filtriSys_Homepage__Menu','FEtichetta,Fspacer,FstatoRigheOrdine,Fspacer,Fpartner,FtipologiaInvio,Fspacer,Freferente,FprodottoCodice,FtipoOrdine,Fspacer'),(1038,3,'mattoniSys_ProductListinoType_list','Name,Description'),(1039,5,'mattoniSys_ProductListinoType_list','Name,Description'),(1040,8,'mattoniSys_ProductListinoType_list','Name,Description'),(1041,21,'mattoniSys_ProductListinoType_list','Name,Description'),(1042,53,'mattoniSys_ProductListinoType_list','Name,Description'),(1043,54,'mattoniSys_ProductListinoType_list','Name,Description'),(1044,55,'mattoniSys_ProductListinoType_list','Name,Description'),(1045,3,'mattoniSys_DeclarationOriginRow_fromOrder','RagioneSociale,Codice,Descrizione,FornitoreProdottoCodice,FornitoreProdottoDescrizione,LastDeclarationOriginRowFromCodePartner_CountryCode,LastDeclarationOriginRowFromCodePartner_Country,LastDeclarationOriginRowFromCodePartner_CreationDate,LastDeclarationOriginRowFromCodePartner_StartDate'),(1046,5,'mattoniSys_DeclarationOriginRow_fromOrder','RagioneSociale,Codice,Descrizione,FornitoreProdottoCodice,FornitoreProdottoDescrizione,LastDeclarationOriginRowFromCodePartner_CountryCode,LastDeclarationOriginRowFromCodePartner_Country,LastDeclarationOriginRowFromCodePartner_CreationDate,LastDeclarationOriginRowFromCodePartner_StartDate'),(1047,8,'mattoniSys_DeclarationOriginRow_fromOrder','RagioneSociale,Codice,Descrizione,FornitoreProdottoCodice,FornitoreProdottoDescrizione,LastDeclarationOriginRowFromCodePartner_CountryCode,LastDeclarationOriginRowFromCodePartner_Country,LastDeclarationOriginRowFromCodePartner_CreationDate,LastDeclarationOriginRowFromCodePartner_StartDate'),(1048,21,'mattoniSys_DeclarationOriginRow_fromOrder','RagioneSociale,Codice,Descrizione,FornitoreProdottoCodice,FornitoreProdottoDescrizione,LastDeclarationOriginRowFromCodePartner_CountryCode,LastDeclarationOriginRowFromCodePartner_Country,LastDeclarationOriginRowFromCodePartner_CreationDate,LastDeclarationOriginRowFromCodePartner_StartDate'),(1049,53,'mattoniSys_DeclarationOriginRow_fromOrder','RagioneSociale,Codice,Descrizione,FornitoreProdottoCodice,FornitoreProdottoDescrizione,LastDeclarationOriginRowFromCodePartner_CountryCode,LastDeclarationOriginRowFromCodePartner_Country,LastDeclarationOriginRowFromCodePartner_CreationDate,LastDeclarationOriginRowFromCodePartner_StartDate'),(1050,54,'mattoniSys_DeclarationOriginRow_fromOrder','RagioneSociale,Codice,Descrizione,FornitoreProdottoCodice,FornitoreProdottoDescrizione,LastDeclarationOriginRowFromCodePartner_CountryCode,LastDeclarationOriginRowFromCodePartner_Country,LastDeclarationOriginRowFromCodePartner_CreationDate,LastDeclarationOriginRowFromCodePartner_StartDate'),(1051,55,'mattoniSys_DeclarationOriginRow_fromOrder','RagioneSociale,Codice,Descrizione,FornitoreProdottoCodice,FornitoreProdottoDescrizione,LastDeclarationOriginRowFromCodePartner_CountryCode,LastDeclarationOriginRowFromCodePartner_Country,LastDeclarationOriginRowFromCodePartner_CreationDate,LastDeclarationOriginRowFromCodePartner_StartDate'),(1072,58,'ggAnticipoAvvisoValutazione','-1'),(1073,58,'text_cell_maxsize','40'),(1074,58,'google.maps.x',''),(1075,58,'google.maps.y',''),(1076,3,'sys_session_token_duration','5184000'),(1137,3,'ggAnticipoAvvisoValutazione','-1'),(1175,3,'',''),(1177,58,'language.locale','en'),(1179,58,'sys_session_token_duration','5184000'),(1197,53,'sys_session_token_duration','5184000'),(1198,53,'showFloatingMenu','false'),(1199,53,'filtriSys__Menu','Fvisto,FstatoRigheOrdine,Fspacer,Fpartner,Freferente,Fspacer,FprodottoCodice,FordineNumero,FtipoOrdine,Fspacer,Fdata'),(1263,59,'language.locale','it'),(1282,59,'sys_session_token_duration','5184000'),(1337,58,'lffFornitoreIds','287,288,291'),(1338,21,'sys_session_token_duration','5184000'),(1356,3,'mattoneParametriSessione','#className:OrdineRiga;oA10O:true;frnO:true;oA1O:false;#className:Ordine;oDEmO:false;oNumO:true;oDEmO:false;#className:Fornitore;RagioneSociale:false;null:null;null:null;#className:ProductFormulaItem;Type:true;null:null;null:null;#className:Previsionale;DataEmissione:true;FornitoreRS:false;null:null;'),(1359,59,'lffFornitoreIds','293,295'),(1360,3,'5.ordiniParametriRicerca_menu_ordini_out','fS:0;fgVistoOrdineRiga:0;fgSelezionatoOrdineRiga:0;fgRigaDisabled:0;fSDDT:0;fSRDA:0;fSPrevisionale:0;fSDocBox:0;fF:0;fR:0;fD:0;fC:0;fTag:0;fgDdtOutTrue:0;fgDdtOutFalse:1;fgFPrd:0;fgFLot:0;fgFPrdSld:0;fgFPrdPur:0;fgFPrdMaster:0;fgInvoiceOutTrue:0;fgInvoiceOutFalse:0;fgCQ:0;fgQI:0;fgPt:0;fgPlt:0;st:;ot:;tag:;sd:;st_rda:;scq:;snc:;ecq:;sp:;sdbox:;fid:287;dt:,,,,,,,,,;on:;pd:;pc:;nc:;pcF:;bc:;frmlCode:;frmlDesc:;prcssCode:;prcssDesc:;ref:;otFamiglia:1;oCheckVisto:1;oCheckSelezionato:1;fabbisognoStatus:0;dtPBClow:;dtPBChi:;dtPBXlow:;dtPBXhi:;dtNCClow:;dtNCChi:;NCorigine:0;NCstato:;dtCQClow:;dtCQChi:;dtCQChlow:;dtCQChhi:;CQorigine:0;CQstato:;CQesito:;QIcode:;QIdescription:;QItipo:0;dtVL:0911241845;fgVLdt:1;fgLcod:0;fgLcodPart:0;fgLdescPart:0;fgLpum:0;fgLpartCM:0;fgLkm:0;codePartL:;descPartL:;prMinL:-1.0;prMaxL:-1.0;fgLpr:0;fgLnota:0;prUmL:;notaL:;VFfcmidL:0;dtVFClow:;dtVFChi:;dtVFSlow:;dtVFShi:;fgVFftp:0;fgVFftpv:0;fgVFftprv:0;fgVFftpnc:0;fgVFftprc:0;VFfcmid:0;VFfcmtid:0;fgTipInvio:0;ftinvio:;pt:;plt:;DivisoreParametri:null;_oprValueMap:null;_oprValueMap:null;_oprValueMap:null;fgFiltriGroup_AuthorizationLevel:0;fgFiltriGroup_FlastDelNoteNum:0;fgFiltriGroup_FddtAuxRow5:0;fgFiltriGroup_FddtAuxRowNum5:0;fgFiltriGroup_FddtAuxRow6:0;fgFiltriGroup_FddtAuxRowNum4:0;fgFiltriGroup_FddtAuxRow3:0;fgFiltriGroup_FddtAuxRowNum3:0;fgFiltriGroup_FddtAuxRow4:0;fgFiltriDataConsegna:0;fgFiltriGroup_FddtAuxRowNum2:0;fgFiltriGroup_FddtAuxRow1:0;fgFiltriGroup_FddtAuxRowNum1:0;fgFiltriGroup_FddtAuxRow2:0;fgFiltriGroup_FddtAuxHead1:0;fgFiltriGroup_FprezzoRiga:0;fgFiltriGroup_FddtAuxHead2:0;fgor_Prezzo_Diff:0;fgFiltriGroup_FquantitaRiga:0;fgFiltriGroup_FddtAuxRow9:0;fgFiltriGroup_FquantitaUnitaMisuraCodice:0;fgFiltriGroup_FddtAuxRow7:0;fgFiltriGroup_FddtAuxRow8:0;fgFiltriGroup_FdestinazioneIndirizzoOrdineRiga:0;fgor_Quantita_Diff:0;fgFiltriGroup_FddtAuxHead10:0;fgFiltriGroup_FlastDelNoteDate:0;fgFiltriGroup_FnotaReso:0;fgFiltriGroup_FddtAuxHeadDate3:0;fgFiltriGroup_FddtAuxHeadDate4:0;fgFiltriGroup_FddtAuxHeadDate1:0;fgFiltriProdottoCodice:0;fgFiltriProdottoDescrizione:0;fgFiltriGroup_FddtAuxHeadDate2:0;fgFiltriGroup_FlastTransDate:0;fgFiltriGroup_FddtAuxHeadDate5:0;fgor_DataConsegna_Diff:0;fgFiltriEtichette:0;fgFiltriAllegatiRiga:0;fgFiltriGroup_LastDeliveryDate:0;fgFiltriGroup_FordineRigaAttribute:0;fgFiltriGroup_FddtAuxRowDate5:0;fgor_OrdineNumero_Diff:0;fgFiltriGroup_FnumeroDdt:0;fgFiltriRitardoRisposta:0;fgFiltriDataRicezione:0;fgFiltriGroup_FddtAuxHeadNum1:0;fgFiltriGroup_FddtAuxHeadNum2:0;fgFiltriGroup_FddtAuxRowDate1:0;fgFiltriGroup_FddtAuxRowDate2:0;fgFiltriGroup_FddtAuxRowDate3:0;fgFiltriGroup_FddtAuxRowDate4:0;fgFiltriRitardoConsegna:0;fgFiltriTipoOrdine:0;fgFiltriOrdineNumero:0;fgFiltriDataEmissione:0;fgFiltriTipologiaInvio:0;fgFiltriNumeroCommessa:0;fgFiltriGroup_FprezzoUnitaMisuraCodice:0;fgFiltriGroup_FdestinazioneIndirizzoOrdine:0;fgFiltriGroup_FddtAuxRow10:0;fgFiltriGruppiPartner:0;fgFiltriAllegatiTestata:0;fgFiltriDataAccettazione:0;fgFiltriGroup_FordineAttribute:0;fgFiltriGroup_FddtAuxHead9:0;fgFiltriGroup_FquantitaUnitaMisura:0;fgFiltriBolleDirezione:0;fgFiltriGroup_FddtAuxHead7:0;fgFiltriGroup_FddtAuxHead8:0;fgFiltriGroup_FddtAuxHead5:0;fgFiltriGroup_FddtAuxHead6:0;fgFiltriGroup_FddtAuxHead3:0;fgFiltriGroup_FddtAuxHead4:0;fgFiltriGroup_FprezzoUnitaMisura:0;fgFiltriGroup_FstatoTipo:0;fgFiltriGroup_FddtAuxHeadNum5:0;fgDifferenzeValoriProp:0;fgFiltriGroup_FddtAuxHeadNum3:0;fgFiltriGroup_FddtAuxHeadNum4:0;fgFiltriGroup_FfornitoreConfigurazione:0;_oprValueMap:null;_oprValueMap:null;DivisoreParametri:null'),(1361,3,'5.custom.ordiniParametriRicerca_menu_ordini_out','');
/*!40000 ALTER TABLE `persona_configurazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piano_campionamento`
--

DROP TABLE IF EXISTS `piano_campionamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piano_campionamento` (
  `PIANO_CAMPIONAMENTO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(64) NOT NULL DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `NUMERO_ORDINE` int(11) NOT NULL,
  `PREDEFINITO` int(11) NOT NULL DEFAULT '0',
  `COLLAUDO_TIPO_ID` int(11) NOT NULL,
  PRIMARY KEY (`PIANO_CAMPIONAMENTO_ID`),
  UNIQUE KEY `CODE` (`CODE`,`COLLAUDO_TIPO_ID`),
  KEY `COLLAUDO_TIPO_ID` (`COLLAUDO_TIPO_ID`),
  CONSTRAINT `piano_campionamento_ibfk_1` FOREIGN KEY (`COLLAUDO_TIPO_ID`) REFERENCES `collaudo_tipo` (`COLLAUDO_TIPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piano_campionamento`
--

LOCK TABLES `piano_campionamento` WRITE;
/*!40000 ALTER TABLE `piano_campionamento` DISABLE KEYS */;
INSERT INTO `piano_campionamento` VALUES (2,'Semplice ridotto','',1,1,1);
/*!40000 ALTER TABLE `piano_campionamento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `piano_campionamento_cella`
--

DROP TABLE IF EXISTS `piano_campionamento_cella`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `piano_campionamento_cella` (
  `PIANO_CAMPIONAMENTO_CELLA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PIANO_CAMPIONAMENTO_ID` int(11) NOT NULL,
  `NUMEROSITA_CAMPIONE_ID` int(11) NOT NULL,
  `QUALITA_LIVELLO_ID` int(11) NOT NULL,
  `NUMERO_ACCETTAZIONE` int(11) DEFAULT NULL,
  `NUMERO_RIFIUTO` int(11) NOT NULL,
  `NUMERO_CAMPIONE` int(11) NOT NULL,
  PRIMARY KEY (`PIANO_CAMPIONAMENTO_CELLA_ID`),
  UNIQUE KEY `PIANO_CAMPIONAMENTO_ID_2` (`PIANO_CAMPIONAMENTO_ID`,`NUMEROSITA_CAMPIONE_ID`,`QUALITA_LIVELLO_ID`),
  KEY `PIANO_CAMPIONAMENTO_ID` (`PIANO_CAMPIONAMENTO_ID`),
  KEY `NUMEROSITA_CAMPIONE_ID` (`NUMEROSITA_CAMPIONE_ID`),
  KEY `QUALITA_LIVELLO_ID` (`QUALITA_LIVELLO_ID`),
  CONSTRAINT `piano_campionamento_cella_ibfk_1` FOREIGN KEY (`PIANO_CAMPIONAMENTO_ID`) REFERENCES `piano_campionamento` (`PIANO_CAMPIONAMENTO_ID`),
  CONSTRAINT `piano_campionamento_cella_ibfk_2` FOREIGN KEY (`NUMEROSITA_CAMPIONE_ID`) REFERENCES `numerosita_campione` (`NUMEROSITA_CAMPIONE_ID`),
  CONSTRAINT `piano_campionamento_cella_ibfk_3` FOREIGN KEY (`QUALITA_LIVELLO_ID`) REFERENCES `qualita_livello` (`QUALITA_LIVELLO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=417 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `piano_campionamento_cella`
--

LOCK TABLES `piano_campionamento_cella` WRITE;
/*!40000 ALTER TABLE `piano_campionamento_cella` DISABLE KEYS */;
INSERT INTO `piano_campionamento_cella` VALUES (1,2,1,1,0,1,2),(2,2,2,1,0,1,2),(3,2,3,1,0,1,2),(4,2,4,1,0,1,3),(5,2,5,1,0,1,5),(6,2,6,1,0,1,8),(7,2,7,1,0,1,13),(8,2,8,1,0,1,20),(9,2,9,1,0,1,32),(10,2,10,1,0,1,50),(11,2,11,1,0,1,80),(12,2,12,1,0,1,125),(13,2,13,1,0,1,200),(14,2,14,1,0,1,315),(15,2,15,1,0,1,500),(16,2,16,1,0,1,800),(17,2,1,2,0,1,2),(18,2,2,2,0,1,2),(19,2,3,2,0,1,2),(20,2,4,2,0,1,3),(21,2,5,2,0,1,5),(22,2,6,2,0,1,8),(23,2,7,2,0,1,13),(24,2,8,2,0,1,20),(25,2,9,2,0,1,32),(26,2,10,2,0,1,50),(27,2,11,2,0,1,80),(28,2,12,2,0,1,125),(29,2,13,2,0,1,200),(30,2,14,2,0,1,315),(31,2,15,2,0,1,500),(32,2,16,2,0,1,800),(33,2,1,3,0,1,2),(34,2,2,3,0,1,2),(35,2,3,3,0,1,2),(36,2,4,3,0,1,3),(37,2,5,3,0,1,5),(38,2,6,3,0,1,8),(39,2,7,3,0,1,13),(40,2,8,3,0,1,20),(41,2,9,3,0,1,32),(42,2,10,3,0,1,50),(43,2,11,3,0,1,80),(44,2,12,3,0,1,125),(45,2,13,3,0,1,200),(46,2,14,3,0,1,315),(47,2,15,3,0,2,500),(48,2,16,3,0,2,800),(49,2,1,4,0,1,2),(50,2,2,4,0,1,2),(51,2,3,4,0,1,2),(52,2,4,4,0,1,3),(53,2,5,4,0,1,5),(54,2,6,4,0,1,8),(55,2,7,4,0,1,13),(56,2,8,4,0,1,20),(57,2,9,4,0,1,32),(58,2,10,4,0,1,50),(59,2,11,4,0,1,80),(60,2,12,4,0,1,125),(61,2,13,4,0,1,200),(62,2,14,4,0,2,315),(63,2,15,4,0,2,500),(64,2,16,4,1,3,800),(65,2,1,5,0,1,2),(66,2,2,5,0,1,2),(67,2,3,5,0,1,2),(68,2,4,5,0,1,3),(69,2,5,5,0,1,5),(70,2,6,5,0,1,8),(71,2,7,5,0,1,13),(72,2,8,5,0,1,20),(73,2,9,5,0,1,32),(74,2,10,5,0,1,50),(75,2,11,5,0,1,80),(76,2,12,5,0,1,125),(77,2,13,5,0,2,200),(78,2,14,5,0,2,315),(79,2,15,5,1,3,500),(80,2,16,5,1,4,800),(81,2,1,6,0,1,2),(82,2,2,6,0,1,2),(83,2,3,6,0,1,2),(84,2,4,6,0,1,3),(85,2,5,6,0,1,5),(86,2,6,6,0,1,8),(87,2,7,6,0,1,13),(88,2,8,6,0,1,20),(89,2,9,6,0,1,32),(90,2,10,6,0,1,50),(91,2,11,6,0,1,80),(92,2,12,6,0,2,125),(93,2,13,6,0,2,200),(94,2,14,6,1,3,315),(95,2,15,6,1,4,500),(96,2,16,6,2,5,800),(97,2,1,7,0,1,2),(98,2,2,7,0,1,2),(99,2,3,7,0,1,2),(100,2,4,7,0,1,3),(101,2,5,7,0,1,5),(102,2,6,7,0,1,8),(103,2,7,7,0,1,13),(104,2,8,7,0,1,20),(105,2,9,7,0,1,32),(106,2,10,7,0,1,50),(107,2,11,7,0,2,80),(108,2,12,7,0,2,125),(109,2,13,7,1,3,200),(110,2,14,7,1,4,315),(111,2,15,7,2,5,500),(112,2,16,7,3,6,800),(113,2,1,8,0,1,2),(114,2,2,8,0,1,2),(115,2,3,8,0,1,2),(116,2,4,8,0,1,3),(117,2,5,8,0,1,5),(118,2,6,8,0,1,8),(119,2,7,8,0,1,13),(120,2,8,8,0,1,20),(121,2,9,8,0,1,32),(122,2,10,8,0,2,50),(123,2,11,8,0,2,80),(124,2,12,8,1,3,125),(125,2,13,8,1,4,200),(126,2,14,8,2,5,315),(127,2,15,8,3,6,500),(128,2,16,8,5,8,800),(129,2,1,9,0,1,2),(130,2,2,9,0,1,2),(131,2,3,9,0,1,2),(132,2,4,9,0,1,3),(133,2,5,9,0,1,5),(134,2,6,9,0,1,8),(135,2,7,9,0,1,13),(136,2,8,9,0,1,20),(137,2,9,9,0,2,32),(138,2,10,9,0,2,50),(139,2,11,9,1,3,80),(140,2,12,9,1,4,125),(141,2,13,9,2,5,200),(142,2,14,9,3,6,315),(143,2,15,9,5,8,500),(144,2,16,9,7,10,800),(145,2,1,10,0,1,2),(146,2,2,10,0,1,2),(147,2,3,10,0,1,2),(148,2,4,10,0,1,3),(149,2,5,10,0,1,5),(150,2,6,10,0,1,8),(151,2,7,10,0,1,13),(152,2,8,10,0,2,20),(153,2,9,10,0,2,32),(154,2,10,10,1,3,50),(155,2,11,10,1,4,80),(156,2,12,10,2,5,125),(157,2,13,10,3,6,200),(158,2,14,10,5,8,315),(159,2,15,10,7,10,500),(160,2,16,10,10,13,800),(161,2,1,11,0,1,2),(162,2,2,11,0,1,2),(163,2,3,11,0,1,2),(164,2,4,11,0,1,3),(165,2,5,11,0,1,5),(166,2,6,11,0,1,8),(167,2,7,11,0,2,13),(168,2,8,11,0,2,20),(169,2,9,11,1,3,32),(170,2,10,11,1,4,50),(171,2,11,11,2,5,80),(172,2,12,11,3,6,125),(173,2,13,11,5,8,200),(174,2,14,11,7,10,315),(175,2,15,11,10,13,500),(176,2,16,11,10,13,800),(177,2,1,12,0,1,2),(178,2,2,12,0,1,2),(179,2,3,12,0,1,2),(180,2,4,12,0,1,3),(181,2,5,12,0,1,5),(182,2,6,12,0,2,8),(183,2,7,12,0,2,13),(184,2,8,12,1,3,20),(185,2,9,12,1,4,32),(186,2,10,12,2,5,50),(187,2,11,12,3,6,80),(188,2,12,12,5,8,125),(189,2,13,12,7,10,200),(190,2,14,12,10,13,315),(191,2,15,12,10,13,500),(192,2,16,12,10,13,800),(193,2,1,13,0,1,2),(194,2,2,13,0,1,2),(195,2,3,13,0,1,2),(196,2,4,13,0,1,3),(197,2,5,13,0,2,5),(198,2,6,13,0,2,8),(199,2,7,13,1,3,13),(200,2,8,13,1,4,20),(201,2,9,13,2,5,32),(202,2,10,13,3,6,50),(203,2,11,13,5,8,80),(204,2,12,13,7,10,125),(205,2,13,13,10,13,200),(206,2,14,13,10,13,315),(207,2,15,13,10,13,500),(208,2,16,13,10,13,800),(209,2,1,14,0,1,2),(210,2,2,14,0,1,2),(211,2,3,14,0,1,2),(212,2,4,14,0,2,3),(213,2,5,14,0,2,5),(214,2,6,14,1,3,8),(215,2,7,14,1,4,13),(216,2,8,14,2,5,20),(217,2,9,14,3,6,32),(218,2,10,14,5,8,50),(219,2,11,14,7,10,80),(220,2,12,14,10,13,125),(221,2,13,14,10,13,200),(222,2,14,14,10,13,315),(223,2,15,14,10,13,500),(224,2,16,14,10,13,800),(225,2,1,15,0,1,2),(226,2,2,15,0,1,2),(227,2,3,15,0,2,2),(228,2,4,15,0,2,3),(229,2,5,15,1,3,5),(230,2,6,15,1,4,8),(231,2,7,15,2,5,13),(232,2,8,15,3,6,20),(233,2,9,15,5,8,32),(234,2,10,15,7,10,50),(235,2,11,15,10,13,80),(236,2,12,15,10,13,125),(237,2,13,15,10,13,200),(238,2,14,15,10,13,315),(239,2,15,15,10,13,500),(240,2,16,15,10,13,800),(241,2,1,16,0,2,2),(242,2,2,16,0,2,2),(243,2,3,16,0,2,2),(244,2,4,16,1,3,3),(245,2,5,16,1,4,5),(246,2,6,16,2,5,8),(247,2,7,16,3,6,13),(248,2,8,16,5,8,20),(249,2,9,16,7,10,32),(250,2,10,16,10,13,50),(251,2,11,16,10,13,80),(252,2,12,16,10,13,125),(253,2,13,16,10,13,200),(254,2,14,16,10,13,315),(255,2,15,16,10,13,500),(256,2,16,16,10,13,800),(257,2,1,17,0,2,2),(258,2,2,17,0,2,2),(259,2,3,17,1,3,2),(260,2,4,17,1,4,3),(261,2,5,17,2,5,5),(262,2,6,17,3,6,8),(263,2,7,17,5,8,13),(264,2,8,17,7,10,20),(265,2,9,17,10,13,32),(266,2,10,17,10,13,50),(267,2,11,17,10,13,80),(268,2,12,17,10,13,125),(269,2,13,17,10,13,200),(270,2,14,17,10,13,315),(271,2,15,17,10,13,500),(272,2,16,17,10,13,800),(273,2,1,18,1,2,2),(274,2,2,18,1,3,2),(275,2,3,18,1,4,2),(276,2,4,18,2,5,3),(277,2,5,18,3,6,5),(278,2,6,18,5,8,8),(279,2,7,18,7,10,13),(280,2,8,18,10,13,20),(281,2,9,18,10,13,32),(282,2,10,18,10,13,50),(283,2,11,18,10,13,80),(284,2,12,18,10,13,125),(285,2,13,18,10,13,200),(286,2,14,18,10,13,315),(287,2,15,18,10,13,500),(288,2,16,18,10,13,800),(289,2,1,19,2,3,2),(290,2,2,19,2,4,2),(291,2,3,19,2,5,2),(292,2,4,19,3,6,3),(293,2,5,19,5,8,5),(294,2,6,19,7,10,8),(295,2,7,19,10,13,13),(296,2,8,19,10,13,20),(297,2,9,19,10,13,32),(298,2,10,19,10,13,50),(299,2,11,19,10,13,80),(300,2,12,19,10,13,125),(301,2,13,19,10,13,200),(302,2,14,19,10,13,315),(303,2,15,19,10,13,500),(304,2,16,19,10,13,800),(305,2,1,20,3,4,2),(306,2,2,20,3,5,2),(307,2,3,20,3,6,2),(308,2,4,20,5,8,3),(309,2,5,20,7,10,5),(310,2,6,20,10,13,8),(311,2,7,20,10,13,13),(312,2,8,20,10,13,20),(313,2,9,20,10,13,32),(314,2,10,20,10,13,50),(315,2,11,20,10,13,80),(316,2,12,20,10,13,125),(317,2,13,20,10,13,200),(318,2,14,20,10,13,315),(319,2,15,20,10,13,500),(320,2,16,20,10,13,800),(321,2,1,21,5,6,2),(322,2,2,21,5,6,2),(323,2,3,21,5,8,2),(324,2,4,21,7,10,3),(325,2,5,21,10,13,5),(326,2,6,21,10,13,8),(327,2,7,21,10,13,13),(328,2,8,21,10,13,20),(329,2,9,21,10,13,32),(330,2,10,21,10,13,50),(331,2,11,21,10,13,80),(332,2,12,21,10,13,125),(333,2,13,21,10,13,200),(334,2,14,21,10,13,315),(335,2,15,21,10,13,500),(336,2,16,21,10,13,800),(337,2,1,22,7,8,2),(338,2,2,22,7,8,2),(339,2,3,22,7,10,2),(340,2,4,22,10,13,3),(341,2,5,22,14,17,5),(342,2,6,22,14,17,8),(343,2,7,22,14,17,13),(344,2,8,22,14,17,20),(345,2,9,22,14,17,32),(346,2,10,22,14,17,50),(347,2,11,22,14,17,80),(348,2,12,22,14,17,125),(349,2,13,22,14,17,200),(350,2,14,22,14,17,315),(351,2,15,22,14,17,500),(352,2,16,22,14,17,800),(353,2,1,23,10,11,2),(354,2,2,23,10,11,2),(355,2,3,23,10,13,2),(356,2,4,23,14,17,3),(357,2,5,23,21,24,5),(358,2,6,23,21,24,8),(359,2,7,23,21,24,13),(360,2,8,23,21,24,20),(361,2,9,23,21,24,32),(362,2,10,23,21,24,50),(363,2,11,23,21,24,80),(364,2,12,23,21,24,125),(365,2,13,23,21,24,200),(366,2,14,23,21,24,315),(367,2,15,23,21,24,500),(368,2,16,23,21,24,800),(369,2,1,24,14,15,2),(370,2,2,24,14,15,2),(371,2,3,24,14,17,2),(372,2,4,24,21,24,3),(373,2,5,24,21,24,5),(374,2,6,24,21,24,8),(375,2,7,24,21,24,13),(376,2,8,24,21,24,20),(377,2,9,24,21,24,32),(378,2,10,24,21,24,50),(379,2,11,24,21,24,80),(380,2,12,24,21,24,125),(381,2,13,24,21,24,200),(382,2,14,24,21,24,315),(383,2,15,24,21,24,500),(384,2,16,24,21,24,800),(385,2,1,25,21,22,2),(386,2,2,25,21,22,2),(387,2,3,25,21,24,2),(388,2,4,25,21,24,3),(389,2,5,25,21,24,5),(390,2,6,25,21,24,8),(391,2,7,25,21,24,13),(392,2,8,25,21,24,20),(393,2,9,25,21,24,32),(394,2,10,25,21,24,50),(395,2,11,25,21,24,80),(396,2,12,25,21,24,125),(397,2,13,25,21,24,200),(398,2,14,25,21,24,315),(399,2,15,25,21,24,500),(400,2,16,25,21,24,800),(401,2,1,26,30,31,2),(402,2,2,26,30,31,2),(403,2,3,26,30,31,2),(404,2,4,26,30,31,3),(405,2,5,26,30,31,5),(406,2,6,26,30,31,8),(407,2,7,26,30,31,13),(408,2,8,26,30,31,20),(409,2,9,26,30,31,32),(410,2,10,26,30,31,50),(411,2,11,26,30,31,80),(412,2,12,26,30,31,125),(413,2,13,26,30,31,200),(414,2,14,26,30,31,315),(415,2,15,26,30,31,500),(416,2,16,26,30,31,800);
/*!40000 ALTER TABLE `piano_campionamento_cella` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugin_db`
--

DROP TABLE IF EXISTS `plugin_db`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugin_db` (
  `PLUGIN_DB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `NOME` varchar(99) DEFAULT NULL,
  `DESCRIZIONE` varchar(254) DEFAULT NULL,
  `CLASSE` varchar(254) DEFAULT NULL,
  `ATTIVO` int(11) DEFAULT '0',
  PRIMARY KEY (`PLUGIN_DB_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`NOME`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `plugin_db_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugin_db`
--

LOCK TABLES `plugin_db` WRITE;
/*!40000 ALTER TABLE `plugin_db` DISABLE KEYS */;
INSERT INTO `plugin_db` VALUES (1,5,'inM2','Importa file Xml generati da gestionale','it.democenter.m2.scambioDati.plugin.ImportFileXmlEng',1),(2,5,'trMGAttSendEcho','Spedizione delle documentazioni come allegati delle mail','it.democenter.m2.om.plugins.pluginTrigger.ordini.MailAttach_OrdineRigaDocumentazione',1),(3,5,'outSd','Scrive in output i file generati da Iungo','it.democenter.m2.scambioDati.plugin.ExportFile',1),(4,5,'AlignInfo','Processo per l\'allineamento orario delle informazioni','it.democenter.m2.om.plugins.AlignInfo',1),(5,5,'reportScheduler','Schedulazione Report','it.democenter.m2.customers.scambioDati._common.TimedAction_scheduledReport',1),(6,5,'ChangeSubject','Cambia gli oggetti delle email inviate','it.democenter.m2.customers.scambioDati._common.ChangeMailSubject',1),(7,5,'invioPdC','Permette di definire quando inviare la previsione di consegna al fornitore','it.democenter.m2.customers.scambioDati._common.TimedAction_invioPrevisioneDiConsegna',1),(8,5,'exportDDT','Esporta un DDT ogni volta che viene salvata una sua rigaDDT nello stato sped','it.democenter.m2.om.plugins.pluginTrigger.PluginTrigger_DocumentoTrasporto_onSave_writeFileXML_conditions',1);
/*!40000 ALTER TABLE `plugin_db` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plugin_db_configurazione`
--

DROP TABLE IF EXISTS `plugin_db_configurazione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plugin_db_configurazione` (
  `PLUGIN_DB_CONFIGURAZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PLUGIN_DB_ID` int(11) NOT NULL,
  `NOME` varchar(254) NOT NULL,
  `VALORE` mediumtext,
  PRIMARY KEY (`PLUGIN_DB_CONFIGURAZIONE_ID`),
  UNIQUE KEY `PLUGIN_DB_ID_2` (`PLUGIN_DB_ID`,`NOME`),
  KEY `PLUGIN_DB_ID` (`PLUGIN_DB_ID`),
  CONSTRAINT `plugin_db_configurazione_ibfk_1` FOREIGN KEY (`PLUGIN_DB_ID`) REFERENCES `plugin_db` (`PLUGIN_DB_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plugin_db_configurazione`
--

LOCK TABLES `plugin_db_configurazione` WRITE;
/*!40000 ALTER TABLE `plugin_db_configurazione` DISABLE KEYS */;
INSERT INTO `plugin_db_configurazione` VALUES (1,1,'workDirectory','${it.democenter.m2.scambioDati.BaseDir}/toIungo'),(2,1,'doneDirectory','${it.democenter.m2.scambioDati.BaseDir}/doneIungo'),(3,1,'dataFileExtension','.xml'),(4,1,'acknowledgeFileExtension','.ack'),(5,1,'errorFileExtension','.err'),(6,1,'acknowledgeDeleteBeforeDataFile','true'),(7,1,'checkIntervalSeconds','20'),(8,1,'aggiungiData','false'),(9,1,'userName','sysScheduler'),(10,1,'userPassword','sysScheduler'),(11,1,'eventoSegnalato_default',''),(12,1,'sdOrdineTipo_ordineWfNome_default','simple'),(13,1,'ordineWfStatoNome_default','crea'),(14,1,'ordineRemoveOrdineRigasNotInSdOrdine','false'),(15,1,'eventoSegnalatoForOrdineRigasNotInSdOrdine',''),(16,1,'importErrorEmail',''),(17,1,'previsionaleTipoOrdineNome_default','FD'),(18,1,'importazioneBloccante','false'),(19,3,'checkIntervalSeconds','20'),(20,3,'userName','sysScheduler'),(21,3,'userPassword','sysScheduler'),(22,3,'errorEmail','monitor@iungo.it'),(23,1,'syncronizedRun','true'),(24,1,'fileCharsetName','8859_1'),(25,1,'deleteAttributesNotInDataSlice','false'),(26,1,'inheritsAttachments',''),(27,1,'syncProduct','add'),(28,1,'syncProductList',''),(29,2,'enable_purchase','FAX,MAIL,MAIL POST,MAIL PDF'),(30,2,'enable_sale','FAX,MAIL,MAIL POST,MAIL PDF'),(31,2,'enable_rfq','FAX,MAIL,MAIL POST,MAIL PDF'),(32,2,'enable_working','FAX,MAIL,MAIL POST,MAIL PDF'),(33,2,'enable_communication','FAX,MAIL,MAIL POST,MAIL PDF'),(34,4,'checkIntervalSeconds','3600'),(35,4,'userName','sysScheduler'),(36,4,'userPassword','sysScheduler'),(37,4,'errorEmail','monitor@iungo.it'),(38,4,'runAtTime','05:00:00'),(39,4,'maxTolleranceTime',''),(40,4,'runAtDay','all'),(41,4,'runAtMonth','all'),(42,4,'runAtDayOfMonth','all'),(43,4,'startDateIntervalDays',''),(44,4,'intervalDays',''),(45,4,'stopAtTime',''),(46,4,'excludedOrderTypes',''),(47,4,'excludedStatusRow','crea,chiuso,annullato'),(48,5,'checkIntervalSeconds','600'),(49,5,'userName','sysScheduler'),(50,5,'userPassword','sysScheduler'),(51,5,'errorEmail','monitor@iungo.it'),(52,5,'runAtTime',''),(53,5,'maxTolleranceTime',''),(54,5,'runAtDay','all'),(55,5,'runAtMonth','all'),(56,5,'runAtDayOfMonth','all'),(57,5,'startDateIntervalDays',''),(58,5,'intervalDays',''),(59,5,'stopAtTime',''),(60,5,'iungoLocalUrl','${it.democenter.m2.IungoUrl}'),(61,1,'scheduleNumberControl','false'),(62,1,'syncDeclaration','false'),(63,1,'syncDeclarationInheritsAttachments','false'),(64,7,'checkIntervalSeconds','300'),(65,7,'runAtMonth','all'),(66,7,'runAtDay','Sunday'),(67,7,'runAtTime','00:00:00'),(68,7,'errorEmail',''),(69,7,'userName','sysScheduler'),(70,7,'userPassword','sysScheduler'),(71,2,'userName','sysScheduler'),(72,2,'userPassword','sysScheduler'),(73,1,'alignClassCommodities',''),(74,7,'maxTolleranceTime',''),(75,7,'runAtDayOfMonth','all'),(76,7,'startDateIntervalDays',''),(77,7,'intervalDays',''),(78,7,'stopAtTime',''),(79,8,'workDirectory','${it.democenter.m2.scambioDati.BaseDir}/fromIungo'),(80,8,'dataFileExtension','.xml'),(81,8,'acknowledgeFileExtension','.ack'),(82,8,'exportType','ack'),(83,8,'lockFileExtension','lock'),(84,8,'dirLockFileName','_.lock'),(85,8,'saveOnRicevuto','true'),(86,8,'saveOnSpedito','true'),(87,8,'useShortFileName','true'),(88,8,'userName','sysScheduler'),(89,8,'userPassword','sysScheduler'),(90,8,'useDotFileName','true'),(91,8,'saveOnImportXml','false');
/*!40000 ALTER TABLE `plugin_db_configurazione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `previsionale`
--

DROP TABLE IF EXISTS `previsionale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `previsionale` (
  `PREVISIONALE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_ID` int(11) NOT NULL,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `DATA_EMISSIONE` datetime DEFAULT NULL,
  `STATO` varchar(30) NOT NULL,
  `NUMERO` varchar(50) DEFAULT '',
  `EMAIL` varchar(600) DEFAULT '',
  `INDIRIZZO` varchar(254) DEFAULT '',
  `INDIRIZZO_CODICE` varchar(50) DEFAULT '',
  `NOTA` varchar(254) DEFAULT NULL,
  `AUX_HEAD1` varchar(99) DEFAULT NULL,
  `AUX_HEAD2` varchar(99) DEFAULT NULL,
  `AUX_HEAD3` varchar(99) DEFAULT NULL,
  `AUX_HEAD4` varchar(99) DEFAULT NULL,
  `AUX_HEAD5` varchar(99) DEFAULT NULL,
  `AUX_HEAD6` varchar(99) DEFAULT NULL,
  `AUX_HEAD7` varchar(99) DEFAULT NULL,
  `AUX_HEAD8` varchar(99) DEFAULT NULL,
  `AUX_HEAD9` varchar(99) DEFAULT NULL,
  `AUX_HEAD10` varchar(99) DEFAULT NULL,
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`PREVISIONALE_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `previsionale_ibfk_1` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`),
  CONSTRAINT `previsionale_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `previsionale`
--

LOCK TABLES `previsionale` WRITE;
/*!40000 ALTER TABLE `previsionale` DISABLE KEYS */;
/*!40000 ALTER TABLE `previsionale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `previsionale_riga`
--

DROP TABLE IF EXISTS `previsionale_riga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `previsionale_riga` (
  `PREVISIONALE_RIGA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PREVISIONALE_ID` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `ORDINE_NUMERO` varchar(50) DEFAULT NULL,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `ORDINE_DATA` datetime DEFAULT NULL,
  `ORDINE_NUMERO_RIGA` varchar(30) DEFAULT '',
  `PRODOTTO_CODICE` varchar(200) DEFAULT '',
  `PRODOTTO_DESCRIZIONE` varchar(254) DEFAULT '',
  `QUANTITA` decimal(15,5) DEFAULT '0.00000',
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `DATA_CONSEGNA` datetime DEFAULT NULL,
  `NUMERO_RIGA` varchar(30) DEFAULT '',
  `PRODOTTO_CODICE_FORNITORE` varchar(200) DEFAULT '',
  `PRODOTTO_DESCRIZIONE_FORNITORE` varchar(254) DEFAULT '',
  `PRODOTTO_TIPO` varchar(30) DEFAULT '',
  `QUANTITA_MANCANTE` decimal(15,5) DEFAULT '0.00000',
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT '',
  `QUANTITA_UNITA_MISURA` varchar(100) DEFAULT '',
  `DATA_CHIUSURA` datetime DEFAULT NULL,
  `INDIRIZZO` varchar(254) DEFAULT '',
  `INDIRIZZO_CODICE` varchar(50) DEFAULT '',
  `PRIORITA` varchar(30) DEFAULT '',
  `NOTA` varchar(254) DEFAULT '',
  `AUX_ROW1` varchar(99) DEFAULT '',
  `AUX_ROW2` varchar(99) DEFAULT '',
  `AUX_ROW3` varchar(99) DEFAULT '',
  `AUX_ROW4` varchar(99) DEFAULT '',
  `AUX_ROW5` varchar(99) DEFAULT '',
  `AUX_ROW6` varchar(99) DEFAULT '',
  `AUX_ROW7` varchar(99) DEFAULT '',
  `AUX_ROW8` varchar(99) DEFAULT '',
  `AUX_ROW9` varchar(99) DEFAULT '',
  `AUX_ROW10` varchar(99) DEFAULT '',
  PRIMARY KEY (`PREVISIONALE_RIGA_ID`),
  KEY `PREVISIONALE_ID` (`PREVISIONALE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  KEY `PREVISIONALE_RIGA_PRODOTTO_CODICE` (`PRODOTTO_CODICE`),
  CONSTRAINT `previsionale_riga_ibfk_1` FOREIGN KEY (`PREVISIONALE_ID`) REFERENCES `previsionale` (`PREVISIONALE_ID`),
  CONSTRAINT `previsionale_riga_ibfk_2` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `previsionale_riga_ibfk_3` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `previsionale_riga`
--

LOCK TABLES `previsionale_riga` WRITE;
/*!40000 ALTER TABLE `previsionale_riga` DISABLE KEYS */;
/*!40000 ALTER TABLE `previsionale_riga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `processing`
--

DROP TABLE IF EXISTS `processing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `processing` (
  `PROCESSING_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `CODE` varchar(254) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `TYPE` varchar(254) DEFAULT '',
  `NOTA` mediumtext,
  PRIMARY KEY (`PROCESSING_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `processing_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `processing`
--

LOCK TABLES `processing` WRITE;
/*!40000 ALTER TABLE `processing` DISABLE KEYS */;
/*!40000 ALTER TABLE `processing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `PRODUCT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `PRODUCT_TYPE_ID` int(11) NOT NULL,
  `CODE` varchar(254) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `CODE_INTERNAL` varchar(254) DEFAULT '',
  `UNIT_OF_MEASURE` varchar(100) DEFAULT '',
  `FG_SOLD` int(11) DEFAULT NULL,
  `FG_PURCHASED` int(11) DEFAULT NULL,
  `CREATION_DATE` datetime DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `PREZZO` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `AUX_ITEM1` varchar(99) DEFAULT '',
  `AUX_ITEM2` varchar(99) DEFAULT '',
  `AUX_ITEM3` varchar(99) DEFAULT '',
  `AUX_ITEM4` varchar(99) DEFAULT '',
  `AUX_ITEM5` varchar(99) DEFAULT '',
  `AUX_ITEM6` varchar(99) DEFAULT '',
  `AUX_ITEM7` varchar(99) DEFAULT '',
  `AUX_ITEM8` varchar(99) DEFAULT '',
  `AUX_ITEM9` varchar(99) DEFAULT '',
  `AUX_ITEM10` varchar(99) DEFAULT '',
  `CODE_MASTER` varchar(100) DEFAULT NULL,
  `FG_IS_MASTER` int(1) NOT NULL DEFAULT '0',
  `COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `UNIT_OF_MEASURE_PRICE` varchar(30) DEFAULT '',
  `UNIT_OF_MEASURE_PRICE_CODE` varchar(3) DEFAULT '',
  `UNIT_OF_MEASURE_CODE` varchar(3) DEFAULT '',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_ID`),
  UNIQUE KEY `PRODUCT_DITTA_IDX` (`CODE`,`DITTA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `PRODUCT_TYPE_ID` (`PRODUCT_TYPE_ID`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `product_ibfk_2` FOREIGN KEY (`PRODUCT_TYPE_ID`) REFERENCES `product_type` (`PRODUCT_TYPE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=242 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (233,5,11,'Item1','Item 1','','Nr',1,0,NULL,NULL,0.00000,'','','','','','','','','','',NULL,0,1.00000,'','','',NULL),(234,5,12,'ItemA','Item A','','Nr.',0,1,NULL,NULL,0.00000,'','','','','','','','','','',NULL,0,1.00000,'','','',NULL),(235,5,12,'ItemB','Item B','','Nr.',0,1,NULL,NULL,0.00000,'','','','','','','','','','',NULL,0,1.00000,'','','',NULL),(236,5,13,'ItemX','Item X','','Nr.',0,0,NULL,NULL,0.00000,'','','','','','','','','','',NULL,0,1.00000,'','','',NULL),(237,5,12,'ItemC','Item C','','Nr.',0,1,NULL,NULL,0.00000,'','','','','','','','','','',NULL,0,1.00000,'','','',NULL),(238,5,14,'8.7.1.12345','AN.T.LAB.P.BAUDSL MPO90x8x123','','NR',0,1,NULL,NULL,0.00000,'','','','','','','','','','','',0,1.00000,'','','',NULL),(239,5,14,'8.7.1.152125','AN.T.LAB.P.BAUDSL 12412','','NR',0,1,NULL,NULL,0.00000,'','','','','','','','','','','',0,1.00000,'','','',NULL),(240,5,14,'8.124.1.12345','AN.T.asf 123','','NR',0,1,NULL,NULL,0.00000,'','','','','','','','','','','',0,1.00000,'','','',NULL),(241,5,14,'8.7.1245.12345','AN.T.LAB.P.BAUDSL 12521','','NR',0,1,NULL,NULL,0.00000,'','','','','','','','','','','',0,1.00000,'','','',NULL);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_batch`
--

DROP TABLE IF EXISTS `product_batch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_batch` (
  `PRODUCT_BATCH_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `CODE` varchar(254) DEFAULT NULL,
  `QUANTITY` decimal(12,5) DEFAULT '0.00000',
  `QUANTITY_REMAINING` decimal(12,5) DEFAULT '0.00000',
  `QUANTITY_SCRAP` decimal(12,5) DEFAULT '0.00000',
  `CREATION_DATE` datetime DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `USAGE_START_DATE` datetime DEFAULT NULL,
  `USAGE_END_DATE` datetime DEFAULT NULL,
  `FG_SOLD` int(11) DEFAULT NULL,
  `FG_PURCHASED` int(11) DEFAULT NULL,
  `AUX_BATCH1` varchar(99) DEFAULT '',
  `AUX_BATCH2` varchar(99) DEFAULT '',
  `AUX_BATCH3` varchar(99) DEFAULT '',
  `AUX_BATCH4` varchar(99) DEFAULT '',
  `AUX_BATCH5` varchar(99) DEFAULT '',
  `AUX_BATCH6` varchar(99) DEFAULT '',
  `AUX_BATCH7` varchar(99) DEFAULT '',
  `AUX_BATCH8` varchar(99) DEFAULT '',
  `AUX_BATCH9` varchar(99) DEFAULT '',
  `AUX_BATCH10` varchar(99) DEFAULT '',
  PRIMARY KEY (`PRODUCT_BATCH_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`),
  CONSTRAINT `product_batch_ibfk_1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `product` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_batch`
--

LOCK TABLES `product_batch` WRITE;
/*!40000 ALTER TABLE `product_batch` DISABLE KEYS */;
INSERT INTO `product_batch` VALUES (61,237,'C0001',100.00000,99.00000,0.00000,'2007-10-22 00:00:00',NULL,NULL,NULL,0,0,'','','','','','','','','',''),(62,236,'X0001',1.00000,0.00000,0.00000,'2008-01-23 00:00:00',NULL,NULL,NULL,0,0,'','','','','','','','','',''),(63,233,'10001',1.00000,0.00000,0.00000,'2008-01-23 00:00:00',NULL,NULL,NULL,0,0,'','','','','','','','','',''),(64,234,'A0001',100.00000,99.00000,0.00000,'2008-01-23 00:00:00',NULL,NULL,NULL,0,0,'','','','','','','','','',''),(65,235,'B0001',100.00000,99.00000,0.00000,'2008-01-23 00:00:00',NULL,NULL,NULL,0,0,'','','','','','','','','','');
/*!40000 ALTER TABLE `product_batch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_batch_formula`
--

DROP TABLE IF EXISTS `product_batch_formula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_batch_formula` (
  `PRODUCT_BATCH_FORMULA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `PRODUCT_FORMULA_ID` int(11) DEFAULT NULL,
  `CODE` varchar(254) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `DATE_START` datetime DEFAULT NULL,
  `DATE_END` datetime DEFAULT NULL,
  `NOTA` mediumtext,
  `MULTIPLIER` decimal(12,5) DEFAULT '1.00000',
  PRIMARY KEY (`PRODUCT_BATCH_FORMULA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `PRODUCT_FORMULA_ID` (`PRODUCT_FORMULA_ID`),
  CONSTRAINT `product_batch_formula_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `product_batch_formula_ibfk_2` FOREIGN KEY (`PRODUCT_FORMULA_ID`) REFERENCES `product_formula` (`PRODUCT_FORMULA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_batch_formula`
--

LOCK TABLES `product_batch_formula` WRITE;
/*!40000 ALTER TABLE `product_batch_formula` DISABLE KEYS */;
INSERT INTO `product_batch_formula` VALUES (33,5,10,'BoMX Proc 1','Processo 1 per distinta BoMX','2008-01-23 00:00:00','2008-01-23 00:00:00','',1.00000),(34,5,8,'BoM1 Proc 1','Processo 1 per distinta BoM1','2008-01-23 00:00:00','2008-01-23 00:00:00','BoM1--23-01-2008',1.00000);
/*!40000 ALTER TABLE `product_batch_formula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_batch_formula_item`
--

DROP TABLE IF EXISTS `product_batch_formula_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_batch_formula_item` (
  `PRODUCT_BATCH_FORMULA_ITEM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_BATCH_FORMULA_ID` int(11) NOT NULL,
  `TYPE` varchar(3) DEFAULT NULL,
  `PRODUCT_BATCH_ID` int(11) NOT NULL,
  `QUANTITY` decimal(12,5) DEFAULT '0.00000',
  `QUANTITY_SCRAP` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`PRODUCT_BATCH_FORMULA_ITEM_ID`),
  KEY `PRODUCT_BATCH_FORMULA_ID` (`PRODUCT_BATCH_FORMULA_ID`),
  KEY `PRODUCT_BATCH_ID` (`PRODUCT_BATCH_ID`),
  CONSTRAINT `product_batch_formula_item_ibfk_1` FOREIGN KEY (`PRODUCT_BATCH_FORMULA_ID`) REFERENCES `product_batch_formula` (`PRODUCT_BATCH_FORMULA_ID`),
  CONSTRAINT `product_batch_formula_item_ibfk_2` FOREIGN KEY (`PRODUCT_BATCH_ID`) REFERENCES `product_batch` (`PRODUCT_BATCH_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_batch_formula_item`
--

LOCK TABLES `product_batch_formula_item` WRITE;
/*!40000 ALTER TABLE `product_batch_formula_item` DISABLE KEYS */;
INSERT INTO `product_batch_formula_item` VALUES (101,33,'in',61,1.00000,0.00000),(102,33,'out',62,1.00000,0.00000),(103,34,'out',63,1.00000,0.00000),(104,34,'in',62,1.00000,0.00000),(105,34,'in',64,1.00000,0.00000),(106,34,'in',65,1.00000,0.00000);
/*!40000 ALTER TABLE `product_batch_formula_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_batch_formula_qualita_istruzione`
--

DROP TABLE IF EXISTS `product_batch_formula_qualita_istruzione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_batch_formula_qualita_istruzione` (
  `PRODUCT_BATCH_FORMULA_QUALITA_ISTRUZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_BATCH_FORMULA_ID` int(11) NOT NULL,
  `QUALITA_ISTRUZIONE_ID` int(11) NOT NULL,
  PRIMARY KEY (`PRODUCT_BATCH_FORMULA_QUALITA_ISTRUZIONE_ID`),
  KEY `PRODUCT_BATCH_FORMULA_ID` (`PRODUCT_BATCH_FORMULA_ID`),
  KEY `QUALITA_ISTRUZIONE_ID` (`QUALITA_ISTRUZIONE_ID`),
  CONSTRAINT `product_batch_formula_qualita_istruzione_ibfk_1` FOREIGN KEY (`PRODUCT_BATCH_FORMULA_ID`) REFERENCES `product_batch_formula` (`PRODUCT_BATCH_FORMULA_ID`),
  CONSTRAINT `product_batch_formula_qualita_istruzione_ibfk_2` FOREIGN KEY (`QUALITA_ISTRUZIONE_ID`) REFERENCES `qualita_istruzione` (`QUALITA_ISTRUZIONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_batch_formula_qualita_istruzione`
--

LOCK TABLES `product_batch_formula_qualita_istruzione` WRITE;
/*!40000 ALTER TABLE `product_batch_formula_qualita_istruzione` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_batch_formula_qualita_istruzione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_formula`
--

DROP TABLE IF EXISTS `product_formula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_formula` (
  `PRODUCT_FORMULA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `CODE` varchar(254) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `NOTA` mediumtext,
  `CREATION_DATE` datetime DEFAULT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_FORMULA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `product_formula_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_formula`
--

LOCK TABLES `product_formula` WRITE;
/*!40000 ALTER TABLE `product_formula` DISABLE KEYS */;
INSERT INTO `product_formula` VALUES (8,5,'BoM1','Bill Of Material for Item 1','',NULL,NULL),(10,5,'BoMX','Bill Of Material for Item X','',NULL,NULL);
/*!40000 ALTER TABLE `product_formula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_formula_item`
--

DROP TABLE IF EXISTS `product_formula_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_formula_item` (
  `PRODUCT_FORMULA_ITEM_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_FORMULA_ID` int(11) NOT NULL,
  `TYPE` varchar(3) DEFAULT NULL,
  `PRODUCT_ID` int(11) NOT NULL,
  `QUANTITY` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`PRODUCT_FORMULA_ITEM_ID`),
  KEY `PRODUCT_FORMULA_ID` (`PRODUCT_FORMULA_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`),
  CONSTRAINT `product_formula_item_ibfk_1` FOREIGN KEY (`PRODUCT_FORMULA_ID`) REFERENCES `product_formula` (`PRODUCT_FORMULA_ID`),
  CONSTRAINT `product_formula_item_ibfk_2` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `product` (`PRODUCT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_formula_item`
--

LOCK TABLES `product_formula_item` WRITE;
/*!40000 ALTER TABLE `product_formula_item` DISABLE KEYS */;
INSERT INTO `product_formula_item` VALUES (52,8,'in',234,2.00000),(53,8,'in',235,4.00000),(54,8,'out',233,1.00000),(65,8,'in',236,1.00000),(66,10,'in',237,1.00000),(67,10,'out',236,1.00000);
/*!40000 ALTER TABLE `product_formula_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_formula_processing`
--

DROP TABLE IF EXISTS `product_formula_processing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_formula_processing` (
  `PRODUCT_FORMULA_PROCESSING_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_FORMULA_ID` int(11) NOT NULL,
  `PROCESSING_ID` int(11) NOT NULL,
  `RESOURCE_ID` int(11) NOT NULL,
  `TIME_SETUP_SECONDS` decimal(12,5) DEFAULT '0.00000',
  `TIME_PER_UNIT_SECONDS` decimal(12,5) DEFAULT '0.00000',
  `QUANTITY` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  PRIMARY KEY (`PRODUCT_FORMULA_PROCESSING_ID`),
  KEY `PRODUCT_FORMULA_ID` (`PRODUCT_FORMULA_ID`),
  KEY `PROCESSING_ID` (`PROCESSING_ID`),
  KEY `RESOURCE_ID` (`RESOURCE_ID`),
  CONSTRAINT `product_formula_processing_ibfk_1` FOREIGN KEY (`PRODUCT_FORMULA_ID`) REFERENCES `product_formula` (`PRODUCT_FORMULA_ID`),
  CONSTRAINT `product_formula_processing_ibfk_2` FOREIGN KEY (`PROCESSING_ID`) REFERENCES `processing` (`PROCESSING_ID`),
  CONSTRAINT `product_formula_processing_ibfk_3` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource` (`RESOURCE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_formula_processing`
--

LOCK TABLES `product_formula_processing` WRITE;
/*!40000 ALTER TABLE `product_formula_processing` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_formula_processing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_formula_processing_secondary_resource`
--

DROP TABLE IF EXISTS `product_formula_processing_secondary_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_formula_processing_secondary_resource` (
  `PRODUCT_FORMULA_PROCESSING_SECONDARY_RESOURCE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_FORMULA_PROCESSING_ID` int(11) NOT NULL,
  `RESOURCE_ID` int(11) NOT NULL,
  `NOTA` mediumtext,
  PRIMARY KEY (`PRODUCT_FORMULA_PROCESSING_SECONDARY_RESOURCE_ID`),
  KEY `PRODUCT_FORMULA_PROCESSING_ID` (`PRODUCT_FORMULA_PROCESSING_ID`),
  CONSTRAINT `product_formula_processing_secondary_resource_ibfk_1` FOREIGN KEY (`PRODUCT_FORMULA_PROCESSING_ID`) REFERENCES `product_formula_processing` (`PRODUCT_FORMULA_PROCESSING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_formula_processing_secondary_resource`
--

LOCK TABLES `product_formula_processing_secondary_resource` WRITE;
/*!40000 ALTER TABLE `product_formula_processing_secondary_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_formula_processing_secondary_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_listino`
--

DROP TABLE IF EXISTS `product_listino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_listino` (
  `PRODUCT_LISTINO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `CODE_PARTNER` varchar(254) DEFAULT '',
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT NULL,
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `VALIDITY_START_DATE` datetime DEFAULT NULL,
  `VALIDITY_END_DATE` datetime DEFAULT NULL,
  `NOTA` varchar(254) DEFAULT '',
  `DESCRIPTION_PARTNER` varchar(254) DEFAULT '',
  `COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `PREZZO_UNITA_MISURA_CODICE` varchar(3) DEFAULT '',
  `QUANTITA_UNITA_MISURA` varchar(30) DEFAULT '',
  `QUANTITA_UNITA_MISURA_CODICE` varchar(3) DEFAULT '',
  `AUX_ITEM1` varchar(99) DEFAULT '',
  `AUX_ITEM2` varchar(99) DEFAULT '',
  `AUX_ITEM3` varchar(99) DEFAULT '',
  `AUX_ITEM4` varchar(99) DEFAULT '',
  `AUX_ITEM5` varchar(99) DEFAULT '',
  `AUX_ITEM6` varchar(99) DEFAULT '',
  `AUX_ITEM7` varchar(99) DEFAULT '',
  `AUX_ITEM8` varchar(99) DEFAULT '',
  `AUX_ITEM9` varchar(99) DEFAULT '',
  `AUX_ITEM10` varchar(99) DEFAULT '',
  `PRODUCT_LISTINO_TYPE_ID` int(11) DEFAULT '0',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  `ORIGIN_COUNTRY_CODE` varchar(25) DEFAULT '',
  PRIMARY KEY (`PRODUCT_LISTINO_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `product_listino_ibfk_1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `product` (`PRODUCT_ID`),
  CONSTRAINT `product_listino_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_listino`
--

LOCK TABLES `product_listino` WRITE;
/*!40000 ALTER TABLE `product_listino` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_listino` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_listino_type`
--

DROP TABLE IF EXISTS `product_listino_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_listino_type` (
  `PRODUCT_LISTINO_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `NAME` varchar(99) NOT NULL,
  `DESCRIPTION` varchar(254) DEFAULT NULL,
  `AUX_ITEM1_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM1_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM2_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM2_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM3_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM3_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM4_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM4_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM5_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM5_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM6_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM6_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM7_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM7_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM8_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM8_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM9_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM9_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM10_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM10_TYPE` varchar(10) DEFAULT '',
  PRIMARY KEY (`PRODUCT_LISTINO_TYPE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `PRODUCT_LISTINO_TYPE_NAME_DITTA_IDX` (`NAME`,`DITTA_ID`),
  CONSTRAINT `product_listino_type_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_listino_type`
--

LOCK TABLES `product_listino_type` WRITE;
/*!40000 ALTER TABLE `product_listino_type` DISABLE KEYS */;
INSERT INTO `product_listino_type` VALUES (1,5,'generic','generic product list type','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `product_listino_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_misura_qualita_istruzione`
--

DROP TABLE IF EXISTS `product_misura_qualita_istruzione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_misura_qualita_istruzione` (
  `PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_ID` int(11) NOT NULL,
  `QUALITA_ISTRUZIONE_ID` int(11) DEFAULT NULL,
  `NOMINALE` decimal(12,5) DEFAULT '0.00000',
  `MINIMA` decimal(12,5) DEFAULT '0.00000',
  `MASSIMA` decimal(12,5) DEFAULT '0.00000',
  `UNIT` varchar(100) DEFAULT '',
  PRIMARY KEY (`PRODUCT_MISURA_QUALITA_ISTRUZIONE_ID`),
  UNIQUE KEY `PRODUCT_ID_2` (`PRODUCT_ID`,`QUALITA_ISTRUZIONE_ID`),
  KEY `PRODUCT_ID` (`PRODUCT_ID`),
  KEY `QUALITA_ISTRUZIONE_ID` (`QUALITA_ISTRUZIONE_ID`),
  CONSTRAINT `product_misura_qualita_istruzione_ibfk_1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `product` (`PRODUCT_ID`),
  CONSTRAINT `product_misura_qualita_istruzione_ibfk_2` FOREIGN KEY (`QUALITA_ISTRUZIONE_ID`) REFERENCES `qualita_istruzione` (`QUALITA_ISTRUZIONE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_misura_qualita_istruzione`
--

LOCK TABLES `product_misura_qualita_istruzione` WRITE;
/*!40000 ALTER TABLE `product_misura_qualita_istruzione` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_misura_qualita_istruzione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_type`
--

DROP TABLE IF EXISTS `product_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_type` (
  `PRODUCT_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `NAME` varchar(99) NOT NULL,
  `DESCRIPTION` varchar(254) DEFAULT NULL,
  `AUX_ITEM1_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM1_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM2_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM2_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM3_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM3_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM4_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM4_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM5_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM5_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM6_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM6_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM7_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM7_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM8_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM8_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM9_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM9_TYPE` varchar(10) DEFAULT '',
  `AUX_ITEM10_DESCR` varchar(254) DEFAULT '',
  `AUX_ITEM10_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH1_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH1_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH2_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH2_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH3_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH3_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH4_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH4_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH5_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH5_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH6_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH6_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH7_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH7_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH8_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH8_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH9_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH9_TYPE` varchar(10) DEFAULT '',
  `AUX_BATCH10_DESCR` varchar(254) DEFAULT '',
  `AUX_BATCH10_TYPE` varchar(10) DEFAULT '',
  PRIMARY KEY (`PRODUCT_TYPE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `PRODUCT_TYPE_NAME_DITTA_IDX` (`NAME`,`DITTA_ID`),
  CONSTRAINT `product_type_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_type`
--

LOCK TABLES `product_type` WRITE;
/*!40000 ALTER TABLE `product_type` DISABLE KEYS */;
INSERT INTO `product_type` VALUES (11,5,'PFinale','Prodotto finale','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(12,5,'PAcquistato','Prodotto acquistato esternamente','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(13,5,'PInterno','Prodotto realizzato internamente','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(14,5,'general','general product type','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `product_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_type_qualita_istruzione`
--

DROP TABLE IF EXISTS `product_type_qualita_istruzione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_type_qualita_istruzione` (
  `PRODUCT_TYPE_QUALITA_ISTRUZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_TYPE_ID` int(11) NOT NULL,
  `QUALITA_ISTRUZIONE_ID` int(11) NOT NULL,
  `QUALITA_LIVELLO_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PRODUCT_TYPE_QUALITA_ISTRUZIONE_ID`),
  UNIQUE KEY `PRODUCT_TYPE_ID_2` (`PRODUCT_TYPE_ID`,`QUALITA_ISTRUZIONE_ID`),
  KEY `PRODUCT_TYPE_ID` (`PRODUCT_TYPE_ID`),
  KEY `QUALITA_ISTRUZIONE_ID` (`QUALITA_ISTRUZIONE_ID`),
  KEY `QUALITA_LIVELLO_ID` (`QUALITA_LIVELLO_ID`),
  CONSTRAINT `product_type_qualita_istruzione_ibfk_1` FOREIGN KEY (`PRODUCT_TYPE_ID`) REFERENCES `product_type` (`PRODUCT_TYPE_ID`),
  CONSTRAINT `product_type_qualita_istruzione_ibfk_2` FOREIGN KEY (`QUALITA_ISTRUZIONE_ID`) REFERENCES `qualita_istruzione` (`QUALITA_ISTRUZIONE_ID`),
  CONSTRAINT `product_type_qualita_istruzione_ibfk_3` FOREIGN KEY (`QUALITA_LIVELLO_ID`) REFERENCES `qualita_livello` (`QUALITA_LIVELLO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_type_qualita_istruzione`
--

LOCK TABLES `product_type_qualita_istruzione` WRITE;
/*!40000 ALTER TABLE `product_type_qualita_istruzione` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_type_qualita_istruzione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualita_istruzione`
--

DROP TABLE IF EXISTS `qualita_istruzione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qualita_istruzione` (
  `QUALITA_ISTRUZIONE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(64) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `FG_PRODOTTO` int(11) DEFAULT '1',
  `FG_MISURA` int(11) DEFAULT '0',
  `NOTA` mediumtext,
  `QUALITA_ISTRUZIONE_TIPO_ID` int(11) NOT NULL,
  PRIMARY KEY (`QUALITA_ISTRUZIONE_ID`),
  KEY `QUALITA_ISTRUZIONE_TIPO_ID` (`QUALITA_ISTRUZIONE_TIPO_ID`),
  CONSTRAINT `qualita_istruzione_ibfk_1` FOREIGN KEY (`QUALITA_ISTRUZIONE_TIPO_ID`) REFERENCES `qualita_istruzione_tipo` (`QUALITA_ISTRUZIONE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualita_istruzione`
--

LOCK TABLES `qualita_istruzione` WRITE;
/*!40000 ALTER TABLE `qualita_istruzione` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualita_istruzione` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualita_istruzione_tipo`
--

DROP TABLE IF EXISTS `qualita_istruzione_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qualita_istruzione_tipo` (
  `QUALITA_ISTRUZIONE_TIPO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `CODE` varchar(64) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  PRIMARY KEY (`QUALITA_ISTRUZIONE_TIPO_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `qualita_istruzione_tipo_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualita_istruzione_tipo`
--

LOCK TABLES `qualita_istruzione_tipo` WRITE;
/*!40000 ALTER TABLE `qualita_istruzione_tipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualita_istruzione_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualita_livello`
--

DROP TABLE IF EXISTS `qualita_livello`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qualita_livello` (
  `QUALITA_LIVELLO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NUMERO` decimal(12,5) DEFAULT '0.00000',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `NUMERO_ORDINE` int(11) NOT NULL,
  PRIMARY KEY (`QUALITA_LIVELLO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualita_livello`
--

LOCK TABLES `qualita_livello` WRITE;
/*!40000 ALTER TABLE `qualita_livello` DISABLE KEYS */;
INSERT INTO `qualita_livello` VALUES (1,0.01000,'Primo livello AQL',1),(2,0.01500,'',2),(3,0.02500,'',3),(4,0.04000,'',4),(5,0.06500,'',5),(6,0.10000,'',6),(7,0.25000,'',7),(8,0.40000,'',8),(9,0.15000,'',9),(10,0.65000,'',10),(11,1.00000,'',11),(12,1.50000,'',12),(13,2.50000,'',13),(14,4.00000,'',14),(15,6.50000,'',15),(16,10.00000,'',16),(17,15.00000,'',17),(18,25.00000,'',18),(19,40.00000,'',19),(20,65.00000,'',20),(21,100.00000,'',21),(22,150.00000,'',22),(23,250.00000,'',23),(24,400.00000,'',24),(25,650.00000,'',25),(26,1000.00000,'',26);
/*!40000 ALTER TABLE `qualita_livello` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qualita_origine`
--

DROP TABLE IF EXISTS `qualita_origine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qualita_origine` (
  `QUALITA_ORIGINE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `TIPOLOGIA_ITEM` int(11) NOT NULL,
  `CODE` varchar(64) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `FG_LIBERA` int(11) DEFAULT '1',
  PRIMARY KEY (`QUALITA_ORIGINE_ID`),
  UNIQUE KEY `TIPOLOGIA_ITEM` (`TIPOLOGIA_ITEM`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `qualita_origine_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qualita_origine`
--

LOCK TABLES `qualita_origine` WRITE;
/*!40000 ALTER TABLE `qualita_origine` DISABLE KEYS */;
/*!40000 ALTER TABLE `qualita_origine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue_inbound_ws`
--

DROP TABLE IF EXISTS `queue_inbound_ws`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_inbound_ws` (
  `QUEUE_INBOUND_WS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `QUEUE_MASTER_ID` int(11) NOT NULL,
  `CREATION_DATE` datetime NOT NULL,
  `DITTA_ID` int(11) DEFAULT NULL,
  `RAW_DATA` longblob,
  `SD_DATA` longblob,
  `PARAMETERS` longblob,
  `OBJECT_TYPE` int(11) DEFAULT NULL,
  `OBJECT_ID` int(11) DEFAULT NULL,
  `STATUS` varchar(30) NOT NULL,
  `EXCEPTION` longblob,
  `QUEUE_RAW_DATA_ID` int(11) DEFAULT NULL,
  `BLOCKED_BY_QUEUE_INBOUND_WS_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`QUEUE_INBOUND_WS_ID`),
  KEY `QUEUE_MASTER_ID` (`QUEUE_MASTER_ID`),
  KEY `QUEUE_INBOUND_WS_IDX` (`OBJECT_TYPE`,`OBJECT_ID`),
  CONSTRAINT `queue_inbound_ws_ibfk_1` FOREIGN KEY (`QUEUE_MASTER_ID`) REFERENCES `queue_master` (`QUEUE_MASTER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue_inbound_ws`
--

LOCK TABLES `queue_inbound_ws` WRITE;
/*!40000 ALTER TABLE `queue_inbound_ws` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue_inbound_ws` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue_master`
--

DROP TABLE IF EXISTS `queue_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_master` (
  `QUEUE_MASTER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UNIQUE_IDENTIFIER` varchar(30) NOT NULL,
  `DITTA_ID` int(11) DEFAULT NULL,
  `QUEUE_TYPE` varchar(30) NOT NULL,
  `COMUNICATION_TYPE` varchar(30) DEFAULT NULL,
  `REMOTE_TRANSMISSION_NUMBER` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`QUEUE_MASTER_ID`),
  UNIQUE KEY `UNIQUE_IDENTIFIER` (`UNIQUE_IDENTIFIER`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue_master`
--

LOCK TABLES `queue_master` WRITE;
/*!40000 ALTER TABLE `queue_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `queue_raw_data`
--

DROP TABLE IF EXISTS `queue_raw_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `queue_raw_data` (
  `QUEUE_RAW_DATA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `QUEUE_MASTER_ID` int(11) NOT NULL,
  `RAW_DATA` longblob,
  PRIMARY KEY (`QUEUE_RAW_DATA_ID`),
  KEY `QUEUE_MASTER_ID` (`QUEUE_MASTER_ID`),
  CONSTRAINT `queue_raw_data_ibfk_1` FOREIGN KEY (`QUEUE_MASTER_ID`) REFERENCES `queue_master` (`QUEUE_MASTER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `queue_raw_data`
--

LOCK TABLES `queue_raw_data` WRITE;
/*!40000 ALTER TABLE `queue_raw_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `queue_raw_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda`
--

DROP TABLE IF EXISTS `rda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda` (
  `RDA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `RDA_TIPO_ID` int(11) NOT NULL,
  `NUMERO_RDA` varchar(100) DEFAULT '',
  `DESCRIZIONE` varchar(254) DEFAULT '',
  `DATA_EMISSIONE` datetime DEFAULT NULL,
  `DATA_INIZIO` datetime DEFAULT NULL,
  `DATA_FINE` datetime DEFAULT NULL,
  `REFERENTE` varchar(254) DEFAULT NULL,
  `REFERENTE_TELEFONO` varchar(30) DEFAULT NULL,
  `DESTINAZIONE_INDIRIZZO` varchar(254) DEFAULT NULL,
  `CORRIERE` varchar(99) DEFAULT NULL,
  `IMBALLO` varchar(99) DEFAULT NULL,
  `IMBALLO_CODICE` varchar(2) DEFAULT NULL,
  `NOTA_RESO` varchar(99) DEFAULT NULL,
  `ESENTE_IVA` int(11) DEFAULT NULL,
  `VALUTA` varchar(20) DEFAULT NULL,
  `VALUTA_CODICE` varchar(3) DEFAULT NULL,
  `MODALITA_PAGAMENTO` varchar(50) DEFAULT NULL,
  `MODALITA_PAGAMENTO_CODICE` varchar(2) DEFAULT NULL,
  `INCOTERM_CODICE` varchar(3) DEFAULT NULL,
  `INCOTERM_LUOGO` varchar(255) DEFAULT NULL,
  `TRASPORTO_MODALITA_CODICE` varchar(3) DEFAULT NULL,
  `PAGAMENTO_TERMINI_CODICE` varchar(50) DEFAULT NULL,
  `SCONTO1` decimal(12,5) DEFAULT '0.00000',
  `SCONTO2` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  `AUX_HEAD1` varchar(99) DEFAULT NULL,
  `AUX_HEAD2` varchar(99) DEFAULT NULL,
  `AUX_HEAD3` varchar(99) DEFAULT NULL,
  `AUX_HEAD4` varchar(99) DEFAULT NULL,
  `AUX_HEAD5` varchar(255) DEFAULT NULL,
  `AUX_HEAD6` varchar(99) DEFAULT NULL,
  `AUX_HEAD7` varchar(99) DEFAULT NULL,
  `AUX_HEAD8` varchar(99) DEFAULT NULL,
  `AUX_HEAD9` varchar(99) DEFAULT NULL,
  `AUX_HEAD10` varchar(99) DEFAULT NULL,
  `AUX_HEAD_DATE1` datetime DEFAULT NULL,
  `AUX_HEAD_DATE2` datetime DEFAULT NULL,
  `AUX_HEAD_DATE3` datetime DEFAULT NULL,
  `AUX_HEAD_DATE4` datetime DEFAULT NULL,
  `AUX_HEAD_DATE5` datetime DEFAULT NULL,
  `AUX_HEAD_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_HEAD_NUM5` decimal(12,5) DEFAULT '0.00000',
  `AUTHOR` varchar(254) DEFAULT NULL,
  `REFERENTE_FAX` varchar(30) DEFAULT NULL,
  `REFERENTE_EMAIL` varchar(600) DEFAULT NULL,
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`RDA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `RDA_TIPO_ID` (`RDA_TIPO_ID`),
  CONSTRAINT `rda_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `rda_ibfk_2` FOREIGN KEY (`RDA_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda`
--

LOCK TABLES `rda` WRITE;
/*!40000 ALTER TABLE `rda` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda_alternativa`
--

DROP TABLE IF EXISTS `rda_alternativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda_alternativa` (
  `RDA_ALTERNATIVA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RDA_RIGA_ID` int(11) DEFAULT NULL,
  `MARCA` varchar(100) DEFAULT '',
  `QUANTITA_ALTERNATIVA` decimal(12,5) DEFAULT NULL,
  `PREZZO_ALTERNATIVA` decimal(12,5) DEFAULT NULL,
  `AUX1` varchar(255) DEFAULT NULL,
  `AUX2` varchar(255) DEFAULT NULL,
  `AUX3` varchar(255) DEFAULT NULL,
  `AUX4` varchar(255) DEFAULT NULL,
  `AUX5` varchar(255) DEFAULT NULL,
  `AUX6` varchar(255) DEFAULT NULL,
  `AUX7` varchar(255) DEFAULT NULL,
  `AUX8` varchar(255) DEFAULT NULL,
  `AUX9` varchar(255) DEFAULT NULL,
  `AUX10` varchar(255) DEFAULT NULL,
  `AUX_NUM1` decimal(12,5) DEFAULT NULL,
  `AUX_NUM2` decimal(12,5) DEFAULT NULL,
  `AUX_NUM3` decimal(12,5) DEFAULT NULL,
  `AUX_NUM4` decimal(12,5) DEFAULT NULL,
  `AUX_NUM5` decimal(12,5) DEFAULT NULL,
  `AUX_DATE1` datetime DEFAULT NULL,
  `AUX_DATE2` datetime DEFAULT NULL,
  `AUX_DATE3` datetime DEFAULT NULL,
  `AUX_DATE4` datetime DEFAULT NULL,
  `AUX_DATE5` datetime DEFAULT NULL,
  PRIMARY KEY (`RDA_ALTERNATIVA_ID`),
  KEY `RDA_RIGA_ID` (`RDA_RIGA_ID`),
  CONSTRAINT `rda_alternativa_ibfk_1` FOREIGN KEY (`RDA_RIGA_ID`) REFERENCES `rda_riga` (`RDA_RIGA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda_alternativa`
--

LOCK TABLES `rda_alternativa` WRITE;
/*!40000 ALTER TABLE `rda_alternativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda_alternativa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda_attribute`
--

DROP TABLE IF EXISTS `rda_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda_attribute` (
  `RDA_ATTRIBUTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RDA_ID` int(11) NOT NULL,
  `NAME` varchar(254) NOT NULL,
  `VALUE` varchar(254) DEFAULT '',
  `TYPE` varchar(15) DEFAULT 's',
  `DICTIONARY` int(11) DEFAULT '0',
  PRIMARY KEY (`RDA_ATTRIBUTE_ID`),
  UNIQUE KEY `RDA_ID_2` (`RDA_ID`,`NAME`),
  KEY `RDA_ID` (`RDA_ID`),
  CONSTRAINT `rda_attribute_ibfk_1` FOREIGN KEY (`RDA_ID`) REFERENCES `rda` (`RDA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda_attribute`
--

LOCK TABLES `rda_attribute` WRITE;
/*!40000 ALTER TABLE `rda_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda_fornitore`
--

DROP TABLE IF EXISTS `rda_fornitore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda_fornitore` (
  `RDA_FORNITORE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_ID` int(11) NOT NULL,
  `RDA_ID` int(11) NOT NULL,
  PRIMARY KEY (`RDA_FORNITORE_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `RDA_ID` (`RDA_ID`),
  CONSTRAINT `rda_fornitore_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `rda_fornitore_ibfk_2` FOREIGN KEY (`RDA_ID`) REFERENCES `rda` (`RDA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda_fornitore`
--

LOCK TABLES `rda_fornitore` WRITE;
/*!40000 ALTER TABLE `rda_fornitore` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda_fornitore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda_matrice`
--

DROP TABLE IF EXISTS `rda_matrice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda_matrice` (
  `RDA_MATRICE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RDA_RIGA_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `RDA_ALTERNATIVA_ID` int(11) DEFAULT NULL,
  `RDO_RIGA_ID` int(11) DEFAULT NULL,
  `AUX_MATRICE1` varchar(99) DEFAULT NULL,
  `AUX_MATRICE2` varchar(99) DEFAULT NULL,
  `AUX_MATRICE3` varchar(99) DEFAULT NULL,
  `AUX_MATRICE4` varchar(99) DEFAULT NULL,
  `AUX_MATRICE5` varchar(99) DEFAULT NULL,
  `AUX_MATRICE_DATE1` datetime DEFAULT NULL,
  `AUX_MATRICE_DATE2` datetime DEFAULT NULL,
  `AUX_MATRICE_DATE3` datetime DEFAULT NULL,
  `AUX_MATRICE_DATE4` datetime DEFAULT NULL,
  `AUX_MATRICE_DATE5` datetime DEFAULT NULL,
  `AUX_MATRICE_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_MATRICE_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_MATRICE_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_MATRICE_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_MATRICE_NUM5` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`RDA_MATRICE_ID`),
  KEY `RDA_RIGA_ID` (`RDA_RIGA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `RDO_RIGA_ID` (`RDO_RIGA_ID`),
  KEY `RDA_ALTERNATIVA_ID` (`RDA_ALTERNATIVA_ID`),
  CONSTRAINT `rda_matrice_ibfk_1` FOREIGN KEY (`RDA_RIGA_ID`) REFERENCES `rda_riga` (`RDA_RIGA_ID`),
  CONSTRAINT `rda_matrice_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `rda_matrice_ibfk_3` FOREIGN KEY (`RDO_RIGA_ID`) REFERENCES `ordine_riga` (`ORDINE_RIGA_ID`),
  CONSTRAINT `rda_matrice_ibfk_4` FOREIGN KEY (`RDA_ALTERNATIVA_ID`) REFERENCES `rda_alternativa` (`RDA_ALTERNATIVA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda_matrice`
--

LOCK TABLES `rda_matrice` WRITE;
/*!40000 ALTER TABLE `rda_matrice` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda_matrice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda_riga`
--

DROP TABLE IF EXISTS `rda_riga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda_riga` (
  `RDA_RIGA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `RDA_RIGA_TIPO_ID` int(11) NOT NULL,
  `RDA_ID` int(11) DEFAULT NULL,
  `IDENTIFICATIVO` varchar(30) DEFAULT '',
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  `NUMERO_RIGA` varchar(30) DEFAULT '',
  `PRODOTTO_CODICE` varchar(100) DEFAULT '',
  `PRODOTTO_DESCRIZIONE` varchar(1000) DEFAULT '',
  `CLASSE_MERCEOLOGICA` varchar(30) DEFAULT '',
  `QUANTITA` decimal(12,5) DEFAULT '0.00000',
  `DATA_CONSEGNA` datetime DEFAULT NULL,
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `VALUTA` varchar(20) DEFAULT NULL,
  `VALUTA_CODICE` varchar(3) DEFAULT NULL,
  `REFERENTE` varchar(254) DEFAULT NULL,
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT '',
  `PREZZO_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `QUANTITA_UNITA_MISURA` varchar(100) DEFAULT '',
  `QUANTITA_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `NUMERO_COMMESSA` varchar(100) DEFAULT '',
  `SCONTO1` decimal(12,5) DEFAULT '0.00000',
  `SCONTO2` decimal(12,5) DEFAULT '0.00000',
  `SCONTO3` decimal(12,5) DEFAULT '0.00000',
  `SCONTO4` decimal(12,5) DEFAULT '0.00000',
  `AUMENTO1` decimal(12,5) DEFAULT '0.00000',
  `AUMENTO2` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  `AUX_ROW1` varchar(99) DEFAULT '',
  `AUX_ROW2` varchar(99) DEFAULT '',
  `AUX_ROW3` varchar(99) DEFAULT '',
  `AUX_ROW4` varchar(99) DEFAULT '',
  `AUX_ROW5` varchar(255) DEFAULT '',
  `AUX_ROW6` varchar(99) DEFAULT '',
  `AUX_ROW7` varchar(99) DEFAULT '',
  `AUX_ROW8` varchar(99) DEFAULT '',
  `AUX_ROW9` varchar(99) DEFAULT '',
  `AUX_ROW10` varchar(99) DEFAULT '',
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_DATE3` datetime DEFAULT NULL,
  `AUX_ROW_DATE4` datetime DEFAULT NULL,
  `AUX_ROW_DATE5` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `FG_DISABLED_NOTE` varchar(99) DEFAULT '',
  `AUTHOR` varchar(254) DEFAULT NULL,
  `PRICE_TARGET` decimal(12,5) DEFAULT '0.00000',
  `SOURCE` varchar(3) DEFAULT NULL,
  `REF_RDA_RIGA_ID` int(11) DEFAULT '-1',
  `COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `AUX_ROW11` varchar(99) DEFAULT '',
  `AUX_ROW12` varchar(99) DEFAULT '',
  `AUX_ROW13` varchar(99) DEFAULT '',
  `AUX_ROW14` varchar(99) DEFAULT '',
  `AUX_ROW15` varchar(99) DEFAULT '',
  `AUX_ROW16` varchar(99) DEFAULT '',
  `AUX_ROW17` varchar(99) DEFAULT '',
  `AUX_ROW18` varchar(99) DEFAULT '',
  `AUX_ROW19` varchar(99) DEFAULT '',
  `AUX_ROW20` varchar(99) DEFAULT '',
  `AUX_ROW_DATE6` datetime DEFAULT NULL,
  `AUX_ROW_DATE7` datetime DEFAULT NULL,
  `AUX_ROW_DATE8` datetime DEFAULT NULL,
  `AUX_ROW_DATE9` datetime DEFAULT NULL,
  `AUX_ROW_DATE10` datetime DEFAULT NULL,
  `AUX_ROW_NUM6` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM7` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM8` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM9` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM10` decimal(12,5) DEFAULT '0.00000',
  `DESTINAZIONE_INDIRIZZO` varchar(255) DEFAULT '',
  `CAMBIO` decimal(12,5) DEFAULT '1.00000',
  `IVA_PERC` decimal(12,5) DEFAULT '0.00000',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  `STATO` varchar(30) DEFAULT '',
  PRIMARY KEY (`RDA_RIGA_ID`),
  KEY `RDA_ID` (`RDA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `RDA_RIGA_TIPO_ID` (`RDA_RIGA_TIPO_ID`),
  CONSTRAINT `rda_riga_ibfk_1` FOREIGN KEY (`RDA_ID`) REFERENCES `rda` (`RDA_ID`),
  CONSTRAINT `rda_riga_ibfk_2` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `rda_riga_ibfk_3` FOREIGN KEY (`RDA_RIGA_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda_riga`
--

LOCK TABLES `rda_riga` WRITE;
/*!40000 ALTER TABLE `rda_riga` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda_riga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda_riga_attribute`
--

DROP TABLE IF EXISTS `rda_riga_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda_riga_attribute` (
  `RDA_RIGA_ATTRIBUTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RDA_RIGA_ID` int(11) NOT NULL,
  `NAME` varchar(254) NOT NULL,
  `VALUE` varchar(254) DEFAULT '',
  `TYPE` varchar(15) DEFAULT 's',
  `DICTIONARY` int(11) DEFAULT '0',
  PRIMARY KEY (`RDA_RIGA_ATTRIBUTE_ID`),
  UNIQUE KEY `RDA_RIGA_ID_2` (`RDA_RIGA_ID`,`NAME`),
  KEY `RDA_RIGA_ID` (`RDA_RIGA_ID`),
  CONSTRAINT `rda_riga_attribute_ibfk_1` FOREIGN KEY (`RDA_RIGA_ID`) REFERENCES `rda_riga` (`RDA_RIGA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda_riga_attribute`
--

LOCK TABLES `rda_riga_attribute` WRITE;
/*!40000 ALTER TABLE `rda_riga_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda_riga_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rda_riga_log`
--

DROP TABLE IF EXISTS `rda_riga_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rda_riga_log` (
  `RDA_RIGA_LOG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RDA_RIGA_ID` int(11) NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `RDA_RIGA_TIPO_ID` int(11) NOT NULL,
  `RDA_ID` int(11) DEFAULT NULL,
  `IDENTIFICATIVO` varchar(30) DEFAULT '',
  `STATO` varchar(30) DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  `NUMERO_RIGA` varchar(30) DEFAULT '',
  `PRODOTTO_CODICE` varchar(100) DEFAULT '',
  `PRODOTTO_DESCRIZIONE` varchar(1000) DEFAULT '',
  `CLASSE_MERCEOLOGICA` varchar(30) DEFAULT '',
  `QUANTITA` decimal(12,5) DEFAULT '0.00000',
  `DATA_CONSEGNA` datetime DEFAULT NULL,
  `PREZZO` decimal(12,5) DEFAULT '0.00000',
  `VALUTA` varchar(20) DEFAULT NULL,
  `VALUTA_CODICE` varchar(3) DEFAULT NULL,
  `REFERENTE` varchar(254) DEFAULT NULL,
  `AUTHOR` varchar(254) DEFAULT NULL,
  `PREZZO_UNITA_MISURA` varchar(100) DEFAULT '',
  `PREZZO_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `QUANTITA_UNITA_MISURA` varchar(100) DEFAULT '',
  `QUANTITA_UNITA_MISURA_CODICE` varchar(3) DEFAULT NULL,
  `NUMERO_COMMESSA` varchar(100) DEFAULT '',
  `REF_RDA_RIGA_ID` int(11) DEFAULT '-1',
  `SCONTO1` decimal(12,5) DEFAULT '0.00000',
  `SCONTO2` decimal(12,5) DEFAULT '0.00000',
  `SCONTO3` decimal(12,5) DEFAULT '0.00000',
  `SCONTO4` decimal(12,5) DEFAULT '0.00000',
  `AUMENTO1` decimal(12,5) DEFAULT '0.00000',
  `AUMENTO2` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  `AUX_ROW1` varchar(99) DEFAULT '',
  `AUX_ROW2` varchar(99) DEFAULT '',
  `AUX_ROW3` varchar(99) DEFAULT '',
  `AUX_ROW4` varchar(99) DEFAULT '',
  `AUX_ROW5` varchar(255) DEFAULT '',
  `AUX_ROW6` varchar(99) DEFAULT '',
  `AUX_ROW7` varchar(99) DEFAULT '',
  `AUX_ROW8` varchar(99) DEFAULT '',
  `AUX_ROW9` varchar(99) DEFAULT '',
  `AUX_ROW10` varchar(99) DEFAULT '',
  `AUX_ROW11` varchar(99) DEFAULT '',
  `AUX_ROW12` varchar(99) DEFAULT '',
  `AUX_ROW13` varchar(99) DEFAULT '',
  `AUX_ROW14` varchar(99) DEFAULT '',
  `AUX_ROW15` varchar(99) DEFAULT '',
  `AUX_ROW16` varchar(99) DEFAULT '',
  `AUX_ROW17` varchar(99) DEFAULT '',
  `AUX_ROW18` varchar(99) DEFAULT '',
  `AUX_ROW19` varchar(99) DEFAULT '',
  `AUX_ROW20` varchar(99) DEFAULT '',
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_DATE3` datetime DEFAULT NULL,
  `AUX_ROW_DATE4` datetime DEFAULT NULL,
  `AUX_ROW_DATE5` datetime DEFAULT NULL,
  `AUX_ROW_DATE6` datetime DEFAULT NULL,
  `AUX_ROW_DATE7` datetime DEFAULT NULL,
  `AUX_ROW_DATE8` datetime DEFAULT NULL,
  `AUX_ROW_DATE9` datetime DEFAULT NULL,
  `AUX_ROW_DATE10` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM6` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM7` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM8` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM9` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM10` decimal(12,5) DEFAULT '0.00000',
  `PRICE_TARGET` decimal(12,5) DEFAULT '0.00000',
  `FG_DISABLED` int(11) DEFAULT '0',
  `FG_DISABLED_NOTE` varchar(99) DEFAULT '',
  `SOURCE` varchar(3) DEFAULT '',
  `COEF_QTY_PRICE` decimal(12,5) NOT NULL DEFAULT '1.00000',
  `DESTINAZIONE_INDIRIZZO` varchar(254) DEFAULT NULL,
  `CAMBIO` decimal(12,5) DEFAULT '1.00000',
  `IVA_PERC` decimal(12,5) DEFAULT '0.00000',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  `EVENTO_NOME` varchar(30) NOT NULL,
  `USER_NAME` varchar(99) NOT NULL,
  `MOTIVO` varchar(254) DEFAULT NULL,
  `DATA` datetime DEFAULT NULL,
  `FG_ALLINEAMENTO` int(11) DEFAULT '0',
  `DOCUMENTAZIONE_CHANGES` mediumtext,
  `VISTO_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`RDA_RIGA_LOG_ID`),
  KEY `RDA_RIGA_ID` (`RDA_RIGA_ID`),
  KEY `RDA_ID` (`RDA_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `VISTO_NOTIFICA_LOG_RICEVUTA_ID` (`VISTO_NOTIFICA_LOG_RICEVUTA_ID`),
  KEY `MODFORN_NOTIFICA_LOG_RICEVUTA_ID` (`MODFORN_NOTIFICA_LOG_RICEVUTA_ID`),
  CONSTRAINT `rda_riga_log_ibfk_1` FOREIGN KEY (`RDA_RIGA_ID`) REFERENCES `rda_riga` (`RDA_RIGA_ID`),
  CONSTRAINT `rda_riga_log_ibfk_2` FOREIGN KEY (`RDA_ID`) REFERENCES `rda` (`RDA_ID`),
  CONSTRAINT `rda_riga_log_ibfk_3` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rda_riga_log`
--

LOCK TABLES `rda_riga_log` WRITE;
/*!40000 ALTER TABLE `rda_riga_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `rda_riga_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refilling`
--

DROP TABLE IF EXISTS `refilling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refilling` (
  `REFILLING_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `ISSUE_DATE` datetime NOT NULL,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `REFILLING_NUMBER` varchar(30) DEFAULT '',
  `REFILLING_STATUS` varchar(30) DEFAULT '',
  `EMAIL` varchar(600) DEFAULT '',
  `NOTA` mediumtext,
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`REFILLING_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`FORNITORE_ID`,`ISSUE_DATE`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  CONSTRAINT `refilling_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `refilling_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `refilling_ibfk_3` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refilling`
--

LOCK TABLES `refilling` WRITE;
/*!40000 ALTER TABLE `refilling` DISABLE KEYS */;
/*!40000 ALTER TABLE `refilling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refilling_row`
--

DROP TABLE IF EXISTS `refilling_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refilling_row` (
  `REFILLING_ROW_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REFILLING_ID` int(11) NOT NULL,
  `ITEM_CODE` varchar(254) NOT NULL DEFAULT '',
  `REFILLING_DATE` datetime NOT NULL,
  `REFILLING_QTY` decimal(12,5) DEFAULT '0.00000',
  `REFILLING_TYPE` varchar(254) DEFAULT '',
  `ORDINE_NUMERO` varchar(50) DEFAULT NULL,
  `ORDINE_TIPO_ID` int(11) NOT NULL,
  `ORDINE_DATA` datetime DEFAULT NULL,
  `ORDINE_NUMERO_RIGA` varchar(30) DEFAULT '',
  `REFILLING_LINE_NUMBER` varchar(30) DEFAULT '',
  `FG_MULTIPLE_SUPPLIER` int(11) DEFAULT NULL,
  `NOTA` mediumtext,
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  PRIMARY KEY (`REFILLING_ROW_ID`),
  UNIQUE KEY `REFILLING_ID_2` (`REFILLING_ID`,`ITEM_CODE`,`REFILLING_DATE`),
  KEY `REFILLING_ID` (`REFILLING_ID`),
  KEY `ORDINE_TIPO_ID` (`ORDINE_TIPO_ID`),
  CONSTRAINT `refilling_row_ibfk_1` FOREIGN KEY (`REFILLING_ID`) REFERENCES `refilling` (`REFILLING_ID`),
  CONSTRAINT `refilling_row_ibfk_2` FOREIGN KEY (`ORDINE_TIPO_ID`) REFERENCES `ordine_tipo` (`ORDINE_TIPO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refilling_row`
--

LOCK TABLES `refilling_row` WRITE;
/*!40000 ALTER TABLE `refilling_row` DISABLE KEYS */;
/*!40000 ALTER TABLE `refilling_row` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relazioni_tra_oggetti`
--

DROP TABLE IF EXISTS `relazioni_tra_oggetti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relazioni_tra_oggetti` (
  `RELAZIONI_TRA_OGGETTI_ID` int(11) NOT NULL AUTO_INCREMENT,
  `OBJECT_MASTER_TYPE` int(11) NOT NULL,
  `OBJECT_MASTER_ID` int(11) NOT NULL,
  `OBJECT_RELATED_TYPE` int(11) NOT NULL,
  `OBJECT_RELATED_ID` int(11) NOT NULL,
  `PESO_ORDINE` int(11) DEFAULT '0',
  `INTERNAL_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`RELAZIONI_TRA_OGGETTI_ID`),
  UNIQUE KEY `OBJECT_MASTER_TYPE` (`OBJECT_MASTER_TYPE`,`OBJECT_MASTER_ID`,`OBJECT_RELATED_TYPE`,`OBJECT_RELATED_ID`),
  KEY `RELAZIONI_TRA_OGGETTI_RELATED_IDX` (`OBJECT_RELATED_TYPE`,`OBJECT_RELATED_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relazioni_tra_oggetti`
--

LOCK TABLES `relazioni_tra_oggetti` WRITE;
/*!40000 ALTER TABLE `relazioni_tra_oggetti` DISABLE KEYS */;
INSERT INTO `relazioni_tra_oggetti` VALUES (5,20,16996,10,22,0,NULL),(7,20,16996,10,24,0,NULL),(12,20,17001,10,29,0,NULL),(18,20,17017,10,35,0,NULL),(19,20,17037,10,36,0,NULL);
/*!40000 ALTER TABLE `relazioni_tra_oggetti` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relazioni_tra_oggetti_attribute`
--

DROP TABLE IF EXISTS `relazioni_tra_oggetti_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relazioni_tra_oggetti_attribute` (
  `RELAZIONI_TRA_OGGETTI_ATTRIBUTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RELAZIONI_TRA_OGGETTI_ID` int(11) NOT NULL,
  `NAME` varchar(254) NOT NULL,
  `VALUE` varchar(254) DEFAULT '',
  `TYPE` varchar(15) DEFAULT 's',
  `DICTIONARY` int(11) DEFAULT '0',
  PRIMARY KEY (`RELAZIONI_TRA_OGGETTI_ATTRIBUTE_ID`),
  UNIQUE KEY `RELAZIONI_TRA_OGGETTI_ID_2` (`RELAZIONI_TRA_OGGETTI_ID`,`NAME`),
  KEY `RELAZIONI_TRA_OGGETTI_ID` (`RELAZIONI_TRA_OGGETTI_ID`),
  CONSTRAINT `relazioni_tra_oggetti_attribute_ibfk_1` FOREIGN KEY (`RELAZIONI_TRA_OGGETTI_ID`) REFERENCES `relazioni_tra_oggetti` (`RELAZIONI_TRA_OGGETTI_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relazioni_tra_oggetti_attribute`
--

LOCK TABLES `relazioni_tra_oggetti_attribute` WRITE;
/*!40000 ALTER TABLE `relazioni_tra_oggetti_attribute` DISABLE KEYS */;
/*!40000 ALTER TABLE `relazioni_tra_oggetti_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report_scheduler`
--

DROP TABLE IF EXISTS `report_scheduler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report_scheduler` (
  `REPORT_SCHEDULER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UNIQUE_IDENTIFIER` varchar(15) NOT NULL,
  `USER_NAME` varchar(99) DEFAULT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `INSERT_DATE` datetime NOT NULL,
  `FIRST_START_DATE` datetime DEFAULT NULL,
  `NEXT_RUN_DATE` datetime DEFAULT NULL,
  `RUN_HOUR` int(11) DEFAULT NULL,
  `RUN_MINUTE` int(11) DEFAULT NULL,
  `REPETITION` varchar(2) DEFAULT NULL,
  `REPETITION_INTERVAL` int(11) DEFAULT '1',
  `DESCRIPTION` mediumtext,
  `OPR` longblob,
  `STARTING_COMPUTE_DATE` datetime DEFAULT NULL,
  `EMAIL` varchar(600) DEFAULT '',
  `FILENAME` varchar(254) DEFAULT NULL,
  `LAST_RUN_DATE` datetime DEFAULT NULL,
  `LAST_SEND_DATE` datetime DEFAULT NULL,
  `ENABLE_SEND` int(11) DEFAULT '0',
  `RUNNING` int(11) DEFAULT '0',
  `PARENT_IDENTIFIER` varchar(15) DEFAULT '',
  `LOCK_PARAM` varchar(10) DEFAULT '1',
  PRIMARY KEY (`REPORT_SCHEDULER_ID`),
  UNIQUE KEY `UNIQUE_IDENTIFIER` (`UNIQUE_IDENTIFIER`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `report_scheduler_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report_scheduler`
--

LOCK TABLES `report_scheduler` WRITE;
/*!40000 ALTER TABLE `report_scheduler` DISABLE KEYS */;
/*!40000 ALTER TABLE `report_scheduler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource` (
  `RESOURCE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RESOURCE_TYPE_ID` int(11) NOT NULL,
  `CODE` varchar(254) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `AVAILABILITY_MINUTES` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  `COSTO_ORARIO` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`RESOURCE_ID`),
  KEY `RESOURCE_TYPE_ID` (`RESOURCE_TYPE_ID`),
  CONSTRAINT `resource_ibfk_1` FOREIGN KEY (`RESOURCE_TYPE_ID`) REFERENCES `resource_type` (`RESOURCE_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource`
--

LOCK TABLES `resource` WRITE;
/*!40000 ALTER TABLE `resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_occupation`
--

DROP TABLE IF EXISTS `resource_occupation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_occupation` (
  `RESOURCE_OCCUPATION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRODUCT_BATCH_FORMULA_ID` int(11) NOT NULL,
  `RESOURCE_PROCESSING_CAPABILITES_ID` int(11) NOT NULL,
  `STATUS` varchar(30) DEFAULT NULL,
  `QUANTITY` decimal(12,5) DEFAULT '0.00000',
  `TIME_SETUP_SECONDS` decimal(12,5) DEFAULT '0.00000',
  `TIME_PER_UNIT_SECONDS` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  PRIMARY KEY (`RESOURCE_OCCUPATION_ID`),
  KEY `PRODUCT_BATCH_FORMULA_ID` (`PRODUCT_BATCH_FORMULA_ID`),
  KEY `RESOURCE_PROCESSING_CAPABILITES_ID` (`RESOURCE_PROCESSING_CAPABILITES_ID`),
  KEY `RESOURCE_OCCUPATION_STATUS` (`STATUS`),
  CONSTRAINT `resource_occupation_ibfk_1` FOREIGN KEY (`PRODUCT_BATCH_FORMULA_ID`) REFERENCES `product_batch_formula` (`PRODUCT_BATCH_FORMULA_ID`),
  CONSTRAINT `resource_occupation_ibfk_2` FOREIGN KEY (`RESOURCE_PROCESSING_CAPABILITES_ID`) REFERENCES `resource_processing_capabilites` (`RESOURCE_PROCESSING_CAPABILITES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_occupation`
--

LOCK TABLES `resource_occupation` WRITE;
/*!40000 ALTER TABLE `resource_occupation` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_occupation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_processing_capabilites`
--

DROP TABLE IF EXISTS `resource_processing_capabilites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_processing_capabilites` (
  `RESOURCE_PROCESSING_CAPABILITES_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RESOURCE_ID` int(11) NOT NULL,
  `PROCESSING_ID` int(11) NOT NULL,
  `DESCRIPTION` varchar(254) DEFAULT '',
  `NOTA` mediumtext,
  PRIMARY KEY (`RESOURCE_PROCESSING_CAPABILITES_ID`),
  KEY `RESOURCE_ID` (`RESOURCE_ID`),
  KEY `PROCESSING_ID` (`PROCESSING_ID`),
  CONSTRAINT `resource_processing_capabilites_ibfk_1` FOREIGN KEY (`RESOURCE_ID`) REFERENCES `resource` (`RESOURCE_ID`),
  CONSTRAINT `resource_processing_capabilites_ibfk_2` FOREIGN KEY (`PROCESSING_ID`) REFERENCES `processing` (`PROCESSING_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_processing_capabilites`
--

LOCK TABLES `resource_processing_capabilites` WRITE;
/*!40000 ALTER TABLE `resource_processing_capabilites` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_processing_capabilites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_type`
--

DROP TABLE IF EXISTS `resource_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resource_type` (
  `RESOURCE_TYPE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `CODE` varchar(254) DEFAULT '',
  `DESCRIPTION` varchar(254) DEFAULT '',
  `NOTA` mediumtext,
  `PRIMARIA` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`RESOURCE_TYPE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `resource_type_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_type`
--

LOCK TABLES `resource_type` WRITE;
/*!40000 ALTER TABLE `resource_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revision`
--

DROP TABLE IF EXISTS `revision`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `revision` (
  `REVISION_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOCUMENT_ID` int(11) NOT NULL,
  `LAST_MODIFIED_BY_PRIME` datetime DEFAULT NULL,
  `LAST_MODIFIED_BY_SUPPLIER` datetime DEFAULT NULL,
  `VOTE` decimal(19,6) DEFAULT NULL,
  `MANDATORY_COMPILATION_RATE` decimal(19,6) DEFAULT NULL,
  `COMPILATION_RATE` decimal(19,6) DEFAULT NULL,
  `STATO` varchar(30) DEFAULT NULL,
  `LAST_INTERNAL_USER` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`REVISION_ID`),
  KEY `DOCUMENT_ID` (`DOCUMENT_ID`),
  CONSTRAINT `revision_ibfk_1` FOREIGN KEY (`DOCUMENT_ID`) REFERENCES `document` (`DOCUMENT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revision`
--

LOCK TABLES `revision` WRITE;
/*!40000 ALTER TABLE `revision` DISABLE KEYS */;
/*!40000 ALTER TABLE `revision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riga_email`
--

DROP TABLE IF EXISTS `riga_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riga_email` (
  `RIGA_EMAIL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `EMAIL_ID` decimal(19,0) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `MD5` varchar(50) DEFAULT NULL,
  `TEMPLATE` varchar(50) DEFAULT NULL,
  `DATA_INSERIMENTO` datetime DEFAULT NULL,
  PRIMARY KEY (`RIGA_EMAIL_ID`),
  UNIQUE KEY `EMAIL_ID` (`EMAIL_ID`,`OBJECT_ID`,`OBJECT_TYPE`),
  KEY `DITTA_ID` (`DITTA_ID`),
  CONSTRAINT `riga_email_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riga_email`
--

LOCK TABLES `riga_email` WRITE;
/*!40000 ALTER TABLE `riga_email` DISABLE KEYS */;
INSERT INTO `riga_email` VALUES (8,5,1455719363181,17172,20,'WyTxGJKnaPk1N5kpBaT9vg==','ordineFornitoreRiepilogo','2016-02-17 15:29:24'),(9,5,1455719363181,17173,20,'Geexeo798-tMY4fXaFA81A==','ordineFornitoreRiepilogo','2016-02-17 15:29:24'),(10,5,1455719363181,17174,20,'0htGdCaaZ5lvrK8FU-SXbg==','ordineFornitoreRiepilogo','2016-02-17 15:29:24'),(11,5,1455719363181,17175,20,'RGdlNb6mPZ3Q64LfQmDw9w==','ordineFornitoreRiepilogo','2016-02-17 15:29:24'),(12,5,1455719363181,1145,60,NULL,'ordineFornitoreRiepilogo','2016-02-17 15:29:24'),(13,5,1458045217517,17172,20,'C37YFt6zO0j7kwIcGCOm9w==','ordineFornitoreRiepilogo','2016-03-15 13:33:39'),(14,5,1458045217517,17173,20,'Geexeo798-tMY4fXaFA81A==','ordineFornitoreRiepilogo','2016-03-15 13:33:39'),(15,5,1458045217517,17174,20,'0htGdCaaZ5lvrK8FU-SXbg==','ordineFornitoreRiepilogo','2016-03-15 13:33:39'),(16,5,1458045217517,17175,20,'RGdlNb6mPZ3Q64LfQmDw9w==','ordineFornitoreRiepilogo','2016-03-15 13:33:39'),(17,5,1458045217517,1145,60,NULL,'ordineFornitoreRiepilogo','2016-03-15 13:33:39'),(18,5,1460641679700,17002,20,'LsaTPy9wsx0wOQsSDe4Suw==','fornitorePrevisioneConsegna','2016-04-14 15:48:00'),(19,5,1460641679700,17001,20,'MoSaTHgKcJjibj29AsJyeQ==','fornitorePrevisioneConsegna','2016-04-14 15:48:00'),(20,5,1460641679700,17037,20,'6niRxT3oUAbmkMbws3jsag==','fornitorePrevisioneConsegna','2016-04-14 15:48:00'),(21,5,1460641679700,16995,20,'xZGlt8RsTGpE7eQIZ5nRfQ==','fornitorePrevisioneConsegna','2016-04-14 15:48:00'),(22,5,1462874736458,17176,20,'3RglfiRDf3ZkF-h7QAn4Og==','ordineFornitoreNuovo','2016-05-10 12:05:37'),(23,5,1462874736458,17177,20,'9kgAjqIMtvPgKkZ8Mc5Zyw==','ordineFornitoreNuovo','2016-05-10 12:05:37'),(24,5,1462874736458,17178,20,'u8AmTFUHMsxaJHGaeb58Kg==','ordineFornitoreNuovo','2016-05-10 12:05:37'),(25,5,1462874736458,17179,20,'6eN4fiM8TVsBjJoeaZcVlA==','ordineFornitoreNuovo','2016-05-10 12:05:37'),(26,5,1462874736458,1146,60,NULL,'ordineFornitoreNuovo','2016-05-10 12:05:37');
/*!40000 ALTER TABLE `riga_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `SCHEDULE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORDINE_RIGA_ID` int(11) NOT NULL,
  `ORDINE_RIGA_LOG_ID` int(11) DEFAULT NULL,
  `SCHEDULE_NUMBER` varchar(50) NOT NULL DEFAULT '',
  `SCHEDULE_ISSUE_DATE` datetime NOT NULL,
  `SCHEDULE_SNAPSHOOT_DATE` datetime DEFAULT NULL,
  `QTY` decimal(12,5) DEFAULT '0.00000',
  `QTY_DELIVERED` decimal(12,5) DEFAULT '0.00000',
  `PRICE` decimal(12,5) DEFAULT '0.00000',
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `CLOSURE_DATE` datetime DEFAULT NULL,
  `DELIVERY_ADDRESS` varchar(254) DEFAULT '',
  `PRIORITY` varchar(50) DEFAULT '',
  `NOTE` mediumtext,
  `FG_SUPPLIER` int(11) DEFAULT '0',
  `AUX_ROW1` varchar(99) DEFAULT '',
  `AUX_ROW2` varchar(99) DEFAULT '',
  `AUX_ROW3` varchar(99) DEFAULT '',
  `AUX_ROW4` varchar(99) DEFAULT '',
  `AUX_ROW5` varchar(99) DEFAULT '',
  `AUX_ROW6` varchar(99) DEFAULT '',
  `AUX_ROW7` varchar(99) DEFAULT '',
  `AUX_ROW8` varchar(99) DEFAULT '',
  `AUX_ROW9` varchar(99) DEFAULT '',
  `AUX_ROW10` varchar(99) DEFAULT '',
  `AUX_ROW_DATE1` datetime DEFAULT NULL,
  `AUX_ROW_DATE2` datetime DEFAULT NULL,
  `AUX_ROW_DATE3` datetime DEFAULT NULL,
  `AUX_ROW_DATE4` datetime DEFAULT NULL,
  `AUX_ROW_DATE5` datetime DEFAULT NULL,
  `AUX_ROW_NUM1` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM2` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM3` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM4` decimal(12,5) DEFAULT '0.00000',
  `AUX_ROW_NUM5` decimal(12,5) DEFAULT '0.00000',
  PRIMARY KEY (`SCHEDULE_ID`),
  UNIQUE KEY `SCHEDULE_SNAPSHOOT_DATE` (`SCHEDULE_SNAPSHOOT_DATE`,`ORDINE_RIGA_ID`,`SCHEDULE_NUMBER`,`SCHEDULE_ISSUE_DATE`),
  KEY `ORDINE_RIGA_ID` (`ORDINE_RIGA_ID`),
  KEY `ORDINE_RIGA_LOG_ID` (`ORDINE_RIGA_LOG_ID`),
  CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`ORDINE_RIGA_ID`) REFERENCES `ordine_riga` (`ORDINE_RIGA_ID`),
  CONSTRAINT `schedule_ibfk_2` FOREIGN KEY (`ORDINE_RIGA_LOG_ID`) REFERENCES `ordine_riga_log` (`ORDINE_RIGA_LOG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_version`
--

DROP TABLE IF EXISTS `schema_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_version` (
  `version_rank` int(11) NOT NULL,
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`version`),
  KEY `schema_version_vr_idx` (`version_rank`),
  KEY `schema_version_ir_idx` (`installed_rank`),
  KEY `schema_version_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_version`
--

LOCK TABLES `schema_version` WRITE;
/*!40000 ALTER TABLE `schema_version` DISABLE KEYS */;
INSERT INTO `schema_version` VALUES (1,1,'1','baseline','SQL','V1__baseline.sql',-1307484191,'iungo','2016-05-12 07:31:07',64493,1),(2,2,'2','resize columns','SQL','V2__resize_columns.sql',-516690489,'iungo','2016-05-12 07:31:28',20885,1),(3,3,'3','emailDittaConfigurazione','SQL','V3__emailDittaConfigurazione.sql',1256989693,'iungo','2016-05-12 07:31:28',163,1),(4,4,'4','emailDittaDrop','SQL','V4__emailDittaDrop.sql',-1467889288,'iungo','2016-05-12 07:31:33',4661,1),(5,5,'5','add helper sp to drop anonymous default constraints','SQL','V5__add_helper_sp_to_drop_anonymous_default_constraints.sql',22868047,'iungo','2016-05-12 07:31:33',2,1),(6,6,'6','some aux as text','SQL','V6__some_aux_as_text.sql',1190812113,'iungo','2016-05-12 07:31:48',15182,1),(7,7,'7','ordine attribure as text','SQL','V7__ordine_attribure_as_text.sql',-1301452413,'iungo','2016-05-12 07:31:51',2366,1);
/*!40000 ALTER TABLE `schema_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock`
--

DROP TABLE IF EXISTS `stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock` (
  `STOCK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DITTA_ID` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `ISSUE_DATE` datetime NOT NULL,
  `TRASMISSION_NUMBER` int(11) DEFAULT '0',
  `NOTA` mediumtext,
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  `LAST_TRANSMISSION_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`STOCK_ID`),
  UNIQUE KEY `DITTA_ID_2` (`DITTA_ID`,`FORNITORE_ID`,`ISSUE_DATE`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`),
  CONSTRAINT `stock_ibfk_2` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock`
--

LOCK TABLES `stock` WRITE;
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stock_row`
--

DROP TABLE IF EXISTS `stock_row`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stock_row` (
  `STOCK_ROW_ID` int(11) NOT NULL AUTO_INCREMENT,
  `STOCK_ID` int(11) NOT NULL,
  `ITEM_CODE` varchar(254) NOT NULL DEFAULT '',
  `STOCK_QTY` decimal(12,5) DEFAULT '0.00000',
  `NOTA` mediumtext,
  `AUX1` varchar(99) DEFAULT '',
  `AUX2` varchar(99) DEFAULT '',
  `AUX3` varchar(99) DEFAULT '',
  `AUX4` varchar(99) DEFAULT '',
  `AUX5` varchar(99) DEFAULT '',
  `AUX6` varchar(99) DEFAULT '',
  `AUX7` varchar(99) DEFAULT '',
  `AUX8` varchar(99) DEFAULT '',
  `AUX9` varchar(99) DEFAULT '',
  `AUX10` varchar(99) DEFAULT '',
  PRIMARY KEY (`STOCK_ROW_ID`),
  UNIQUE KEY `STOCK_ID_2` (`STOCK_ID`,`ITEM_CODE`),
  KEY `STOCK_ID` (`STOCK_ID`),
  CONSTRAINT `stock_row_ibfk_1` FOREIGN KEY (`STOCK_ID`) REFERENCES `stock` (`STOCK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stock_row`
--

LOCK TABLES `stock_row` WRITE;
/*!40000 ALTER TABLE `stock_row` DISABLE KEYS */;
/*!40000 ALTER TABLE `stock_row` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_io_file`
--

DROP TABLE IF EXISTS `sys_io_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_io_file` (
  `SYS_IO_FILE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DATE` datetime NOT NULL,
  `DITTA_ID` int(11) NOT NULL,
  `OBJECT_TYPE` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `FILE_PATH` varchar(1500) NOT NULL,
  `FILE_NAME` varchar(255) NOT NULL,
  `USER_NAME` varchar(99) DEFAULT NULL,
  `FG_INBOUND` int(11) NOT NULL,
  `QUEUE_TYPE` varchar(30) DEFAULT NULL,
  `QUEUE_ID` int(11) DEFAULT '0',
  PRIMARY KEY (`SYS_IO_FILE_ID`),
  KEY `DITTA_ID` (`DITTA_ID`),
  KEY `OGGETTO_RIFERITO` (`OBJECT_TYPE`,`OBJECT_ID`),
  CONSTRAINT `sys_io_file_ibfk_1` FOREIGN KEY (`DITTA_ID`) REFERENCES `ditta` (`DITTA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_io_file`
--

LOCK TABLES `sys_io_file` WRITE;
/*!40000 ALTER TABLE `sys_io_file` DISABLE KEYS */;
INSERT INTO `sys_io_file` VALUES (1,'2015-12-14 10:05:29',5,30,1219,'/private/var/iungo/iungo/scambioDati//doneIungo/','14PO1021528-20150831114858.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0),(2,'2015-12-14 10:05:29',5,20,17173,'/private/var/iungo/iungo/scambioDati//doneIungo/','14PO1021528-20150831114858.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0),(3,'2015-12-14 10:05:29',5,20,17174,'/private/var/iungo/iungo/scambioDati//doneIungo/','14PO1021528-20150831114858.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0),(4,'2015-12-14 10:05:29',5,20,17175,'/private/var/iungo/iungo/scambioDati//doneIungo/','14PO1021528-20150831114858.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0),(5,'2016-05-10 12:05:16',5,20,17176,'/private/var/iungo/iungo/scambioDati//doneIungo/','import_new_order.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0),(6,'2016-05-10 12:05:16',5,20,17177,'/private/var/iungo/iungo/scambioDati//doneIungo/','import_new_order.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0),(7,'2016-05-10 12:05:16',5,20,17178,'/private/var/iungo/iungo/scambioDati//doneIungo/','import_new_order.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0),(8,'2016-05-10 12:05:16',5,20,17179,'/private/var/iungo/iungo/scambioDati//doneIungo/','import_new_order.xml','sysScheduler',1,'QUEUE_FS_INBOUND',0);
/*!40000 ALTER TABLE `sys_io_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_variabile`
--

DROP TABLE IF EXISTS `sys_variabile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_variabile` (
  `SYS_VARIABILE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOME` varchar(254) NOT NULL,
  `VALORE` varchar(1500) DEFAULT NULL,
  PRIMARY KEY (`SYS_VARIABILE_ID`),
  UNIQUE KEY `NOME` (`NOME`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_variabile`
--

LOCK TABLES `sys_variabile` WRITE;
/*!40000 ALTER TABLE `sys_variabile` DISABLE KEYS */;
INSERT INTO `sys_variabile` VALUES (1,'versione','m2-5-00-0'),(2,'versioneM2','m2-5-106-17'),(4,'edition','PROCUREMENT'),(5,'license','UhQXR7p7WuBX+bkz4eYtzeFgBCZ1BgpgMJYbsIlVGF1N5skQPL25YXTBGhw/+nZRkGSGdFNImqtW\r\niNccmRqwhsL9896+ZlFxcH2iTE6YskZbcjH6iSb8z3uIkgpRXMcuJ7aenxafi7xhA5UTpt54qhFR\r\n/vSqUX2CudBnv//dcTo='),(6,'users.maximumUsers','8'),(7,'users.maximumConnections','8'),(8,'license.expirationDate','2099-12-31 00:00:00'),(10,'it.democenter.m2.Persona.conf.numeroRecordPerPagina','10'),(11,'it.democenter.m2.Persona.conf.mattoniSys_Ditta_list','RagioneSociale-GroupName-Via-Cap-Citta-Email-PhoneNumber'),(12,'it.democenter.m2.Persona.conf.mattoniSys_Ordine_list','oTipoO-oNumO-frnO-oDEmO-oRefO-ddDrSt'),(13,'it.democenter.m2.Persona.conf.mattoniSys_Ordine_formRiga','rNumO-pcO-rEsAz-qtaI-pqtaO-przI-pprzO-dtcI-pdtcO-llmO-ddDrSt'),(14,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_list','frnO-oTipoO-oNumO-rNumO-pcO-rEsAz-qtaI-pqtaO-przI-pprzO-dtcI-pdtcO-llmO-ddDrSt'),(15,'it.democenter.m2.Persona.conf.mattoniSys_ReportOrdineRiga_listOrdineRiga','frnO-oTipoO-oNumO-rNumO-pcO-rEsAz-qtaI-pqtaO-przI-pprzO-dtcI-pdtcO-llmO-ddDrSt'),(16,'it.democenter.m2.Persona.conf.mattoniSys_DocumentoTrasporto_list','ddDrSt-frnO-dNumO-dDatO-dColNO-dNotaO-ddtRic-ddari-dcolri'),(17,'it.democenter.m2.Persona.conf.mattoniSys_DocumentoTrasporto_formRiga','dNRgO-oNumO-oNRgO-dPrCO-orQO-orQAO-qtaO-fSaldo-dracc-ddaai-qtaai-fSaldAI'),(18,'it.democenter.m2.Persona.conf.mattoniSys_DocumentoTrasportoRiga_list','frnO-dNumO-dDatO-dDaRO-dNRgO-oNumO-oNRgO-dPrCO-orQO-orQAO-qtaO-fSaldo-dracc-ddaai-qtaai-fSaldAI'),(19,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_formDDTRiga','dNumO-dDatO-dDaRO-dNRgO-qtaO-fSaldo-dracc-ddaai-qtaai-fSaldAI'),(20,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_search','ordineTipoFgOutbound-oTipoO-oNumO-pcO-pdO'),(21,'it.democenter.m2.Persona.conf.mattoniSys_PrevisionaleRiga_list','DataEmissione-FornitoreRS-OrdineTipoNome-OrdineNumero-OrdineData-OrdineNumeroRiga-ProdottoCodice-Quantita-Prezzo-DataConsegna'),(22,'it.democenter.m2.Persona.conf.mattoniSys_Previsionale_formRiga','OrdineTipoNome-OrdineNumero-OrdineData-OrdineNumeroRiga-ProdottoCodice-Quantita-Prezzo-DataConsegna'),(23,'it.democenter.m2.Persona.conf.mattoniSys_Previsionale_list','FornitoreRS-OrdineTipoNome-DataEmissione-Stato-Nota'),(24,'it.democenter.m2.Persona.conf.mattoniSys_Fornitore_list','RagioneSociale-Citta-Provincia-Paese'),(25,'it.democenter.m2.Persona.conf.mattoniSys_Fornitore_search','RagioneSociale-Citta'),(26,'it.democenter.m2.Persona.conf.mattoniSys_Fornitore_searchInline','RagioneSociale-Citta-Provincia'),(27,'it.democenter.m2.Persona.conf.mattoniSys_Documentazione_list','DocumentazioneTipoDescrizione-FileName-FileSize-FileLastModified-Descrizione'),(28,'it.democenter.m2.Persona.conf.mattoniSys_ProductType_list','Name-Description'),(29,'it.democenter.m2.Persona.conf.mattoniSys_ProductBatch_list','Code-ProductCode-ProductDescription-Quantity-CreationDate-QuantityRemaining'),(30,'it.democenter.m2.Persona.conf.mattoniSys_Product_formBatchList_list','Code-Quantity-QuantityScrap-CreationDate-ExpirationDate-QuantityRemaining'),(31,'it.democenter.m2.Persona.conf.mattoniSys_ProductFormula_list','Code-Description-Nota-CreationDate-ExpirationDate'),(32,'it.democenter.m2.Persona.conf.mattoniSys_ProductFormula_formItem','Type-QuantityI-ProductCode-ProductDescription-ProductTypeDescription'),(33,'it.democenter.m2.Persona.conf.mattoniSys_Product_formFormula','Type-ProductFormulaCode-ProductFormulaDescription-Quantity'),(34,'it.democenter.m2.Persona.conf.mattoniSys_DocumentoTrasporto_list_searchDDT','dNumO-dDatO-frnO-dColNO-FgOutbound'),(35,'it.democenter.m2.Persona.conf.mattoniSys_Product_list','ProductTypeName-Code-Description-CodeInternal-UnitOfMeasure-FgSold-FgPurchased'),(36,'it.democenter.m2.Persona.conf.mattoniSys_Product_searchInline','ProductTypeName-Code-Description-UnitOfMeasure'),(37,'it.democenter.m2.Persona.conf.mattoniSys_ProductBatch_formDocumentoTrasportoRiga','frnO-FgOutbound-dDatO-dNumO-dNRgO-dDaRO-oNumO-oNRgO-ProductUnitOfMeasure-qtaO'),(38,'it.democenter.m2.Persona.conf.mattoniSys_ProductBatchFormula_list','Code-Description-DateStart-yDateEnd'),(39,'it.democenter.m2.Persona.conf.mattoniSys_ProductBatchFormula_formItem','Type-ProductCode-ProductBatchCode-ProductBatchFormulaCode-ProductUnitOfMeasure-Quantity-QuantityScrap'),(40,'it.democenter.m2.Persona.conf.mattoniSys_ProductBatch_search','Code-Quantity-QuantityRemaining-QuantityScrap-CreationDate-ExpirationDate'),(41,'it.democenter.m2.Persona.conf.mattoniSys_ProductFormula_formProductBatchFormulaInsert','Type-ProductTypeName-ProductCode-Quantity'),(42,'it.democenter.m2.Persona.conf.mattoniSys_DittaCertificateRole_list','RoleName-Nota'),(43,'it.democenter.m2.Persona.conf.mattoniSys_Ditta_search','RagioneSociale-Citta-Paese-PhoneNumber-WebSite'),(44,'it.democenter.m2.Persona.conf.mattoniSys_ProductListino_list','ProductCode-ProductDescription-FornitoreRagioneSociale-CodePartner-PrezzoUnitaMisura-Prezzo-ValidityStartDate-ValidityEndDate-Nota'),(45,'it.democenter.m2.Persona.conf.mattoniSys_Product_formProductListinoList','ProductCode-ProductDescription-FornitoreRagioneSociale-CodePartner-PrezzoUnitaMisura-Prezzo-ValidityStartDate-ValidityEndDate-Nota'),(46,'it.democenter.m2.Persona.conf.mattoniSys_Invoice_list','FornitoreRagioneSociale-FgOutbound-InvoiceNumber-DestinationAddress-Note-EmissionDate'),(47,'it.democenter.m2.Persona.conf.mattoniSys_Invoice_formInvoiceRow','Number-ProductCode-ProductDescription-ProductCodePartner-Quantity-Price-Amount'),(48,'it.democenter.m2.Persona.conf.mattoniSys_InvoiceRow_list','FornitoreRagioneSociale-FgOutbound-EmissionDate-InvoiceNumber-Number-ProductCode-ProductDescription-ProductCodePartner-Amount'),(49,'it.democenter.m2.Persona.conf.mattoniSys_ResourceType_search','Code-Description'),(50,'it.democenter.m2.Persona.conf.mattoniSys_ResourceType_list','Code-Description-Notait.democenter.m2.Persona.conf.mattoniSys_Resource_list=Code-Description-ResourceTypeCode-ResourceTypeDescription-AvailabilityMinutes'),(51,'it.democenter.m2.Persona.conf.mattoniSys_Ordine_listSpec','ordineTipoFgOutbound-oTipoO-oNumO-oDEmO-frnO'),(52,'it.democenter.m2.Persona.conf.mattoniSys_OrdineTipo_search','Nome-Descrizione'),(53,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_listSpec','oNumO-rNumO-oTipoO-rEsAz'),(54,'it.democenter.m2.Persona.conf.mattoniSys_Processing_list','Code-Description-Type'),(55,'it.democenter.m2.Persona.conf.mattoniSys_Resource_formProcessingCapabilities','ProcessingCode-TimeSetupSeconds-TimePerUnitSeconds-Description'),(56,'it.democenter.m2.Persona.conf.mattoniSys_Processing_search','Code-Description-Type'),(57,'it.democenter.m2.Persona.conf.mattoniSys_Processing_formResourceCapabilities','ResourceCode-ResourceDescription-ProcessingType-TimeSetupSeconds-TimePerUnitSeconds-Description'),(58,'it.democenter.m2.Persona.conf.mattoniSys_DocumentoTrasporto_listSpec','FgOutbound-ddDrSt-dNumO-dDatO-frnO-dcolni-ddtRic-ddari-dcolri'),(59,'it.democenter.m2.Persona.conf.mattoniSys_DocumentoTrasportoRiga_listSpec','frnO-FgOutbound-dNumO-dNRgO-ProductCode-ProductBatchCode-fSaldo-ProductCodePartner-ProductBatchCodePartner-dracc-qtaai-ddaai-fSaldAI'),(60,'it.democenter.m2.Persona.conf.mattoniSys_ProductFormula_formProcessing','ProductFormulaCode-ProcessingCode-Nota'),(61,'it.democenter.m2.Persona.conf.mattoniSys_Processing_formProductFormula','ProductFormulaCode-ProductFormulaDescription-ProcessingCode-ProcessingDescription-Nota'),(62,'it.democenter.m2.Persona.conf.mattoniSys_ProductBatchFormula_formResourceOccupation','Status-Quantity-TimeSetupSeconds-TimePerUnitSeconds'),(63,'it.democenter.m2.Persona.conf.mattoniSys_ResourceProcessingCapabilites_search','ResourceCode-ProcessingCode-ProcessingType-ResourceTypeCode-ResourceDescription-ProcessingDescription'),(64,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_schedules','OrdineRigaLog-ScheduleNumber-ScheduleIssueDate-ScheduleSnapshootDate-Priority-Qty-DeliveryDate-QtyDelivered-ClosureDate-DeliveryAddress-Note'),(65,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_schedules_last','OrdineRigaLog-ScheduleNumber-ScheduleIssueDate-QtyI-DeliveryDateI-Priority-Note'),(66,'it.democenter.m2.Persona.conf.mattoniSys_DocumentazioneBox_list','OrdineTipoNome-Numero-DataEmissione-ContactPerson-TransmissionMode-Nota'),(67,'it.democenter.m2.Persona.conf.mattoniSys_DocumentazioneDestinatario_list','OrdineTipoNome-Numero-DataEmissione-FornitoreRagioneSociale-ContactPerson-TransmissionMode-Email-FaxNumber-PartnerGroup-RecipientName-Nota'),(68,'it.democenter.m2.Persona.conf.mattoniSys_DocumentazioneBox_documentazione','FornitoreRagioneSociale-Email-FaxNumber-PartnerGroup-RecipientName-Nota'),(69,'it.democenter.m2.Persona.conf.mattoniSys_rdo.Ordine_list','oTipoO-oNumO-frnO-oDEmO-oRefO-oValO'),(70,'it.democenter.m2.Persona.conf.mattoniSys_rdo.OrdineRiga_list','rNumO-rEsAz-frnO-pcO-pdO-qta2RigheI-qumO-prz2RigheI-pumO-dtc2RigheI-llmoSFull'),(71,'it.democenter.m2.Persona.conf.mattoniSys_rdo.Ordine_formRiga','rNumO-rEsAz-frnO-pcO-pdO-qta2RigheI-qumO-prz2RigheI-pumO-dtc2RigheI-llmoSFull'),(72,'it.democenter.m2.Persona.conf.mattoniSys_RdaFabbisogno_list','clasMerc-prodCod-prodDesc-delDate-qty-umQta-nota'),(73,'it.democenter.m2.Persona.conf.mattoniSys_RdaVistaElenco','FornitoreRagioneSociale-clasMerc-prodCod-altOmolog-qtaAlt'),(74,'it.democenter.m2.Persona.conf.mattoniSys_Rda_list','NumRda-Descr-DataEm-DataIn-DataFin-Ref-Nota'),(75,'it.democenter.m2.Persona.conf.mattoniSys_RdaRow_list','nRiga-id-clasMerc-prodCod-prodDesc-delDate-qty-umQta-price-umPrezzo-nota'),(76,'it.democenter.m2.Persona.conf.mattoniSys_Rda_formRiga','nRiga-id-clasMerc-prodCod-prodDesc-delDate-qty-umQta-price-umPrezzo-nota'),(77,'it.democenter.m2.Persona.conf.mattoniSys_NotificaLog_list','DataSpedizione-Template-Indirizzo-Soggetto'),(78,'it.democenter.m2.Persona.conf.filtriSys__Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(79,'it.democenter.m2.Persona.conf.filtriSys_vendite._Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(80,'it.democenter.m2.Persona.conf.filtriSys_rdo._Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(81,'it.democenter.m2.Persona.conf.filtriSys_Report__Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(82,'it.democenter.m2.Persona.conf.filtriSys_Report_vendite._Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(83,'it.democenter.m2.Persona.conf.filtriSys_Report_rdo._Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(84,'it.democenter.m2.Persona.conf.filtriSys_Sinottico__Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(85,'it.democenter.m2.Persona.conf.filtriSys_previsionale_Menu','Fpartner-FstatoPrevisionale-FdataEmissionePrevisionale-Fspacer-FordineNumero-FprodottoCodice-FprodottoDescrizione-FdataEmissione-FdataConsegna-Fspacer-FtipoOrdine-Fspacer'),(86,'it.democenter.m2.Persona.conf.filtriSys_ContoLavoro._Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(87,'it.democenter.m2.Persona.conf.filtriSys_Report_ContoLavoro._Menu','Fvisto-FstatoRigheOrdine-Fspacer-Fpartner-Freferente-Fspacer-FprodottoCodice-FordineNumero-FtipoOrdine-Fspacer-Fdata'),(88,'it.democenter.m2.Persona.conf.filtriSys_invoice_Menu','Fpartner'),(89,'it.democenter.m2.Persona.conf.filtriSys_stock_Menu','Fpartner-Fprodotto-FdataEmissione'),(90,'it.democenter.m2.Persona.conf.filtriSys_refilling_Menu','Fpartner-FdataEmissione-Fprodotto'),(91,'it.democenter.m2.Persona.conf.filtriSys_documentazione_Menu','FstatoDocumentazioneBox-FtipoOrdine-Fpartner-FdataEmissione-FdocBoxNumber-FdocBoxEmail-FdocBoxReferente-FdocBoxRecipientName-Fspacer'),(92,'iungo_plugin_last_start_date','2016-05-12 09:35:50'),(93,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_headerShort','frnO-oTipoO-oNumO-rNumO-pcO-dtcO-qtaO-przO'),(94,'it.democenter.m2.Persona.conf.mattoniSys_ItemPartner_list','Fornitore-ItemCode-ItemDescription-PartnerItemCode-PartnerItemDescription-CreationDate-ExpirationDate-ItemType-PriceUnit-QtyUnit'),(95,'it.democenter.m2.Persona.conf.mattoniSys_ScheduleMixedInput','SchedQtyI-SchedDeliveryDateI'),(96,'it.democenter.m2.Persona.conf.mattoniSys_OrdineRiga_list_supplyChain','oNumO-rNumO-frnO-qtaO'),(97,'it.democenter.m2.Persona.conf.filtriSys_transcodifica_Menu','Fpartner-FproductCode-FproductCodePartner-Ftipologia-Fspacer-FdataCreazione-FdataTermine-Fspacer'),(98,'it.democenter.m2.Persona.conf.filtriSys_rda_Menu','FstatoRda,Fspacer,FdataEmissione,FrdaNumber,FrdaRigaCod,FrdaReferente'),(99,'versioneDatiM2','m2-5-106-17'),(100,'it.democenter.m2.Persona.conf.mattoniSys_rdo.MatriceSimulazione','rigaTooltip,hasDoc'),(101,'it.democenter.m2.Persona.conf.mattoniSys_rdo.Ordine_inline','hasDoc,llmoSFull,pcO,rEsAz,prz2RigheI,dtc2RigheI,qta2RigheI,orSconto1I_2Righe,qumO,oRefO'),(102,'it.democenter.m2.Persona.conf.mattoniSys_Document_list','DocumentId,DocumentTypeName,Fornitore,LastModifiedByPrime,LastModifiedBySupplier,rEsAz,CompilationRate,MandatoryCompilationRate,Vote'),(103,'it.democenter.m2.Persona.conf.mattoniSys_FieldValue_list','FieldValueId,Text,Description,CurrentValue,Optional,Domain,DocumentazioneSize,Scope,Deleted'),(104,'it.democenter.m2.Persona.conf.mattoniSys_Revision_list','RevisionId,LastModifiedByPrime,LastModifiedBySupplier,Vote,MandatoryCompilationRate,CompilationRate'),(105,'it.democenter.m2.Persona.conf.mattoniSys_DocumentType_list','Name,Description'),(106,'it.democenter.m2.Persona.conf.mattoniSys_Field_list','Text,Description,Category,Domain,Scope,Optional,AddToVote,Deleted,Position'),(107,'it.democenter.m2.Persona.conf.mattoniSys_EnumValue_list','Text,Description,Weight'),(108,'it.democenter.m2.Persona.conf.mattoniSys_Supplier_document_list','DocumentId,DocumentTypeName,Fornitore,LastModifiedByPrime,LastModifiedBySupplier,rEsAz,MandatoryCompilationRate,CompilationRate,Vote'),(109,'it.democenter.m2.Persona.conf.filtriSys_document_Menu','Fpartner,Fspacer,FstatoWorkFlow,FtipoOrdine,FdocumentLastModifiedByPrime,FdocumentLastModifiedBySuppFpartner,Fspacer,FstatoWorkFlow,FtipoOrdine,FdocumentLastModifiedByPrime,FdocumentLastModifiedBySupplier,Fspacer,FcompilationRate,FmandatoryCompilationRate'),(110,'users.maximumUsersViewers','0'),(111,'users.system.maximumUsers','8'),(112,'it.democenter.m2.Persona.conf.mattoniSys_Asyncop_list','aoTipoOrdO,aoAuthorO,aoNumO,aoDateCrO,aoNameO,aoClassNameO,aoExceptionO,rEsAz,aoThreadLiveO,aoDateEvO,aoEventO,aoEventReO,aollog'),(113,'it.democenter.m2.Persona.conf.mattoniSys_Asyncop_log_list','aoDateEvO,aoUserO,aoEventO,aoEventReO,aoNumO,aoNameO,aoClassNameO,aoDateCrO,aoExceptionO,aoThreadIdO'),(114,'it.democenter.m2.Persona.conf.filtriSys_asyncop_Menu','FstatoWorkFlow,FasyncOpClassName,FdataCreazione,FdataEvento,FasyncOpName,FasyncOpNumber,FtipoOrdine,FasyncOpUsername,Fspacer,FasyncOpAuthor'),(115,'iungo_enable_user_can_do_action_cache','true'),(116,'testConnectionBeforeMail','12-05-2016'),(117,'iungo_last_start_date','2016-05-12 09:35:52'),(118,'testConnectionBeforeImport','10-05-2016');
/*!40000 ALTER TABLE `sys_variabile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token_validity`
--

DROP TABLE IF EXISTS `token_validity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_validity` (
  `TOKEN_VALIDITY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `OBJECT_TYPE` int(11) NOT NULL,
  `OBJECT_ID` int(11) NOT NULL,
  `TOKEN` varchar(254) NOT NULL,
  `CREATION_DATE` datetime NOT NULL,
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `DURATION_SECOND` int(11) NOT NULL,
  PRIMARY KEY (`TOKEN_VALIDITY_ID`),
  UNIQUE KEY `TOKEN` (`TOKEN`),
  KEY `TOKEN_VALIDITY_IDX` (`OBJECT_TYPE`,`OBJECT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token_validity`
--

LOCK TABLES `token_validity` WRITE;
/*!40000 ALTER TABLE `token_validity` DISABLE KEYS */;
INSERT INTO `token_validity` VALUES (1,300,3,'5080e35c-8914-4532-8fc3-4b964ef57006','2015-11-04 11:52:39','2016-01-03 11:52:39',5184000),(2,300,3,'663d8616-65c8-4cf0-91d6-ebc64c6f1643','2015-11-09 11:54:07','2016-01-08 11:54:07',5184000),(3,300,3,'1b0b4d3e-6dd4-45a5-be5e-8c127c0f9f40','2015-11-09 12:06:17','2016-01-08 12:06:17',5184000),(4,300,3,'9e6b65f6-17ca-463d-b2ce-826922dc0d8c','2015-11-09 12:06:18','2016-01-08 12:06:18',5184000),(5,300,3,'af407e62-a019-48e7-a5fa-bbfbeff2f686','2015-11-09 12:06:18','2016-01-08 12:06:18',5184000),(6,300,3,'21e9735b-a5ca-4620-aabb-c5a605d421ca','2015-11-09 12:07:45','2016-01-08 12:07:45',5184000),(7,300,3,'d81d7619-e438-41fc-a4b8-61e7013838e8','2015-11-09 12:07:46','2016-01-08 12:07:46',5184000),(8,300,3,'f77ea8bc-91c6-416b-b06e-9d82fe15463a','2015-11-09 12:07:46','2016-01-08 12:07:46',5184000),(9,300,3,'3cbc0ef8-76ac-4b07-98b4-b2d0c7b6d5dd','2015-11-09 12:09:30','2016-01-08 12:09:30',5184000),(10,300,3,'445c9ea0-9b27-4d15-8f47-ddd594847143','2015-11-10 14:04:04','2016-01-09 14:04:04',5184000),(11,300,3,'e6ea5a3e-28ce-4b32-a220-45668998c0f3','2015-11-10 15:26:04','2016-01-09 15:26:04',5184000),(12,300,3,'026fe875-930b-4dcd-bbc1-72c048a3fa6b','2015-11-10 16:26:00','2016-01-09 16:26:00',5184000),(13,300,3,'d76c604a-f263-4ea3-9ceb-7846e300337b','2015-11-10 17:05:43','2016-01-09 17:05:43',5184000),(14,300,3,'f599bc9b-0202-4bf5-b0d3-521727871fe4','2015-11-10 17:06:26','2016-01-09 17:06:26',5184000),(15,300,3,'ae5a12b7-36e0-4eeb-a5e7-0b1643caad29','2015-11-10 17:07:20','2016-01-09 17:07:20',5184000),(16,300,3,'1e98aae8-0d08-4529-8354-70d991da4efa','2015-11-10 17:08:41','2016-01-09 17:08:41',5184000),(17,300,58,'68eab625-778e-4a43-8e13-e8cfcc2e8880','2015-11-10 17:11:43','2016-01-09 17:11:43',5184000),(18,300,3,'44cf1570-b48a-4fb3-a71f-21995d6d0af5','2015-11-18 17:34:48','2016-01-17 17:34:48',5184000),(19,300,3,'a019cc2c-1dce-47d5-a7f1-d033b779057f','2015-11-18 17:39:05','2016-01-17 17:39:05',5184000),(20,300,53,'98d3d1a7-df7b-4319-aee9-bcc447bc5ff2','2015-12-04 09:43:32','2016-02-02 09:43:32',5184000),(21,300,3,'662eb8d6-a335-4fe5-a40c-17cc03ca179e','2015-12-04 09:43:42','2016-02-02 09:43:42',5184000),(22,300,3,'8865533a-23bd-4333-8d4a-2a102366cd91','2015-12-04 09:45:28','2016-02-02 09:45:28',5184000),(23,300,3,'b20b35fd-1489-445a-ada3-8a62ee62b16c','2015-12-04 11:56:31','2016-02-02 11:56:31',5184000),(24,300,3,'4ed5dc02-fcdd-48b1-b9ff-83dcc5907235','2015-12-07 16:02:16','2016-02-05 16:02:16',5184000),(25,300,3,'510e85f6-630b-4f0f-9209-d9edd072b3f3','2015-12-07 17:59:55','2016-02-05 17:59:55',5184000),(26,300,3,'70febe2f-73d0-4d84-8fbf-ab5137b5aa95','2015-12-11 16:09:44','2016-02-09 16:09:44',5184000),(27,300,3,'d29324e0-d59c-441a-a66d-d5e8d7684a19','2015-12-14 10:05:28','2016-02-12 10:05:28',5184000),(28,300,59,'7e76a9f7-b498-4945-bedf-6e640d408910','2015-12-14 10:05:42','2016-02-12 10:05:42',5184000),(29,300,3,'70b68f24-8911-4589-9f64-a05e0c49fc6c','2016-02-05 12:22:08','2016-04-05 13:22:08',5184000),(30,300,3,'5fb13c97-ba61-4ce7-9a16-65e42426fe9b','2016-02-05 12:25:10','2016-04-05 13:25:10',5184000),(31,300,3,'5440acaa-a916-48c1-9e78-0273b4306cef','2016-02-17 15:28:53','2016-04-17 16:28:53',5184000),(32,300,59,'95f1c805-71b3-4233-a958-2f7c73f4a4de','2016-02-17 15:29:23','2016-04-17 16:29:23',5184000),(33,300,59,'79530b14-81dc-4dcb-ab08-7f8e9071a61e','2016-03-08 14:19:45','2016-05-07 15:19:45',5184000),(34,300,59,'2696ea51-8b72-4fea-98cb-7062942e1da1','2016-03-15 13:33:38','2016-05-14 14:33:38',5184000),(35,300,3,'1ebd9f8f-a69a-4937-aa2e-e17c62098f04','2016-03-15 13:36:21','2016-05-14 14:36:21',5184000),(36,300,3,'41b9563a-2443-466d-85d2-2561aa2973bd','2016-03-16 14:21:55','2016-05-15 15:21:55',5184000),(37,300,3,'128fc667-a880-43e9-9c15-6521488aac6b','2016-03-16 14:22:29','2016-05-15 15:22:29',5184000),(38,300,3,'84af607b-98da-46a7-9e02-0ff6808d67a2','2016-03-16 15:10:44','2016-05-15 16:10:44',5184000),(39,300,3,'36c25eb8-3eb7-4e96-95c5-89a21e6fadf8','2016-03-16 15:16:53','2016-05-15 16:16:53',5184000),(40,300,3,'fca5a463-7398-4b8e-94e1-5dfa5de92805','2016-04-14 14:44:28','2016-06-13 14:44:28',5184000),(41,300,3,'2383ba59-93d9-4a7b-b118-14161e223d8d','2016-04-14 15:47:35','2016-06-13 15:47:35',5184000),(42,300,58,'895f1856-99ac-4d8a-9496-94765dbd1624','2016-04-14 15:48:00','2016-06-13 15:48:00',5184000),(43,300,56,'b49d002c-61b9-4fd2-b648-a9a4d9e18665','2016-04-14 15:55:45','2016-06-13 15:55:45',5184000),(44,300,56,'446c2e25-df55-462b-b32e-b569d6cd8660','2016-04-14 16:05:45','2016-06-13 16:05:45',5184000),(45,300,56,'3fc8149a-53f2-4dae-b1bd-02279efa3f03','2016-04-14 16:15:45','2016-06-13 16:15:45',5184000),(46,300,3,'e6457ea8-e94b-4596-8a6c-847620a2a7a6','2016-04-14 17:03:32','2016-06-13 17:03:32',5184000),(47,300,3,'7f70d365-62db-4a95-b6d7-84508a210034','2016-05-10 10:51:04','2016-07-09 10:51:04',5184000),(48,300,21,'5e454b3e-deea-4e28-a0c7-719729d42fa5','2016-05-10 11:00:53','2016-07-09 11:00:53',5184000),(49,300,21,'331f7cda-a869-4750-a41b-66a1a8ccad3a','2016-05-10 11:10:53','2016-07-09 11:10:53',5184000),(50,300,21,'93fd90b6-dfcd-497e-9a74-52275c1f9633','2016-05-10 11:20:53','2016-07-09 11:20:53',5184000),(51,300,21,'7d7b429c-63e3-4cc8-9eb0-9565c4aaa0bd','2016-05-10 11:30:53','2016-07-09 11:30:53',5184000),(52,300,21,'41359e56-7373-4b18-88b6-b3f7837cd214','2016-05-10 11:40:53','2016-07-09 11:40:53',5184000),(53,300,21,'474db108-ac07-4ed2-b9f7-ecff6ba1792a','2016-05-10 11:50:53','2016-07-09 11:50:53',5184000),(54,300,21,'4204b582-1c85-4975-9e96-3a0fa306c867','2016-05-10 12:00:53','2016-07-09 12:00:53',5184000),(55,300,59,'d5dfab49-5ea5-45aa-966b-fed5ea9fb3c4','2016-05-10 12:05:36','2016-07-09 12:05:36',5184000);
/*!40000 ALTER TABLE `token_validity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turbine_group`
--

DROP TABLE IF EXISTS `turbine_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turbine_group` (
  `GROUP_ID` int(11) NOT NULL,
  `GROUP_NAME` varchar(99) NOT NULL,
  `OBJECTDATA` mediumblob,
  PRIMARY KEY (`GROUP_ID`),
  UNIQUE KEY `GROUP_NAME` (`GROUP_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turbine_group`
--

LOCK TABLES `turbine_group` WRITE;
/*!40000 ALTER TABLE `turbine_group` DISABLE KEYS */;
INSERT INTO `turbine_group` VALUES (400,'global','NULL'),(480,'base',NULL),(520,'base2',NULL);
/*!40000 ALTER TABLE `turbine_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turbine_permission`
--

DROP TABLE IF EXISTS `turbine_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turbine_permission` (
  `PERMISSION_ID` int(11) NOT NULL,
  `PERMISSION_NAME` varchar(99) NOT NULL,
  `OBJECTDATA` mediumblob,
  PRIMARY KEY (`PERMISSION_ID`),
  UNIQUE KEY `PERMISSION_NAME` (`PERMISSION_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turbine_permission`
--

LOCK TABLES `turbine_permission` WRITE;
/*!40000 ALTER TABLE `turbine_permission` DISABLE KEYS */;
INSERT INTO `turbine_permission` VALUES (171978,'ditta_delete','NULL'),(171979,'ditta_insert','NULL'),(171980,'ditta_read','NULL'),(171981,'ditta_update','NULL'),(171982,'docs_sys.OrdineRiga_delete','NULL'),(171983,'docs_sys.OrdineRiga_insert','NULL'),(171984,'docs_sys.OrdineRiga_read','NULL'),(171985,'docs_sys.OrdineRiga_update','NULL'),(171986,'documentazione_delete','NULL'),(171987,'documentazione_insert','NULL'),(171988,'documentazione_read','NULL'),(171993,'documentazione_update','NULL'),(172002,'email_check_enable','NULL'),(172003,'fornitore_delete','NULL'),(172004,'fornitore_insert','NULL'),(172006,'fornitore_read','NULL'),(172007,'fornitore_update','NULL'),(172011,'ordine_delete','NULL'),(172012,'ordine_insert','NULL'),(172014,'ordine_read','NULL'),(172015,'ordine_riga_delete','NULL'),(172016,'ordine_riga_insert','NULL'),(172019,'ordine_riga_log_read','NULL'),(172021,'ordine_riga_notifica_delete','NULL'),(172023,'ordine_riga_notifica_read','NULL'),(172027,'ordine_riga_update','NULL'),(172028,'ordine_sollecito','NULL'),(172030,'ordine_tipo_delete','NULL'),(172031,'ordine_tipo_insert','NULL'),(172032,'ordine_tipo_read','NULL'),(172033,'ordine_tipo_update','NULL'),(172034,'ordine_update','NULL'),(172050,'persona_configurazione_update','NULL'),(172051,'persona_delete','NULL'),(172056,'persona_insert','NULL'),(172057,'persona_read','NULL'),(172058,'persona_update','NULL'),(172060,'wf_chiuso_mForn','NULL'),(172061,'wf_chiuso_prop','NULL'),(172062,'wf_conf_annullato','NULL'),(172063,'wf_conf_chiuso','NULL'),(172064,'wf_conf_chiuso_test','NULL'),(172065,'wf_conf_mForn','NULL'),(172066,'wf_conf_prop','NULL'),(172067,'wf_crea_prop','NULL'),(172068,'wf_mForn_annullato','NULL'),(172069,'wf_mForn_chiuso','NULL'),(172070,'wf_mForn_conf','NULL'),(172071,'wf_mForn_mForn','NULL'),(172072,'wf_mForn_prop','NULL'),(172073,'wf_prop_annullato','NULL'),(172074,'wf_prop_chiuso','NULL'),(172075,'wf_prop_conf','NULL'),(172076,'wf_prop_conf_pr','NULL'),(172077,'wf_prop_mForn','NULL'),(172078,'wf_prop_prop','NULL'),(172079,'wf_prop_visto','NULL'),(172080,'wf_visto_annullato','NULL'),(172081,'wf_visto_chiuso','NULL'),(172082,'wf_visto_conf','NULL'),(172083,'wf_visto_conf_pr','NULL'),(172084,'wf_visto_mForn','NULL'),(172085,'wf_visto_prop','NULL'),(175247,'wf_annullato_prop','NULL'),(178521,'wf_chiuso_conf','NULL'),(181795,'system_menu','NULL'),(181796,'system_tools','NULL'),(181797,'ditta_user','NULL'),(181798,'ordine_gui_mass_signal','NULL'),(181799,'ordine_riga_dump_xml','NULL'),(181800,'system_role_delete','NULL'),(181802,'persona_change_password','NULL'),(181803,'ordine_read_testata','NULL'),(181804,'system_role_read','NULL'),(181805,'user','NULL'),(181810,'ordine_dump_xml','NULL'),(181811,'system_role_insert','NULL'),(181812,'ordine_update_mattone','NULL'),(181813,'ordine_riga_read_dettaglio','NULL'),(181815,'system_restart_email_readers','NULL'),(181816,'ordine_update_critic','NULL'),(181817,'ordine_riga_update_mattone','NULL'),(181818,'system_role_update','NULL'),(181819,'system_run_garbage_collector','NULL'),(181820,'ordine_riepilogo','NULL'),(185069,'ordine_report_read','NULL'),(185070,'ordine_riga_report_by_time','NULL'),(188348,'wf_crea_chiuso','NULL'),(188357,'documentazione_dirBrowser','NULL'),(188358,'wf_crea_annullato','NULL'),(191617,'persona_set_homepage',NULL),(191638,'menu_ordini_out',NULL),(191649,'system_restart_language_caches',NULL),(191650,'user_multipleLog',NULL),(206350,'notifica_show_alert',NULL),(209634,'ordine_riga_log_read_true_value',NULL),(209638,'plugin_admin',NULL),(218380,'ordine_show_visto',NULL),(219734,'ordine_riga_taglia_read',NULL),(219735,'ordine_riga_taglia_update',NULL),(223796,'plugin_import_ordine',NULL),(226504,'product_batch_formula_read',NULL),(226505,'stock_delete',NULL),(226506,'product_batch_dump_xml',NULL),(226507,'invoice_read',NULL),(226508,'ordine_wf_evento_update',NULL),(226509,'ordine_wf_evento_read',NULL),(226510,'menu_ordini_rdo',NULL),(226511,'ditta_trust_server_trace_xml_auth_delete',NULL),(226512,'processing_delete',NULL),(226513,'previsionale_read',NULL),(226514,'documento_trasporto_riga_update_mattone',NULL),(226515,'wf_customer_crea_annullato',NULL),(226516,'refilling_dump_xml',NULL),(226517,'ordine_riga_split_update',NULL),(226518,'wf_customer_crea_conf',NULL),(226519,'documentazione_tipo_read',NULL),(226520,'wf_conf_split',NULL),(226521,'ordine_wf_read',NULL),(226522,'product_formula_insert',NULL),(226523,'resource_type_insert',NULL),(226524,'product_listino_update',NULL),(226525,'product_update',NULL),(226526,'product_batch_read_pdfDocument',NULL),(226527,'refilling_delete',NULL),(226528,'ditta_certificate_role_update',NULL),(226529,'trace_xml_client_mobile_read',NULL),(226530,'previsionale_dump_xml',NULL),(226531,'menu_ordini_in',NULL),(226532,'ordine_riga_split_read',NULL),(226533,'stock_dump_xml',NULL),(226534,'wf_split_split',NULL),(226535,'resource_type_read',NULL),(226536,'wf_prop_split',NULL),(226537,'product_formula_processing_insert',NULL),(226538,'resource_delete',NULL),(226539,'product_type_delete',NULL),(226540,'product_formula_item_delete',NULL),(226541,'product_listino_delete',NULL),(226542,'product_dump_xml',NULL),(226543,'documento_trasporto_riga_dump_xml',NULL),(226544,'stock_read',NULL),(226545,'product_listino_insert',NULL),(226546,'resource_read',NULL),(226547,'documento_trasporto_ricevimento',NULL),(226548,'barcode_servlet_read',NULL),(226549,'resource_occupation_delete',NULL),(226550,'documento_trasporto_update_mattone',NULL),(226551,'refilling_insert',NULL),(226552,'refilling_update',NULL),(226553,'invoice_delete',NULL),(226554,'processing_read',NULL),(226555,'resource_occupation_read',NULL),(226556,'processing_update',NULL),(226557,'ditta_certificate_role_insert',NULL),(226558,'documento_trasporto_dump_xml',NULL),(226559,'resource_type_update',NULL),(226560,'wf_customer_chiuso_annullato',NULL),(226561,'fornitore_previsione_consegna',NULL),(226562,'documento_trasporto_insert',NULL),(226563,'ordine_riga_split_export',NULL),(226564,'resource_update',NULL),(226565,'product_batch_read',NULL),(226566,'previsionale_matrice_read',NULL),(226567,'documento_trasporto_read',NULL),(226568,'system_permission_delete',NULL),(226569,'product_tracexml_searchbatch',NULL),(226570,'trace_xml_menu',NULL),(226571,'previsionale_update',NULL),(226572,'documentazione_tipo_insert',NULL),(226573,'ordine_wf_stato_read',NULL),(226574,'resource_insert',NULL),(226575,'product_listino_read',NULL),(226576,'product_batch_formula_delete',NULL),(226577,'wf_customer_conf_chiuso',NULL),(226578,'wf_visto_split',NULL),(226579,'documentazione_tipo_update',NULL),(226580,'product_read',NULL),(226581,'refilling_matrice_read',NULL),(226582,'product_formula_processing_delete',NULL),(226583,'ditta_certificate_role_delete',NULL),(226584,'system_permission_insert',NULL),(226585,'invoice_update',NULL),(226586,'resource_processing_capabilites_delete',NULL),(226587,'ditta_trust_server_trace_xml_auth_update',NULL),(226588,'documento_trasporto_riga_insert',NULL),(226589,'resource_processing_capabilites_update',NULL),(226590,'product_tracexml_storehistory',NULL),(226591,'resource_occupation_insert',NULL),(226592,'product_formula_processing_read',NULL),(226593,'resource_processing_capabilites_insert',NULL),(226594,'product_batch_insert',NULL),(226595,'product_formula_update',NULL),(226596,'product_formula_item_update',NULL),(226597,'product_formula_read',NULL),(226598,'resource_processing_capabilites_read',NULL),(226599,'documento_trasporto_delete',NULL),(226600,'ditta_trust_server_trace_xml_auth_insert',NULL),(226601,'ditta_trust_server_trace_xml_auth_read',NULL),(226602,'previsionale_insert',NULL),(226603,'stock_update',NULL),(226604,'product_insert',NULL),(226605,'documento_trasporto_riga_delete',NULL),(226606,'invoice_insert',NULL),(226607,'product_batch_formula_insert',NULL),(226608,'documento_trasporto_riga_update',NULL),(226609,'product_formula_delete',NULL),(226610,'resource_type_delete',NULL),(226611,'wf_customer_conf_annullato',NULL),(226612,'pdf_servlet_read',NULL),(226613,'refilling_read',NULL),(226614,'product_batch_formula_update',NULL),(226615,'processing_insert',NULL),(226616,'documentazione_tipo_delete',NULL),(226617,'system_permission_read',NULL),(226618,'ditta_certificate_role_read',NULL),(226619,'product_delete',NULL),(226620,'product_type_insert',NULL),(226621,'product_type_update',NULL),(226622,'product_tracexml_storetrustcertificate',NULL),(226623,'documento_trasporto_riga_accettazione',NULL),(226624,'product_type_read',NULL),(226625,'product_batch_delete',NULL),(226626,'product_formula_item_read',NULL),(226627,'previsionale_delete',NULL),(226628,'product_formula_item_insert',NULL),(226629,'product_formula_processing_update',NULL),(226630,'documento_trasporto_update',NULL),(226631,'product_batch_update',NULL),(226632,'resource_occupation_update',NULL),(226633,'ordine_wf_stato_update',NULL),(226634,'custom_mattone',NULL),(226635,'stock_insert',NULL),(227858,'non_conformita_delete',NULL),(227859,'docs_sys.NonConformita_read',NULL),(227860,'docs_sys.NonConformita_delete',NULL),(227861,'piano_campionamento_read',NULL),(227862,'qualita_origine_insert',NULL),(227863,'qualita_istruzione_read',NULL),(227864,'qualita_livello_read',NULL),(227865,'wf_controSplit_controSplit',NULL),(227866,'livello_collaudo_insert',NULL),(227867,'docs_sys.ControlloQualita_delete',NULL),(227868,'piano_campionamento_update',NULL),(227869,'ordine_report_grafici_read',NULL),(227870,'collaudo_tipo_insert',NULL),(227871,'collaudo_tipo_delete',NULL),(227872,'docs_sys.ControlloQualita_update',NULL),(227873,'non_conformita_read',NULL),(227874,'ordine_gui_action_button',NULL),(227875,'controllo_qualita_read',NULL),(227876,'azione_qualita_read',NULL),(227877,'qualita_livello_update',NULL),(227878,'non_conformita_update',NULL),(227879,'wf_split_controSplit',NULL),(227880,'fornitore_valutazione_insert',NULL),(227881,'docs_sys.NonConformita_update',NULL),(227882,'qualita_origine_read',NULL),(227883,'piano_campionamento_insert',NULL),(227884,'controllo_qualita_update',NULL),(227885,'esito_controllo_qualita_read',NULL),(227886,'collaudo_tipo_read',NULL),(227887,'numerosita_campione_read',NULL),(227888,'non_conformita_insert',NULL),(227889,'qualita_istruzione_update',NULL),(227890,'collaudo_tipo_update',NULL),(227891,'qualita_livello_insert',NULL),(227892,'azione_qualita_insert',NULL),(227893,'qualita_livello_delete',NULL),(227894,'qualita_origine_delete',NULL),(227895,'fornitore_valutazione_read',NULL),(227896,'livello_collaudo_delete',NULL),(227897,'numerosita_campione_insert',NULL),(227898,'controllo_qualita_insert',NULL),(227899,'qualita_istruzione_tipo_insert',NULL),(227900,'qualita_istruzione_tipo_delete',NULL),(227901,'qualita_istruzione_insert',NULL),(227902,'fornitore_valutazione_update',NULL),(227903,'wf_controSplit_split',NULL),(227904,'livello_collaudo_update',NULL),(227905,'piano_campionamento_delete',NULL),(227906,'azione_qualita_update',NULL),(227907,'qualita_origine_update',NULL),(227908,'controllo_qualita_delete',NULL),(227909,'numerosita_campione_update',NULL),(227910,'qualita_istruzione_tipo_read',NULL),(227911,'docs_sys.ControlloQualita_insert',NULL),(227912,'numerosita_campione_delete',NULL),(227913,'qualita_istruzione_delete',NULL),(227914,'docs_sys.ControlloQualita_read',NULL),(227915,'azione_qualita_delete',NULL),(227916,'livello_collaudo_read',NULL),(227917,'docs_sys.NonConformita_insert',NULL),(227918,'fornitore_valutazione_delete',NULL),(227919,'qualita_istruzione_tipo_update',NULL),(230566,'docs_sys.Fornitore_delete',NULL),(230567,'docs_sys.RdaRiga_read',NULL),(230568,'ordine_report_manage',NULL),(230569,'docs_sys.RdaRiga_delete',NULL),(230570,'firma_pulsante_rinvio',NULL),(230571,'docs_sys.Fornitore_update',NULL),(230572,'ordine_riga_update_critic_mattone',NULL),(230573,'docs_sys.RdaRiga_insert',NULL),(230574,'show_menu_for_not_user',NULL),(230575,'firma_update',NULL),(230576,'docs_sys.Ordine_insert',NULL),(230577,'docs_sys.Ordine_delete',NULL),(230578,'show_online_registration',NULL),(230579,'show_gmaps',NULL),(230580,'firma_insert',NULL),(230581,'docs_sys.Ordine_read',NULL),(230582,'firma_read',NULL),(230583,'firma_delete',NULL),(230584,'docs_sys.Ordine_update',NULL),(230585,'docs_sys.Fornitore_insert',NULL),(230586,'wf_firma',NULL),(230587,'menu_sinottico',NULL),(230588,'docs_sys.RdaRiga_update',NULL),(230589,'fornitore_valutazione_config',NULL),(230590,'persona_manPage_admin',NULL),(230591,'docs_sys.Fornitore_read',NULL),(230592,'fornitore_valutazione_azione',NULL),(233274,'rdo_ordine_riga_delete',NULL),(233275,'docs_sys.RdaRdo_read',NULL),(233276,'contoLavoro_ordine_insert',NULL),(233277,'vendite_ordine_update_critic',NULL),(233278,'spedizione_dump_xml',NULL),(233279,'supplyChain_draftPo_showLink',NULL),(233280,'docNonConformita_ordine_update_critic',NULL),(233281,'item_partner_insert',NULL),(233282,'vendite_ordine_preview',NULL),(233283,'contoLavoro_ordine_riga_read_dettaglio',NULL),(233284,'schedule_update',NULL),(233285,'contoLavoro_ordine_riga_update_critic_mattone',NULL),(233286,'layout_destinatari_update',NULL),(233287,'vendite_ordine_riga_update',NULL),(233288,'docNonConformita_ordine_insert',NULL),(233289,'rdo_ordine_update',NULL),(233290,'vendite_ordine_riga_read_dettaglio',NULL),(233291,'contoLavoro_ordine_riga_update_mattone',NULL),(233292,'item_partner_dump_xml',NULL),(233293,'schedule_menu',NULL),(233294,'rda_insert',NULL),(233295,'docNonConformita_ordine_riga_read_dettaglio',NULL),(233296,'blacklist_read',NULL),(233297,'schedule_update_mattone',NULL),(233298,'ordine_preview',NULL),(233299,'docNonConformita_ordine_read_testata',NULL),(233300,'rdo_ordine_riga_update_critic_mattone',NULL),(233301,'docs_sys.RdaRdo_delete',NULL),(233302,'menu_ordini_rda',NULL),(233303,'rdo_ordine_preview',NULL),(233304,'rdo_ordine_update_critic',NULL),(233305,'spedizione_send',NULL),(233306,'contoLavoro_ordine_read_testata',NULL),(233307,'rda_fabbisogni_read',NULL),(233308,'vendite_ordine_riga_delete',NULL),(233309,'layout_destinatari_insert',NULL),(233310,'supplyChain_assignPlant',NULL),(233311,'contoLavoro_ordine_preview',NULL),(233312,'vendite_ordine_update_mattone',NULL),(233313,'blacklist_update',NULL),(233314,'docNonConformita_ordine_riga_update_critic_mattone',NULL),(233315,'docNonConformita_ordine_riga_insert',NULL),(233316,'fornitore_previsione_consegna_preview',NULL),(233317,'vendite_ordine_read',NULL),(233318,'rda_matrice_customAction',NULL),(233319,'docNonConformita_ordine_update_mattone',NULL),(233320,'spedizione_delete',NULL),(233321,'wf_prop_annullato_rdo',NULL),(233322,'layout_destinatari_delete',NULL),(233323,'vendite_ordine_riga_update_mattone',NULL),(233324,'docNonConformita_ordine_update',NULL),(233325,'rdo_ordine_riga_read_dettaglio',NULL),(233326,'fornitore_groupManage',NULL),(233327,'vendite_ordine_riga_insert',NULL),(233328,'contoLavoro_ordine_update_critic',NULL),(233329,'contoLavoro_ordine_update',NULL),(233330,'docs_sys.RdaRdo_insert',NULL),(233331,'vendite_ordine_read_testata',NULL),(233332,'schedule_read',NULL),(233333,'fornitore_dump_xml',NULL),(233334,'docNonConformita_ordine_riga_update_mattone',NULL),(233335,'contoLavoro_ordine_delete',NULL),(233336,'ordine_riga_dump_excel',NULL),(233337,'ordine_report_send',NULL),(233338,'rda_update',NULL),(233339,'supplyChain_draftPo',NULL),(233340,'vendite_ordine_insert',NULL),(233341,'rdo_ordine_insert',NULL),(233342,'rdo_ordine_delete',NULL),(233343,'schedule_delete',NULL),(233344,'rda_matrice_read',NULL),(233345,'menu_ordini_contoLavoro',NULL),(233346,'item_partner_update',NULL),(233347,'rdo_ordine_riga_update_mattone',NULL),(233348,'wf_mForn_annullato_rdo',NULL),(233349,'rdo_ordine_read',NULL),(233350,'spedizione_update',NULL),(233351,'rda_delete',NULL),(233352,'rdo_ordine_read_testata',NULL),(233353,'wf_conf_conf',NULL),(233354,'item_partner_delete',NULL),(233355,'item_partner_read',NULL),(233356,'docNonConformita_ordine_read',NULL),(233357,'ordine_riga_send_riepilogo',NULL),(233358,'rdo_ordine_update_mattone',NULL),(233359,'contoLavoro_ordine_update_mattone',NULL),(233360,'docNonConformita_ordine_riga_delete',NULL),(233361,'ordine_dump_excel',NULL),(233362,'rda_read',NULL),(233363,'user_fornitore',NULL),(233364,'layout_destinatari_read',NULL),(233365,'schedule_insert',NULL),(233366,'vendite_ordine_update',NULL),(233367,'contoLavoro_ordine_riga_insert',NULL),(233368,'contoLavoro_ordine_riga_delete',NULL),(233369,'docNonConformita_ordine_preview',NULL),(233370,'product_send_docs',NULL),(233371,'vendite_ordine_delete',NULL),(233372,'rdo_ordine_riga_insert',NULL),(233373,'rdo_ordine_riga_update',NULL),(233374,'spedizione_read',NULL),(233375,'contoLavoro_ordine_read',NULL),(233376,'notifica_send',NULL),(233377,'spedizione_insert',NULL),(233378,'contoLavoro_ordine_riga_update',NULL),(233379,'item_supplyChain_read',NULL),(233380,'docNonConformita_ordine_delete',NULL),(233381,'vendite_ordine_riga_update_critic_mattone',NULL),(233382,'blacklist_delete',NULL),(233383,'rda_make_rdo',NULL),(233384,'notifica_delete',NULL),(233385,'docNonConformita_ordine_riga_update',NULL),(233386,'rda_make_rdaHead',NULL),(233387,'menu_ordini_docNonConformita',NULL),(233388,'docs_sys.RdaRdo_update',NULL),(235982,'vendite_ordine_copy',NULL),(235983,'ordine_copy',NULL),(235984,'ordine_show_visto_manuale',NULL),(235985,'contoLavoro_ordine_copy',NULL),(238690,'rdo_ordine_previewPdf',NULL),(238691,'io_files_read',NULL),(238692,'menu_homepage',NULL),(238693,'contoLavoro_documento_trasporto_read',NULL),(238694,'contoLavoro_ordine_previewPdf',NULL),(238695,'ordine_previewPdf',NULL),(238696,'vendite_documento_trasporto_read',NULL),(238697,'favorites_marker',NULL),(238698,'label_manage',NULL),(238699,'docNonConformita_documento_trasporto_read',NULL),(238700,'notifica_sent_preview',NULL),(238701,'label_read',NULL),(238702,'notifica_received_preview',NULL),(238703,'previsionale_preview',NULL),(238704,'vendite_previewPdf',NULL),(238705,'io_files_manage',NULL),(238706,'label_apply',NULL),(238707,'label_readAll',NULL),(238708,'label_manageAll',NULL),(241398,'system_license',NULL),(241399,'questionary_type_delete',NULL),(241400,'wf_mForn_daCertificare_origin',NULL),(241401,'private_readAll',NULL),(241402,'questionary_log_read',NULL),(241403,'rda_riga_log_read',NULL),(241404,'wf_rda_attivo',NULL),(241405,'rda_fabbisogni_insert',NULL),(241406,'declarationOrigin_export',NULL),(241407,'report_send',NULL),(241408,'wf_conf_mForn_albo',NULL),(241409,'product_listino_type_delete',NULL),(241410,'declarationOrigin_update',NULL),(241411,'declarationOrigin_sendRicevuta',NULL),(241412,'declarationOrigin_previewPdf',NULL),(241413,'wf_prop_mForn_albo',NULL),(241414,'wf_ddt_annullato',NULL),(241415,'persona_sysconfig_update',NULL),(241416,'documento_trasporto_export_manual',NULL),(241417,'wf_ddt_consegnato',NULL),(241418,'user_viewer',NULL),(241419,'wf_ddt_spedito',NULL),(241420,'questionary_type_update',NULL),(241421,'fornitore_avviso_ricezione',NULL),(241422,'questionary_read',NULL),(241423,'declarationOrigin_insert',NULL),(241424,'fornitore_avviso_ricezione_preview',NULL),(241425,'calendar_delete',NULL),(241426,'aste_read',NULL),(241427,'rda_deactive',NULL),(241428,'wf_conf_annullato_rdo',NULL),(241429,'wf_ddt_ricevuto',NULL),(241430,'questionary_update',NULL),(241431,'wf_crea_prop_albo',NULL),(241432,'rda_dump_excel',NULL),(241433,'questionary_update_mattone',NULL),(241434,'product_listino_type_read',NULL),(241435,'help_manual',NULL),(241436,'wf_prop_annullato_origin',NULL),(241437,'wf_ddt_accettato',NULL),(241438,'ordine_wf_update',NULL),(241439,'report_manage',NULL),(241440,'report_read',NULL),(241441,'documento_trasporto_riga_log_read',NULL),(241442,'declarationOrigin_preview',NULL),(241443,'wf_mForn_conf_albo',NULL),(241444,'declarationOrigin_log_read',NULL),(241445,'wf_conf_annullato_origin',NULL),(241446,'declarationOrigin_delete',NULL),(241447,'wf_conf_chiuso_albo',NULL),(241448,'wf_chiuso_mForn_albo',NULL),(241449,'documento_trasporto_riga_read_dettaglio',NULL),(241450,'calendar_update',NULL),(241451,'declarationOrigin_update_mattone',NULL),(241452,'aste_start',NULL),(241453,'documentazione_insert_from_service',NULL),(241454,'fornitore_merge',NULL),(241455,'questionary_insert',NULL),(241456,'product_listino_type_update',NULL),(241457,'calendar_insert',NULL),(241458,'rda_append_rdaRow',NULL),(241459,'user_system',NULL),(241460,'previsionale_send',NULL),(241461,'fornitore_categoria_manage',NULL),(241462,'wf_prop_prop_albo',NULL),(241463,'declarationOrigin_read',NULL),(241464,'rda_riga_dump_excel',NULL),(241465,'product_sticker_manage',NULL),(241466,'wf_rda_annullato',NULL),(241467,'wf_rda_completato',NULL),(241468,'wf_mForn_mForn_albo',NULL),(241469,'product_listino_type_insert',NULL),(241470,'wf_conf_prop_albo',NULL),(241471,'wf_mForn_mForn_pr_albo',NULL),(241472,'rda_fabbisogni_dump_excel',NULL),(241473,'wf_mForn_annullato_origin',NULL),(241474,'fornitore_categoria_modulo_manage',NULL),(241475,'calendar_read',NULL),(241476,'questionary_delete',NULL),(241477,'questionary_type_read',NULL),(241478,'declarationOrigin_send',NULL),(241479,'wf_rda_inRDO',NULL),(241480,'wf_rda_inRDA',NULL),(241481,'report_schedule_manage',NULL),(241482,'private_apply',NULL),(241483,'report_schedule_read',NULL),(241484,'persona_sysconfig_read',NULL),(241485,'questionary_type_insert',NULL),(241486,'wf_mForn_prop_albo',NULL),(244106,'wf_conf_annullato_orig',NULL),(246814,'db_ordine_riga_update',NULL),(246815,'wf_asyncop_pleaseinterrupt',NULL),(246816,'asyncop_log_read',NULL),(246817,'rda_matriceSim_rdaPopUp_updateRda',NULL),(246818,'rdo_ordine_riga_read',NULL),(246819,'wf_asyncop_sleep',NULL),(246820,'persona_change_others_password',NULL),(246821,'db_ordine_delete',NULL),(246822,'wf_asyncop_killed',NULL),(246823,'docNonConformita_db_ordine_insert',NULL),(246824,'wf_asyncop_resume',NULL),(246825,'docNonConformita_db_ordine_delete',NULL),(246826,'db_ordine_read',NULL),(246827,'ditta_sysconfig_update',NULL),(246828,'asyncop_read_dettaglio',NULL),(246829,'db_ordine_insert',NULL),(246830,'contoLavoro_db_ordine_riga_read',NULL),(246831,'wf_asyncop_error',NULL),(246832,'wf_asyncop_terminated',NULL),(246833,'rdo_db_ordine_riga_update',NULL),(246834,'rda_matriceSim_read',NULL),(246835,'menu_asyncops',NULL),(246836,'rdo_db_ordine_riga_delete',NULL),(246837,'queue_ws_update',NULL),(246838,'docNonConformita_db_ordine_riga_delete',NULL),(246839,'contoLavoro_db_ordine_riga_delete',NULL),(246840,'asyncop_log_delete',NULL),(246841,'asyncop_insert',NULL),(246842,'docNonConformita_db_ordine_read',NULL),(246843,'contoLavoro_db_ordine_riga_update',NULL),(246844,'ordine_riga_read',NULL),(246845,'wf_asyncop_create',NULL),(246846,'db_ordine_riga_read',NULL),(246847,'exception_update',NULL),(246848,'rda_update_mattone',NULL),(246849,'wf_asyncop_interrupted',NULL),(246850,'queue_ws_delete',NULL),(246851,'rdo_db_ordine_insert',NULL),(246852,'vendite_db_ordine_riga_update',NULL),(246853,'notifica_read',NULL),(246854,'vendite_db_ordine_update',NULL),(246855,'docNonConformita_db_ordine_riga_update',NULL),(246856,'asyncop_log_update',NULL),(246857,'db_ordine_update',NULL),(246858,'wf_asyncop_pleasesleep',NULL),(246859,'exception_delete',NULL),(246860,'exception_insert',NULL),(246861,'vendite_ordine_riga_read',NULL),(246862,'rdo_db_ordine_read',NULL),(246863,'rdo_db_ordine_update',NULL),(246864,'db_ordine_riga_insert',NULL),(246865,'declarationOrigin_gui_mass_signal',NULL),(246866,'contoLavoro_db_ordine_delete',NULL),(246867,'asyncop_read',NULL),(246868,'contoLavoro_db_ordine_read',NULL),(246869,'contoLavoro_db_ordine_insert',NULL),(246870,'vendite_db_ordine_insert',NULL),(246871,'db_ordine_riga_delete',NULL),(246872,'asyncop_update',NULL),(246873,'exception_read',NULL),(246874,'docNonConformita_db_ordine_riga_read',NULL),(246875,'vendite_db_ordine_riga_insert',NULL),(246876,'vendite_db_ordine_delete',NULL),(246877,'vendite_db_ordine_riga_delete',NULL),(246878,'contoLavoro_ordine_riga_read',NULL),(246879,'rdo_db_ordine_riga_insert',NULL),(246880,'contoLavoro_db_ordine_riga_insert',NULL),(246881,'asyncop_log_insert',NULL),(246882,'docNonConformita_db_ordine_riga_insert',NULL),(246883,'docNonConformita_ordine_riga_read',NULL),(246884,'asyncop_delete',NULL),(246885,'docNonConformita_db_ordine_update',NULL),(246886,'rdo_db_ordine_riga_read',NULL),(246887,'queue_ws_insert',NULL),(246888,'rdo_db_ordine_delete',NULL),(246889,'vendite_db_ordine_riga_read',NULL),(246890,'vendite_db_ordine_read',NULL),(246891,'wf_asyncop_kill',NULL),(246892,'wf_asyncop_run',NULL),(246893,'rda_matriceSim_rdaPopUp',NULL),(246894,'contoLavoro_db_ordine_update',NULL),(246895,'queue_ws_read',NULL),(246896,'wf_crea_forecast',NULL),(246897,'wf_forecast_annullato',NULL),(246898,'wf_forecast_conf',NULL),(248168,'fornitore_home_read',NULL),(249522,'wf_controsplit_prop',NULL),(249523,'wf_forn_annullato',NULL),(249524,'wf_forn_non_annullato',NULL);
/*!40000 ALTER TABLE `turbine_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turbine_role`
--

DROP TABLE IF EXISTS `turbine_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turbine_role` (
  `ROLE_ID` int(11) NOT NULL,
  `ROLE_NAME` varchar(99) NOT NULL,
  `OBJECTDATA` mediumblob,
  PRIMARY KEY (`ROLE_ID`),
  UNIQUE KEY `ROLE_NAME` (`ROLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turbine_role`
--

LOCK TABLES `turbine_role` WRITE;
/*!40000 ALTER TABLE `turbine_role` DISABLE KEYS */;
INSERT INTO `turbine_role` VALUES (300,'turbine_root','NULL'),(394,'Amministratore','NULL'),(395,'Buyer','NULL'),(396,'Magazziniere','NULL'),(397,'Pianificatore','NULL'),(398,'_CheckMailManuale','NULL'),(399,'_DocumentRigaOrdine','NULL'),(400,'_InvioManualeNotifiche','NULL'),(401,'_InvioManualePrevisioneConsegna','NULL'),(402,'_Ordine-DdT_DumpXML','NULL'),(403,'_PersonalizzaForm','NULL'),(404,'_RiepilogoOrdiniBackground','NULL'),(405,'_SollecitoBackground','NULL'),(406,'_SollecitoOnline','NULL'),(407,'a_Buyer','NULL'),(408,'member','NULL'),(409,'notAUser','NULL'),(410,'system_io_role','NULL'),(411,'system_mail_input','NULL'),(412,'user','NULL'),(510,'system_generic_user',NULL),(511,'supplyChain_buyer',NULL),(512,'supplyChain_planner',NULL),(513,'user_fornitore',NULL),(530,'system_io_fornitore',NULL),(531,'user_viewer',NULL),(532,'Albo_Prime_Admin',NULL),(533,'Albo_Prime_User',NULL),(534,'Albo_Supplier',NULL),(550,'system_service_user',NULL),(551,'AsyncOp_internal',NULL),(552,'AsyncOp_user',NULL),(553,'Scheduler',NULL),(560,'supplier',NULL),(561,'supplierSplit',NULL);
/*!40000 ALTER TABLE `turbine_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turbine_role_permission`
--

DROP TABLE IF EXISTS `turbine_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turbine_role_permission` (
  `ROLE_ID` int(11) NOT NULL,
  `PERMISSION_ID` int(11) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`PERMISSION_ID`),
  KEY `PERMISSION_ID` (`PERMISSION_ID`),
  KEY `ROLE_ID` (`ROLE_ID`),
  KEY `PERMISSION_ID_2` (`PERMISSION_ID`),
  CONSTRAINT `turbine_role_permission_ibfk_1` FOREIGN KEY (`ROLE_ID`) REFERENCES `turbine_role` (`ROLE_ID`),
  CONSTRAINT `turbine_role_permission_ibfk_2` FOREIGN KEY (`PERMISSION_ID`) REFERENCES `turbine_permission` (`PERMISSION_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turbine_role_permission`
--

LOCK TABLES `turbine_role_permission` WRITE;
/*!40000 ALTER TABLE `turbine_role_permission` DISABLE KEYS */;
INSERT INTO `turbine_role_permission` VALUES (300,171978),(550,171978),(300,171979),(410,171979),(550,171979),(300,171980),(394,171980),(398,171980),(410,171980),(411,171980),(550,171980),(560,171980),(300,171981),(394,171981),(410,171981),(411,171981),(550,171981),(300,171982),(394,171982),(395,171982),(399,171982),(410,171982),(510,171982),(560,171982),(300,171983),(394,171983),(395,171983),(399,171983),(410,171983),(510,171983),(560,171983),(300,171984),(394,171984),(395,171984),(399,171984),(410,171984),(510,171984),(560,171984),(300,171985),(394,171985),(395,171985),(399,171985),(410,171985),(510,171985),(560,171985),(300,171986),(395,171986),(410,171986),(300,171987),(395,171987),(410,171987),(560,171987),(300,171988),(395,171988),(410,171988),(560,171988),(300,171993),(395,171993),(410,171993),(510,171993),(560,171993),(300,172002),(394,172002),(398,172002),(410,172002),(300,172003),(394,172003),(410,172003),(300,172004),(394,172004),(410,172004),(300,172006),(394,172006),(395,172006),(396,172006),(397,172006),(410,172006),(411,172006),(553,172006),(560,172006),(300,172007),(394,172007),(395,172007),(397,172007),(410,172007),(411,172007),(553,172007),(560,172007),(300,172011),(394,172011),(407,172011),(410,172011),(300,172012),(394,172012),(407,172012),(410,172012),(300,172014),(394,172014),(395,172014),(396,172014),(397,172014),(410,172014),(411,172014),(511,172014),(512,172014),(553,172014),(560,172014),(300,172015),(394,172015),(407,172015),(410,172015),(300,172016),(394,172016),(407,172016),(410,172016),(300,172019),(394,172019),(395,172019),(397,172019),(410,172019),(411,172019),(553,172019),(560,172019),(561,172019),(300,172021),(394,172021),(400,172021),(410,172021),(300,172023),(394,172023),(395,172023),(396,172023),(397,172023),(400,172023),(410,172023),(553,172023),(300,172027),(394,172027),(399,172027),(407,172027),(410,172027),(411,172027),(300,172028),(406,172028),(300,172030),(394,172030),(410,172030),(300,172031),(394,172031),(410,172031),(300,172032),(394,172032),(395,172032),(396,172032),(397,172032),(410,172032),(553,172032),(560,172032),(300,172033),(394,172033),(410,172033),(300,172034),(394,172034),(407,172034),(410,172034),(411,172034),(300,172050),(403,172050),(300,172051),(550,172051),(300,172056),(410,172056),(550,172056),(300,172057),(394,172057),(395,172057),(396,172057),(397,172057),(408,172057),(410,172057),(550,172057),(553,172057),(560,172057),(300,172058),(410,172058),(550,172058),(560,172058),(300,172060),(394,172060),(300,172061),(394,172061),(300,172062),(394,172062),(300,172063),(394,172063),(300,172064),(394,172064),(300,172065),(394,172065),(411,172065),(530,172065),(560,172065),(300,172066),(394,172066),(395,172066),(397,172066),(410,172066),(300,172067),(394,172067),(395,172067),(410,172067),(300,172068),(394,172068),(300,172069),(394,172069),(300,172070),(394,172070),(395,172070),(300,172071),(394,172071),(411,172071),(530,172071),(560,172071),(300,172072),(394,172072),(395,172072),(410,172072),(300,172073),(394,172073),(300,172074),(394,172074),(300,172075),(394,172075),(411,172075),(530,172075),(560,172075),(300,172076),(394,172076),(300,172077),(394,172077),(411,172077),(530,172077),(560,172077),(300,172078),(394,172078),(395,172078),(397,172078),(410,172078),(300,172079),(394,172079),(411,172079),(300,172080),(394,172080),(300,172081),(394,172081),(300,172082),(394,172082),(411,172082),(560,172082),(300,172083),(394,172083),(411,172083),(300,172084),(394,172084),(411,172084),(560,172084),(300,172085),(394,172085),(395,172085),(397,172085),(410,172085),(300,175247),(410,175247),(300,178521),(410,178521),(411,178521),(300,181795),(550,181795),(300,181796),(550,181796),(300,181797),(408,181797),(300,181798),(395,181798),(405,181798),(300,181799),(402,181799),(410,181799),(300,181800),(300,181802),(412,181802),(300,181803),(394,181803),(395,181803),(397,181803),(410,181803),(511,181803),(512,181803),(553,181803),(300,181804),(300,181805),(412,181805),(300,181810),(402,181810),(300,181811),(300,181812),(394,181812),(395,181812),(560,181812),(300,181813),(394,181813),(395,181813),(397,181813),(410,181813),(511,181813),(512,181813),(553,181813),(560,181813),(300,181815),(300,181816),(394,181816),(395,181816),(410,181816),(560,181816),(300,181817),(394,181817),(395,181817),(560,181817),(300,181818),(300,181819),(300,181820),(395,181820),(404,181820),(300,185069),(300,185070),(300,188348),(300,188357),(395,188357),(300,188358),(300,191617),(412,191617),(300,191638),(395,191638),(560,191638),(300,191649),(300,191650),(510,191650),(300,206350),(410,206350),(300,209634),(300,209638),(553,209638),(300,218380),(408,218380),(300,219734),(300,219735),(300,222453),(300,223796),(410,223796),(530,223796),(300,225152),(300,226504),(300,226505),(300,226506),(300,226507),(510,226507),(560,226507),(300,226508),(300,226509),(394,226509),(395,226509),(396,226509),(397,226509),(410,226509),(411,226509),(511,226509),(512,226509),(553,226509),(560,226509),(300,226510),(395,226510),(300,226511),(300,226512),(300,226513),(411,226513),(300,226514),(300,226515),(300,226516),(300,226517),(395,226517),(397,226517),(553,226517),(560,226517),(561,226517),(300,226518),(300,226519),(395,226519),(410,226519),(560,226519),(300,226520),(530,226520),(561,226520),(300,226521),(394,226521),(395,226521),(396,226521),(397,226521),(410,226521),(411,226521),(511,226521),(512,226521),(553,226521),(560,226521),(300,226522),(300,226523),(300,226524),(300,226525),(410,226525),(300,226526),(300,226527),(300,226528),(300,226529),(300,226530),(300,226531),(300,226532),(395,226532),(397,226532),(553,226532),(560,226532),(561,226532),(300,226533),(300,226534),(530,226534),(561,226534),(300,226535),(300,226536),(530,226536),(561,226536),(300,226537),(300,226538),(300,226539),(300,226540),(300,226541),(300,226542),(300,226543),(300,226544),(410,226544),(300,226545),(300,226546),(300,226547),(300,226548),(300,226549),(300,226550),(300,226551),(410,226551),(300,226552),(410,226552),(300,226553),(510,226553),(560,226553),(300,226554),(300,226555),(300,226556),(300,226557),(300,226558),(300,226559),(300,226560),(300,226561),(395,226561),(410,226561),(300,226562),(411,226562),(560,226562),(300,226563),(300,226564),(300,226565),(300,226566),(300,226567),(411,226567),(560,226567),(300,226568),(300,226569),(300,226570),(300,226571),(411,226571),(560,226571),(300,226572),(395,226572),(410,226572),(300,226573),(394,226573),(395,226573),(396,226573),(397,226573),(410,226573),(411,226573),(511,226573),(512,226573),(553,226573),(560,226573),(300,226574),(300,226575),(300,226576),(300,226577),(300,226578),(561,226578),(300,226579),(395,226579),(410,226579),(300,226580),(395,226580),(410,226580),(300,226581),(300,226582),(300,226583),(300,226584),(300,226585),(510,226585),(560,226585),(300,226586),(300,226587),(300,226588),(411,226588),(560,226588),(300,226589),(300,226590),(300,226591),(300,226592),(300,226593),(300,226594),(300,226595),(300,226596),(300,226597),(300,226598),(300,226599),(300,226600),(300,226601),(300,226602),(300,226603),(410,226603),(300,226604),(410,226604),(300,226605),(411,226605),(560,226605),(300,226606),(560,226606),(300,226607),(300,226608),(411,226608),(560,226608),(300,226609),(300,226610),(300,226611),(300,226612),(300,226613),(410,226613),(300,226614),(300,226615),(300,226616),(395,226616),(300,226617),(300,226618),(300,226619),(300,226620),(410,226620),(300,226621),(410,226621),(300,226622),(300,226623),(300,226624),(395,226624),(410,226624),(300,226625),(300,226626),(300,226627),(300,226628),(300,226629),(300,226630),(411,226630),(560,226630),(300,226631),(300,226632),(300,226633),(300,226634),(560,226634),(300,226635),(410,226635),(300,227858),(300,227859),(300,227860),(300,227861),(300,227862),(300,227863),(300,227864),(300,227865),(300,227866),(300,227867),(300,227868),(300,227869),(300,227870),(300,227871),(300,227872),(300,227873),(300,227874),(300,227875),(300,227876),(300,227877),(300,227878),(300,227879),(300,227880),(300,227881),(300,227882),(300,227883),(300,227884),(300,227885),(300,227886),(300,227887),(300,227888),(300,227889),(300,227890),(300,227891),(300,227892),(300,227893),(300,227894),(300,227895),(300,227896),(300,227897),(300,227898),(300,227899),(300,227900),(300,227901),(300,227902),(300,227903),(530,227903),(561,227903),(300,227904),(300,227905),(300,227906),(300,227907),(300,227908),(300,227909),(300,227910),(300,227911),(300,227912),(300,227913),(300,227914),(300,227915),(300,227916),(300,227917),(300,227918),(300,227919),(300,227927),(300,227933),(300,230566),(300,230567),(395,230567),(410,230567),(300,230568),(300,230569),(395,230569),(300,230570),(300,230571),(410,230571),(300,230572),(300,230573),(395,230573),(410,230573),(300,230574),(300,230575),(300,230576),(394,230576),(395,230576),(399,230576),(410,230576),(510,230576),(560,230576),(300,230577),(394,230577),(395,230577),(399,230577),(410,230577),(510,230577),(560,230577),(300,230578),(300,230579),(300,230580),(300,230581),(394,230581),(395,230581),(399,230581),(410,230581),(510,230581),(560,230581),(300,230582),(300,230583),(300,230584),(394,230584),(395,230584),(399,230584),(410,230584),(510,230584),(560,230584),(300,230585),(410,230585),(300,230586),(300,230587),(300,230588),(395,230588),(410,230588),(300,230589),(300,230590),(300,230591),(410,230591),(300,230592),(300,233274),(394,233274),(407,233274),(410,233274),(300,233275),(395,233275),(410,233275),(510,233275),(300,233276),(394,233276),(407,233276),(410,233276),(300,233277),(394,233277),(395,233277),(560,233277),(300,233278),(395,233278),(300,233279),(511,233279),(300,233280),(394,233280),(395,233280),(410,233280),(560,233280),(300,233281),(410,233281),(300,233282),(394,233282),(395,233282),(396,233282),(397,233282),(410,233282),(511,233282),(512,233282),(560,233282),(300,233283),(394,233283),(395,233283),(410,233283),(511,233283),(512,233283),(560,233283),(300,233284),(300,233285),(300,233286),(300,233287),(394,233287),(399,233287),(407,233287),(410,233287),(411,233287),(300,233288),(394,233288),(407,233288),(410,233288),(300,233289),(394,233289),(407,233289),(410,233289),(411,233289),(300,233290),(394,233290),(395,233290),(410,233290),(511,233290),(512,233290),(560,233290),(300,233291),(394,233291),(395,233291),(560,233291),(300,233292),(300,233293),(300,233294),(410,233294),(300,233295),(394,233295),(395,233295),(410,233295),(511,233295),(512,233295),(560,233295),(300,233296),(300,233297),(300,233298),(394,233298),(395,233298),(396,233298),(397,233298),(410,233298),(511,233298),(512,233298),(560,233298),(300,233299),(394,233299),(395,233299),(410,233299),(511,233299),(512,233299),(560,233299),(300,233300),(300,233301),(395,233301),(510,233301),(300,233302),(300,233303),(394,233303),(395,233303),(396,233303),(397,233303),(410,233303),(511,233303),(512,233303),(560,233303),(300,233304),(394,233304),(395,233304),(410,233304),(560,233304),(300,233305),(395,233305),(300,233306),(394,233306),(395,233306),(410,233306),(511,233306),(512,233306),(560,233306),(300,233307),(410,233307),(300,233308),(394,233308),(407,233308),(410,233308),(300,233309),(300,233310),(512,233310),(300,233311),(394,233311),(395,233311),(396,233311),(397,233311),(410,233311),(511,233311),(512,233311),(560,233311),(300,233312),(394,233312),(395,233312),(560,233312),(300,233313),(300,233314),(300,233315),(394,233315),(407,233315),(410,233315),(300,233316),(394,233316),(395,233316),(396,233316),(397,233316),(410,233316),(511,233316),(512,233316),(560,233316),(300,233317),(394,233317),(395,233317),(396,233317),(397,233317),(410,233317),(411,233317),(511,233317),(512,233317),(560,233317),(300,233318),(300,233319),(394,233319),(395,233319),(560,233319),(300,233320),(395,233320),(300,233321),(394,233321),(395,233321),(397,233321),(410,233321),(300,233322),(300,233323),(394,233323),(395,233323),(560,233323),(300,233324),(394,233324),(407,233324),(410,233324),(300,233325),(394,233325),(395,233325),(410,233325),(511,233325),(512,233325),(560,233325),(300,233326),(395,233326),(300,233327),(394,233327),(407,233327),(410,233327),(300,233328),(394,233328),(395,233328),(410,233328),(560,233328),(300,233329),(394,233329),(407,233329),(410,233329),(300,233330),(395,233330),(410,233330),(510,233330),(300,233331),(394,233331),(395,233331),(410,233331),(511,233331),(512,233331),(560,233331),(300,233332),(300,233333),(300,233334),(394,233334),(395,233334),(560,233334),(300,233335),(394,233335),(407,233335),(410,233335),(300,233336),(300,233337),(300,233338),(410,233338),(300,233339),(511,233339),(300,233340),(394,233340),(407,233340),(410,233340),(300,233341),(394,233341),(407,233341),(410,233341),(300,233342),(394,233342),(407,233342),(410,233342),(300,233343),(300,233344),(300,233345),(300,233346),(410,233346),(300,233347),(394,233347),(395,233347),(560,233347),(300,233348),(394,233348),(395,233348),(397,233348),(410,233348),(300,233349),(394,233349),(395,233349),(396,233349),(397,233349),(410,233349),(411,233349),(511,233349),(512,233349),(560,233349),(300,233350),(395,233350),(300,233351),(300,233352),(394,233352),(395,233352),(410,233352),(511,233352),(512,233352),(560,233352),(300,233353),(300,233354),(300,233355),(410,233355),(300,233356),(394,233356),(395,233356),(396,233356),(397,233356),(410,233356),(511,233356),(512,233356),(560,233356),(300,233357),(300,233358),(394,233358),(395,233358),(560,233358),(300,233359),(394,233359),(395,233359),(560,233359),(300,233360),(394,233360),(407,233360),(410,233360),(300,233361),(300,233362),(410,233362),(513,233363),(560,233363),(300,233364),(300,233365),(300,233366),(394,233366),(407,233366),(410,233366),(411,233366),(300,233367),(394,233367),(407,233367),(410,233367),(300,233368),(394,233368),(407,233368),(410,233368),(300,233369),(394,233369),(395,233369),(396,233369),(397,233369),(410,233369),(511,233369),(512,233369),(560,233369),(300,233370),(300,233371),(394,233371),(407,233371),(410,233371),(300,233372),(394,233372),(407,233372),(410,233372),(300,233373),(394,233373),(399,233373),(407,233373),(410,233373),(411,233373),(300,233374),(395,233374),(560,233374),(300,233375),(394,233375),(395,233375),(396,233375),(397,233375),(410,233375),(511,233375),(512,233375),(560,233375),(300,233376),(394,233376),(400,233376),(410,233376),(300,233377),(395,233377),(300,233378),(394,233378),(399,233378),(407,233378),(410,233378),(300,233379),(511,233379),(512,233379),(300,233380),(394,233380),(407,233380),(410,233380),(300,233381),(300,233382),(300,233383),(300,233384),(394,233384),(407,233384),(410,233384),(300,233385),(394,233385),(399,233385),(407,233385),(410,233385),(300,233386),(300,233387),(300,233388),(395,233388),(410,233388),(510,233388),(300,235982),(300,235983),(300,235984),(300,235985),(300,236018),(300,238690),(300,238691),(410,238691),(560,238691),(300,238692),(394,238692),(395,238692),(410,238692),(300,238693),(300,238694),(300,238695),(300,238696),(300,238697),(394,238697),(395,238697),(396,238697),(397,238697),(410,238697),(511,238697),(512,238697),(560,238697),(300,238698),(394,238698),(395,238698),(396,238698),(397,238698),(410,238698),(511,238698),(512,238698),(560,238698),(300,238699),(300,238700),(394,238700),(395,238700),(396,238700),(397,238700),(410,238700),(510,238700),(511,238700),(512,238700),(560,238700),(300,238701),(394,238701),(395,238701),(396,238701),(397,238701),(410,238701),(511,238701),(512,238701),(560,238701),(300,238702),(394,238702),(395,238702),(396,238702),(397,238702),(410,238702),(511,238702),(512,238702),(560,238702),(300,238703),(394,238703),(395,238703),(396,238703),(397,238703),(410,238703),(511,238703),(512,238703),(560,238703),(300,238704),(300,238705),(410,238705),(560,238705),(300,238706),(394,238706),(395,238706),(396,238706),(397,238706),(410,238706),(511,238706),(512,238706),(560,238706),(300,238707),(300,238708),(300,241398),(300,241399),(532,241399),(300,241400),(394,241400),(395,241400),(300,241401),(300,241402),(532,241402),(533,241402),(300,241403),(300,241404),(410,241404),(300,241405),(410,241405),(300,241406),(300,241407),(510,241407),(300,241408),(532,241408),(533,241408),(534,241408),(300,241409),(300,241410),(300,241411),(300,241412),(510,241412),(300,241413),(532,241413),(533,241413),(534,241413),(300,241414),(410,241414),(411,241414),(530,241414),(300,241415),(395,241415),(397,241415),(553,241415),(300,241416),(300,241417),(410,241417),(411,241417),(530,241417),(531,241418),(300,241419),(410,241419),(411,241419),(530,241419),(560,241419),(300,241420),(532,241420),(300,241421),(300,241422),(532,241422),(533,241422),(300,241423),(300,241424),(300,241425),(300,241426),(300,241427),(300,241428),(300,241429),(410,241429),(411,241429),(530,241429),(300,241430),(532,241430),(533,241430),(300,241431),(532,241431),(533,241431),(300,241432),(300,241433),(532,241433),(533,241433),(300,241434),(300,241435),(408,241435),(300,241436),(300,241437),(410,241437),(411,241437),(530,241437),(300,241438),(300,241439),(300,241440),(510,241440),(300,241441),(411,241441),(560,241441),(300,241442),(510,241442),(300,241443),(532,241443),(533,241443),(300,241444),(300,241445),(394,241445),(300,241446),(300,241447),(532,241447),(533,241447),(300,241448),(532,241448),(533,241448),(534,241448),(300,241449),(411,241449),(560,241449),(300,241450),(300,241451),(300,241452),(300,241453),(395,241453),(300,241454),(300,241455),(532,241455),(533,241455),(300,241456),(300,241457),(300,241458),(300,241459),(553,241459),(300,241460),(300,241461),(410,241461),(300,241462),(532,241462),(533,241462),(300,241463),(300,241464),(300,241465),(300,241466),(300,241467),(300,241468),(532,241468),(533,241468),(534,241468),(300,241469),(300,241470),(410,241470),(532,241470),(533,241470),(300,241471),(532,241471),(533,241471),(300,241472),(300,241473),(394,241473),(300,241474),(300,241475),(300,241476),(532,241476),(533,241476),(300,241477),(532,241477),(300,241478),(300,241479),(300,241480),(300,241481),(300,241482),(300,241483),(300,241484),(395,241484),(397,241484),(553,241484),(560,241484),(300,241485),(532,241485),(300,241486),(532,241486),(533,241486),(300,241571),(300,244106),(300,246814),(395,246814),(397,246814),(410,246814),(553,246814),(560,246814),(300,246815),(552,246815),(300,246816),(408,246816),(412,246816),(513,246816),(531,246816),(550,246816),(553,246816),(300,246817),(300,246818),(395,246818),(410,246818),(411,246818),(300,246819),(551,246819),(300,246820),(300,246821),(410,246821),(300,246822),(551,246822),(300,246823),(410,246823),(300,246824),(552,246824),(300,246825),(410,246825),(300,246826),(394,246826),(395,246826),(396,246826),(397,246826),(410,246826),(411,246826),(511,246826),(512,246826),(553,246826),(560,246826),(300,246827),(300,246828),(408,246828),(412,246828),(513,246828),(531,246828),(553,246828),(300,246829),(410,246829),(300,246830),(410,246830),(300,246831),(551,246831),(300,246832),(551,246832),(300,246833),(395,246833),(410,246833),(560,246833),(300,246834),(300,246835),(408,246835),(412,246835),(513,246835),(531,246835),(553,246835),(300,246836),(410,246836),(300,246837),(410,246837),(300,246838),(410,246838),(300,246839),(410,246839),(300,246840),(412,246840),(553,246840),(300,246841),(412,246841),(553,246841),(300,246842),(410,246842),(300,246843),(410,246843),(560,246843),(300,246844),(395,246844),(397,246844),(410,246844),(411,246844),(553,246844),(560,246844),(300,246845),(552,246845),(300,246846),(395,246846),(397,246846),(410,246846),(411,246846),(553,246846),(560,246846),(300,246847),(300,246848),(300,246849),(551,246849),(300,246850),(410,246850),(300,246851),(410,246851),(300,246852),(410,246852),(560,246852),(300,246853),(395,246853),(397,246853),(411,246853),(553,246853),(560,246853),(300,246854),(410,246854),(560,246854),(300,246855),(410,246855),(560,246855),(300,246856),(412,246856),(553,246856),(300,246857),(395,246857),(397,246857),(410,246857),(553,246857),(300,246858),(552,246858),(300,246859),(300,246860),(300,246861),(411,246861),(300,246862),(395,246862),(410,246862),(300,246863),(410,246863),(560,246863),(300,246864),(410,246864),(300,246865),(300,246866),(410,246866),(300,246867),(408,246867),(412,246867),(513,246867),(531,246867),(553,246867),(300,246868),(410,246868),(300,246869),(410,246869),(300,246870),(410,246870),(300,246871),(410,246871),(300,246872),(550,246872),(553,246872),(300,246873),(300,246874),(410,246874),(300,246875),(410,246875),(300,246876),(410,246876),(300,246877),(410,246877),(300,246878),(300,246879),(410,246879),(300,246880),(410,246880),(300,246881),(412,246881),(553,246881),(300,246882),(410,246882),(300,246883),(300,246884),(412,246884),(553,246884),(300,246885),(410,246885),(560,246885),(300,246886),(395,246886),(410,246886),(300,246887),(410,246887),(300,246888),(410,246888),(300,246889),(410,246889),(300,246890),(410,246890),(300,246891),(552,246891),(300,246892),(551,246892),(300,246893),(300,246894),(410,246894),(560,246894),(300,246895),(410,246895),(550,246895),(300,246896),(394,246896),(395,246896),(410,246896),(300,246897),(394,246897),(300,246898),(394,246898),(395,246898),(397,246898),(410,246898),(300,248168),(300,249522),(300,249523),(300,249524),(300,260394);
/*!40000 ALTER TABLE `turbine_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turbine_scheduled_job`
--

DROP TABLE IF EXISTS `turbine_scheduled_job`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turbine_scheduled_job` (
  `JOB_ID` int(11) NOT NULL,
  `SECOND` int(11) NOT NULL DEFAULT '-1',
  `MINUTE` int(11) NOT NULL DEFAULT '-1',
  `HOUR` int(11) NOT NULL DEFAULT '-1',
  `WEEK_DAY` int(11) NOT NULL DEFAULT '-1',
  `DAY_OF_MONTH` int(11) NOT NULL DEFAULT '-1',
  `TASK` varchar(99) NOT NULL,
  `EMAIL` varchar(99) DEFAULT NULL,
  `PROPERTY` mediumblob,
  PRIMARY KEY (`JOB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turbine_scheduled_job`
--

LOCK TABLES `turbine_scheduled_job` WRITE;
/*!40000 ALTER TABLE `turbine_scheduled_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `turbine_scheduled_job` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turbine_user`
--

DROP TABLE IF EXISTS `turbine_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turbine_user` (
  `USER_ID` int(11) NOT NULL,
  `LOGIN_NAME` varchar(99) NOT NULL,
  `PASSWORD_VALUE` varchar(32) NOT NULL,
  `FIRST_NAME` varchar(99) NOT NULL,
  `LAST_NAME` varchar(99) NOT NULL,
  `EMAIL` varchar(99) DEFAULT NULL,
  `CONFIRM_VALUE` varchar(99) DEFAULT NULL,
  `MODIFIED` datetime DEFAULT NULL,
  `CREATED` datetime DEFAULT NULL,
  `LAST_LOGIN` datetime DEFAULT NULL,
  `OBJECTDATA` mediumblob,
  PRIMARY KEY (`USER_ID`),
  UNIQUE KEY `LOGIN_NAME` (`LOGIN_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turbine_user`
--

LOCK TABLES `turbine_user` WRITE;
/*!40000 ALTER TABLE `turbine_user` DISABLE KEYS */;
INSERT INTO `turbine_user` VALUES (422,'anonymous','CpL6syMBNMym6t2YmDJbmyrm','Sistema','Sistema',NULL,NULL,NULL,NULL,'2016-05-10 10:50:57','�\�\0sr\0java.util.Hashtable�%!J\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0_access_countersr\0java.lang.Integer⠤���8\0I\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0x'),(460,'turbine','z/T5tyh7llwvmNkkwgzjYwSl','Sistema','Sistema',NULL,NULL,NULL,NULL,'2016-03-16 14:21:55','�\�\0sr\0java.util.Hashtable�%!J\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0_access_countersr\0java.lang.Integer⠤���8\0I\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0x'),(510,'sysScheduler','dtWNzkl8fVVjgqCmphsF0jF0','Sistema','Sistema',NULL,NULL,NULL,NULL,'2016-05-10 12:00:53','�\�\0sr\0java.util.Hashtable�%!J\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0\0w\0\0\0\0\0\0\0x'),(600,'acquisti','EgJSZlA477JeCHNdyVqbhBcb','--','--',NULL,NULL,NULL,NULL,'2015-12-04 09:43:32','�\�\0sr\0java.util.Hashtable�%!J\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0_access_countersr\0java.lang.Integer⠤���8\0I\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0x'),(680,'sysIOuser_fornitore','W04dt9RDMqSGC3E3+r/rjbFq','--','--',NULL,NULL,NULL,NULL,NULL,'‚Äö√†√∂‚àö√Ü‚Äö√†√∂‚Äö√†√®‚âà√≠¬¨¬©‚Äö√†√∂‚àö√Ü‚Äö√†√∂‚Äö√†√®‚âà√≠¬¨¬©\0sr\0java.util.Hashtable‚Äö√†√∂‚àö√Ü‚Äö√†√∂‚Äö√†√®‚âà√≠¬¨¬©%!J‚Äö√†√∂‚àö√Ü‚Äö√†√∂‚Äö√†√®‚âà√≠¬¨¬©\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0\0w\0\0\0\0\0\0\0x'),(700,'sysIungoService','ML1He32VGE9jlKy+9qacqQub','sysIungoService','sysIungoService',NULL,NULL,NULL,NULL,NULL,'¬¨¬®¬¨¬Æ¬¨¬®¬¨√Ü‚Äö√Ñ√∂‚àö‚Ä†‚àö‚àÇ‚Äö√Ñ√∂‚àö¬¢‚Äö√Ñ‚Ä†\0sr\0java.util.Hashtable¬¨¬®¬¨¬Æ¬¨¬®‚Äö√ë¬¢%!J‚Äö√Ñ√∂‚àö‚Ä†‚àö‚àÇ¬¨¬®‚àö√º¬¨¬®¬¨¬Æ‚Äö√Ñ√∂‚àö‚Ä†‚àö¬Æ\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0\0w\0\0\0\0\0\0\0x'),(710,'fornitore@localmail','1JhQU8P3xKjHC8201yclV8AM','--','--',NULL,NULL,NULL,NULL,'2016-04-14 15:48:30','�\�\0sr\0java.util.Hashtable�%!J\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0\0w\0\0\0\0\0\0\0x'),(711,'fornitore@localmail.it','UvZmk+wzfV9IPu9/Y0ad7fr2','--','--',NULL,NULL,NULL,NULL,'2016-05-10 12:06:00','�\�\0sr\0java.util.Hashtable�%!J\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0_access_countersr\0java.lang.Integer⠤���8\0I\0valuexr\0java.lang.Number����\��\0\0xp\0\0\0x'),(721,'turbine_root','4vUZ7HvhmiKt/W54Xyuz7oza','--','--',NULL,NULL,NULL,NULL,NULL,'�\�\0sr\0java.util.Hashtable�%!J\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0\0w\0\0\0\0\0\0\0x');
/*!40000 ALTER TABLE `turbine_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `turbine_user_group_role`
--

DROP TABLE IF EXISTS `turbine_user_group_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `turbine_user_group_role` (
  `USER_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL,
  PRIMARY KEY (`USER_ID`,`GROUP_ID`,`ROLE_ID`),
  KEY `GROUP_ID` (`GROUP_ID`,`ROLE_ID`),
  KEY `ROLE_ID` (`ROLE_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `GROUP_ID_2` (`GROUP_ID`),
  KEY `ROLE_ID_2` (`ROLE_ID`),
  CONSTRAINT `turbine_user_group_role_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `turbine_user` (`USER_ID`),
  CONSTRAINT `turbine_user_group_role_ibfk_2` FOREIGN KEY (`GROUP_ID`) REFERENCES `turbine_group` (`GROUP_ID`),
  CONSTRAINT `turbine_user_group_role_ibfk_3` FOREIGN KEY (`ROLE_ID`) REFERENCES `turbine_role` (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `turbine_user_group_role`
--

LOCK TABLES `turbine_user_group_role` WRITE;
/*!40000 ALTER TABLE `turbine_user_group_role` DISABLE KEYS */;
INSERT INTO `turbine_user_group_role` VALUES (460,400,300),(460,480,300),(721,400,300),(510,400,394),(510,480,394),(600,400,395),(600,480,395),(600,480,399),(600,400,403),(600,480,404),(600,480,405),(600,480,406),(460,520,408),(510,480,408),(600,480,408),(710,480,408),(711,480,408),(510,400,410),(510,480,410),(510,400,412),(600,400,412),(710,400,412),(711,400,412),(680,400,530),(680,480,530),(700,400,550),(700,480,550),(700,520,550),(510,400,551),(510,400,553),(710,400,560),(710,480,560),(711,400,560),(711,480,560);
/*!40000 ALTER TABLE `turbine_user_group_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `url_support`
--

DROP TABLE IF EXISTS `url_support`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `url_support` (
  `URL_SUPPORT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `INDIRIZZO` varchar(255) NOT NULL,
  `ATTIVO` int(11) DEFAULT '1',
  `TIPO` varchar(15) DEFAULT '',
  PRIMARY KEY (`URL_SUPPORT_ID`),
  UNIQUE KEY `INDIRIZZO` (`INDIRIZZO`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `url_support`
--

LOCK TABLES `url_support` WRITE;
/*!40000 ALTER TABLE `url_support` DISABLE KEYS */;
INSERT INTO `url_support` VALUES (1,'http://www.iungoapps.com/iungo_support',1,'supportPhp');
/*!40000 ALTER TABLE `url_support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valutazione_fornitore`
--

DROP TABLE IF EXISTS `valutazione_fornitore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valutazione_fornitore` (
  `VALUTAZIONE_FORNITORE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `VALUTAZIONE_DATE` datetime NOT NULL,
  `VALUTAZIONE_SCADENZA_DATE` datetime DEFAULT NULL,
  `CON_DATI` int(11) NOT NULL,
  `FORNITORE_ID` int(11) NOT NULL,
  `VALUTATORE` varchar(32) NOT NULL,
  `NOTA` varchar(99) DEFAULT NULL,
  `AUX_HEAD1` varchar(99) DEFAULT NULL,
  `AUX_HEAD2` varchar(99) DEFAULT NULL,
  `VALUTAZIONE` decimal(12,5) NOT NULL,
  `FORNITORE_CLASSEMERITO_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`VALUTAZIONE_FORNITORE_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `FORNITORE_CLASSEMERITO_ID` (`FORNITORE_CLASSEMERITO_ID`),
  CONSTRAINT `valutazione_fornitore_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `valutazione_fornitore_ibfk_2` FOREIGN KEY (`FORNITORE_CLASSEMERITO_ID`) REFERENCES `fornitore_classemerito` (`FORNITORE_CLASSEMERITO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valutazione_fornitore`
--

LOCK TABLES `valutazione_fornitore` WRITE;
/*!40000 ALTER TABLE `valutazione_fornitore` DISABLE KEYS */;
/*!40000 ALTER TABLE `valutazione_fornitore` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valutazione_fornitore_classifica`
--

DROP TABLE IF EXISTS `valutazione_fornitore_classifica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valutazione_fornitore_classifica` (
  `VALUTAZIONE_FORNITORE_CLASSIFICA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_ID` int(11) NOT NULL,
  `VALUTAZIONE` decimal(12,5) NOT NULL,
  `VALUTAZIONE_DATE` datetime NOT NULL,
  `FORNITORE_CLASSEMERITO_ID` int(11) DEFAULT NULL,
  `CON_DATI` int(11) NOT NULL,
  PRIMARY KEY (`VALUTAZIONE_FORNITORE_CLASSIFICA_ID`),
  KEY `FORNITORE_ID` (`FORNITORE_ID`),
  KEY `FORNITORE_CLASSEMERITO_ID` (`FORNITORE_CLASSEMERITO_ID`),
  CONSTRAINT `valutazione_fornitore_classifica_ibfk_1` FOREIGN KEY (`FORNITORE_ID`) REFERENCES `fornitore` (`FORNITORE_ID`),
  CONSTRAINT `valutazione_fornitore_classifica_ibfk_2` FOREIGN KEY (`FORNITORE_CLASSEMERITO_ID`) REFERENCES `fornitore_classemerito` (`FORNITORE_CLASSEMERITO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valutazione_fornitore_classifica`
--

LOCK TABLES `valutazione_fornitore_classifica` WRITE;
/*!40000 ALTER TABLE `valutazione_fornitore_classifica` DISABLE KEYS */;
/*!40000 ALTER TABLE `valutazione_fornitore_classifica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valutazione_fornitore_riga`
--

DROP TABLE IF EXISTS `valutazione_fornitore_riga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valutazione_fornitore_riga` (
  `VALUTAZIONE_FORNITORE_RIGA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORNITORE_PESO_ID` int(11) NOT NULL,
  `VALUTAZIONE_FORNITORE_ID` int(11) NOT NULL,
  `PESO` int(11) NOT NULL,
  `PESO_EFFETTIVO` decimal(12,5) DEFAULT NULL,
  PRIMARY KEY (`VALUTAZIONE_FORNITORE_RIGA_ID`),
  KEY `FORNITORE_PESO_ID` (`FORNITORE_PESO_ID`),
  KEY `VALUTAZIONE_FORNITORE_ID` (`VALUTAZIONE_FORNITORE_ID`),
  CONSTRAINT `valutazione_fornitore_riga_ibfk_1` FOREIGN KEY (`FORNITORE_PESO_ID`) REFERENCES `fornitore_peso` (`FORNITORE_PESO_ID`),
  CONSTRAINT `valutazione_fornitore_riga_ibfk_2` FOREIGN KEY (`VALUTAZIONE_FORNITORE_ID`) REFERENCES `valutazione_fornitore` (`VALUTAZIONE_FORNITORE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valutazione_fornitore_riga`
--

LOCK TABLES `valutazione_fornitore_riga` WRITE;
/*!40000 ALTER TABLE `valutazione_fornitore_riga` DISABLE KEYS */;
/*!40000 ALTER TABLE `valutazione_fornitore_riga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'iungolive'
--

--
-- Dumping routines for database 'iungolive'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-12  9:40:08
